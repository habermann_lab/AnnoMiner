
$(document).ready(function(){

	//include script and the navbar:
	
	$("#includenavbar").load("navbar.html");
	$("#includefooter").load("footer.html");

	$(".annobuttons").hide();
	$("#progress").hide();
	$("#UpTab").hide();
	$(".step2").hide();
	$("#btn_dwl_rslt").hide();
	$(".Aboutannotation").hide();
	$(".Aboutenrichment").hide();
	$(".About").hide();
	$("#new").hide();
	$(".promoter").css('display','none');
	$(".strand").css('display','none');
	$(".integration").css('display','none');
	$(".highlighter").css('display','none');


	//////////////////////
	// Page Navigations //
	//////////////////////

	//solution to refresh the forms once that the user refresh the page from browser button
	$(document).ready(function(e) {
	    var $input = $('#refresh');
	    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
	});

	//to have the collapse feature of the three containers
	$("#Up, #Up2, #Up3, #Up4").collapse({"toggle": true, 'parent': '#navaccordion'});
		
	$(window).resize(function() {
	  var windowsize = $(window).width();
		if (windowsize > 1150) {
				$('body').addClass("sol");
		}
		if (windowsize < 1150) {
				$('body').removeClass("sol");
		}
	});
				
	//Action about page
	$("#AboutAnno").click(function(){

		$(".Aboutinit").hide();
		$(".Aboutenrichment").hide();
		$(".About").show();
		$(".Aboutannotation").show();
		$("html, body").delay(30).animate({scrollTop: $('#Aboutannotation').offset().top - 150}, 2000);
	});
	$("#AboutEnrich").click(function(){

		$(".Aboutinit").hide();
		$(".Aboutannotation").hide();
		$(".About").show();
		$(".Aboutenrichment").show();
		$("html, body").delay(30).animate({scrollTop: $('#Aboutenrichment').offset().top - 150}, 2000);
	});
	
	//////////////////////
	// Global variables //
	//////////////////////

	var last_datatable=null;
	var last_datatable_header=null;

	//default : 

	var assembly = "hg19";
	var species = "hg19";
	var ovtype = '0';
	var ovval_abs = '1';
	var ovval_rel = '1';



	var genomes = {
		"hg19":["refseq","ensembl","genecode","ucsc"],
		"hg38":["refseq","genecode","ucsc"],
		"mm9":["refseq","ensembl","ucsc"],
		"mm10":["refseq","ucsc","genecode"],
		"dm3":["refseq","ensembl","flybase"],
		"dm6":["refseq","ensembl"],
		"ce11":["refseq","ensembl","wormbase"],
		"sacCer3":["ensembl","refseq"]
	};

	var assemblies = {
		"genes":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm9">Mus musculus (mm9)</option><option value="mm10">Mus musculus (mm10)</option><option value="dm3">Drosophila melanogaster (dm3)</option><option value="dm6">Drosophila melanogaster (dm6)</option><option value="ce11">Cenorhabditis elegans (ce11/WBcel245)</option><option value="sacCer3">S. cerevisiae (sacCer3)</option>',
		"corr":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm9">Mus musculus (mm9)</option><option value="mm10">Mus musculus (mm10)</option><option value="dm3">Drosophila melanogaster (dm3)</option><option value="dm6">Drosophila melanogaster (dm6)</option><option value="ce11">Cenorhabditis elegans (ce11/WBcel245)</option><option value="sacCer3">S. cerevisiae (sacCer3)</option>',
		"TFs":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm10">Mus musculus (mm10)</option><option value="dm3">Drosophila melanogaster (dm3)</option><option value="dm6">Drosophila melanogaster (dm6)</option><option value="ce11">Cenorhabditis elegans (ce11/WBcel245)</option>',
		"HMs":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm10">Mus musculus (mm10)</option>',
		"NG":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm9">Mus musculus (mm9)</option><option value="mm10">Mus musculus (mm10)</option><option value="dm3">Drosophila melanogaster (dm3)</option><option value="dm6">Drosophila melanogaster (dm6)</option><option value="ce11">Cenorhabditis elegans (ce11/WBcel245)</option><option value="sacCer3">S. cerevisiae (sacCer3)</option>',
		"LRI":'<option value="hg19">Homo sapiens (hg19)</option><option value="hg38">Homo sapiens (hg38)</option><option value="mm9">Mus musculus (mm9)</option><option value="mm10">Mus musculus (mm10)</option><option value="dm3">Drosophila melanogaster (dm3)</option><option value="dm6">Drosophila melanogaster (dm6)</option><option value="ce11">Cenorhabditis elegans (ce11/WBcel245)</option><option value="sacCer3">S. cerevisiae (sacCer3)</option>'
	};

	//Plots variables colours:

	var dblue = "#45A29E";
	var lblue = "#66FCF1";
	var red = "#F13C20";
	var peak = '#1F2833';
	var lgreen = "#029488";
	var dgreen = "#02625a";

	//////////////////////////
	// peak2gene annotation //
	//////////////////////////


	var gplotdata = [];
	var cr_gn1 = false;
	var gselect = [0,0,0,0,0,0,0,0,0,0];
	var gcols_df = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var gcols = [];
	
	//////////////////////////
	// peakCorrelation ///////
	//////////////////////////

	var gplotdata1 = [];
	var gplotdata2 = [];
	var gplotdata21 = [];
	var gplotdata22 = [];
	var gplotdata23 = [];
	var cr_gn2 = false;
	var cr_gn21 = false;
	var cr_gn22 = false;
	var cr_gn23 = false;
	var gselect1 = [0,0,0,0,0,0,0,0,0,0];
	var gselect2 = [0,0,0,0,0,0,0,0,0,0];
	var gselect21 = [0,0,0,0,0,0,0,0,0,0];
	var gselect22 = [0,0,0,0,0,0,0,0,0,0];
	var gselect23 = [0,0,0,0,0,0,0,0,0,0];
	var gcols_df1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var gcols_df2 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
	var gcols_df21 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var gcols_df22 =[lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
	var gcols_df23 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var gcols1 = [];
	var gcols2 = [];
	var gcols21 = [];
	var gcols22 = [];
	var gcols23 = [];
	////////////////////////////
	// Nearby Genes annotation /
	////////////////////////////


	var NG_plotdata = [];
	var cr_NG = false;
	var NG_select = [0,0,0,0,0,0,0,0,0,0,0];
	let up = '#55a4ed';
	let down = '#ffa6a6';
	let equal = '#99e89d';
	let def = '#f2f2f2';
	let highlight = 'red';
	let infoHighlight= [];
	let zerotwo = '#d2fcfb';
	let zerofour = '#50BCEB';
	let zerosix = '#2465d6';
	let zeroeigth = '#1e0aa1';
	let one = '#000536';

	//////////////////////////////////////
	// LongRangeInteractions annotation //
	//////////////////////////////////////


	var LRI_plotdata = [];

	var cr_LRI = false;

	var LRI_select = [0,0]; 

	var LRI_cols_df = [lblue,dblue];

	var LRI_cols = [];

	//////////////////////
	// TF/HM Enrichment //
	//////////////////////

	var enrichProbe = [];
	var enrichValues = [];
	var enrichTitles = [];

	//////////////////////////////////
	// function definition ///////////
	//////////////////////////////////


	var remove = "empty";
	// update the uploads
	function UploadsCheck(remove) {

		var cginame = "/AnnoMiner/UploadsCheck";
		$.post(
			cginame,
			{removeTrack:remove},
			function(data){
				if (data.data != null){
					removeResults();
					$("#UpTab").show();						
					$("#Up2").show();
					utracks = data.data;
					updateAnalysisSelections();
					updateUTrackView();
					$(".step1").hide();
					$(".step2").show();
				}else{
					table.clear().draw();
				}
			});
	}
	
	//function to add label to the custom upload fields
	function addFields() {
		$("#trackformat2").empty();
		var coln = $("#colSelect").val();
		for(var i=1; i<coln; ++i){

			$("#trackformat2").append('<div class="form-group row align-items-center"><label for="text-input" class="col-2 col-form-label">Label '+(i+1)+' column</label><div class="col-7"><input class="form-control" type="text" value="Label '+(i+1)+'" name="Label '+(i+1)+'"></div><div class="col-3"><select class="form-control" id="type'+(i+2)+'" name="type'+(i+2)+'"><option>text</option><option>num</option></select></div></div></div>');
			
		}
	}
	var table;
	// updateUTrackView //
	function updateUTrackView() {
		$(".DTin").empty();
		var titles = [{"sTitle":"Track id","sWidth":"100%"},{"sTitle":"Data-type","sWidth":"100%"},{"sTitle":"Date","sWidth":"100%"},{"sTitle":"Erase","sWidth":"100%"}];
		var values = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			values.push([utracks[i].id, utracks[i].type, utracks[i].date]);

		}

		if(utracks.length<11){
			table = $("#utrack_view").DataTable({
				"aaData": values,
				"columnDefs": [
				{
					"data": null,
					"defaultContent": "<button type='button' class='btn btn-danger'>X</button>",
					"targets": -1
				}
				],
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers",
				"sDom" : '<"col-sm-6"f>'
			});
		} else {
			table = $("#utrack_view").DataTable({
				"aaData": values,
				"columnDefs": [
					{
						"data": null,
						"defaultContent": "<button class='btn btn-danger' style='padding:0!important!'>X</button>",
						"targets": -1
					}
					],
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers"
			});
		}

		$('#utrack_view tbody').on( 'click', 'button', function () {
			//get data from datatable
			let tableData = table.rows( $(this).parents('tr') ).data();
			let indexRemove = 0;
			let id =tableData[0][0];
			let date =tableData[0][2];

			for( var i=0; i<utracks.length; ++i ) {
				if((utracks[i].id==id)&&(utracks[i].date==date)){
					indexRemove = i;
				}

			};

			remove = utracks[indexRemove].id.toString();
			//update
			UploadsCheck(remove);
			// updateUTrackView();
			remove = "empty";
		});


		$('div.dataTables_filter').appendTo('#filt');
		$('div.dataTables_length').appendTo('#rank');

		$("#paginate").append($(".dataTables_paginate"));
		$("#info").append($(".dataTables_info"));
		
		$(".usertrack").show();

		if($('#utrack_manager').is(':hidden')) {
            $('#utrack_manager').slideDown();
        }
	};


	// functions to format and order correctly the JQuery datatables
	
	//ordering strings in absolute way
	jQuery.extend(jQuery.fn.dataTableExt.oSort, {
	    "lower-string-pre": function(a) {
	        return a.toLowerCase();
	    },
	    "lower-string-asc": function(a, b) {
	        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
	    },
	    "lower-string-desc": function(a, b) {
	        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
	    }
	});
	//ordering floats effectively even in presence of characters 
	jQuery.fn.dataTableExt.oSort['numeric-custom-asc'] = function(a,b) {
		var x = parseFloat(a);
		var y = parseFloat(b);
		return ((isNaN(y) || x < y) ? -1 : ((isNaN(x) || x > y) ? 1 : 0));
		};
	jQuery.fn.dataTableExt.oSort['numeric-custom-desc'] = function(a,b) {
		var x = parseFloat(a);
		var y = parseFloat(b);
		return ((isNaN(x) || x < y) ? 1 : ((isNaN(y) || x > y) ? -1 : 0));
		}; 
	
	


	//format the tsv data for the download
	function JSON2TSV(objArray, head=true) {

		var sep = "\t"
		var array = objArray;
	    var str = '';
		var line = '';
		
		if(head){
		
			for (var i in last_datatable_header) {
            	line += last_datatable_header[i] + sep;
        	}

			line = line.slice(0, -1);
			str += line + '\r\n';
		
		}

	    for (var i = 0; i < array.length; i++) {
	        var line = '';
            for (var index in array[i]) {
                line += array[i][index] + sep;
            }
	        line = line.slice(0, -1);
	        str += line + '\r\n';
	    }
	    return str;
	};

	//defining the formatting of each column in the datatable 
	function defineHeaderFormatting(titles, titlestype){

		labeltypes = titlestype;
		last_datatable_header = titles;
		perc = (100/(last_datatable_header.length)).toString();
	
		var types=[];

		for(var i=0;i<(last_datatable_header.length);i++){
			if(labeltypes[i]=="num"){
				types[i]="numeric-custom";
			}else{
				types[i]="lower-string";
			}
		}			

		switch(last_datatable_header.length){
			case 3:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]}];
				break;
			case 4:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]}];
				break;
			case 5:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]}];
				break;
			case 6:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]},{"sTitle":last_datatable_header[5],"sWidth":perc+"%", "sType": types[5]}];
				break;
			case 7:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]},{"sTitle":last_datatable_header[5],"sWidth":perc+"%", "sType": types[5]},{"sTitle":last_datatable_header[6],"sWidth":perc+"%", "sType": types[6]}];
				break;
			case 8:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]},{"sTitle":last_datatable_header[5],"sWidth":perc+"%", "sType": types[5]},{"sTitle":last_datatable_header[6],"sWidth":perc+"%", "sType": types[6]},{"sTitle":last_datatable_header[7],"sWidth":perc+"%", "sType": types[7]}];
				break;
			case 9:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]},{"sTitle":last_datatable_header[5],"sWidth":perc+"%", "sType": types[5]},{"sTitle":last_datatable_header[6],"sWidth":perc+"%", "sType": types[6]},{"sTitle":last_datatable_header[7],"sWidth":perc+"%", "sType": types[7]},{"sTitle":last_datatable_header[8],"sWidth":perc+"%", "sType": types[8]}];
				break;
			case 10:
				var p2g_titles = [{"sTitle":last_datatable_header[0],"sWidth":perc+"%", "sType": types[1]},{"sTitle":last_datatable_header[1],"sWidth":perc+"%", "sType": "lower-string"},{"sTitle":last_datatable_header[2],"sWidth":perc+"%", "sType": types[2]},{"sTitle":last_datatable_header[3],"sWidth":perc+"%", "sType": types[3]},{"sTitle":last_datatable_header[4],"sWidth":perc+"%", "sType": types[4]},{"sTitle":last_datatable_header[5],"sWidth":perc+"%", "sType": types[5]},{"sTitle":last_datatable_header[6],"sWidth":perc+"%", "sType": types[6]},{"sTitle":last_datatable_header[7],"sWidth":perc+"%", "sType": types[7]},{"sTitle":last_datatable_header[8],"sWidth":perc+"%", "sType": types[8]},{"sTitle":last_datatable_header[9],"sWidth":perc+"%", "sType": types[9]}];
				break;
		}

		return p2g_titles;
	};	

	//////////////////////////////
	// test data upload funcions//
	//////////////////////////////

	// automatic upload of the test ID list
	function testIdUp() {
		var cginame = "/AnnoMiner/ManageUploads";
		var ass = $("#assembly").val();

		$.post(
			cginame,
			{assembly:ass,ul_type:"idlist",trackid:"IDLIST_EXAMPLE"}, function() {UploadsCheck(remove);});
	};

	// automatic upload of the test genomic region file
	function testBEDUp() {
		var cginame = "/AnnoMiner/ManageUploads";
		var ass = $("#assembly").val();

		$.post(
			cginame,
			{assembly:ass,ul_type:"peaks",trackid:"BED_EXAMPLE"}, function() {UploadsCheck(remove);});
	};

	// automatic upload of the test custom annotation file 
	function testCustomUp() {
		var cginame = "/AnnoMiner/ManageUploads";
		var ass = $("#assembly").val();

		$.post(
			cginame,
			{assembly:ass,ul_type:"custom annotation",trackid:"CUSTOM_EXAMPLE"}, function() {UploadsCheck(remove);});
	};

	// function to clean central container when upload data or loaded test file
	function removeResults(){
		$("#textout").empty();
		$("#textout2").empty();
		$("#textout3").empty();	
		$("#gtable_container").empty();
		$("#plots").empty();
		$("#plots").append('<div class="row" id="tab1_secondplot"></div>');
		$("#plots").append('<div class="row" id="tab1_thirdplot"></div>');
		$("#plots").append('<div class="row" id="tab1_fourthplot"></div>');
		$("#plots").append('<div class="row" id="tab1_fifthplot"></div>');
		$("#plots").append('<div class="row" id="tab1_sixthplot"></div>');
		$(".annobuttons").hide();
		$(".title").remove();
	};


	// updateAnalysisSelections //
	function updateAnalysisSelections() {

		var analysis = $('#analyis').val();
		$("#trackoptions").empty();
		$("#trackoptions2").empty();
		$("#trackoptions21").empty();
		$("#trackoptions22").empty();
		$("#trackoptions23").empty();
		$("#trackoptions3").empty();
		

		if( analysis == "genes" ) {
			addGenomeSel();
			addTrackSel();
			addTrackSel3();
		} else if ( analysis == "corr" ) {
			addGenomeSel();
			tracks = $("#number_tracks").val();
			if(tracks==2){
				addTrackSel();
				addTrackSel2();	
			}else if(tracks==3){
				addTrackSel();
				addTrackSel2();
				addTrackSel21();	
			}else if(tracks==4){
				addTrackSel();
				addTrackSel2();	
				addTrackSel21();
				addTrackSel22();
			}else{
				addTrackSel();
				addTrackSel2();
				addTrackSel21();
				addTrackSel22();
				addTrackSel23();	
			}	
			addTrackSel3();
		} else if ( analysis == "TFs" ) {
			addGenomeSel();
			addListSel();
		} else if ( analysis == "HMs" ) {
			addGenomeSel();
			addListSel();
		}else if ( analysis == "NG" ) {
			addGenomeSel();
			addTrackSel();
			addTrackSel3();
		}else if ( analysis == "LRI" ) {
			addGenomeSel();
			addTrackSel();
			addTrackSel3();
		}

	};

	// addAssemblySel //

	function addAssemblySel() {

		var analysis = $('#analyis').val();
		$("#assembly0").empty();
		var ostring = '<hr class="my-4"><label>Choose the reference genome:</label><select id="assembly" name="assembly" class="form-control">';
		ostring += assemblies[analysis];
		$("#assembly0").append( ostring+'</select><hr class="my-4">');

	};

	// addGenomeSel //

	function addGenomeSel() {

		var ostring = '<div class="form-group"><label>Select a genome-annotation resource:</label>';
		ostring += buildSelectField( {name:"ganno",values:genomes[species]} );
		ostring += '</div>';
		$("#trackoptions").append( '<div>'+ostring+'<hr class="my-4"></div>' );

	};

	// addTrackSel: 1st peak track//

	function addTrackSel() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno",values:ulists} );
		}

		ostring += '<hr class="my-4"></div>';

		$("#trackoptions").append( '<div>'+ostring+'</div>' );

	};


	
	// addTrackSel2: 2nd peak track //

	function addTrackSel2() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a second track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno2",values:ulists} );
		}

		ostring += '<hr class="my-4"></div>';

		$("#trackoptions2").append( '<div>'+ostring+'</div>' );

	};

	// addTrackSel21: 3rd peak track //

	function addTrackSel21() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a third track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno21",values:ulists} );
		}

		ostring += '<hr class="my-4"></div>';

		$("#trackoptions21").append( '<div>'+ostring+'</div>' );

	};	

	// addTrackSel22: 4th peak track //

	function addTrackSel22() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a fourth track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno22",values:ulists} );
		}

		ostring += '<hr class="my-4"></div>';

		$("#trackoptions22").append( '<div>'+ostring+'</div>' );

	};

	// addTrackSel23: 5th peak track //

	function addTrackSel23() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a fifth track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno23",values:ulists} );
		}

		ostring += '<hr class="my-4"></div>';

		$("#trackoptions23").append( '<div>'+ostring+'</div>' );

	};
	
	// addTrackSel3: custom annotation track//

	function addTrackSel3() {

		var track;
		var ulists = new Array();
		var analysis = $('#analyis').val();

		if( (analysis == "NG") || (analysis == "LRI")){
			ulists = new Array();
		}else{
			ulists.push("null");
		}

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "custom annotation" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a track with your custom annotation:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload an expression file first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno3",values:ulists} );
			ostring += '<hr class="my-4"></div>';
		}

		$("#trackoptions3").append( '<div>'+ostring+'</div>' );

	};

	// buildSelectField : for custom fields//

	function buildSelectField(data) {
		var ostring = '<select id="'+data.name+'" name="'+data.name+'" class="form-control" >';

		for( var i=0; i<data.values.length; ++i ) {
			ostring += '<option value="'+data.values[i]+'">'+data.values[i]+'</option>';
		}
		ostring += '</select>';
		return ostring;
	};
	
	// addListSel: gene id list //

	function addListSel() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "idlist" ) { ulists.push( track.id ); }
		}


		var ostring = '<div class="form-group"><label>Select ID-list</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload an ID-list first!</div>";
		} else {
			ostring += buildSelectField( {name:"ianno",values:ulists} );
		}

		ostring += '</div>';

		$("#trackoptions").append( '<div>'+ostring+'</div>' );

	};


	////////////////////
	// plot functions//
	///////////////////

	// peak annotation plot //
	function gplot(data, tid) {

		$(".title").remove();		
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").width("600px");
		$('<div class="container-fluid title" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 25px;color:'+dblue+'">Basepair-coverage in: ' + '<span style="font-size: 20px;color:'+peak+'">'+tid +'</span></p></div></div>').insertBefore("#tab1_secondplot");
		var plot1 = $.jqplot('tab1_secondplot', [data],{

			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
			},

			seriesColors: gcols,

			axesDefaults: {
				tickRenderer: $.jqplot.CanvasAxisTickRenderer,
				tickOptions: {
					angle: -30,
					fontSize: '10pt'
				}
			},

			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer
				},
				yaxis: {
					label: "covered / total length",
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
				}
			},

			grid: {
				background: '#ffffff',
				shadow: false
			}


		});

		if (!(cr_gn)){
			$('#tab1_secondplot').bind('jqplotDataClick',
				function (ev, seriesIndex, pointIndex, data) {

					var i = data[0] - 1;

					if (gselect[i] == 1) {
						gselect[i] = 0;
						gcols[i] = gcols_df[i];
					} else {
						gselect[i] = 1;
						gcols[i] = red;
					}

					gplot(gplotdata, tid);

				

				}
			);

			cr_gn = true
		}

	};//function gplot()

	// enrichment plot //
	function enrichplot(enrichValues, enrichTitles) {

		$(".title").remove();		
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").width("600px");

		var plot1 = $.jqplot('tab1_secondplot', [enrichValues],{

			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				pointLabels: { show: true },
				rendererOptions: {
					fillToZero: true, 
					barWidth:30, 
					barMargin:30, 
					varyBarColor:true}
			},

			seriesColors: enrichcols,

			axesDefaults: {
				tickRenderer: $.jqplot.CanvasAxisTickRenderer,
				tickOptions: {
					angle: -30,
					fontSize: '10pt'
				}
			},

			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					ticks:enrichTitles
				},
				yaxis: {
					label: "Combined score",
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
				}
			},

			grid: {
				background: '#ffffff',
				shadow: false
			}


		});
		$("#tab1_secondplot").bind('jqplotDataHighlight',
			function (ev, seriesIndex, pointIndex, enrichValues) {
				let i = enrichValues[0];
				$(this).prop('title','Probe: '+enrichProbe[i-1]);

		});


	};//function enrichplot()

	// peak integration plot //
	function corrplot(data , tag, gcols, gselect, cr_gn, tid, gcols_df) {

			$("#title"+tag).remove();		
			var plt_data=data;
			var ash ="#"+tag;

			$(ash).empty();
			$(ash).width("600px");
			$('<div class="container-fluid title" id="title'+tag+'" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 25px;color:'+gcols_df[3]+'">Basepair-coverage in: <span style="font-size: 20px;color:'+peak+'">'+tid +'</span></p></div></div>').insertBefore(ash);
			
			var plot1 = $.jqplot(tag, [data],{

				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
				},

				seriesColors: gcols,

				axesDefaults: {
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						angle: -30,
						fontSize: '10pt'
					}
				},

				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer
					},
					yaxis: {
						label: "covered / total length",
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer
					}
				},

				grid: {
					background: '#ffffff',
					shadow: false
				}


			});

			if (!(cr_gn)){
				$(ash).bind('jqplotDataClick',
					function (ev, seriesIndex, pointIndex, data) {

						var i = data[0] - 1;
						
						if (gselect[i] == 1) {
							gselect[i] = 0;
							gcols[i] = gcols_df[i];
						} else {
							gselect[i] = 1;
							gcols[i] = red;
						}
						corrplot(plt_data , tag, gcols, gselect, cr_gn,tid,gcols_df);
					}
				);
				cr_gn = true;
			}
	};

	// nearby genes plot //
	function NGplot(data, tid, logs, discriminate) {

		$(".title").remove();	
		$("#tab1_secondplot").empty();	
		$("#tab1_secondplot").addClass("genePlot");

		colors = [];
		
		
		if(discriminate==1){
			logs.forEach(function (log, index) {

				if(parseFloat(log[0])>0){
					colors[index]=up;
					infoHighlight[index] = ", " + parseFloat(log[0]).toFixed(0) + " up-regulated genes (versus "+ parseFloat(log[1]).toFixed(0)+" down-regulated)";
				}else if(parseFloat(log[0])<0){
					colors[index]=down;
					let num = parseFloat(log[0]).toFixed(0);
					infoHighlight[index] = ", " + Math.abs(num) + " down-regulated genes (versus "+ parseFloat(log[1]).toFixed(0)+" up-regulated)";
				}else if(log[0]=="NaN"){
					colors[index]=equal;
					infoHighlight[index] = ", equal number of up and down-regulated genes ("+parseFloat(log[1]).toFixed(0)+")";
				}else{
					colors[index]=def;
					infoHighlight[index] = ", no expessed genes";
				}
			});
			//add legend
			var legend = '</div><div class="col-2" style="padding-left:50px"><div class="row">legend:</div><div class="row" style="margin-top: 10px"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+up+'"><p style="margin-left: 20px">=up</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+down+'"><p style="margin-left: 20px">=down</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+equal+'"><p style="margin-left: 20px">=equal</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+def+'"><p style="margin-left: 20px">=default</p></div></div></div></div>';
		}else{
			logs.forEach(function (log, index) {

				if((parseFloat(log[0])>0)&&(parseFloat(log[0])<0.2)){
					colors[index]=zerotwo;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.2)&&(parseFloat(log[0])<0.4)){
					colors[index]=zerofour;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.4)&&(parseFloat(log[0])<0.6)){
					colors[index]=zerosix;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.6)&&(parseFloat(log[0])<0.8)){
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
					colors[index]=zeroeigth;
				}else if((parseFloat(log[0])>=0.8)&&(parseFloat(log[0])<=1)){
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
					colors[index]=one;
				}else{
					colors[index]=def;
					infoHighlight[index] = ", no expessed genes";
				}
			});
			//add legend
			var legend = '</div><div class="col-2" style="padding-left:50px"><div class="row">legend:</div><div class="row" style="margin-top: 10px"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerotwo+'"><p style="margin-left: 20px"> 0-0.2</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerofour+'"><p style="margin-left: 20px"> 0.2-0.4</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerosix+'"><p style="margin-left: 20px"> 0.4-0.6</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zeroeigth+'"><p style="margin-left: 20px"> 0.6-0.8</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+one+'"><p style="margin-left: 20px"> 0.8-1</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+def+'"><p style="margin-left: 20px">=default</p></div></div></div></div>';
		}



		$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row" style="margin-top:0px"><p class="col-lg-12" style="text-align: center;font-size: 25px;color:#45A29E">Nearby Genes: <span style="font-size: 20px;color:'+peak+'">'+tid +'</span></p></div><div class="row am-row"><div class="col-10" style="padding-left:0px;padding-right:0px"><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-4" style="font-style: italic;padding-left:20px">overlap</div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-1 box" style="background-color:'+colors[0]+'" id="box1"></div><div class="col-1 box" style="background-color:'+colors[1]+'" id="box2"></div><div class="col-1 box" style="background-color:'+colors[2]+'" id="box3"></div><div class="col-1 box" style="background-color:'+colors[3]+'" id="box4"></div><div class="col-1 box" style="background-color:'+colors[4]+'" id="box5"></div><div class="col-1 box" style="background-color:'+colors[5]+'" id="box6"></div><div class="col-1 box" style="background-color:'+colors[6]+'" id="box7"></div><div class="col-1 box" style="background-color:'+colors[7]+'" id="box8"></div><div class="col-1 box" style="background-color:'+colors[8]+'" id="box9"></div><div class="col-1 box" style="background-color:'+colors[9]+'" id="box10"></div><div class="col-1 box" style="background-color:'+colors[10]+'" id="box11"></div></div><div class="row am-row2" style="margin-top: -45px;margin-left:65px;margin-right:20px"><div class="col-1">5<sup>TH</sup></div><div class="col-1">4<sup>TH</sup></div><div class="col-1">3<sup>RD</sup></div><div class="col-1">2<sup>ND</sup></div><div class="col-1">1<sup>ST</sup></div><div class="col-1"></div><div class="col-1">1<sup>ST</sup></div><div class="col-1">2<sup>ND</sup></div><div class="col-1">3<sup>RD</sup></div><div class="col-1">4<sup>TH</sup></div><div class="col-1">5<sup>TH</sup></div></div>'+legend+'</div>');

        $('[id^="box"]').click(function(){

    		id=this.id;
			val=id.split("box")[1]-1;

			if(data[val]!=0){

				var color = NG_select[val] ? colors[val]:'red';
				$(this).css('background-color', color);
				NG_select[val] = NG_select[val] ? 0:1;

	   		}

		});

		$('.box').mouseleave(function(){
        	id=this.id;
        	val=id.split("box")[1]-1;
        	if(!NG_select[val]){
				$(this).css('background-color', colors[val]);
			}
		});

		$('.box').mouseenter(function(){
			id=this.id;
        	val=id.split("box")[1]-1;
        	if(!NG_select[val]){
				$(this).css('background-color', highlight);
				$(this).prop('title','Number of genes: '+data[val] + infoHighlight[val]);
			}
			if(data[val]==0){
				$(this).prop('title','No genes here..');
			}
		});


	};

	// long range interactions plot //
	function LRIplot(data, tid, logs, discriminate, left, right) {
		$(".title").remove();		
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").addClass("genePlot");

		colors = [];

		if(discriminate==1){
			logs.forEach(function (log, index) {

				if(parseFloat(log[0])>0){
					colors[index]=up;
					infoHighlight[index] = ", " + parseFloat(log[0]).toFixed(0) + " up-regulated genes (versus "+ parseFloat(log[1]).toFixed(0)+" down-regulated)";
				}else if(parseFloat(log[0])<0){
					colors[index]=down;
					let num = parseFloat(log[0]).toFixed(0);
					infoHighlight[index] = ", " + Math.abs(num) + " down-regulated genes (versus "+ parseFloat(log[1]).toFixed(0)+" up-regulated)";
				}else if(log[0]=="NaN"){
					colors[index]=equal;
					infoHighlight[index] = ", equal number of up and down-regulated genes ("+parseFloat(log[1]).toFixed(0)+")";
				}else{
					colors[index]=def;
					infoHighlight[index] = ", no expessed genes";
				}
			});
			//add legend
			var legend = '</div><div class="col-2" style="padding-left:50px"><div class="row">legend:</div><div class="row" style="margin-top: 10px"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+up+'"><p style="margin-left: 20px">=up</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+down+'"><p style="margin-left: 20px">=down</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+equal+'"><p style="margin-left: 20px">=equal</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+def+'"><p style="margin-left: 20px">=default</p></div></div></div></div>';
		}else{
			logs.forEach(function (log, index) {

				if((parseFloat(log[0])>0)&&(parseFloat(log[0])<0.2)){
					colors[index]=zerotwo;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.2)&&(parseFloat(log[0])<0.4)){
					colors[index]=zerofour;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.4)&&(parseFloat(log[0])<0.6)){
					colors[index]=zerosix;
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
				}else if((parseFloat(log[0])>=0.6)&&(parseFloat(log[0])<0.8)){
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
					colors[index]=zeroeigth;
				}else if((parseFloat(log[0])>=0.8)&&(parseFloat(log[0])<=1)){
					infoHighlight[index] = ", coverage: " + parseFloat(log[0]).toFixed(2);
					colors[index]=one;
				}else{
					colors[index]=def;
					infoHighlight[index] = ", no expessed genes";
				}
			});
			//add legend
			var legend = '</div><div class="col-2" style="padding-left:50px"><div class="row">legend:</div><div class="row" style="margin-top: 10px"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerotwo+'"><p style="margin-left: 20px"> 0-0.2</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerofour+'"><p style="margin-left: 20px"> 0.2-0.4</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zerosix+'"><p style="margin-left: 20px"> 0.4-0.6</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+zeroeigth+'"><p style="margin-left: 20px"> 0.6-0.8</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+one+'"><p style="margin-left: 20px"> 0.8-1</p></div></div><div class="row"><div style="width:20px;height:20px;border:1px solid #000; background-color: '+def+'"><p style="margin-left: 20px">=default</p></div></div></div></div>';
		}

		$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 35px;color:#45A29E">Long Range Interactions: <span style="font-size: 35px;color:'+peak+'">'+tid +'</span></p></div><div class="row am-row"><div class="col-10"><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-2"></div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-5 box" id="box1" style="background-color:'+colors[0]+'" ></div><div class="col-1 boxm"></div><div class="col-5 box" id="box2" style="background-color:'+colors[1]+'"></div></div><div class="row am-row2" style="margin-top: -45px;"><div class="col-5" style="text-align: center;">'+left+' Kb</div><div class="col-1" style="text-align: center; margin-left: -0.95rem" >peak</div><div class="col-5" style="text-align: center;margin-left: 0.75rem">'+right+' Kb</div></div>'+legend+'</div></div></div>');

		$('[id^="box"]').click(function(){
				id=this.id;
				val=id.split("box")[1]-1;
				if(data[val]!=0){
				var color = LRI_select[val] ? '#45A29E':'red';
			$(this).css('background-color', color);
			LRI_select[val] = LRI_select[val] ? 0:1;}
		});
		
		$('.box').mouseleave(function(){
				id=this.id;
				val=id.split("box")[1]-1;
				if(!LRI_select[val]){
					$(this).css('background-color', colors[val]);}
		});
		$('.box').mouseenter(function(){

			id=this.id;
				val=id.split("box")[1]-1;
				if(!LRI_select[val]){
					$(this).css('background-color', highlight);
					$(this).prop('title','Number of genes: '+data[val] + infoHighlight[val]);
				}
				if(data[val]==0){
					$(this).prop('title','No genes here..');
				}
		});

		


	}//function LRIplot()

	///////////////////////////////////
	// main function for each feature//
	///////////////////////////////////

	// TF/HM enrichment //
	function tfscan() {
		$(".annobuttons").hide();
		$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);

		var cginame = "/AnnoMiner/TFScan";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#ianno").val();
		var ovtype = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var prom_us = $("#promoter_us").val();
		var prom_ds = $("#promoter_ds").val();
		var enrich = $('#analyis').val();

		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");

		if(last_datatable!=null) {
			last_datatable.destroy();
		}

		$("#gtable").remove();
		$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,ovval:ovl,ovtype:ovtype, promoter_us:prom_us, promoter_ds:prom_ds, enrich:enrich},
			function(data){

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}

				if (data.data != null){

						$(".title").remove();
						$(".annobuttons").show();
						$(".annobuttons2").hide();
						$("#cpy_list").hide();
						$("#download_converted_list").show();
						//plot
						enrichValues = data.enrichValues;
						enrichTitles = data.enrichTitles;
						enrichProbe = data.enrichProbe;
						enrichcols = [lblue,lblue,lblue,lblue,lblue,lblue,lblue,lblue,lblue,lblue];
						convertedIDs = data.convertedIDs;
						// alert(convertedIDs);
						enrichplot(enrichValues,enrichTitles);

						if(enrich=="HMs"){
							$('<div class="container-fluid title" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 25px;color:#45A29E">Histone marks enriched in: ' + '<span style="font-size: 20px;color:'+peak+'">'+tid +'</span></p></div></div>').insertBefore("#tab1_secondplot");
							var tfscan_titles = [{"sTitle":"HM"},{"sTitle":"Sample"},{"sTitle":"Treatment"},{"sTitle":"Replicate"},{"sTitle":"List Hits"},{"sTitle":"List Size"},{"sTitle":"Genome Hits"},{"sTitle":"Genome Size"},{"sTitle":"Target IDs"},{"sTitle":"Target Genes"},{"sTitle":"Score", "sType":"numeric-custom"},{"sTitle":"P-value","sType":"numeric-custom"},{"sTitle":"FDR","sType":"numeric-custom"},{"sTitle":"Combined score","sType":"numeric-custom"}];
						}else{
							$('<div class="container-fluid title" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 25px;color:#45A29E">Transciption factors enriched in: ' + '<span style="font-size: 20px;color:'+peak+'">'+tid +'</span></p></div></div>').insertBefore("#tab1_secondplot");
							var tfscan_titles = [{"sTitle":"TF"},{"sTitle":"Sample"},{"sTitle":"Treatment"},{"sTitle":"Replicate"},{"sTitle":"List Hits"},{"sTitle":"List Size"},{"sTitle":"Genome Hits"},{"sTitle":"Genome Size"},{"sTitle":"Target IDs"},{"sTitle":"Target Genes"},{"sTitle":"Score", "sType":"numeric-custom"},{"sTitle":"P-value","sType":"numeric-custom"},{"sTitle":"FDR","sType":"numeric-custom"},{"sTitle":"Combined score","sType":"numeric-custom"}];
						}

						$('#btn_converted_list').on( 'click', function () {
							var tsv = JSON2TSV( convertedIDs , false);
							window.open("data:text/tab-separated-values;charset=utf-8," + escape(tsv));

						});
						
						$("#gtable").addClass("resize");

						last_datatable_header = data.titles;
						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": tfscan_titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple",
							"order": [[13,'desc']],
							"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
							"aoColumnDefs": [
							{ "bVisible": true, "aTargets": [ 0,1,9,10,12,13 ] },
							{ "aTargets": [10], "mRender": function (data, type, full) {return (parseFloat(data).toFixed(2));}},
							{ "aTargets": [12], "mRender": function (data, type, full) {return (parseFloat(data).toFixed(2));}},
							{ "aTargets": [13], "mRender": function (data, type, full) {return (parseFloat(data).toFixed(2));}},
							{ "bVisible": false, "aTargets": [ '_all' ] }
							]
						});
					
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						
						// $('#gtable').on( 'click', 'td', function () {
						// 			if(last_datatable.cell( this ).data()){
						// 				var cell = last_datatable.cell( this ).data();
						// 				alert( cell.replace(/;/g, "\n") );
						// }});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {

								$(this).toggleClass("cellev");
								if(this.cellIndex==4){
									$(this).attr('title', 'Enrichment score : ("List Hits"/"List size") / ("Genome Hits"/"Genome size")');
								}else if(this.cellIndex==5){
									$(this).attr('title', 'FDR : corrected P-value (Benjamini-Hochberg method)');
								}else if(this.cellIndex==6){
									$(this).attr('title', 'Combined score : (Enrichment score)*(-log10(P-value))');
								}
						});
						// $('#gtable').on( 'mouseover mouseout', 'tr', function () {
						// 		$(this).toggleClass("rowev");
						// });
						$('#gtable').on( 'click', 'td', function () {
							if(this.cellIndex==0){
								var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
								window.open(link);
							}else{
								var cell = last_datatable.cell( this ).data();
								alert( cell.replace(/;/g, "\n") );
							}
						});

						$("#btn_dwl_rslt").show();
						$("#new").show();

				} else {

					$("#btn_dwl_rslt").hide();
					$("#new").hide();

				}


				$("#progress").hide();


			},

			"json"

		);

	};

	
	// gene centered annotation //
	function peak2gene() {

		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/PeakToGene";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		var tid3 = $("#tanno3").val();
		var ovl_s = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();

		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");

		if(last_datatable!=null) {
			last_datatable.destroy();
		}

		$("#gtable").remove();
		$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[gselect],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can,trackid3:tid3},
			function(data){
				
				gplotdata = [];

				if (data.flankcov){
					
					gplotdata = data.flankcov;
					gcols = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					gplot(gplotdata, tid);
					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}


				if (data.data != null){

					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);


						var titles = defineHeaderFormatting(data.titles,data.labeltypes);

						$(".annobuttons").show();
						$("#cpy_list").show();
						$("#download_converted_list").hide();
						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						$('#gtable').on( 'click', 'td', function () {
								if(this.cellIndex==2||this.cellIndex==1||this.cellIndex==0){
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);
								}else{
									var cell = last_datatable.cell( this ).data();
									alert( cell.replace(/;/g, "\n") );
								}
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});
						//clipboard
						$('#btn_cpy_list').prop("onclick", null).off("click");
						$('#btn_cpy_list').on( 'click', function () {
							var geneList = data.geneList;
							copy2clipboard(geneList.toString().replaceAll(",", "\n"));
						});

				}else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

			},

			"json"


		);

		gselect = [0,0,0,0,0,0,0,0,0,0];
		cr_gn = false


	};


	//peak correlation function 
	function peakCorrelation() {

		$(".annobuttons").hide();
		//inizialize variables
		var cginame = "/AnnoMiner/PeakCorrelation";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid1 = $("#tanno").val();
		var tid2 = $("#tanno2").val();
		var tracks = $("#number_tracks").val();
		if(tracks==3){
			var tid21 = $("#tanno21").val();	
		}else if(tracks==4){
			var tid21 = $("#tanno21").val();
			var tid22 = $("#tanno22").val();
		}else if(tracks==5){
			var tid21 = $("#tanno21").val();
			var tid22 = $("#tanno22").val();
			var tid23 = $("#tanno23").val();
		}else{
	
		}	
		var tid3 = $("#tanno3").val();
		var ovl_s = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();

		//inizialize the plots
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");
		$("#tab1_thirdplot").empty();
		$("#tab1_thirdplot").height("0px");
		$("#tab1_fourthplot").empty();
		$("#tab1_fourthplot").height("0px");
		$("#tab1_fifthplot").empty();
		$("#tab1_fifthplot").height("0px");
		$("#tab1_sixthplot").empty();
		$("#tab1_sixthplot").height("0px");

		if(last_datatable!=null) {
			last_datatable.destroy();
		}

		$("#gtable").remove();
		$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		if(tracks==2){
		$.post(
			cginame,
			{assembly:ass,gtype:gtp,tracks:tracks, trackid1:tid1,trackid2:tid2,'targets1[]':[gselect1],'targets2[]':[gselect2],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can,trackid3:tid3},
			function(data){
				
				gplotdata1 = [];
				gplotdata2 = [];

				if ((data.flankcov1)&&(data.flankcov2)){
					
					gplotdata1 = data.flankcov1;
					gcols1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					corrplot(gplotdata1,"tab1_secondplot", gcols1, gselect1, cr_gn1,tid1,gcols_df1);
					
					gplotdata2 = data.flankcov2;
					gcols2 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
					corrplot(gplotdata2,"tab1_thirdplot", gcols2, gselect2,cr_gn2,tid2,gcols_df2);

					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {

					$(".title").remove();
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
					$("#tab1_thirdplot").empty();
					$("#tab1_thirdplot").height("0px");
				}

				if (data.message1 != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message1+'</div>');
				}
				if (data.message2 != null){
					$("#textout3").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message2+'</div>');
				}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

					var titles = defineHeaderFormatting(data.titles, data.labeltypes);
						
						
						$(".annobuttons").show();
						$("#cpy_list").show();
						$("#download_converted_list").hide();
						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");


						$('#gtable').on( 'click', 'td', function () {
									if(this.cellIndex==2||this.cellIndex==1||this.cellIndex==0){
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);}else{
									var cell = last_datatable.cell( this ).data();
									alert( cell.replace(/;/g, "\n") );}
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});
						//clipboard
						$('#btn_cpy_list').prop("onclick", null).off("click");
						$('#btn_cpy_list').on( 'click', function () {
							var geneList = data.geneList;
							copy2clipboard(geneList.toString().replaceAll(",", "\n"));
						});

				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

			},

			"json"


		);

		gselect1 = [0,0,0,0,0,0,0,0,0,0];
		gselect2 = [0,0,0,0,0,0,0,0,0,0];
		cr_gn1 = false;
		cr_gn2 = false;

		}else if(tracks==3){

			$.post(
				cginame,
				{assembly:ass,gtype:gtp,tracks:tracks,trackid1:tid1,trackid2:tid2,trackid21:tid21,'targets1[]':[gselect1],'targets2[]':[gselect2],'targets21[]':[gselect21],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can,trackid3:tid3},
				function(data){

					gplotdata1 = [];
					gplotdata2 = [];
					gplotdata21 = [];
	
					if ((data.flankcov1)&&(data.flankcov2)&&(data.flankcov21)){
						
						gplotdata1 = data.flankcov1;
						gcols1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata1,"tab1_secondplot", gcols1, gselect1, cr_gn1,tid1,gcols_df1);
						
						gplotdata2 = data.flankcov2;
						gcols2 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
						corrplot(gplotdata2,"tab1_thirdplot", gcols2, gselect2,cr_gn2,tid2,gcols_df2);

						gplotdata21 = data.flankcov21;
						gcols21 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata21,"tab1_fourthplot", gcols21, gselect21,cr_gn21,tid21,gcols_df21);
	
						$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');
	
	
					} else {
	
						$(".title").remove();
						$("#tab1_secondplot").empty();
						$("#tab1_secondplot").height("0px");
						$("#tab1_thirdplot").empty();
						$("#tab1_thirdplot").height("0px");
						$("#tab1_fourthplot").empty();
						$("#tab1_fourthplot").height("0px");
					}
	
					if (data.message1 != null){
						$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message1+'</div>');
					}
					if (data.message2 != null){
						$("#textout3").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message2+'</div>');
					}
	
					if (data.data != null){
						$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);
	
						var titles = defineHeaderFormatting(data.titles, data.labeltypes);
							
							
							$(".annobuttons").show();
							$("#cpy_list").show();
							$("#download_converted_list").hide();
							last_datatable = $("#gtable").DataTable({
								"aaData": data.data,
								"aoColumns": titles,
								"bProcessing": true,
								"bDestroy": true,
								"bAutoWidth": false,
								"pagingType": "simple"
	
							});
							//to solve page resize issues
							$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
							$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");
	
	
							$('#gtable').on( 'click', 'td', function () {
										if(this.cellIndex==2||this.cellIndex==1||this.cellIndex==0){
										var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
										window.open(link);}else{
										var cell = last_datatable.cell( this ).data();
										alert( cell.replace(/;/g, "\n") );}
							});
	
							$('#gtable').on( 'mouseover mouseout', 'td', function () {
									$(this).toggleClass("cellev");
							});
							$('#gtable').on( 'mouseover mouseout', 'tr', function () {
									$(this).toggleClass("rowev");
							});
							//clipboard
							$('#btn_cpy_list').prop("onclick", null).off("click");
							$('#btn_cpy_list').on( 'click', function () {
								var geneList = data.geneList;
								copy2clipboard(geneList.toString().replaceAll(",", "\n"));
							});
	
					} else {
						$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
						$("#btn_dwl_rslt").hide();
						$("#new").hide();
					}
	
					$("#progress").hide();
	
				},
	
				"json"
	
	
			);
	
			gselect1 = [0,0,0,0,0,0,0,0,0,0];
			gselect2 = [0,0,0,0,0,0,0,0,0,0];
			gselect21 = [0,0,0,0,0,0,0,0,0,0];
			cr_gn1 = false;
			cr_gn2 = false;
			cr_gn21 = false;

		}else if(tracks==4){

			$.post(
				cginame,
				{assembly:ass,gtype:gtp,tracks:tracks,trackid1:tid1,trackid2:tid2,trackid21:tid21,trackid22:tid22,'targets1[]':[gselect1],'targets2[]':[gselect2],'targets21[]':[gselect21],'targets22[]':[gselect22],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can,trackid3:tid3},
				function(data){
					
					gplotdata1 = [];
					gplotdata2 = [];
					gplotdata21 = [];
					gplotdata22 = [];
	
					if ((data.flankcov1)&&(data.flankcov2)&&(data.flankcov21)&&(data.flankcov22)){
						
						gplotdata1 = data.flankcov1;
						gcols1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata1,"tab1_secondplot", gcols1, gselect1, cr_gn1,tid1,gcols_df1);
						
						gplotdata2 = data.flankcov2;
						gcols2 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
						corrplot(gplotdata2,"tab1_thirdplot", gcols2, gselect2,cr_gn2,tid2,gcols_df2);

						gplotdata21 = data.flankcov21;
						gcols21 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata21,"tab1_fourthplot", gcols21, gselect21,cr_gn21,tid21,gcols_df21);

						gplotdata22 = data.flankcov22;
						gcols22 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
						corrplot(gplotdata22,"tab1_fifthplot", gcols22, gselect22,cr_gn22,tid22,gcols_df22);
	
						$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');
	
	
					} else {
	
						$(".title").remove();
						$("#tab1_secondplot").empty();
						$("#tab1_secondplot").height("0px");
						$("#tab1_thirdplot").empty();
						$("#tab1_thirdplot").height("0px");
						$("#tab1_fourthplot").empty();
						$("#tab1_fourthplot").height("0px");
						$("#tab1_fifthplot").empty();
						$("#tab1_fifthplot").height("0px");
					}
	
					if (data.message1 != null){
						$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message1+'</div>');
					}
					if (data.message2 != null){
						$("#textout3").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message2+'</div>');
					}
	
					if (data.data != null){
						$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);
	
						var titles = defineHeaderFormatting(data.titles, data.labeltypes);
							
							
							$(".annobuttons").show();
							$("#cpy_list").show();
							$("#download_converted_list").hide();
							last_datatable = $("#gtable").DataTable({
								"aaData": data.data,
								"aoColumns": titles,
								"bProcessing": true,
								"bDestroy": true,
								"bAutoWidth": false,
								"pagingType": "simple"
	
							});
							//to solve page resize issues
							$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
							$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");
	
	
							$('#gtable').on( 'click', 'td', function () {
										if(this.cellIndex==2||this.cellIndex==1||this.cellIndex==0){
										var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
										window.open(link);}else{
										var cell = last_datatable.cell( this ).data();
										alert( cell.replace(/;/g, "\n") );}
							});
	
							$('#gtable').on( 'mouseover mouseout', 'td', function () {
									$(this).toggleClass("cellev");
							});
							$('#gtable').on( 'mouseover mouseout', 'tr', function () {
									$(this).toggleClass("rowev");
							});
							//clipboard
							$('#btn_cpy_list').prop("onclick", null).off("click");
							$('#btn_cpy_list').on( 'click', function () {
								var geneList = data.geneList;
								copy2clipboard(geneList.toString().replaceAll(",", "\n"));
							});
	
					} else {
						$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
						$("#btn_dwl_rslt").hide();
						$("#new").hide();
					}
	
					$("#progress").hide();
	
				},
	
				"json"
	
	
			);
	
			gselect1 = [0,0,0,0,0,0,0,0,0,0];
			gselect2 = [0,0,0,0,0,0,0,0,0,0];
			gselect21 = [0,0,0,0,0,0,0,0,0,0];
			gselect22 = [0,0,0,0,0,0,0,0,0,0];
			cr_gn1 = false;
			cr_gn2 = false;
			cr_gn21 = false;
			cr_gn22 = false;

		}else if(tracks==5){

			$.post(
				cginame,
				{assembly:ass,gtype:gtp,tracks:tracks,trackid1:tid1,trackid2:tid2,trackid21:tid21,trackid22:tid22,trackid23:tid23,'targets1[]':[gselect1],'targets2[]':[gselect2],'targets21[]':[gselect21],'targets22[]':[gselect22],'targets23[]':[gselect23],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can,trackid3:tid3},
				function(data){
					
					gplotdata1 = [];
					gplotdata2 = [];
					gplotdata21 = [];
					gplotdata22 = [];
					gplotdata23 = [];
	
					if ((data.flankcov1)&&(data.flankcov2)&&(data.flankcov21)&&(data.flankcov22)&&(data.flankcov23)){
						
						gplotdata1 = data.flankcov1;
						gcols1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata1,"tab1_secondplot", gcols1, gselect1, cr_gn1,tid1,gcols_df1);
						
						gplotdata2 = data.flankcov2;
						gcols2 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
						corrplot(gplotdata2,"tab1_thirdplot", gcols2, gselect2,cr_gn2,tid2,gcols_df2);

						gplotdata21 = data.flankcov21;
						gcols21 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata21,"tab1_fourthplot", gcols21, gselect21,cr_gn21,tid21,gcols_df21);

						gplotdata22 = data.flankcov22;
						gcols22 = [lgreen,lgreen,lgreen,dgreen,dgreen,dgreen,dgreen,lgreen,lgreen,lgreen];
						corrplot(gplotdata22,"tab1_fifthplot", gcols22, gselect22,cr_gn22,tid22,gcols_df22);

						gplotdata23 = data.flankcov23;
						gcols23 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
						corrplot(gplotdata23,"tab1_sixthplot", gcols23, gselect23,cr_gn23,tid23,gcols_df23);


						$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');
	
	
					} else {
	
						$(".title").remove();
						$("#tab1_secondplot").empty();
						$("#tab1_secondplot").height("0px");
						$("#tab1_thirdplot").empty();
						$("#tab1_thirdplot").height("0px");
						$("#tab1_fourthplot").empty();
						$("#tab1_fourthplot").height("0px");
						$("#tab1_fifthplot").empty();
						$("#tab1_fifthplot").height("0px");
						$("#tab1_sixthplot").empty();
						$("#tab1_sixthplot").height("0px");
					}
	
					if (data.message1 != null){
						$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message1+'</div>');
					}
					if (data.message2 != null){
						$("#textout3").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message2+'</div>');
					}
	
					if (data.data != null){
						$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);
	
						var titles = defineHeaderFormatting(data.titles, data.labeltypes);
							
							
							$(".annobuttons").show();
							$("#cpy_list").show();
							$("#download_converted_list").hide();
							last_datatable = $("#gtable").DataTable({
								"aaData": data.data,
								"aoColumns": titles,
								"bProcessing": true,
								"bDestroy": true,
								"bAutoWidth": false,
								"pagingType": "simple"
	
							});
							//to solve page resize issues
							$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
							$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");
	
	
							$('#gtable').on( 'click', 'td', function () {
										if(this.cellIndex==2||this.cellIndex==1||this.cellIndex==0){
										var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
										window.open(link);}else{
										var cell = last_datatable.cell( this ).data();
										alert( cell.replace(/;/g, "\n") );}
							});
	
							$('#gtable').on( 'mouseover mouseout', 'td', function () {
									$(this).toggleClass("cellev");
							});
							$('#gtable').on( 'mouseover mouseout', 'tr', function () {
									$(this).toggleClass("rowev");
							});
							//clipboard
							$('#btn_cpy_list').prop("onclick", null).off("click");
							$('#btn_cpy_list').on( 'click', function () {
								var geneList = data.geneList;
								copy2clipboard(geneList.toString().replaceAll(",", "\n"));
							});
	
					} else {
						$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
						$("#btn_dwl_rslt").hide();
						$("#new").hide();
					}
	
					$("#progress").hide();
	
				},
	
				"json"
	
	
			);
	
			gselect1 = [0,0,0,0,0,0,0,0,0,0];
			gselect2 = [0,0,0,0,0,0,0,0,0,0];
			gselect21 = [0,0,0,0,0,0,0,0,0,0];
			gselect22 = [0,0,0,0,0,0,0,0,0,0];
			gselect23 = [0,0,0,0,0,0,0,0,0,0];
			cr_gn1 = false;
			cr_gn2 = false;
			cr_gn21 = false;
			cr_gn22 = false;
			cr_gn23 = false;
		}

	};

	// nearby gene function //
	function NearbyGenes() {

		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/NearbyGenes";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		var tid3 = $("#tanno3").val();
		var discriminateCol = $("input[name='highlighter']:checked").val();
		var strnd = $("input[name='strand']:checked").val();

		$("#tab1_secondplot").empty();

		if(last_datatable!=null) {
			last_datatable.destroy();
		}
		$("#gtable").remove();
		$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[NG_select], strand: strnd,trackid3:tid3, highlighter:discriminateCol},
			function(data){

				NG_plotdata = [];
				NG_plotlog2FC = [];
				
				if (data.flankcov){
					NG_plotdata = data.flankcov;
					NG_plotlog2FC = data.log2FC;
					NGplot(NG_plotdata,tid,NG_plotlog2FC, discriminateCol);
					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

					var titles = defineHeaderFormatting(data.titles, data.labeltypes);
						$(".annobuttons").show();
						$("#cpy_list").show();
						$("#download_converted_list").hide();
						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");


						$('#gtable').on( 'click', 'td', function () {
									if(this.cellIndex==1||this.cellIndex==0){
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);}else{
									var cell = last_datatable.cell( this ).data();
									alert( cell.replace(/;/g, "\n") );}
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});
						//clipboard
						$('#btn_cpy_list').prop("onclick", null).off("click");
						$('#btn_cpy_list').on( 'click', function () {
							var geneList = data.geneList;
							copy2clipboard(geneList.toString().replaceAll(",", "\n"));
						});
				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

	 		},

			"json"


	 	);

		NG_select = [0,0,0,0,0,0,0,0,0,0,0];

		cr_NG = false;

	};
	// long range interactions //
	function LongRangeInteractions() {
			$(".annobuttons").hide();
			var cginame = "/AnnoMiner/LongRangeInteractions";
			var ass = $("#assembly").val();
			var gtp = $("#ganno").val();
			var tid = $("#tanno").val();
			var tid3 = $("#tanno3").val();
			var right = $("#right").val();
			var left = $("#left").val();
			var strnd = $("input[name='strand']:checked").val();
			var can = $("input[name='canonical']:checked").val();
			var discriminateCol = $("input[name='highlighter']:checked").val();
			
			$("#tab1_secondplot").empty();

			if(last_datatable!=null) {
				last_datatable.destroy();
			}
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');
			$.post(
				cginame,
				{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[LRI_select],left:left,right:right,strand: strnd,canonical:can,trackid3:tid3, highlighter : discriminateCol},
				function(data){
					LRI_plotdata = [];
					
					if (data.flankcov){
						
						LRI_plotdata = data.flankcov;
						LRI_plotlog2FC = data.log2FC;
						LRIplot(LRI_plotdata,tid, LRI_plotlog2FC, discriminateCol, left, right);
						$("#textout2").append('</br><div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


					} else {
						$("#tab1_secondplot").empty();
						$("#tab1_secondplot").height("0px");
					}

					if (data.message != null){

						$("#textout").append('</br></br><div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
						}


					if (data.data != null){
						$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

						var titles = defineHeaderFormatting(data.titles, cginame);
							$(".annobuttons").show();
							$("#cpy_list").show();
							$("#download_converted_list").hide();
							last_datatable = $("#gtable").DataTable({
								"aaData": data.data,
								"aoColumns": titles,
								"bProcessing": true,
								"bDestroy": true,
								"bAutoWidth": false,
								"pagingType": "simple"

							});
							//to solve page resize issues
							$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
							$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
							$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

							// $('#gtable').on( 'click', 'td', function () {
							// 	if(this.cellIndex==0||this.cellIndex==1){
							// 			var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
							// 			window.open(link);}
							// });

							$('#gtable').on( 'click', 'td', function () {
										if(this.cellIndex==1||this.cellIndex==0){
										var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
										window.open(link);}else{
										var cell = last_datatable.cell( this ).data();
										alert( cell.replace(/;/g, "\n") );}
							});

							$('#gtable').on( 'mouseover mouseout', 'td', function () {
									$(this).toggleClass("cellev");
							});
							$('#gtable').on( 'mouseover mouseout', 'tr', function () {
									$(this).toggleClass("rowev");
							});
													//clipboard
							$('#btn_cpy_list').prop("onclick", null).off("click");
							$('#btn_cpy_list').on( 'click', function () {
								var geneList = data.geneList;
								copy2clipboard(geneList.toString().replaceAll(",", "\n"));
							});

					} else {
						$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
						$("#btn_dwl_rslt").hide();
						$("#new").hide();
					}

					$("#progress").hide();

				},

				"json"


			);

			LRI_select = [0,0];
			cr_LRI = false;

	};

	////////////////////////
	// accessory functions//
	////////////////////////

	//copy to clipboard function
	function copy2clipboard(text) {
		if(text){
			var clipboard = document.createElement("textarea");
			document.body.appendChild(clipboard);
			/* Get the text field */
			var geneList = text;
			clipboard.value = geneList;
			clipboard.select();
			/* Copy the text inside the text field */
			document.execCommand("copy");
			clipboard.remove();
			/* Alert the copied text */
			alert("Gene list copied to the clipboard, ctrl+v (or right mouse button > paste) to paste it");
		}else{
			alert("None of your genes are present!");
		}
	}; 

	//build link to url function
	function buildLink(ass,gtp,index,id){

		var link='';

		//assembly drosophila
		if(ass=='dm3'||ass=='dm6'){
			if(gtp=='refseq'){
				if(index==0){
					link = 'https://www.ncbi.nlm.nih.gov/gene?term='+id+'[All]%20AND%20%22Drosophila%20melanogaster%22[porgn]&cmd=DetailsSearch';
				}else if (index==1){
					link = 'https://www.ncbi.nlm.nih.gov/gene?term='+id+'[Gene]%20AND%20%22Drosophila%20melanogaster%22[porgn]&cmd=DetailsSearch';
				}
			}else if(gtp=='ensembl'){
				if(index==0){
					link = 'http://flybase.org/reports/'+id+'.html';
				}else if (index==1){
					link = 'http://flybase.org/reports/'+id+'.html';
				}
			}
		}
		//Caenorhabditis elegans (c.elegans)
		if(ass=='ce11'){
			if(gtp=='refseq'){
				if(index==0){
					link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Caenorhabditis+elegans%22%5Bporgn%5D';
				}else if (index==1){
					link = 'https://www.wormbase.org/species/c_elegans/gene/'+id;
				}
			}else if(gtp=='ensembl'){
				if(index==0){
					link = 'http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;t='+id;
				}else if (index==1){
					link = 'http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;g='+id;
				}
			}
		}

		//Mus musculus
		if(ass=='mm9'||ass=='mm10'){
			if(gtp=='refseq'){
				if(index==0){
					link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Mus+musculus%22%5Bporgn%5D';
				}else if (index==1){
					link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BGene%5D+AND+%22Mus+musculus%22%5Bporgn%5D';
				}
			}else if(gtp=='ensembl'|| gtp=='ucsc'){
				if(index==0){
					link = 'https://www.ensembl.org/Mus_musculus/Gene/Summary?t='+id;
				}else if (index==1){
					link = 'https://www.ensembl.org/Mus_musculus/Gene/Summary?g='+id;
				}
			}
		}

		//Saccaromyces cervisae (sacCer3)
		if(ass=='sacCer3'){
			if(index==0 || index==1){
			link='https://www.yeastgenome.org/locus/'+id;}
		}

		//homo sapiens
		if(ass=='hg19'|| ass=='hg38'){
			if(gtp=='refseq'){
				if(index==0){
					link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Homo+sapiens%22%5Bporgn%5D';
				}else if (index==1){
					link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BGene%5D+AND+%22Homo+sapiens%22%5Bporgn%5D';
				}
			}else if(gtp=='ensembl'|| gtp=='ucsc'|| gtp=='genecode'){
				if(index==0){
					link = 'https://www.ensembl.org/Homo_sapiens/Gene/Summary?t='+id;
				}else if (index==1){
					link = 'https://www.ensembl.org/Homo_sapiens/Gene/Summary?g='+id;
				}
			}
		}

		return link;
	
	};

	///////////////////////////
	// User track management //
	///////////////////////////

	var utracks = new Array();
	
	// definition of file upload form
	var ul_trackid_p1 = '<hr class="my-4"><div class="ultrack"><div id="ul_trackid" ><input type="text" name="trackid" class="form-control" value="';
	var ul_trackid_p2 = '"/></div></div>';
	var ul_datatype = '<div class="ultrack2" style="font-size:20px"><select id="ul_type" name="ul_type" class="form-control" >\
							<option value="none" disabled selected>Input files:</option>\
							<option value="peaks">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genomic regions</option>\
							<option value="idlist">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID list</option>\
							<option value="none" disabled></option>\
							<option value="none" disabled>Optional files:</option>\
							<option value="custom annotation">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Custom annotation</option>\
						</select></div>';
	var ul_datatype_batch = '<div class="ultrack2" style="font-size:20px"><select id="ul_type" name="ul_type" class="form-control" >\
						<option value="none" disabled selected>Input files:</option>\
						<option value="peaks">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genomic regions</option>\
						<option value="idlist">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID list</option>\
					</select></div>';
	var ul_button = '<div id="upload_button" style="font-size:20px"><input type="submit" id="ul_button" class="fileUpload btn btn-primary btn-lg" value="Upload"/></div>';

	///////////
	// Modal //
	///////////

	// Get the modal (alert with informations about input formats)
	var modal = document.getElementById('myModal');
	// Get the button that opens the modal
	var btn = document.getElementById("idlist");
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
	// When the user clicks the button, open the modal
	btn.onclick = function() {
		modal.style.display = "block";
	}
	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// Get the modal
	var modal2 = document.getElementById('myModal2');
	// Get the button that opens the modal
	var btn2 = document.getElementById("bedfile");
	// Get the <span> element that closes the modal
	var span2 = document.getElementsByClassName("close2")[0];
	// When the user clicks the button, open the modal
	btn2.onclick = function() {
		modal2.style.display = "block";
	}
	// When the user clicks on <span> (x), close the modal
	span2.onclick = function() {
		modal2.style.display = "none";
	}

	// Get the modal
	var modal3 = document.getElementById('myModal3');
	// Get the button that opens the modal
	var btn3 = document.getElementById("custom");
	// Get the <span> element that closes the modal
	var span3 = document.getElementsByClassName("close3")[0];
	// When the user clicks the button, open the modal
	btn3.onclick = function() {
		modal3.style.display = "block";
	}
	// When the user clicks on <span> (x), close the modal
	span3.onclick = function() {
		modal3.style.display = "none";
	}
	// Get the modal
	var modal4 = document.getElementById('myModal_clipboard');
	// Get the button that opens the modal
	var btn4 = document.getElementById("cpy_list_info");
	// Get the <span> element that closes the modal
	var span4 = document.getElementsByClassName("close4")[0];
	// When the user clicks the button, open the modal
	btn4.onclick = function() {
		modal4.style.display = "block";
	}
	// When the user clicks on <span> (x), close the modal
	span4.onclick = function() {
		modal4.style.display = "none";
	}




	///////////////////////////
	// User track actions    //
	///////////////////////////



	//after the click of upload button the div with your upload is shown
	$('body').on('click', '#ul_button', function() {
		UploadsCheck(remove);
		$("#UpTab").show();
		$("html, body").animate({scrollTop: $('#FU').offset().top - 90}, 500);

	});

	//Check the value of 3 file
	$("#ul_fname").change(function(event){
		
		$("#trackformat").empty();
		$("#trackformat2").empty();
		$("#trackformat3").empty();

		var typeUpload = $("#ul_type").val();

		if(typeUpload=="custom annotation"){

			$("#trackformat3").empty();
			$("#upload_button").empty();
			$("#trackformat").append('</br><label>Custom annotation file format options</label></br>');
			$("#trackformat").append( '<div class="form-group"><label for="colSelect">Column number</label><select class="form-control" id="colSelect" name="colSelect"><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option></select></div>' );
			$("#trackformat").append('<div class="form-group row align-items-center"><label for="text-input" class="col-2 col-form-label">Gene ID</label><div class="col-10"><input class="form-control" type="text" value="Gene ID is mandatory" disabled></div></div>');
			addFields();
			$("#trackformat3").append('<div class="form-group"><label for="formatSelect">File format</label><select class="form-control" name="formatSelect"><option value="0">tsv</option><option value="1">csv</option></select></div>');
			$("#trackformat3").append(ul_button);
		
		}else if (typeUpload=="none"){
		
			$("#trackformat").empty();
			$("#trackformat2").empty();
			$("#trackformat3").empty();

		}else{
			
			$("#trackformat").empty();
			$("#trackformat2").empty();
			$("#trackformat3").empty();
			$("#trackformat3").append(ul_button);
			
		}

	});

	$("#trackformat").change(function(event){
			addFields();
	});


	//Choose the file to upload
	$('#filechoice').change(function(event){

		$("#testParameters").empty();
		$("#testParameters").html("Choose your analysis parameters!!");
		removeResults();
		$("#trackformat").empty();
		$("#trackformat2").empty();
		$("#trackformat3").empty();

		$in=$(this);
		var path = $in.val();
		var fname = path.substring( path.lastIndexOf("\\") + 1, path.lastIndexOf("."));

		$("#ul_fname").empty();
		$("#ul_fname").append(ul_trackid_p1+fname+ul_trackid_p2+ul_datatype);

	});

	// get file path
	$('#submitPath').click(function(){
		var value = document.getElementById("path").value;
		$("#ul_fname").empty();
		$("#ul_fname").append(ul_trackid_p1+value+ul_trackid_p2+ul_datatype_batch);

	});

	// load data 
	$('#ul_target').load(function() {

			$("#ul_alerts").empty();
			var ul_json = $.parseJSON($("#ul_target").contents().text());
			if(ul_json.message!=null){
				$("#ul_alerts").append('<div class="alert alert-danger alert-dismissible" role="alert"><a href="#ul_alerts" class="close" data-dismiss="alert" aria-label="Close">&times;</a>'+ul_json.message+'</div>');
	
			}

			if(ul_json.data!=null){
				utracks = ul_json.data;
				updateAnalysisSelections();
				updateUTrackView(utracks);
				$("html, body").delay(30).animate({scrollTop: $('#AO').offset().top - 90}, 500);
				$(".step1").hide();
				$(".step2").show();
			}
	});





//////////////////////////////////////////////
// Selection of analysis and its parameters //
//////////////////////////////////////////////


	//slider for overlap
	var ov = "bp"

	var slider = document.getElementById("ovl_t");
	var output = document.getElementById("slider_value");
	var output_format = document.getElementById("slider_format");
	output.innerHTML = slider.value; // Display the default slider value
	output_format.innerHTML = ov;
	// Set target-region overlap type
	$('#ovl_s').change( function() {
		ovtype = $("#ovl_s").val();
		if (ovtype == 0) {
			ovval_rel = $("#ovl_t").val();
			$("#ovl_t").val(ovval_abs);
			ov = "bp";
			output.innerHTML = slider.value; // Display the default slider value
			output_format.innerHTML = ov;
			
		}else{
			ovval_abs = $("#ovl_t").val();
			$("#ovl_t").val(ovval_rel);
			ov = "%";
			output.innerHTML = slider.value; // Display the default slider value
			output_format.innerHTML = ov;
		}
	
	});

	// Update the current slider value (each time you drag the slider handle)
	slider.oninput = function() {
		output.innerHTML = slider.value;
	} 
	$("#number_tracks").change(function(event){
		$("#trackoptions").empty();
		$("#trackoptions2").empty();
		$("#trackoptions21").empty();
		$("#trackoptions22").empty();
		$("#trackoptions23").empty();
		tracks = $("#number_tracks").val();
		if(tracks==2){
			addGenomeSel();
			addTrackSel();
			addTrackSel2();	
		}else if(tracks==3){
			addGenomeSel();
			addTrackSel();
			addTrackSel2();
			addTrackSel21();	
		}else if(tracks==4){
			addGenomeSel();
			addTrackSel();
			addTrackSel2();	
			addTrackSel21();
			addTrackSel22();
		}else{
			addGenomeSel();
			addTrackSel();
			addTrackSel2();
			addTrackSel21();
			addTrackSel22();
			addTrackSel23();	
		}	
	});
	// Update on change the assembly
	$('#assembly0').change(function(event){

		species = $("#assembly").val();
		var analysis = $("#analyis").val();
		
		$("#trackoptions").empty();
		$("#trackoptions2").empty();
		$("#trackoptions21").empty();
		$("#trackoptions22").empty();
		$("#trackoptions23").empty();
		
		if( analysis == "genes" || analysis == "NG"|| analysis == "LRI") {
			addGenomeSel();
			addTrackSel();
		} else if ( analysis == "corr" ) {
			addGenomeSel();
			tracks = $("#number_tracks").val();
			if(tracks==2){
				addTrackSel();
				addTrackSel2();	
			}else if(tracks==3){
				addTrackSel();
				addTrackSel2();
				addTrackSel21();	
			}else if(tracks==4){
				addTrackSel();
				addTrackSel2();	
				addTrackSel21();
				addTrackSel22();
			}else{
				addTrackSel();
				addTrackSel2();
				addTrackSel21();
				addTrackSel22();
				addTrackSel23();	
			}	
		} else if ( analysis == "TFs" || analysis == "HMs") {
			addGenomeSel();
			addListSel();
		}
		
		$("#ul_form_assembly").val(species);

		updateAnalysisSelections();

	});

	//choice among dynamic range or custom definition of the upstream promoter cutoff
	$('#promoter_choice').change(function(event){
		var promoter_choice = $("#promoter_choice").val();
		if(promoter_choice=="dynamic"){
			$('#promoter_val').empty();
			$('#promoter_val').html('<input type="hidden" id="promoter_us" value="dynamic" class="promoter">');
		}else{
			$('#promoter_val').empty();
			$('#promoter_val').html('<label>From (upstream the TSS)</label><input type="text" id="promoter_us" value="2000"  class="form-control promoter">');
		}
	});

	//which options show to the user in according to the feature
	$('#analyis').change(function(event){
		var analysis = $("#analyis").val();
		addAssemblySel();
		
		if ( analysis == "genes"){
			$(".strand").hide();
			$(".highlighter").hide();
			$(".ovl").show();
			$(".window").hide();
			$(".tss").show();
			$(".flanking").show();
			$(".canonical").show();
			$(".promoter").hide();
			$(".integration").hide();
		}else if ( analysis == "corr"){
			$(".strand").hide();
			$(".highlighter").hide();
			$(".ovl").show();
			$(".window").hide();
			$(".tss").show();
			$(".flanking").show();
			$(".canonical").show();
			$(".promoter").hide();
			$(".integration").show();
		}else if ( analysis == "TFs"){
			$(".strand").hide();
			$(".highlighter").hide();
			$(".ovl").show();
			$(".tss").hide();
			$('#promoter_choice0').show();
			$('#promoter_val').empty();
			if($('#promoter_choice').val()=="dynamic"){
				$('#promoter_val').html('<input type="hidden" id="promoter_us" value="dynamic" class="promoter">');
			}else{
				$('#promoter_val').html('<label>From (upstream the TSS)</label><input type="text" id="promoter_us" value="2000"  class="form-control promoter">');
			}
			$(".flanking").hide();
			$(".canonical").hide();
			$(".promoter").show();
			$(".window").hide();
			$(".integration").hide();

		}else if (analysis == "HMs"){
			$(".highlighter").hide();
			$(".strand").hide();
			$(".ovl").show();
			$(".tss").hide();
			$(".window").hide();
			$('#promoter_choice0').hide();
			$('#promoter_val').empty();
			$('#promoter_val').html('<label>From (upstream the TSS)</label><input type="text" id="promoter_us" value="2000"  class="form-control promoter">');
			$(".flanking").hide();
			$(".canonical").hide();
			$(".promoter").show();
			$(".integration").hide();

		}else if ( analysis == "NG" ){
			$(".highlighter").show();
			$(".strand").show();
			$(".window").hide();
			$(".ovl").hide();
			$(".tss").hide();
			$(".flanking").hide();
			$(".canonical").hide();
			$(".promoter").hide();
			$(".integration").hide();

		}else if ( analysis == "LRI" ){
			$(".highlighter").show();
			$(".strand").show();
			$(".window").show();
			$(".ovl").hide();
			$(".tss").hide();
			$(".flanking").hide();
			$(".canonical").show();
			$(".promoter").hide();
			$(".integration").hide();
		}

		updateAnalysisSelections();
	});

	//////////////////////
	// action test data //
	//////////////////////

	//load file parameter for the TF enrichment test
	$("#idtest_loader").click(function(){
		testIdUp();
		removeResults();
		$("#testParameters").empty();
		$("#testParameters").css('text-align', 'left');
		$("#testParameters").html("</br><ul><li>Choose the type of analysis: <b>TF Enrichment analysis</b></li>"+
			"<li>Choose the reference genome: <b>Homo sapiens (hg38)</b></li>"+
			"<li>Select a genome-annotation resource: <b>refseq</b></li>"+
			"<li>Select a track with your genomic regions: <b>IDLIST_EXAMPLE</b></li>"+
			"<li>Criterion to select the overlap: <b>bp overlap</b></li>"+
			"<li>Select an overlap value: <b>1</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'>Select values to define the promoter region(bp)</p>"+
			"<ul style='padding-top : 15px'><li><b>Dynamic Ranges</b></li>"+
			"<li>To (downstream the TSS): <b>500</b></li>"+
			"<li>Gene or transcript-based search: <b>Consider all transcripts in search</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'><span class='glyphicon glyphicon-cog'></span> RUN!</p></br></br></br>"+
			"<p>Gene IDs for hg38. Genes binding REST in the promoter region (2000up-500down). Peaks from GSE90068 (ENCFF706DRE), source: ENCODE.</p>"
			);
	});

	// laod file and parameter for the gene centered annotation test
	$("#bedtest_loader").click(function(){
		testBEDUp();
		removeResults();
		$("#testParameters").empty();
		$("#testParameters").css('text-align', 'left');
		$("#testParameters").html("</br><ul><li>Choose the type of analysis: <b>Peak annotation</b></li>"+
			"<li>Choose the reference genome: <b>Homo sapiens (hg38)</b></li>"+
			"<li>Select a genome-annotation resource: <b>refseq</b></li>"+
			"<li>Select a track with your genomic regions: <b>BED_EXAMPLE</b></li>"+
			"<li>Select a track with your custom annotation: <b>null</b></li>"+
			"<li>Criterion to select the overlap: <b>bp overlap</b></li>"+
			"<li>Select an overlap value: <b>1</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'>Select values to define promoter region (bp)</p>"+
			"<ul style='padding-top : 15px'><li>From (upstream the TSS): <b>2000</b></li>"+
			"<li>To (downstream the TSS): <b>500</b></li>"+
			"<li>Select a value to define the Gene-flanking region (bp): <b>3000</b></li>"+
			"<li>Gene or transcript-based search: <b>Consider all transcripts in search</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'><span class='glyphicon glyphicon-cog'></span> RUN!</p></br></br></br>"+
			"<p>Genomic regions for hg38. TF-ChIP experiment targeting REST: GSE90068 (ENCFF706DRE), source: ENCODE.</p>"
			);
	});

	// load file and parameters for the custom annotation test file
	$("#customtest_loader").click(function(){
		testCustomUp();
		removeResults();
		$("#testParameters").empty();
		$("#testParameters").css('text-align', 'left');
		$("#testParameters").html("</br><ul><li>Choose the type of analysis: <b>Peak annotation</b></li>"+
			"<li>Choose the reference genome: <b>Homo sapiens (hg38)</b></li>"+
			"<li>Select a genome-annotation resource: <b>refseq</b></li>"+
			"<li>Select a track with your genomic regions: <b>BED_EXAMPLE</b></li>"+
			"<li>Select a track with your custom annotation: <b>CUSTOM_EXAMPLE</b></li>"+
			"<li>Criterion to select the overlap: <b>bp overlap</b></li>"+
			"<li>Select an overlap value: <b>1</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'>Select values to define the promoter region (bp)</p>"+
			"<ul style='padding-top : 15px'><li>From (upstream the TSS): <b>2000</b></li>"+
			"<li>To (downstream the TSS): <b>500</b></li>"+
			"<li>Select a value to define the Gene-flanking region (bp): <b>3000</b></li>"+
			"<li>Gene or transcript-based search: <b>Consider all transcripts in search</b></li></ul>"+
			"<p style='font-size: 1em; text-align: center'><span class='glyphicon glyphicon-cog'></span> RUN!</p></br></br></br>"+
			"<p>Genomic regions for hg38. TF-ChIP experiment targeting REST: GSE90068 (ENCFF706DRE), source: ENCODE.</p>"+
			"<p>Example annotation file for hg38. Differential expression analysis results of genes binding REST in the promoter region (2000up-500down). Peaks from GSE90068 (ENCFF706DRE), source: ENCODE.</p>"
			);
	});


	//Action in case of button click

	$("#btn_rslt , #textout2").click(function(){

		$(".step2").hide();
		$(".step1").hide();
		$("#progress").show();
		
		$("#textout").empty();
		$("#textout2").empty();
		$("#textout3").empty();

		$("#R").removeClass("d-none d-xl-block");
		$("#FU, #AO").addClass("d-none d-xl-block");

		// TRY TO FIX SECOND PLOT ISSUE
		$("#tab1_secondplot").remove();		
		$("#tab1_thirdplot").remove();
		$("#tab1_fourthplot").remove();		
		$("#tab1_fifthplot").remove();
		$("#tab1_sixthplot").remove();		
		$("#plots").append('<div class="row" id="tab1_secondplot"></div>');
		$("#plots").append('<div class="row" id="tab1_thirdplot"></div>');
		$("#plots").append('<div class="row" id="tab1_fourthplot"></div>');
		$("#plots").append('<div class="row" id="tab1_fifthplot"></div>');
		$("#plots").append('<div class="row" id="tab1_sixthplot"></div>');
		$(".title").remove();		
	

	//function to evoke in base to the type of analysis
		if( $('#analyis').val()== "genes" ) {
			peak2gene();
		}else if ( $('#analyis').val()== "corr" ){
			peakCorrelation();
		}else if ( $('#analyis').val()== "TFs" || $('#analyis').val()== "HMs" ){
			tfscan();
		}else if ( $('#analyis').val()== "NG" ){
			NearbyGenes();
		}else if ( $('#analyis').val()== "LRI" ){
			LongRangeInteractions();
		}

	});
	
	//Action in case of download button click
	$("#btn_dwl_rslt, #btn_dwl_rslt2").click(function(){

		var tsv = JSON2TSV( last_datatable.data() );
		window.open("data:text/tab-separated-values;charset=utf-8," + escape(tsv));

	});

	//Action in case of download button click
	$("#idtest_download").click(function(){
		saveAs("test/ENCFF706DRE_REST_promoter.txt","test_IDList.txt");
	});
	//Action in case of download button click
	$("#bedtest_download").click(function(){
		saveAs("test/ENCFF706DRE_REST.bed","test_genomicRegions.bed");
	});
					//Action in case of download button click
	$("#customtest_download").click(function(){
		saveAs("test/ENCFF706DRE_REST_promoter_fakeAnnotation.txt","test_customAnnotation.txt");
	});

	//Action in case of download button click
	$("#btn_dwl_img").click(function(){	

			window.scrollTo(0,0);
			html2canvas(document.querySelector('#saveFig')).then(function(canvas) {
				console.log(canvas);
				saveAs(canvas.toDataURL(), 'AnnoMiner.png');
			});

	});
	//donwload the dataset
	function saveAs(uri, filename) {

		var link = document.createElement('a');
	
		if (typeof link.download === 'string') {

			link.href = uri;
			link.download = filename;
	
			//Firefox requires the link to be in the body
			document.body.appendChild(link);
	
			//simulate click
			link.click();
	
			//remove the link when done
			document.body.removeChild(link);
	
		} else {
			// alert("out");
			window.open(uri);
	
		}
	}

	UploadsCheck(remove);
	addAssemblySel();
	addGenomeSel();
	addTrackSel();
	addTrackSel3();

	$("html, body").animate({scrollTop: $('#FU').offset().top - 90}, 500);


});