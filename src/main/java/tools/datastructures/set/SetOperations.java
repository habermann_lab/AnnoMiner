package tools.datastructures.set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import annominer.io.file.TSVReader;
import annominer.io.file.TSVWriter;
/**
 * operation on sets and maps
 */
public class SetOperations {
	
	
	
	/** get intersection of two sets
	 * @param a
	 * @param b
	 * @return Set<String>
	 */
	public static Set<String> getIntersection( Set<String> a, Set<String> b ) {
		
		Set<String> c = new HashSet<String>();
		
		for ( String entry : a) {
			
			if ( ( b.contains(entry) ) ) {
				c.add(entry);
			}
		}
		
		return c;
		
	}
	
	
	
	/** get intersection of two sets
	 * @param a
	 * @param b
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getIntersection( Map<String,String[]> a, Set<String> b ) {
		
		Map<String,String[]> c = new HashMap<String,String[]>();
		
		for ( String entry : a.keySet()) {
			
			if (  b.contains(entry) ) {
				c.put(entry,a.get(entry));
			}
		}
		
		return c;
		
	}
	
	
	
	/** get union of two sets
	 * @param a
	 * @param b
	 * @return Set<String>
	 */
	public static Set<String> getUnion( Set<String> a, Set<String> b ) {
		
		Set<String> c = new HashSet<String>();
		
		for(String id : a) { c.add(id);}
		for(String id : b) { c.add(id);}
		
		return c;
		
	}
	
	
	
	
	
	/** get intersection of two maps
	 * @param a
	 * @param b
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getInnerJoin( Map<String,String> a, Map<String,String> b ) {
		
		Map<String,String[]> c = new HashMap<String,String[]>();
		
		Set<String> bkeys = b.keySet();
		
		for ( String entry : a.keySet()) {
			
			if (  bkeys.contains(entry) ) {
				c.put(entry, new String[] { a.get(entry),b.get(entry) } );
			}
		}
		
		return c;
		
	}
	
	
	
	/** get intersection of two maps
	 * @param a
	 * @param b
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getLineMapInnerJoin( Map<String,String[]> a, Map<String,String[]> b ) {
		
		Map<String,String[]> c = new HashMap<String,String[]>();
		
		Set<String> bkeys = b.keySet();
		
		for ( String entry : a.keySet()) {
			
			if (  bkeys.contains(entry) ) {
				
				String[] alist = a.get(entry);
				String[] blist = b.get(entry);
				
				List<String> both = new ArrayList<String>(alist.length + blist.length);
			    Collections.addAll(both, alist);
			    Collections.addAll(both, blist);
				
				c.put(entry, both.toArray(new String[both.size()]) );
			}
		}
		
		return c;
		
	}
	
	
	
	/** get difference among 2 sets of strings
	 * @param a
	 * @param b
	 * @return Set<String>
	 */
	public static Set<String> getDifference( Set<String> a, Set<String> b ) {
		
		Set<String> c = new HashSet<String>();
		
		for ( String entry : a) {
			
			if ( !( b.contains(entry) ) ) {
				c.add(entry);
			}
		}
		
		return c;
		
	}
	
	
	
	/** read set from file
	 * @param filepath
	 * @param column
	 * @param key2lowercase
	 * @return Set<String>
	 */
	public static Set<String> readSetFromFile ( String filepath, int column, boolean key2lowercase ) {
		
		int index = column - 1;
		
		Set<String> list_pids = new HashSet<String>();
		
		TSVReader input = new TSVReader(filepath);
		
		for ( String[] line: input) {
			if( key2lowercase ) {
				list_pids.add(line[index].toLowerCase().trim());
			} else {
				list_pids.add(line[index].trim());
			}
		}
		
		input.close();
	
		return list_pids;
		
	}
	
	
	/** read set from file
	 * @param filepath
	 * @param column
	 * @return Set<String>
	 */
	public static Set<String> readSetFromFile ( String filepath, int column ) {
		
		return readSetFromFile( filepath, column, false);
	}
	
	
	/** read set from file
	 * @param filepath
	 * @param key2lowercase
	 * @return Set<String>
	 */
	public static Set<String> readSetFromFile ( String filepath, boolean key2lowercase ) {
		
		return readSetFromFile( filepath, 1, key2lowercase);
	}
	
	
	/** get set from file
	 * @param filepath
	 * @return Set<String>
	 */
	public static Set<String> readSetFromFile ( String filepath ) {
	
		return readSetFromFile( filepath, 1, false);
	}


	
	
	/** get line from map file
	 * @param filepath
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> readLineMapFromFile ( String filepath ) {
		
		Map<String,String[]> list_pids = new HashMap<String,String[]>();
		
		TSVReader input = new TSVReader(filepath);
		
		for ( String[] line: input) {
			list_pids.put(line[0], Arrays.copyOfRange(line, 1, line.length));
		}
		
		input.close();
		
		return list_pids;
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @param key
	 * @param value
	 * @param key2lowercase
	 * @return Map<String, String>
	 */
	public static Map<String,String> getMapFromFile ( String filepath, int key, int value, boolean key2lowercase ) {
		
		Map<String,String> map = new HashMap<String,String>();
		
		TSVReader ifh = new TSVReader(filepath);
		
		for ( String[] line : ifh ) {
			if( key2lowercase ) {
				map.put(line[key-1].toLowerCase(), line[value-1] );
			} else {
				map.put(line[key-1], line[value-1] );
			}
		}
		
		ifh.close();
		
		return map;
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @param key
	 * @param value
	 * @return Map<String, String>
	 */
	public static Map<String,String> getMapFromFile ( String filepath, int key, int value ) {
		
		return getMapFromFile ( filepath, key, value, false);
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @param key2lowercase
	 * @return Map<String, String>
	 */
	public static Map<String,String> getMapFromFile ( String filepath, boolean key2lowercase ) {
		
		return getMapFromFile ( filepath, 1, 2, key2lowercase );
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @return Map<String, String>
	 */
	public static Map<String,String> getMapFromFile ( String filepath ) {
		
		return getMapFromFile ( filepath, 1, 2, false);
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @param idcolumn
	 * @param key2lowercase
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getLineMapFromFile ( String filepath, int idcolumn, boolean key2lowercase ) {
		
		
		Map<String,String[]> list_pids = new HashMap<String,String[]>();
		
		TSVReader input = new TSVReader(filepath);
		
		idcolumn -= 1;
		
		for ( String[] line: input) {
			
			String[] outstr = new String[line.length-1];
			
			int j=0;
			
			for (int i=0; i<line.length; i++) {
				if ( i != idcolumn) {
					outstr[j]=line[i];
					j++;
				}
			}
			
			if( key2lowercase ) {
				list_pids.put(line[idcolumn].toLowerCase(), outstr );
			} else {
				list_pids.put(line[idcolumn], outstr );
			}
			
		}
		
		input.close();
	
		return list_pids;
		
	}
	
	
	
	/** get map from a file
	 * @param filepath
	 * @param idcolumn
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getLineMapFromFile ( String filepath, int idcolumn ) {
		
		return getLineMapFromFile ( filepath, idcolumn, false );
		
	}
	
	
	/** get map from a file
	 * @param filepath
	 * @param key2lowercase
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getLineMapFromFile ( String filepath, boolean key2lowercase ) {
		
		return getLineMapFromFile ( filepath, 1, key2lowercase );
		
	}
	
	
	/** get map from a file
	 * @param filepath
	 * @return Map<String, String[]>
	 */
	public static Map<String,String[]> getLineMapFromFile ( String filepath ) {
		
		return getLineMapFromFile ( filepath, 1, false );
		
	}
	
	
	
	/** write set To File
	 * @param set
	 * @param filepath
	 */
	public static void writeSetToFile ( Set<String> set, String filepath ) {
		
		
		TSVWriter ofh = new TSVWriter(filepath);
		
		for ( String id: set) {
			
			ofh.writeln(id);
			
		}
		
		ofh.close();
		
	}

	
	
	/** write Map To File
	 * @param map
	 * @param filepath
	 */
	public static void writeMapToFile ( Map<String,String[]> map, String filepath ) {
		
		
		TSVWriter ofh = new TSVWriter(filepath);
		
		for ( String id: map.keySet()) {
			
			ofh.writeln(id, map.get(id));
			
		}
		
		ofh.close();
		
	}
	
	
	
}


