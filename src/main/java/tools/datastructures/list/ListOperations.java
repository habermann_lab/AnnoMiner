package tools.datastructures.list;

import java.util.ArrayList;
import java.util.List;

import annominer.io.file.TSVReader;
/**
 * list reader
 */
public class ListOperations {
	
	
	
	/** read List From File
	 * @param filepath
	 * @param column
	 * @param key2lowercase
	 * @param separator
	 * @return List<String>
	 */
	public static List<String> readListFromFile ( String filepath, int column, boolean key2lowercase ,String separator) {
		
		int index = column - 1;
		
		List<String> list_pids = new ArrayList<String>();
		
		TSVReader input = new TSVReader(filepath,separator);
		
		for ( String[] line: input) {
			
			if( key2lowercase ) {
				list_pids.add(line[index].toLowerCase().trim());
			} else {
				list_pids.add(line[index].trim());
			}
		}
		
		input.close();
	
		return list_pids;
		
	}
	
	/** read List From File
	 * @param filepath
	 * @param column
	 * @param separator
	 * @return List<String>
	 */
	public static List<String> readListFromFile ( String filepath, int column ,String separator) {
		
		return readListFromFile( filepath, column, false, separator);
	}
	
	
	/** read List From File
	 * @param filepath
	 * @param column
	 * @return List<String>
	 */
	public static List<String> readListFromFile ( String filepath, int column ) {
		
		return readListFromFile( filepath, column, false, "\t");
	}
	
	
	/** read List From File
	 * @param filepath
	 * @param key2lowercase
	 * @return List<String>
	 */
	public static List<String> readListFromFile ( String filepath, boolean key2lowercase ) {
		
		return readListFromFile( filepath, 1, key2lowercase,"\t");
	}
	
	
	/** read List From File
	 * @param filepath
	 * @return List<String>
	 */
	public static List<String> readListFromFile ( String filepath ) {
	
		return readListFromFile( filepath, 1, false,"\t");
	}
	
	
	
}
