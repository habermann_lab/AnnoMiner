package annominer;

import java.util.ArrayList;
import java.util.Set;
/**
 * enrichment result object
 */
public class EnrichmentResult implements Comparable<EnrichmentResult> {
	
	public ExperimentAnnotation experiment;
	public double score;
	public double pval;
	public double fdr;
	public double cs;
	
	public int listhits;
	public int listsize;
	public int genomehits;
	public int genomesize;
	
	public Set<String> positives;
	public ArrayList<String> used_list;
	
	/**
	 * constructor of enrichment result
	 * @param experiment TF
	 * @param score
	 * @param pval
	 */
	public EnrichmentResult( ExperimentAnnotation experiment, double score, double pval ) {
		
		this.experiment = experiment;
		this.score = score;
		this.pval = pval;
		
	}
	
	
	/** set false discovery rate
	 * @param fdr
	 */
	//setter for the fdr
	public void setFdr(double fdr) {
		this.fdr = fdr;
	  }
	
	
	/** set combined score
	 * @param cs
	 */
	// setter for the combined score
	public void setCS(double cs) {
		this.cs = cs;
	}

	
	/** compare p value results
	 * @param generesult
	 * @return int
	 */
	@Override
	public int compareTo(EnrichmentResult generesult) {
		
		if (this.pval < generesult.pval) {
			return -1;
		} else if (this.pval > generesult.pval) {
			return 1;
		} else {
			return 0;
		}
		
	}
	
	
	
	/** convert combined score to string
	 * @return String
	 */
	@Override
	public String toString() {
		return Double.toString(this.cs);
	}
	
	
	
	/** get hashcode
	 * @return int
	 */
	@Override
	public int hashCode() {
		return this.experiment.pid.toLowerCase().hashCode();
	}
	
	
	
	/** check if 2 genes are equal
	 * @param obj
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj){
		if (obj instanceof EnrichmentResult)
			return	((EnrichmentResult)obj).experiment.pid.toLowerCase().equals(this.experiment.pid.toLowerCase());
		else
			return false;
	}
	
	
	/** get gene list as string
	 * @return String
	 */
	public String getGeneListAsString() {
		
		int len;
		
		if( this.positives == null ) {
			return "";
		} else {
			len = this.positives.size();
			if( len == 0) {
				return "";
			}
		}
		

		StringBuffer ostring = new StringBuffer();
		
		for (String pos : this.positives) {
				ostring.append(pos);
				ostring.append(";");
		}
		
		return ostring.substring(0, ostring.length() - 1).toString();
		
	}

	
	/** return results as JSON array
	 * @return String
	 */
	public String toJSONArray() {
		
		return ("[\"" + this.experiment.target + "\",\"" + this.experiment.probe + "\",\""
				+ this.experiment.getTreatment() + "\",\"" + this.experiment.getReplicate() + "\",\"" + this.listhits
				+ "\",\"" + this.listsize + "\",\"" + this.genomehits + "\",\"" + this.genomesize + "\",\""
				+ getGeneListAsString() + "\",\"" + this.score + "\",\"" + this.pval + "\"],");
		
		
	}
	
	
	
}
