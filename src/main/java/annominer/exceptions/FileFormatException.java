package annominer.exceptions;

public class FileFormatException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;	
	
	public String format;
	public int line;
	
	public FileFormatException() {
		
	}
	
	public FileFormatException(String format, int line) {
		super("File doesn't have the expected format of type "+format+". Error in line "+line+".");
		this.format = format;
		this.line = line;
	}
	
	
	
}
