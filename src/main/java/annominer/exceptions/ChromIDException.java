package annominer.exceptions;

public class ChromIDException extends Exception {
	
	private static final long serialVersionUID = 1L;	
	
	public ChromIDException() {
		
	}

	public ChromIDException(String organism, String pid) {
		super("Chromosome "+pid+" not found in organism/assembly "+organism);
		
	}

}
