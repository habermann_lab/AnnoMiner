package annominer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;


/**
 * definition of a generic chromosome
 */
public class GenericChromosome<T> implements Iterable<T> {
	
	public final String pid;
	public long length;
	protected List<T> regions;
	
	
	/* Constructors */
	/**
	 * constructor
	 * @param pid
	 * @param length
	 */
	public GenericChromosome(String pid, long length){
		this.pid = pid;
		this.length = length;
		this.regions = new ArrayList<T>(10000);
	}
	
	/**
	 * constructor
	 * @param chrom
	 */
	@SuppressWarnings("unchecked")
	public GenericChromosome(Chromosome chrom){
		this.pid = chrom.pid;
		this.length = chrom.length;
		this.regions = chrom.regions.stream() //.filter(obj -> obj instanceof T)
			    .map(obj -> (T) obj).collect( Collectors.toList() );
	}
	/**
	 * constructor
	 * @param pid
	 * @param length
	 * @param regions
	 */
	public GenericChromosome(String pid, long length, List<T> regions){
		this.pid = pid;
		this.length = length;
		this.regions = regions;
	}
	
	
	/** iterator
	 * @return Iterator<T>
	 */
	/* Interfaces */
	
	
	public Iterator<T> iterator() {
        Iterator<T> iter = regions.iterator();
        return iter;
    }
	
	
	
	/** list iterator
	 * @return ListIterator<T>
	 */
	public ListIterator<T> listIterator() {
        ListIterator<T> iter = regions.listIterator();
        return iter;
    }
	
	
	/** list iterator through i elements
	 * @param i
	 * @return ListIterator<T>
	 */
	public ListIterator<T> listIterator(int i) {
        ListIterator<T> iter = regions.listIterator(i);
        return iter;
    }
	
	
	
	/** add region
	 * @param region
	 */
	/* Methods */
	
	
	public void add(T region) {
		this.regions.add(region);
	}
	
	
	
	/** return number of regions
	 * @return int
	 */
	public int getRegionNumber() {
		return regions.size();
	}
	
	
	
	/** return details in JSON
	 * @return String
	 */
	public String toTSV(){
		return pid + '\t' + String.valueOf(length);
	}
	
	
	/*
	public String toJSON() {
		StringBuilder strg = new StringBuilder();
		if (!regions.isEmpty()) {
			for (T region : regions) {
				strg.append(region.toJSON() + ",\n");
			}
			strg.setLength(strg.length() - 2);
		}
		return "{\"pid\":\"" + pid +"\",\"length\":"+String.valueOf(length)+",\"regions\":["+strg+"]}";
		
	}
	*/
	
	
	
}
