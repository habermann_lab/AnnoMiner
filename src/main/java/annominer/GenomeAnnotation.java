package annominer;


import annominer.io.database.mongodb3.ChromosomeOfTranscriptsMongoDAO;
import annominer.io.database.mongodb3.GenomeMongoDAO;


/**
 * genome annotation
 */
public class GenomeAnnotation extends DataTrack {
	
	protected final boolean canonical;
	
	
	/* Constructors */
	/**
	 * Constructor
	 * @param organism
	 * @param source
	 * @param gtype
	 * @param canonical
	 */
	public GenomeAnnotation(String organism, Source source, String gtype, boolean canonical) {
		
		super(gtype,organism,source);

		this.canonical = canonical;
		this.chromdao = ChromosomeOfTranscriptsMongoDAO.getInstance();/*new MongoClient addressed to the local host*/
		this.trackdao = GenomeMongoDAO.getInstance();/*new MongoClient addressed to the local host and return chromdao ChromosomeMongoDAO*/
		this.trackdao.update(organism, trackid, source);/* update the database */
		
	}
	
	/**
	 * Constructor
	 * @param organism
	 * @param source
	 * @param gtype
	 */
	public GenomeAnnotation(String organism, Source source, String gtype) {
		this(organism,source,gtype,false);
	}
	
	
	/* Interfaces */
	
	
	
	/* Methods */
	
	
	
}


