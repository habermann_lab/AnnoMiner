package annominer.ui.web;

import java.io.Serializable;
/**
 * user provided track
 */
public class UserTrack implements Serializable {
	
	public String trackid;
	public String assembly;
	public String description;
	public String tracktype;
	public String date;
	
	private static final long serialVersionUID = 1L;
	/**
	 * constructor
	 * @param id
	 * @param assembly
	 * @param type
	 * @param date
	 * @param desc
	 */
	public UserTrack ( String id, String assembly, String type, String date, String desc ) {
		
		this.trackid = id;
		this.assembly = assembly;
		this.description = desc;
		this.tracktype = type;
		this.date = date;
	}
	/**
	 * constructor
	 * @param id
	 * @param assembly
	 * @param type
	 * @param date
	 */
	public UserTrack ( String id, String assembly, String type, String date ) {
		
		this.trackid = id;
		this.assembly = assembly;
		this.tracktype = type;
		this.date = date;
		
	}
	
	
	/** results to JSON
	 * @return String
	 */
	public String toJSON() {
		
		return "{\"id\":\""+trackid+"\",\"name\":\""+trackid+"\",\"type\":\""+tracktype+"\",\"date\":\""+date+"\"}";
		
	}
	
	
}

