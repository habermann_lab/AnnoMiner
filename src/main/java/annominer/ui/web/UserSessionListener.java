package annominer.ui.web;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import annominer.io.database.mongodb3.MongoDBConnector;
/**
 * check user session
 */
@WebListener
public class UserSessionListener implements HttpSessionListener {
	
	protected MongoDBConnector mongoconnector;
	
	public UserSessionListener() {
		
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	
	/** create session
	 * @param event
	 */
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		//event.getSession().setMaxInactiveInterval(600);
		//event.getSession().setMaxInactiveInterval(172800);
		event.getSession().setMaxInactiveInterval(1209600);
		
	}
	
	
	/** delete session
	 * @param event
	 */
	@Override
    public void sessionDestroyed(HttpSessionEvent event) {
        
		String userid = event.getSession().getId();
		
		mongoconnector.deleteDatabase(userid);
		
    }

	
	
	
	
	
}
