package annominer.ui.web.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.Chromosome;
import annominer.CustomDataset;
import annominer.GeneCoverage;
import annominer.GenomeAnnotation;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.ui.web.UserTrack;
//import jdk.internal.jline.internal.Log;


@WebServlet(name="PeakCorrelation", urlPatterns={"/PeakCorrelation"})
public class PeakCorrelation extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(PeakCorrelation.class.getCanonicalName());
	private HttpSession session;
	private String assembly, gtype, trackid1, trackid2, trackid21, trackid22, trackid23, trackid3;
	private String[] tsel1, tsel2, tsel21, tsel22, tsel23;
	private ArrayList<CustomDataset> customList;
	private int ovval, tss_us, tss_ds, flrange, margin, colSelect, tracks;
	private boolean ovtype, canonical;
	private Set<Transcript> pos_transcripts1, pos_transcripts2, pos_transcripts21, pos_transcripts22, pos_transcripts23,
			intersections;
	private GeneCoverage coverage1, coverage2, coverage21, coverage22, coverage23;

	private List<int[]> targetintervals1, targetintervals2, targetintervals21, targetintervals22, targetintervals23,
			genomeTi;
	private GenomeRegionTrack peaktrack1, peaktrack2, peaktrack21, peaktrack22, peaktrack23;
	private Map<String, List<Region>> transcript_target_regions1, transcript_target_regions2,
			transcript_target_regions21, transcript_target_regions22, transcript_target_regions23;
	private ConvertIDs testList;
	private File IDFile;
	private boolean customAnno;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	respond(request,response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		respond(request,response);
	}


    private void respond(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	////////////////////////////////
    	// Parse request-parameters : //
    	////////////////////////////////
    	try {
    		assembly = request.getParameter("assembly");/* assembly is the value referring to the reference genome ( for example for the drosophila is "dm3")*/
    		gtype = request.getParameter("gtype");/*gtype is the value referring to the variable "ganno". Ganno takes the value of an item of the "genomes" array that contains all the annotation resources for the chosen reference genome (es. in case of "dm3" we can choose between: refseq, ensembl)*/
    		trackid1 = request.getParameter("trackid1");/*trackid contains the value of the variable "ianno" that is the name of the uploaded file*/
			tsel1 = (request.getParameter("targets1[]")).split(",");/* value of the selected columns of the plot: */
			tracks = Integer.valueOf(request.getParameter("tracks")); // numer of tracks to integrate
			trackid2 = request.getParameter("trackid2");// second file
			tsel2 = (request.getParameter("targets2[]")).split(","); // second file barplot selection
			if (tracks == 3) {
				trackid21 = request.getParameter("trackid21");// 3 file
				tsel21 = (request.getParameter("targets21[]")).split(",");
			} else if (tracks == 4) {
				trackid21 = request.getParameter("trackid21");
				tsel21 = (request.getParameter("targets21[]")).split(",");
				trackid22 = request.getParameter("trackid22");// 4 file
				tsel22 = (request.getParameter("targets22[]")).split(",");
			} else if (tracks == 5) {
				trackid21 = request.getParameter("trackid21");
				tsel21 = (request.getParameter("targets21[]")).split(",");
				trackid22 = request.getParameter("trackid22");
				tsel22 = (request.getParameter("targets22[]")).split(",");
				trackid23 = request.getParameter("trackid23");// 5 file
				tsel23 = (request.getParameter("targets23[]")).split(",");
			}
    		trackid3 = request.getParameter("trackid3"); //custom annotation file

			ovval = Integer.valueOf((request.getParameter("ovval")));/*ovval is the value selected for the overlap required between the TF and the choosen region*/
			tss_us = Integer.valueOf(request.getParameter("tss_us"));/*the value that define the upstream limit of the TSS*/
			tss_ds = Integer.valueOf(request.getParameter("tss_ds"));/*the value that define the downstream limit of the TSS*/
			margin = Integer.valueOf(request.getParameter("flrange"));/*flrange is the choosen value for the flanking region now called margin (because before was multiplied for 3)*/
			flrange =  (int) Math.round(margin/3); /*flanking region(margin divided)*/
		
		if(trackid1==null) {
    		PrintWriter out = response.getWriter();
		 	out.println( "{\"message1\":\"Upload at least an input file before to start!. \"}");
		 	return;			
		}
    	}catch(Exception e){
    		
    		PrintWriter out = response.getWriter();
		 	out.println( "{\"message1\":\"Wrong input values. Check parameters types and sizes. \"}");
		 	return;
    		
    	}
		/*canonical is the value referring to the transcript-based search criteria: 1 = all transcript, 0 = represent genes only by the longest transcript*/
		if ( request.getParameter("canonical").equals("1") ) {
			canonical = true;
		} else {
			canonical = false;
		}
		
		/*ovtype is the value referring to the overlap criteria: 0 = bp overlap, 1 = %overlap*/
		if ( request.getParameter("ovtype").equals("0") ) {
			ovtype = true;
		} else {
			ovtype = false;
		}
		
		/* construct a new source object with this source, host, user*/
		Source source = new Source("sql","genome-mysql.cse.ucsc.edu","genome");

		////////////////////
		// Manage session //
		////////////////////

		session = request.getSession();
	
		customAnno = false;

		if(!trackid3.equals("null")) {
			customAnno = true;
			@SuppressWarnings("unchecked")
			HashMap<String, ArrayList<CustomDataset>> customDatasets = (HashMap<String, ArrayList<CustomDataset>>) session
					.getAttribute("customDatasets");
			
			response.setContentType("application/json;charset=utf-8");

			PrintWriter out = response.getWriter();
			
			/*
			 * control to check if we have uploaded yet any id lists file and, if we had,
			 * store the contents in an array named "list_inputsid"
			 */
			if (customDatasets.isEmpty() == true) {
				out.println("{\"message\":\"Please upload a custom annotation file first!\"}");
				return;
			} else {

				customList = customDatasets.get(trackid3);

				colSelect = customList.listIterator(0).next().getLabels().size();

			}
	
		}
		
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();

		@SuppressWarnings("unchecked")
		Map<String,UserTrack> usertracks = (HashMap<String,UserTrack>)session.getAttribute("usertracks");
		/*collection of the pair track ids (file name): and the relative UserTrack*/
		/*gets the collection from ManageUpload*/
		UserTrack datatrack1, datatrack2, datatrack21 = null, datatrack22 = null, datatrack23 = null;

		
		/*control to check if the user has uploaded at least one file*/
		if( usertracks.isEmpty() == true ) {
			out.println( "{\"message\":\"Please upload a bed-file first.\"}");
			return;
		} else {
			datatrack1 = usertracks.get(trackid1);/*datatrack contains the file name*/
			/* check if the selected file is in datatrack */
			if (datatrack1 == null) {

				out.println("{\"message\":\"An error occurred. Track not found: " + trackid1 + "\"}");

				return;
			}
			datatrack2 = usertracks.get(trackid2);
			if (datatrack2 == null) {

				out.println("{\"message\":\"An error occurred. Track not found: " + trackid2 + "\"}");

				return;
			}
			if (tracks == 3) {
				datatrack21 = usertracks.get(trackid21);
				if (datatrack21 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid21 + "\"}");

					return;
				}
			} else if (tracks == 4) {
				datatrack21 = usertracks.get(trackid21);
				if (datatrack21 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid21 + "\"}");

					return;
				}
				datatrack22 = usertracks.get(trackid22);
				if (datatrack22 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid22 + "\"}");

					return;
				}
			} else if (tracks == 5) {
				datatrack21 = usertracks.get(trackid21);
				if (datatrack21 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid21 + "\"}");

					return;
				}
				datatrack22 = usertracks.get(trackid22);
				if (datatrack22 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid22 + "\"}");

					return;
				}
				datatrack23 = usertracks.get(trackid23);
				if (datatrack23 == null) {

					out.println("{\"message\":\"An error occurred. Track not found: " + trackid23 + "\"}");

					return;
				}
			}
		}


    	////////////////////////////////
		// Calculate and send results //
		////////////////////////////////

		GenomeAnnotation genome = new GenomeAnnotation(assembly, source,
				gtype);/*
						 * create instance of DataTrack, connection with mongoDB and will get the
						 * collections looking in the database "assembly"(es dm3) the respective one
						 * called "trackid"( es ensembl) in dbindex collection (in case the database
						 * doesn't exist yet)
						 */

		pos_transcripts1 = new HashSet<Transcript>();
		pos_transcripts2 = new HashSet<Transcript>();
		peaktrack1 = new GenomeRegionTrack(datatrack1.trackid, session.getId(),
				source);/*
						 * (String trackid, String organism, Source source) connection with mongoDB and
						 * create a db called sessionId and containing two collections one with dbindex
						 * and one with the regions called with the filename
						 */
		peaktrack2 = new GenomeRegionTrack(datatrack2.trackid, session.getId(), source);
		coverage1 = new GeneCoverage(flrange, tss_us,
				tss_ds);/* constructor to instantiate and assign parameters to the object */
		coverage2 = new GeneCoverage(flrange, tss_us, tss_ds);

		transcript_target_regions1 = new HashMap<String, List<Region>>();/*
																			 * constructor of an array containing the
																			 * transcript associated with the regions
																			 */
		transcript_target_regions2 = new HashMap<String, List<Region>>();

		targetintervals1 = combineTargetIntervalSelection(tsel1);/*
																	 * array representing the left/right position of the
																	 * region selected by the user in the barplot
																	 */
		targetintervals2 = combineTargetIntervalSelection(tsel2);

		if (tracks == 3) {
			pos_transcripts21 = new HashSet<Transcript>();
			peaktrack21 = new GenomeRegionTrack(datatrack21.trackid, session.getId(), source);
			coverage21 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions21 = new HashMap<String, List<Region>>();
			targetintervals21 = combineTargetIntervalSelection(tsel21);

		} else if (tracks == 4) {
			pos_transcripts21 = new HashSet<Transcript>();
			pos_transcripts22 = new HashSet<Transcript>();
			peaktrack21 = new GenomeRegionTrack(datatrack21.trackid, session.getId(), source);
			peaktrack22 = new GenomeRegionTrack(datatrack22.trackid, session.getId(), source);
			coverage21 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions21 = new HashMap<String, List<Region>>();
			targetintervals21 = combineTargetIntervalSelection(tsel21);
			coverage22 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions22 = new HashMap<String, List<Region>>();
			targetintervals22 = combineTargetIntervalSelection(tsel22);

		} else if (tracks == 5) {
			pos_transcripts21 = new HashSet<Transcript>();
			pos_transcripts22 = new HashSet<Transcript>();
			pos_transcripts23 = new HashSet<Transcript>();
			peaktrack21 = new GenomeRegionTrack(datatrack21.trackid, session.getId(), source);
			peaktrack22 = new GenomeRegionTrack(datatrack22.trackid, session.getId(), source);
			peaktrack23 = new GenomeRegionTrack(datatrack23.trackid, session.getId(), source);
			coverage21 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions21 = new HashMap<String, List<Region>>();
			targetintervals21 = combineTargetIntervalSelection(tsel21);
			coverage22 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions22 = new HashMap<String, List<Region>>();
			targetintervals22 = combineTargetIntervalSelection(tsel22);
			coverage23 = new GeneCoverage(flrange, tss_us, tss_ds);
			transcript_target_regions23 = new HashMap<String, List<Region>>();
			targetintervals23 = combineTargetIntervalSelection(tsel23);
		}
		

		
		try {

			/*requested target regions from the barplot*/
			if (tracks == 2) {
			if (request.getParameter("targets1[]").equals("0,0,0,0,0,0,0,0,0,0")
					|| request.getParameter("targets2[]").equals("0,0,0,0,0,0,0,0,0,0")) {

	    		parseIntersections( peaktrack1, genome, coverage1, false, true, transcript_target_regions1, pos_transcripts1, targetintervals1);/*checkgene = false and calculate coverage =true*/ 
	    		parseIntersections( peaktrack2, genome, coverage2, false, true, transcript_target_regions2, pos_transcripts2, targetintervals2);

				out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + ",\"flankcov2\":"
							+ coverage2.printCoverageResultsJSON() + ",\"message1\":\"Choose the target-regions\"}");
	    	} else {

				geneIdConvertion(customAnno);

	    		parseIntersections( peaktrack1, genome, coverage1, true, true, transcript_target_regions1, pos_transcripts1, targetintervals1 );/*checkgene = false and calculate coverage =true*/ 
	    		parseIntersections( peaktrack2, genome, coverage2, true, true, transcript_target_regions2, pos_transcripts2, targetintervals2);

	    		out.println( "{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + "," + geneResults2JSON() +",\"flankcov2\":" + coverage2.printCoverageResultsJSON() +"}");

				}
			}
			if (tracks == 3) {
				if (request.getParameter("targets1[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets2[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets21[]").equals("0,0,0,0,0,0,0,0,0,0")) {

					parseIntersections(peaktrack1, genome, coverage1, false, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, false, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, false, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + ",\"flankcov2\":"
							+ coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() + ",\"message1\":\"Choose the target-regions\"}");

				} else {
					geneIdConvertion(customAnno);

					parseIntersections(peaktrack1, genome, coverage1, true, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, true, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, true, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + "," + geneResults2JSON()
							+ ",\"flankcov2\":" + coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() + "}");
				}
			}
			if (tracks == 4) {
				if (request.getParameter("targets1[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets2[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets21[]").equals("0,0,0,0,0,0,0,0,0,0")|| request.getParameter("targets22[]").equals("0,0,0,0,0,0,0,0,0,0")) {

					parseIntersections(peaktrack1, genome, coverage1, false, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, false, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, false, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					parseIntersections(peaktrack22, genome, coverage22, false, true, transcript_target_regions22,
							pos_transcripts22, targetintervals22);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + ",\"flankcov2\":"
							+ coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() +  ",\"flankcov22\":"
							+ coverage22.printCoverageResultsJSON() + ",\"message1\":\"Choose the target-regions\"}");

				} else {
					geneIdConvertion(customAnno);

					parseIntersections(peaktrack1, genome, coverage1, true, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, true, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, true, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					parseIntersections(peaktrack22, genome, coverage22, true, true, transcript_target_regions22,
							pos_transcripts22, targetintervals22);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + "," + geneResults2JSON()
							+ ",\"flankcov2\":" + coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() + ",\"flankcov22\":"
									+ coverage22.printCoverageResultsJSON()+ "}");
				}
			}
			if (tracks == 5) {
				if (request.getParameter("targets1[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets2[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets21[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets22[]").equals("0,0,0,0,0,0,0,0,0,0")
						|| request.getParameter("targets23[]").equals("0,0,0,0,0,0,0,0,0,0")) {

					parseIntersections(peaktrack1, genome, coverage1, false, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, false, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, false, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					parseIntersections(peaktrack22, genome, coverage22, false, true, transcript_target_regions22,
							pos_transcripts22, targetintervals22);
					parseIntersections(peaktrack23, genome, coverage23, false, true, transcript_target_regions23,
							pos_transcripts23, targetintervals23);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + ",\"flankcov2\":"
							+ coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() + ",\"flankcov22\":"
							+ coverage22.printCoverageResultsJSON() + ",\"flankcov23\":"
							+ coverage23.printCoverageResultsJSON() + ",\"message1\":\"Choose the target-regions\"}");

				} else {
					geneIdConvertion(customAnno);

					parseIntersections(peaktrack1, genome, coverage1, true, true, transcript_target_regions1,
							pos_transcripts1, targetintervals1);/* checkgene = false and calculate coverage =true */
					parseIntersections(peaktrack2, genome, coverage2, true, true, transcript_target_regions2,
							pos_transcripts2, targetintervals2);
					parseIntersections(peaktrack21, genome, coverage21, true, true, transcript_target_regions21,
							pos_transcripts21, targetintervals21);
					parseIntersections(peaktrack22, genome, coverage22, true, true, transcript_target_regions22,
							pos_transcripts22, targetintervals22);
					parseIntersections(peaktrack23, genome, coverage23, true, true, transcript_target_regions23,
							pos_transcripts23, targetintervals23);
					out.println("{\"flankcov1\":" + coverage1.printCoverageResultsJSON() + "," + geneResults2JSON()
							+ ",\"flankcov2\":" + coverage2.printCoverageResultsJSON() + ",\"flankcov21\":"
							+ coverage21.printCoverageResultsJSON() + ",\"flankcov22\":"
							+ coverage22.printCoverageResultsJSON() + ",\"flankcov23\":"
							+ coverage23.printCoverageResultsJSON() + "}");
				}
			}

		} catch (ChromIDException e) {
			out.println("{\"message1\":\""+e.getMessage()+"! ... Make sure your organism-selection is correct.\"}");
			return;
		}


    }


    public Map<String,List<Region>> getTargetIntervals(GenomeAnnotation genome) {
    	//define genomeTi	
    	
    	Map<String,List<Region>> tregions = new HashMap<String,List<Region>>();

//    	LOGGER.log(Level.INFO, "Making tregions !!!" );
		for ( Chromosome chrom: genome ) {
			for(Region region: chrom) {
				Transcript trans = (Transcript)region;

				tregions.put(trans.pid, getTargetRegions(trans, genomeTi) );
			}

		}
//		LOGGER.log(Level.INFO, "Done !!!" );


    	return tregions;

    }

	
    /**This method check if the coverage between the transcript region (in the DB) and the region is within the interval chosen by the user.
     * If it is it stores the transcript in a collection called pos_transcripts.
     * 
     * @param transcript
     * @param region
     * @param chromlength
     * @param forward 
     */
	public void genecheck(Transcript transcript, Region region, long chromlength, boolean forward, Map<String,List<Region>> transcript_target_regions, Set<Transcript> pos_transcript, List<int[]> targetintervals) {
		/*get the list of regions belonging to a transcript*/
		List<Region> tregions = transcript_target_regions.get(transcript.pid);

		/*if transcript_target_regions has no regions for the transcript yet*/
		if( tregions == null) {
			tregions = getTargetRegions(transcript, targetintervals);/*will build a list of regions starting from the Transcript*/
			transcript_target_regions.put(transcript.pid, tregions);/*assign the collection of tregions to the transcript pid (chromosome name)*/
		}


		for( Region tregion : tregions ) {/*for each tregion in the list tregions (so belonging to a transcript in the DB)*/

			Region coverage = tregion.intersectionWith(region);/*check intersection between region and tregion and return a region that stores only the overlapping region*/

			if (coverage!=null) {

				boolean match = false;

				if (ovtype) {/*check if the coverage is greater or equal the overlapping value chosen by the user*/
					match = coverage.length() >= ovval;
				} else {
					match = coverage.length() >= ovval * 0.01 * tregion.length();
				}

				if (match) {/*if the overlap is enough*/
				
					pos_transcript.add(transcript);/*transcript in the DB is added to the positive transcript*/
				}
			}

		}

	} //END genecheck

	/**This method parse the intersections and compute the coverages between the track and the regions in the genome
	 * 
	 * @param track
	 * @param genome
	 * @param checkgene 
	 * @param calculatecoverage 
	 * @throws ChromIDException
	 */
	public void parseIntersections(GenomeRegionTrack track, GenomeAnnotation genome, GeneCoverage coverage, boolean checkgene, boolean calculatecoverage, Map<String,List<Region>> transcript_target_regions, Set<Transcript> pos_transcript, List<int[]> targetintervals) throws ChromIDException {
		
		/* Gene-flanking region settings */
		/*int margin = 3 * flrange;*/

		Chromosome chromosome = null;
		Transcript gene=null, fwgene=null;
		long chromsize=0;

		ListIterator<Region> iterator=null,fwiterator=null;
		boolean it_next = true;
		boolean it_ended = false;		
		
		
		for ( Chromosome chromtrack: track) {/*for each document in the collection (uploaded file)*/
			
			chromosome = genome.getChromByID(chromtrack.pid);/*assign to the variable the chromosome in the database (ensembl for instance) that has the same pid (chromosome name) of the one in the uploaded document /

			
			/*check to see if within the chromosome we have gene regions or not*/
			if (chromosome.getRegionNumber() == 0) {/**/
//				LOGGER.log(Level.INFO, "Genome-annotation has no genes for chromosome: " + chromtrack.pid );
				continue;
			}

			chromsize = chromosome.length;/*length of the chromosome in the collections (es.ensembl)*/
			
			coverage.nextChrom(chromsize);/*instantiate an HashSet called geneids*/

			iterator = chromosome.listIterator();/*iterator through the regions of the chromosome*/
			it_next = true;
			it_ended = false;
			

			for ( Region region: chromtrack) {/*for each region in the uploaded document*/

				if (it_ended) {

					//pass;

				} else {
					while (true) {
						if (it_next) {

							if ( iterator.hasNext() ) {
								gene = (Transcript)iterator.next();/*next gene in the chromosome (cast into transcript)*/
								it_next = false;

							} else {

								it_ended = true;
								break;

							}

						}
						
						if (gene.isLeftOf(region, margin)) {
						/*control if the left coordinate of the region (uploaded) is greater than the gene's region right coordinate plus the margin*/
							it_next = true;

						} else if (gene.isRightOf(region, margin)) {	
						/*control if the right coordinate of the region (uploaded) is less than the gene's region left coordinate minus the margin*/

							it_next = false;
							break;

						} else {
							/*In case the user select the canonical option check if it's not the longest transcript */
							if(canonical) {
			 					while(!gene.canonical){
			 						if ( iterator.hasNext() ) { 
			 							gene = (Transcript)iterator.next();
			 						}else{
			 							it_ended = true;
			 							break;
			 							}
			 						}
			 					}

							if (checkgene) { genecheck(gene, region, chromsize, false, transcript_target_regions, pos_transcript, targetintervals); }/*if the coverage between the two regions is enough the transcript will be stored in the pos_transcript collection*/
							if (calculatecoverage) { coverage.addGene(gene,region); } /*calculate the coverage between the gene and all the defined nearby regions*/

							fwiterator= chromosome.listIterator(iterator.nextIndex());/*forward iterator*/
							/*Returns an iterator of the element (Chromosome) through its list (regions). 
							 The parameter is the index of the first element to be returned from the list iterator (by a call to next)*/
							
							while (true) {/*same control for the transcript position for the other chromosomes in the collection i suppose*/

								if( !(fwiterator.hasNext()) )
									break;

								fwgene = (Transcript)fwiterator.next();

								if (fwgene.isRightOf(region, margin)) {

									break;

								} else if (fwgene.isLeftOf(region, margin)) {

									//pass;

								} else {
									
									if(canonical) {
					 					while(!fwgene.canonical){
					 						if ( fwiterator.hasNext() ) { 
					 							fwgene = (Transcript)fwiterator.next();
					 						}else{
					 							it_ended = true;
					 							break;
					 							}
					 						}
					 					}

									if (checkgene) { genecheck(fwgene, region, chromsize, true, transcript_target_regions, pos_transcript, targetintervals); }
									if (calculatecoverage) { coverage.addGene(fwgene,region); }

								}

							}


							it_next = false;
							break;

						}

					}

				}

			}

		}

	}


	/**This method return the array representing the target intervals selected by the user (barplot)
	 * 
	 * @return targets a list of integer
	 */
	private List<int[]> combineTargetIntervalSelection(String[] tsel) {

		int leftborder = -1;
		int rightborder = -1;
		List<int[]> targets = new ArrayList<int[]>();
		targets.add( new int[]{leftborder,rightborder} );
		for( int i=0; i<tsel.length; i++ ) {

			if ( tsel[i].equals("1") ) {

				if ( leftborder != -1 ) {
						rightborder = i;

				} else {
					leftborder = i;
					rightborder = i;
				}
			}
		}

		if (leftborder != -1) {
			targets.add( new int[]{leftborder,rightborder} );
		}
//		LOGGER.log(Level.INFO, "Targets: " + leftborder+rightborder);
		return targets;

	}

	/**
	 * 
	 * @param transcript
	 * @return a list of regions (target regions)
	 */
	private List<Region> getTargetRegions( Transcript transcript , List<int[]> targetintervals) {

		List<Region> tregions = new ArrayList<Region>();

		for( int[] target : targetintervals) {

			long pos1 = -1;
			long pos2 = -1;

			switch( target[0] ) {
				case 0:
					pos1 = transcript.pos5pBy(3*flrange);
					break;
				case 1:
					pos1 = transcript.pos5pBy(2*flrange);
					break;
				case 2:
					pos1 = transcript.pos5pBy(flrange);
					break;
				case 3:
					if (transcript.strand) {
						pos1 = transcript.start() - tss_us;
					} else {
						pos1 = transcript.start() + tss_us;
					}
					break;
				case 4:
					pos1 = transcript.start();
					break;
				case 5:
					pos1 = transcript.cds.start();
					break;
				case 6:
					pos1 = transcript.cds.end();
					break;
				case 7:
					pos1 = transcript.end();
					break;
				case 8:
					pos1 = transcript.pos3pBy(flrange);
					break;
				case 9:
					pos1 = transcript.pos3pBy(2*flrange);
					break;
			}


			switch( target[1] ) {
				case 0:
					pos2 = transcript.pos5pBy(2*flrange);
					break;
				case 1:
					pos2 = transcript.pos5pBy(flrange);
					break;
				case 2:
					pos2 = transcript.start();
					break;
				case 3:
					pos2 = transcript.pos5pBy(-tss_ds);
					if( transcript.strand ) {
						if( pos2 > transcript.right ) {
							pos2 = transcript.right;
						}
					} else {
						if( pos2 < transcript.left ) {
							pos2 = transcript.left;
						}
					}
					break;
				case 4:
					if( target[0] == 4) {
						pos2 = transcript.cds.start();
					} else {
						long tpos = transcript.pos5pBy(-tss_ds);
						if( transcript.strand ) {
							if( tpos > transcript.cds.left ) {
								if( tpos > transcript.right ) {
									pos2 = transcript.right;
								} else {
									pos2 = tpos;
								}
							} else {
								pos2 = transcript.cds.left;
							}
						} else {
							if( tpos < transcript.cds.right ) {
								if( tpos < transcript.left ) {
									pos2 = transcript.left;
								} else {
									pos2 = tpos;
								}
							} else {
								pos2 = transcript.cds.right;
							}
						}
					}
					break;
				case 5:
					pos2 = transcript.cds.end();
					break;
				case 6:
					pos2 = transcript.end();
					break;
				case 7:
					pos2 = transcript.pos3pBy(flrange);
					break;
				case 8:
					pos2 = transcript.pos3pBy(2*flrange);
					break;
				case 9:
					pos2 = transcript.pos3pBy(3*flrange);
					break;
			}

			if( (pos1 != -1) && (pos2 != -1) && (pos1 != pos2) ) {
				tregions.add(new Region(pos1,pos2));
			}

		}

		return tregions;

	}

	/**
	 * This method return a string in JSON format with Primary ID and Symbol for
	 * each gene founded
	 * 
	 * @return string with primary id and symbol for each gene found returned in
	 *         JSON format
	 * @throws FileNotFoundException
	 */
	public String geneResults2JSON() throws FileNotFoundException {

		intersections = new HashSet<Transcript>(pos_transcripts1);

		if (tracks==2){
			intersections.retainAll(pos_transcripts2);
		}else if (tracks==3){
			HashSet<Transcript> intersections_temp;
			intersections_temp = new HashSet<Transcript>(pos_transcripts2);
			intersections_temp.retainAll(pos_transcripts21);
			intersections = new HashSet<Transcript>(intersections_temp);
		}else if (tracks==4){
			HashSet<Transcript> intersections_temp;
			HashSet<Transcript> intersections_temp_2;
			intersections_temp = new HashSet<Transcript>(pos_transcripts2);
			intersections_temp.retainAll(pos_transcripts21);
			intersections_temp_2 = new HashSet<Transcript>(pos_transcripts22);
			intersections_temp_2.retainAll(intersections_temp);
			intersections = new HashSet<Transcript>(intersections_temp_2);
		}else if (tracks==5){
			HashSet<Transcript> intersections_temp;
			HashSet<Transcript> intersections_temp_2;
			HashSet<Transcript> intersections_temp_3;
			intersections_temp = new HashSet<Transcript>(pos_transcripts2);
			intersections_temp.retainAll(pos_transcripts21);
			intersections_temp_2 = new HashSet<Transcript>(pos_transcripts22);
			intersections_temp_2.retainAll(intersections_temp);
			intersections_temp_3 = new HashSet<Transcript>(pos_transcripts23);
			intersections_temp_3.retainAll(intersections_temp_2);
			intersections = new HashSet<Transcript>(intersections_temp_3);
		}

		if( intersections.size() == 0 ) {

			return "\"message2\":\"No matching genes found.\"";

		} else {

			String resultstring = new String();
			String res = new String();
			String geneList = new String();
			ArrayList<String> geneListCheck = new ArrayList<String>();
			
			if(!trackid3.equals("null")) {

				res = annotateResults();

				return res;
				
			}else {

				for (Transcript transcript : intersections) {

					resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\""
							+ testList.dictRef.get(transcript.symbol) + "\"],";
					if (testList.dictRef.get(transcript.symbol) != null
							&& !testList.dictRef.get(transcript.symbol).trim().isEmpty()
							&& !geneListCheck.contains(testList.dictRef.get(transcript.symbol))) {
						geneListCheck.add(testList.dictRef.get(transcript.symbol));
						geneList += "\"" + testList.dictRef.get(transcript.symbol) + "\",";
					}

			}

				resultstring = resultstring.substring(0, resultstring.length() - 1);
				geneList = geneList.substring(0, geneList.length() - 1);
				return "\"labeltypes\":[\"text\",\"text\",\"text\"],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\"],\"geneList\":["
						+ geneList + "],\"data\":["
						+ resultstring + "]";
			
			}

		}



	}

	public String annotateResults() throws FileNotFoundException {

		String resultstring = new String();
		String geneList = new String();
		ArrayList<String> geneListCheck = new ArrayList<String>();
		
		switch(colSelect) {
		
		case 2: {
			
			
			outer: for (Transcript transcript : intersections) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}

				}

				String transcrName = testList.dictRef.get(transcript.symbol);
					
				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName + "\",\""
						+ "" + "\"],";
				
			}
				
			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\"],\"data\":[" + resultstring + "]";
		}

		
		
		case 3: {
			
			

			outer: for (Transcript transcript : intersections) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}

				}

				String transcrName = testList.dictRef.get(transcript.symbol);
					
				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\"],\"data\":[" + resultstring + "]";
		}
		case 4:{
			
			outer :for( Transcript transcript : intersections ) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4() + "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}

				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\"],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\"],\"geneList\":[" + geneList
					+ "],\"data\":[" + resultstring + "]";
		}
		case 5:{
			
			outer :for( Transcript transcript : intersections ) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4() + "\",\"" + item.getLabel5() + "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}

				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\",\"" + types.get(3)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\"],\"data\":[" + resultstring + "]";
		}
		case 6:{
			
			outer : for( Transcript transcript : intersections ) {

				for (CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4() + "\",\"" + item.getLabel5() + "\",\"" + item.getLabel6()
									+ "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}

				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\"\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\",\"" + types.get(3) + "\",\"" + types.get(4)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(5) + "\"],\"data\":[" + resultstring + "]";
		}
	}
		return "number of columns not allowed!";

	}


	/**
	 * This method creates the dictionaries containing the gene ids references
	 * 
	 * @throws FileNotFoundException
	 */
	public void geneIdConvertion(boolean customAnno) throws FileNotFoundException {

		ArrayList<String> test = new ArrayList<String>();

		if (customAnno) {
			Iterator<CustomDataset> iterator = customList.iterator();
			while (iterator.hasNext()) {
				test.add(iterator.next().getSymbol());
			}
		}

		// initialize the converter
		testList = new ConvertIDs(assembly, gtype, test);

		// read IDs test table
		String converter = "geneIDconverter/" + assembly + ".txt";

		String urlConverter = getClass().getClassLoader().getResource(converter).toString().split(":")[1];

		// read IDs test table
		IDFile = new File(urlConverter);

		// create the two dictionaries needed for the Ids convertion
		testList.createDict(testList, IDFile);

	}

}

