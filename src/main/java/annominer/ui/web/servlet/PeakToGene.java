package annominer.ui.web.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.Chromosome;
import annominer.CustomDataset;
import annominer.GeneCoverage;
import annominer.GenomeAnnotation;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.ui.web.UserTrack;;


@WebServlet(name="PeakToGene", urlPatterns={"/PeakToGene"})
public class PeakToGene extends HttpServlet {


	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(PeakToGene.class.getCanonicalName());
	private HttpSession session;
	private String assembly,gtype,trackid,trackid3;
	private String[] tsel;
	private int ovval, tss_us, tss_ds, flrange, margin,colSelect;
	private boolean ovtype, canonical;
	private ArrayList<CustomDataset> customList;
	private Set<Transcript> pos_transcripts;
	private GeneCoverage coverage;
	private List<int[]> targetintervals;
	private Map<String,List<Region>> transcript_target_regions;
	public ConvertIDs testList;
	public File IDFile;
	private boolean customAnno;
//	private long startTime;
//	private long endTime;
//	private long duration;


    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	respond(request,response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		respond(request,response);
	}


    private void respond(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("PeakToGene");
    		////////////////////////////////
    		// Parse request-parameters : //
    		////////////////////////////////
    	try {
			assembly = request.getParameter("assembly");
			gtype = request.getParameter("gtype");
			trackid = request.getParameter("trackid");
    		trackid3 = request.getParameter("trackid3");
			tsel = (request.getParameter("targets[]")).split(",");
			ovval = Integer.valueOf((request.getParameter("ovval")));
			tss_us = Integer.valueOf(request.getParameter("tss_us"));
			tss_ds = Integer.valueOf(request.getParameter("tss_ds"));
			margin = Integer.valueOf(request.getParameter("flrange"));
			flrange = (int) Math.round(margin / 3);
		
		if(trackid==null) {
    		PrintWriter out = response.getWriter();
		 	out.println( "{\"message\":\"Upload at least an input file before to start!. \"}");
		 	return;			
		}
    	}catch(Exception e){
    		PrintWriter out = response.getWriter();
		 	out.println( "{\"message\":\"Wrong input values. Check parameters types and sizes. \"}");
		 	return;
    		
    	}
		if ( request.getParameter("canonical").equals("1") ) {
			canonical = true;
		} else {
			canonical = false;
		}
		
		if ( request.getParameter("ovtype").equals("0") ) {
			ovtype = true;
		} else {
			ovtype = false;
		}

		
//		LOGGER.log(Level.INFO, "Trackid for Peak2Gene: " + trackid ); /*used the logger to print in console the name of the uploaded file used for the analysis*/
//		LOGGER.log(Level.INFO, "Value for assembly: " + assembly ); 
//		LOGGER.log(Level.INFO, "Value for gtype: " + gtype ); 
//		LOGGER.log(Level.INFO, "Value for tsel: " + tsel ); 
//		LOGGER.log(Level.INFO, "Value for ovval: " + ovval ); 
//		LOGGER.log(Level.INFO, "Value for ovtype: " + ovtype ); 
//		LOGGER.log(Level.INFO, "Value for flrange: " + flrange );
//		LOGGER.log(Level.INFO, "Value for margin: " + margin ); 
//		LOGGER.log(Level.INFO, "Value for tss_us: " + tss_us ); 
//		LOGGER.log(Level.INFO, "Value for tss_ds: " + tss_ds ); 
//		LOGGER.log(Level.INFO, "Value for canonical: " + canonical ); 
//		LOGGER.log(Level.INFO, "Value for custom annotation set: " + trackid3 ); 
		
		////////////////////
		// Manage session //
		////////////////////

		session = request.getSession();
				
		customAnno = false;
		if(!trackid3.equals("null")) {

			customAnno = true;
			@SuppressWarnings("unchecked")
			HashMap<String, ArrayList<CustomDataset>> customDatasets = (HashMap<String, ArrayList<CustomDataset>>) session
					.getAttribute("customDatasets");

			response.setContentType("application/json;charset=utf-8");

			PrintWriter out = response.getWriter();
		

			if (customDatasets.isEmpty() == true) {
				out.println("{\"message\":\"Please upload a custom annotation file first!\"}");
				return;
			} else {

				customList = customDatasets.get(trackid3);

				colSelect = customList.listIterator(0).next().getLabels().size();

			}
	
		}

		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		
		@SuppressWarnings("unchecked")
		Map<String,UserTrack> usertracks = (HashMap<String,UserTrack>)session.getAttribute("usertracks");
		UserTrack datatrack;

		
		/*control to check if the user has uploaded at least one file*/
		if( usertracks.isEmpty() == true ) {
			out.println( "{\"message\":\"Please upload a bed-file first.\"}");
			return;
		} else {
			datatrack = usertracks.get(trackid);/*datatrack contains the file name*/
		}
		if( datatrack == null ) {
			out.println( "{\"message\":\"An error occurred. Track not found: "+trackid+"\"}");
			return;
		}

		////////////////////////////////
		// Calculate and send results //
		////////////////////////////////

		pos_transcripts = new HashSet<Transcript>();/*constructor*/
		
//		Source source = new Source("sql","genome-mysql.cse.ucsc.edu","genome");
		// new
		Source source = new Source("sql", "genome-euro-mysql.soe.ucsc.edu", "genome");

		GenomeRegionTrack peaktrack = new GenomeRegionTrack(datatrack.trackid, session.getId(), source);

		GenomeAnnotation genome = new GenomeAnnotation(assembly, source, gtype);

		try {

	    	if ( request.getParameter("targets[]").equals("0,0,0,0,0,0,0,0,0,0") ) {
	    		/*before to choose the regions it just compute the coverage and plot it*/	
//				startTime = System.nanoTime();
				parseIntersections(peaktrack, genome, false, true);
//				endTime = System.nanoTime();
//				duration = (endTime - startTime);
//				System.out.println("first parse: " + (double) duration / 1_000_000_000.0);
				/* compute the coverage between the gene and all the defined regions */
				out.println("{\"flankcov\":" + coverage.printCoverageResultsJSON()
						+ ",\"message\":\"Choose a target-region\"}");
	    		
	    	} else {
//				startTime = System.nanoTime();
				geneIdConvertion(customAnno);
//				endTime = System.nanoTime();
//				duration = (endTime - startTime);
//				System.out.println("conversion time: " + (double) duration / 1_000_000_000.0);

	    		this.targetintervals = combineTargetIntervalSelection();/*array representing the left/right position of the region selected by the user in the barplot*/

				this.transcript_target_regions = new HashMap<String, List<Region>>();

//				startTime = System.nanoTime();
				parseIntersections(peaktrack, genome, true, true);
//				endTime = System.nanoTime();
//				duration = (endTime - startTime);
//				System.out.println("second parse: " + (double) duration / 1_000_000_000.0);

//				startTime = System.nanoTime();
	    		out.println( "{\"flankcov\":" + coverage.printCoverageResultsJSON() + "," + geneResults2JSON() + "}");
//				endTime = System.nanoTime();
//				duration = (endTime - startTime);
//				System.out.println("print time: " + (double) duration / 1_000_000_000.0);
	    	}

		} catch (ChromIDException e) {
			out.println("{\"message\":\""+e.getMessage()+"! ... Make sure your organism-selection is correct.\"}");
			return;
		}


    }


    public Map<String,List<Region>> getTargetIntervals(GenomeAnnotation genome) {

    	Map<String,List<Region>> tregions = new HashMap<String,List<Region>>();

//    	LOGGER.log(Level.INFO, "Making tregions !!!" );
		for ( Chromosome chrom: genome ) {
			for(Region region: chrom) {
				Transcript trans = (Transcript)region;
				tregions.put(trans.pid, getTargetRegions(trans) );
			}
		}
//		LOGGER.log(Level.INFO, "Done !!!" );
    	return tregions;
    }

	
    /**This method check if the coverage between the transcript region (in the DB) and the region is within the interval chosen by the user.
     * If it is it stores the transcript in a collection called pos_transcripts.
     * 
     * @param transcript
     * @param region
     * @param chromlength
     * @param forward 
     */
	public void genecheck(Transcript transcript, Region region, long chromlength, boolean forward) {

		/*get the list of regions belonging to a transcript*/
		List<Region> tregions = this.transcript_target_regions.get(transcript.pid);

		if( tregions == null) {
			tregions = getTargetRegions(transcript);
			this.transcript_target_regions.put(transcript.pid, tregions);
		}

		for (Region tregion : tregions) {

			Region coverage = tregion.intersectionWith(region);

			if (coverage!=null) {

				boolean match = false;

				if (ovtype) {
					match = coverage.length() >= ovval;
				} else {
					match = coverage.length() >= ovval * 0.01 * tregion.length();
				}

				if (match) {
				
					pos_transcripts.add(transcript);
				}
			}

		}

	}

	/**This method parse the intersections and compute the coverages between the track and the regions in the genome
	 * 
	 * @param track
	 * @param genome
	 * @param checkgene 
	 * @param calculatecoverage 
	 * @throws ChromIDException
	 */
	public void parseIntersections(GenomeRegionTrack track, GenomeAnnotation genome, boolean checkgene, boolean calculatecoverage) throws ChromIDException {
		
		Chromosome chromosome = null;
		Transcript gene=null, fwgene=null;
		long chromsize=0;

		ListIterator<Region> iterator=null,fwiterator=null;
		boolean it_next = true;
		boolean it_ended = false;
		
		coverage = new GeneCoverage(flrange, tss_us, tss_ds);
		
		for (Chromosome chromtrack : track) {

			chromosome = genome.getChromByID(chromtrack.pid);
			
			if (chromosome.getRegionNumber() == 0) {/**/
//				LOGGER.log(Level.INFO, "Genome-annotation has no genes for chromosome: " + chromtrack.pid );
				continue;
			}

			chromsize = chromosome.length;
			coverage.nextChrom(chromsize);
			iterator = chromosome.listIterator();
			it_next = true;
			it_ended = false;
			
			for (Region region : chromtrack) {
//				LOGGER.log(Level.INFO, "region"); 
//				LOGGER.log(Level.INFO, "start: " + region.left ); 
//				LOGGER.log(Level.INFO, "stop: " + region.right ); 
				
				if (it_ended) {

					//pass;

				} else {
					while (true) {
						if (it_next) {

							if ( iterator.hasNext() ) {
								gene = (Transcript) iterator.next();
								it_next = false;

							} else {

								it_ended = true;
								break;

							}

						}
						if (gene.isLeftOf(region, margin)) {
							it_next = true;

						} else if (gene.isRightOf(region, margin)) {	
							it_next = false;
							break;

						} else {
							if(canonical) {
			 					while(!gene.canonical){
			 						if ( iterator.hasNext() ) { 
			 							gene = (Transcript)iterator.next();
			 						}else{
										it_ended = true;
										break;
			 							}
			 						}
							}
							if (checkgene) {
								genecheck(gene, region, chromsize, false);
							}
							if (calculatecoverage) {
								coverage.addGene(gene, region);
							}

							fwiterator = chromosome.listIterator(iterator.nextIndex());
							
							while (true) {

								if( !(fwiterator.hasNext()) )
									break;

								fwgene = (Transcript)fwiterator.next();

								if (fwgene.isRightOf(region, margin)) {

									break;

								} else if (fwgene.isLeftOf(region, margin)) {

									//pass;

								} else {
									if(canonical) {
					 					while(!fwgene.canonical){
					 						if ( fwiterator.hasNext() ) { 
					 							fwgene = (Transcript)fwiterator.next();
					 						}else{
					 							it_ended = true;
												break;
					 							}
					 						}
					 					}
									if (checkgene) { genecheck(fwgene, region, chromsize, true); }
									if (calculatecoverage) { coverage.addGene(fwgene,region); }

								}

							}


							it_next = false;
							break;

						}

					}

				}

			}

		}

	}


	/**This method return the array representing the target intervals selected by the user (barplot)
	 * 
	 * @return targets a list of integer
	 */
	private List<int[]> combineTargetIntervalSelection() {

		int leftborder = -1;
		int rightborder = -1;
		List<int[]> targets = new ArrayList<int[]>();
		targets.add( new int[]{leftborder,rightborder} );
		
		for( int i=0; i<tsel.length; i++ ) {

			if ( tsel[i].equals("1") ) {

				if ( leftborder != -1 ) {
						rightborder = i;
				} else {
					leftborder = i;
					rightborder = i;
				}
			}
		}

		if (leftborder != -1) {
			targets.add( new int[]{leftborder,rightborder} );
		}
//		LOGGER.log(Level.INFO, "Targets: " + leftborder+rightborder);
		return targets;

	}

	/**
	 * 
	 * @param transcript
	 * @return a list of regions (target regions)
	 */
	private List<Region> getTargetRegions( Transcript transcript ) {

		List<Region> tregions = new ArrayList<Region>();

		for( int[] target : this.targetintervals) {

			long pos1 = -1;
			long pos2 = -1;

			switch( target[0] ) {
				case 0:
					pos1 = transcript.pos5pBy(3*flrange);
					break;
				case 1:
					pos1 = transcript.pos5pBy(2*flrange);
					break;
				case 2:
					pos1 = transcript.pos5pBy(flrange);
					break;
				case 3:
					if (transcript.strand) {
						pos1 = transcript.start() - tss_us;
					} else {
						pos1 = transcript.start() + tss_us;
					}
					break;
				case 4:
					pos1 = transcript.start();
					break;
				case 5:
					pos1 = transcript.cds.start();
					break;
				case 6:
					pos1 = transcript.cds.end();
					break;
				case 7:
					pos1 = transcript.end();
					break;
				case 8:
					pos1 = transcript.pos3pBy(flrange);
					break;
				case 9:
					pos1 = transcript.pos3pBy(2*flrange);
					break;
			}


			switch( target[1] ) {
				case 0:
					pos2 = transcript.pos5pBy(2*flrange);
					break;
				case 1:
					pos2 = transcript.pos5pBy(flrange);
					break;
				case 2:
					pos2 = transcript.start();
					break;
				case 3:
					pos2 = transcript.pos5pBy(-tss_ds);
					if( transcript.strand ) {
						if( pos2 > transcript.right ) {
							pos2 = transcript.right;
						}
					} else {
						if( pos2 < transcript.left ) {
							pos2 = transcript.left;
						}
					}
					break;
				case 4:
					if( target[0] == 4) {
						pos2 = transcript.cds.start();
					} else {
						long tpos = transcript.pos5pBy(-tss_ds);
						if( transcript.strand ) {
							if( tpos > transcript.cds.left ) {
								if( tpos > transcript.right ) {
									pos2 = transcript.right;
								} else {
									pos2 = tpos;
								}
							} else {
								pos2 = transcript.cds.left;
							}
						} else {
							if( tpos < transcript.cds.right ) {
								if( tpos < transcript.left ) {
									pos2 = transcript.left;
								} else {
									pos2 = tpos;
								}
							} else {
								pos2 = transcript.cds.right;
							}
						}
					}
					break;
				case 5:
					pos2 = transcript.cds.end();
					break;
				case 6:
					pos2 = transcript.end();
					break;
				case 7:
					pos2 = transcript.pos3pBy(flrange);
					break;
				case 8:
					pos2 = transcript.pos3pBy(2*flrange);
					break;
				case 9:
					pos2 = transcript.pos3pBy(3*flrange);
					break;
			}

			if( (pos1 != -1) && (pos2 != -1) && (pos1 != pos2) ) {
				tregions.add(new Region(pos1,pos2));
			}

		}

		return tregions;

	}

	/**
	 * This method return a string in JSON format with Primary ID and Symbol for
	 * each gene founded
	 * 
	 * @return string with primary id and symbol for each gene found returned in
	 *         JSON format
	 * @throws FileNotFoundException
	 */
	public String geneResults2JSON() throws FileNotFoundException {




		if( pos_transcripts.size() == 0 ) {

			return "\"message\":\"No matching genes found. Please check in the analysis options whether your minimal overlap criterion is suitable.\"";

		} else {
			
			String resultstring = new String();
			String res = new String();
			String geneList = new String();
			ArrayList<String> geneListCheck = new ArrayList<String>();
			
			if(!trackid3.equals("null")) {
				
				res = annotateResults();
				
				return res;
	
			}else {
			
				for (Transcript transcript : pos_transcripts) {

					resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\""
							+ testList.dictRef.get(transcript.symbol) + "\"],";
					if (testList.dictRef.get(transcript.symbol) != null
							&& !testList.dictRef.get(transcript.symbol).trim().isEmpty()
							&& !geneListCheck.contains(testList.dictRef.get(transcript.symbol))) {
						geneListCheck.add(testList.dictRef.get(transcript.symbol));
						geneList += "\"" + testList.dictRef.get(transcript.symbol) + "\",";
					}

				}
				geneList = geneList.substring(0, geneList.length() - 1);
				resultstring = resultstring.substring(0, resultstring.length() - 1);
				return "\"labeltypes\":[\"text\",\"text\",\"text\"],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\"],\"geneList\":["
						+ geneList + "],\"data\":["
						+ resultstring + "]";
			
			}
	}
	}
		
	
	public String annotateResults() throws FileNotFoundException {


		String resultstring = new String();
		String geneList = new String();
		ArrayList<String> geneListCheck = new ArrayList<String>();

		switch(colSelect) {
		
		case 2:{
			
			
			outer: for (Transcript transcript : pos_transcripts) {

				for(CustomDataset item : customList) {


					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\"],";

							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}

							continue outer;
						}
					}
						
				}

				String transcrName = testList.dictRef.get(transcript.symbol);
					
				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName + "\",\""
						+ "" + "\"],";
				
			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\"],\"data\":[" + resultstring + "]";
		}

		
		
		case 3:{
			
			

			outer: for (Transcript transcript : pos_transcripts) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\"],";

							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}

							continue outer;
						}
					}
						
				}

				String transcrName = testList.dictRef.get(transcript.symbol);
					
				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\""
						+ "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\"],\"data\":[" + resultstring + "]";
		}
		case 4:{
			
			outer: for (Transcript transcript : pos_transcripts) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4()
									+ "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}
						
				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\""
						+ "\"],";

			}
			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\"],\"data\":[" + resultstring + "]";
		}
		case 5:{
			
			outer: for (Transcript transcript : pos_transcripts) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4() + "\",\""
									+ item.getLabel5() + "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}
						
				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\",\"" + types.get(3)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\"],\"data\":[" + resultstring + "]";
		}
		case 6:{
			
			outer: for (Transcript transcript : pos_transcripts) {

				for(CustomDataset item : customList) {

					String geneSymbol = testList.dict.get(item.getSymbol());
					String geneName = testList.dictRef.get(geneSymbol);
					String transcrName = testList.dictRef.get(transcript.symbol);

					if (geneName != null) {

						if (geneName.equals(transcrName)) {

							resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
									+ "\",\"" + item.getLabel2() + "\",\"" + item.getLabel3() + "\",\""
									+ item.getLabel4() + "\",\"" + item.getLabel5() + "\",\"" + item.getLabel6()
									+ "\"],";
							if (transcrName != null && !transcrName.trim().isEmpty()
									&& !geneListCheck.contains(transcrName)) {
								geneListCheck.add(transcrName);
								geneList += "\"" + transcrName + "\",";
							}
							continue outer;
						}
					}
						
				}

				String transcrName = testList.dictRef.get(transcript.symbol);

				resultstring += "[\"" + transcript.pid + "\",\"" + transcript.symbol + "\",\"" + transcrName
						+ "\",\"\",\"\",\"\",\"\",\"" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1) + "\",\""
					+ types.get(2) + "\",\"" + types.get(3) + "\",\"" + types.get(4)
					+ "\"],\"geneList\":[" + geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Name\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(5) + "\"],\"data\":[" + resultstring + "]";
		}
	}
		return "number of columns not allowed!";

	}

	/**
	 * This method creates the dictionaries containing the gene ids references
	 * 
	 * @throws FileNotFoundException
	 */
	public void geneIdConvertion(boolean customAnno) throws FileNotFoundException {

		ArrayList<String> test = new ArrayList<String>();

		if (customAnno) {
			Iterator<CustomDataset> iterator = customList.iterator();
			while (iterator.hasNext()) {
				test.add(iterator.next().getSymbol());
			}
		}

		// initialize the converter
		testList = new ConvertIDs(assembly, gtype, test);

		// read IDs test table
		String converter = "geneIDconverter/" + assembly + ".txt";

		String urlConverter = getClass().getClassLoader().getResource(converter).toString().split(":")[1];

		// read IDs test table
		IDFile = new File(urlConverter);

		// create the two dictionaries needed for the Ids convertion
		testList.createDict(testList, IDFile);

	}

}
