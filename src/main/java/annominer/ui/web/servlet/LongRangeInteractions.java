package annominer.ui.web.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.Chromosome;
import annominer.CustomDataset;
import annominer.GenomeAnnotation;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.ui.web.UserTrack;
//import jdk.internal.jline.internal.Log;


@WebServlet(name="LongRangeInteractions", urlPatterns={"/LongRangeInteractions"})
public class LongRangeInteractions extends HttpServlet {


	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(LongRangeInteractions.class.getCanonicalName());

	private HttpSession session;
	private ArrayList<CustomDataset> customList;
	private String assembly,gtype,trackid, strandGene,trackid3;
	private String[] tsel;
	private boolean disciminateUpDown, customAnno, log2fc, strand, canonical;
	private int left, right, colSelect, log2fcPOS;
	private List<Integer> targetintervals; //interval selected by the user from the plot
	private HashMap<String, List<String>> geneidsDown, geneidsUp, targetgenes;
	private PrintWriter out;
	private ConvertIDs testList;
	private HashMap<String, CustomDataset> customListMap;
	private File IDFile;
	private Chromosome chromosome;
	private Transcript gene;


    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	respond(request,response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		respond(request,response);
	}


    private void respond(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    ////////////////////////////////
	    // Parse request-parameters : //
		////////////////////////////////
    	try {

			session = request.getSession();
			assembly = request.getParameter("assembly");/* reference genome */
			gtype = request.getParameter("gtype");/* annotation resource */
			trackid = request.getParameter("trackid");/* name of the uploaded file */
			tsel = (request.getParameter("targets[]")).split(","); /* value of the selected regions from the plot: */
			trackid3 = request.getParameter("trackid3");
			right = Integer.valueOf(request.getParameter("right")) * 1000;/* right value of flanking region */
			left = Integer.valueOf(request.getParameter("left")) * 1000;/* lefr value of flanking region */

			// choice for what to highlight in the plot
			if (request.getParameter("highlighter").equals("1")) {
				disciminateUpDown = true;
			} else {
				disciminateUpDown = false;
			}
			if (request.getParameter("canonical").equals("1")) {
				canonical = true;
			} else {
				canonical = false;
			}
			if (request.getParameter("strand").equals("1")) {
				strand = true;
			} else {
				strand = false;
			}
			out = response.getWriter();
			// check for the genomic region file
			if (trackid == null) {
				out.println("{\"message\":\"Upload and select the input file before to start! \"}");
				return;
			}
			if (trackid3 == null) {
				out.println("{\"message\":\"Upload and select the expression file before to start! \"}");
				return;
			}
		} catch (Exception e) {
			out.println("{\"message\":\"Wrong input values. Check parameters types and sizes. \"}");
			return;
		}
//		LOGGER.log(Level.INFO, "strand" + strand); 
//		LOGGER.log(Level.INFO, "Trackid for LongRangeInteractions: " + trackid ); /*uploaded file used for the analysis*/
//		LOGGER.log(Level.INFO, "assembly: " + assembly ); 
//		LOGGER.log(Level.INFO, "gtype: " + gtype ); 
//		LOGGER.log(Level.INFO, "tsel: " + tsel ); 
//		LOGGER.log(Level.INFO, "flrange: " + flrange ); 
//		LOGGER.log(Level.INFO, "dis: " + dis ); 
//		LOGGER.log(Level.INFO, "Value for expression set: " + trackid3 ); 
		
		/* construct a new source object with this source, host, user*/
		Source source = new Source("sql","genome-mysql.cse.ucsc.edu","genome");

		////////////////////
		// Manage session //
		////////////////////
		
		
		customAnno = true;
			
		@SuppressWarnings("unchecked")
		HashMap<String, ArrayList<CustomDataset>> customDatasets = (HashMap<String, ArrayList<CustomDataset>>) session
				.getAttribute("customDatasets");

		response.setContentType("application/json;charset=utf-8");

		/*
		 * control to check if we have uploaded yet any id lists file and, if we had,
		 * store the contents in an array named "list_inputsid"
		 */
		if (customDatasets.isEmpty() == true) {
			out.println("{\"message\":\"Please upload a custom annotation file first!\"}");
			return;
		} else {
			
			customList = customDatasets.get(trackid3);
			colSelect = customList.listIterator(0).next().getLabels().size();
			
		}
//		control if the custom annotation file contains the log2FC field
		log2fc = false;
		String[] log2fcTypo = { "log2fc", "log2foldchange", "log2 fc", "log2 foldchange", "log2 fold change" };
		for (int c = 0; c < colSelect; c++) {
			for (int i = 0; i < log2fcTypo.length; i++) {
				if (customList.listIterator(0).next().getLabels().get(c).equalsIgnoreCase(log2fcTypo[i])) {
					log2fcPOS = c;
					log2fc = true;
					break;
				}
			}
		}
		if (!log2fc) {
			out.println("{\"message\":\"Your custom annotation file must contain the log2FC field!\"}");
			return;
		}
		
		response.setContentType("application/json;charset=utf-8");

		
	 	/*collection of the pair trackid (file name): and the relative UserTrack*/
		/*gets the collection from ManageUpload*/
	 	@SuppressWarnings("unchecked")
		Map<String,UserTrack> usertracks = (HashMap<String,UserTrack>)session.getAttribute("usertracks");
		
		UserTrack datatrack;

		 /*control to check if the user has uploaded at least one file*/
		 if( usertracks.isEmpty() == true ) {
		 	out.println( "{\"message\":\"Please upload a bed-file first.\"}");
		 	return;
		 } else {
		 	datatrack = usertracks.get(trackid);/*datatrack contains the file name*/
		 }
		 /*check if the selected file is in datatrack*/
		 if( datatrack == null ) {

		 	out.println( "{\"message\":\"An error occurred. Track not found: "+trackid+"\"}");

		 	return;
		 }


     	//////////////////////////////// //
		// // Calculate and send results //
		// ////////////////////////////////
		
		 GenomeRegionTrack peaktrack = new GenomeRegionTrack(datatrack.trackid, session.getId(), source);/*(String trackid, String organism, Source source) connection with mongoDB and create a db called sessionId and containing two collections one with dbindex and one with the regions called with the filename*/
		
		 GenomeAnnotation genome = new GenomeAnnotation(assembly, source, gtype);/*create instance of DataTrack, connection with mongoDB and will get the collections looking in the database "assembly"(es dm3) the respective one called "trackid"( es ensembl) in dbindex collection (in case the database doesn't exist yet)*/
		 
		
		geneIdConvertion(customAnno);

		customListMap = new HashMap<String, CustomDataset>();
		for (CustomDataset item : customList) {
			String key = testList.dict.get(item.getSymbol());
			customListMap.put(key, item);
		}

		 try {

		 	/*requested target regions from the plot*/
	     	if ( request.getParameter("targets[]").equals("0,0") ) {//starting condition in which the plot has not been generated yet, no target region selection

				parseIntersections(peaktrack, genome); // build nearby genes

				out.println("{" + printLongRangeInteractionsResultsJSON()
						+ ",\"message\":\"Choose a target-region between the upper intervals\", " + getLog2FC()
						+ "}");

//	     		LOGGER.log(Level.INFO, "printLongRangeInteractionResultsJSON " + printLongRangeInteractionsResultsJSON() ); 
	     	 }
	    		 else {//after the user choose from the barplot the region of interest


	 	     		parseIntersections(peaktrack, genome);
		     		this.targetintervals = combineTargetIntervalSelection();/*array representing the regions selected by the user in the barplot*/
		     		
		     		this.targetgenes = getTargetGenes(); //build an HashMap merging all the genes belonging to the regions selected by the user
		     			     		
				out.println("{" + printLongRangeInteractionsResultsJSON() + "," + geneResults2JSON() + "," + getLog2FC()
						+ "}");// print the content of the HashMap tgenes to be printed as data used to
									// populate the Datatable
//		     		LOGGER.log(Level.INFO, "generesultjson " + geneResults2JSON() );  
	     	}

		 } catch (ChromIDException e) {
		 	out.println("{\"message\":\""+e.getMessage()+"! ... Make sure your organism-selection is correct.\"}");
		 	return;
		 }


     }


	 /**This method check the position of the genes despite the region, and in according to that choose to discard them or store them in the corresponding variable
	  * 
	  * @param track
	  * @param genome
	  */
	 public void parseIntersections(GenomeRegionTrack track, GenomeAnnotation genome) throws ChromIDException {
		 
		 
		chromosome = null;
		gene = null;
		geneidsDown = new HashMap<String, List<String>>();
		geneidsUp = new HashMap<String, List<String>>();
	 	ListIterator<Region> iterator=null;// initialize the iterator through the regions of the chromosome 
	 	boolean it_next = true;
	 	boolean it_ended = false;
	 		
	 	for ( Chromosome chromtrack: track) {/*for each document in the collection (uploaded file)*/
	 		
	 		/*assign to the variable the chromosome in the database (ensembl for instance) that has the same pid (chromosome name) of the one in the uploaded document */
	 		chromosome = genome.getChromByID(chromtrack.pid);
	 		
			
	 		/*check if within the chromosome we have gene regions or not*/
	 		if (chromosome.getRegionNumber() == 0) {
//	 			LOGGER.log(Level.INFO, "Genome-annotation has no genes for chromosome: " + chromtrack.pid );
	 			continue;
	 		}


	 		for ( Region region: chromtrack) {/*for each region in the uploaded document belonging to the same chromosome*/
				/* inizialized an iterator through the regions of the chromosome */
				iterator = chromosome.listIterator();
				it_next = true;
				it_ended = false;

	 			if (it_ended) {

	 				//pass;

	 			} else {
					outer: while (true) {
	 					if (it_next) {

	 						if ( iterator.hasNext() ) {
	 							gene = (Transcript)iterator.next();/*gene in the chromosome (cast into transcript)*/
	 						
	 						} else {
	 							//no more genes annotated in the chromosome
	 							it_ended = true;
								it_next = false;
	 							break;
	 						}

	 					}
	 					
						if (canonical) {
							while (!gene.canonical) {
								if (iterator.hasNext()) {
									gene = (Transcript) iterator.next();
								} else {
									continue outer;
								}
							}
						}
						if (gene.isLeftOf(region, left)) {// out of range upstream
	 					//control if the left coordinate of the region (uploaded) is greater than the gene's right coordinate plus the flrange and the distance
	 						
	 						it_next = true;
//	 						LOGGER.log(Level.INFO, "GENE LEFT");
						} else if (gene.isRightOf(region, right)) {// out of range downstream
	 					/*control if the right coordinate of the region (uploaded) is less than the gene's region left coordinate minus the flrange and the distance value*/
	 						it_next = false;
							it_ended = true;
//	 						LOGGER.log(Level.INFO, "GENE RIGHT");
	 						break;//out of the loop
						} else if (gene.isRightOf(region)) {// gene is in the margin (downstream)
	 						
							if (geneidsDown.containsKey(gene.symbol)) {// if not already present store the candidate
																		// gene

							} else {
	 														
								if (gene.strand) {
									strandGene = "+";
								} else {
									strandGene = "-";
								}

								if (strand) {
									if (strandGene.equals("-")) {
										continue;
									}
								}
//	 							LOGGER.log(Level.INFO, "GENE RIGHT"+gene.symbol);
								List<String> listDown = Arrays.asList(gene.pid, strandGene);
								geneidsDown.put(gene.symbol, listDown);

							}
	 						it_next = true;
	 						
						} else if (gene.isLeftOf(region)) {// gene is in the margin (upstream)
	 						
							if (geneidsUp.containsKey(gene.symbol)) {// if not already present store the candidate gene
							} else {

								if (gene.strand) {
									strandGene = "+";
								} else {
									strandGene = "-";
								}

								if (strand) {
									if (strandGene.equals("+")) {
										continue;
									}
								}
//	 							LOGGER.log(Level.INFO, "GENE LEFT"+gene.symbol);
								List<String> listUp = Arrays.asList(gene.pid, strandGene);
								geneidsUp.put(gene.symbol, listUp);

							}
	 						it_next = true;
	 						

						} else {// overlapping gene


	 						it_next = true;}
	 					
	 				}
	 				
	 			}
	 			
	 		}
	 		
	 	}	
	 	
	 	
	 }

	 /**This method return the array representing the target intervals selected by the user (plot)
	  * 
	  * @return targets a list of integer
	  */
	 private List<Integer> combineTargetIntervalSelection() {

	 	List<Integer> targets = new ArrayList<Integer>();

	 	for( int i=0; i<tsel.length; i++ ) {

	 		if ( tsel[i].equals("1") ) {

	 		targets.add(i);
//	 		LOGGER.log(Level.INFO, "Targets: " + i);
	 		}
	 	}
	 	return targets;

	 }


	public String geneResults2JSON() throws FileNotFoundException {

		String res = new String();
		res = annotateResults();
		return res;


	}

	 /**This method will prepare a string containing the gene counts for each region to build the plot
	  * 
	  * @return string
	  */
	 public String printLongRangeInteractionsResultsJSON() {

			String flankcov = "[";
			
			if ( geneidsUp.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  geneidsUp.size() + "],";
			}
			
			if ( geneidsDown.size() == 0 ) {
				flankcov += "[0]]";
			} else {
				flankcov += "[" +  geneidsDown.size() + "]]";
			}
			//LOGGER.log(Level.INFO, "JqPlot: " + "\"flankcov\":"+flankcov);
			return "\"flankcov\":"+flankcov;
			
	}
	 
	 /**This method merge the containt of the different HashMap in according to the user choice from the plot
	  * 
	  * @return tgenes an HashMap containing the results
	  */
	 private HashMap<String, List<String>> getTargetGenes() {

		 	HashMap<String, List<String>> tgenes = new HashMap<String, List<String>>();
		 	
			for( int target : this.targetintervals) {
				
				switch( target) {
				case 0:
					tgenes.putAll(geneidsUp);
					break;
				case 1:
					tgenes.putAll(geneidsDown);
					break;
			}}
			
			return tgenes;

		}
	 
	public String annotateResults() throws FileNotFoundException {

		String resultstring = new String();
		String geneList = new String();
		ArrayList<String> geneListCheck = new ArrayList<String>();
				
		switch (colSelect) {
				
		case 2: {

			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

//						System.out.println("strKey " + strKey);

					String geneName = testList.dict.get(item.getSymbol());
//						System.out.println("geneName " + geneName);

					String transcrName = testList.dictRef.get(strKey);
//						System.out.println("transcrName " + transcrName);


					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
							}
						continue outer;

						}


					}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\"],";

			}
					
			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\"],\"geneList\":["
					+ geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\"],\"data\":[" + resultstring + "]";
			}

				
				
		case 3: {

			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);


					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

							}

				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\"],\"data\":[" + resultstring + "]";
		}
				
		case 4: {
					
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
							}
						continue outer;

						}


				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\"" + types.get(2) + "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\"],\"data\":[" + resultstring + "]";
			}
		case 5: {
					
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\",\"" + item.getLabel5()
								+ "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
							}
						continue outer;

						}

				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\",\"" + "" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\"" + types.get(2) + "\",\"" + types.get(3) + "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\"],\"data\":[" + resultstring + "]";
			}
		case 6: {
					
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\",\"" + item.getLabel5()
								+ "\",\"" + item.getLabel6() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
							}
						continue outer;

						}

				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\",\"" + "" + "\",\"" + "" + "\"],";

				}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\"" + types.get(2) + "\",\"" + types.get(3) + "\",\"" + types.get(4) + "\"],\"geneList\":["
					+ geneList + "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(5) + "\"],\"data\":[" + resultstring + "]";
		}

		}
		return "number of columns not allowed!";

			}




	/**
	 * This method creates the dictionaries containing the gene ids references
	 * 
	 * @throws FileNotFoundException
	 */
	public void geneIdConvertion(boolean customAnno) throws FileNotFoundException {

		ArrayList<String> test = new ArrayList<String>();

		if (customAnno) {
			Iterator<CustomDataset> iterator = customList.iterator();
			while (iterator.hasNext()) {
				test.add(iterator.next().getSymbol());
			}
		}

		// initialize the converter
		testList = new ConvertIDs(assembly, gtype, test);

		// read IDs test table
		String converter = "geneIDconverter/" + assembly + ".txt";

		String urlConverter = getClass().getClassLoader().getResource(converter).toString().split(":")[1];

		// read IDs test table
		IDFile = new File(urlConverter);

		// create the two dictionaries needed for the Ids convertion
		testList.createDict(testList, IDFile);

	}

	public String getLog2FC() {

		String log2fcValues = "[";

		if (geneidsUp.size() == 0) {
			log2fcValues += "[[0],[0]],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsUp.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();

				if (testList.dictRef.containsKey(test)) {
					if (Float.valueOf(returnLog(test)) > 0) {
						countUp++;
					} else if (Float.valueOf(returnLog(test)) < 0) {
						countDown++;
					}
				}
			}
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsUp.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";

			}
		}
		if (geneidsDown.size() == 0) {
			log2fcValues += "[[0],[0]]]";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsDown.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();

				if (testList.dictRef.containsKey(test)) {
					if (Float.valueOf(returnLog(test)) > 0) {
						countUp++;
					} else if (Float.valueOf(returnLog(test)) < 0) {
						countDown++;
					}
				}
			}
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]]]";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]]]";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]]]";
				} else {
					log2fcValues += "[[0]]]";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsDown.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]]]";

			}
		}
		return "\"log2FC\":" + log2fcValues;

	}

	public String returnLog(String geneID) {
		String LOG2FC = "0";
		switch (log2fcPOS) {
		case 1:
			if (customListMap.containsKey(geneID)) {
				CustomDataset item = customListMap.get(geneID);
				LOG2FC = String.valueOf(item.getLabel2());
				if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
						|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
					LOG2FC = "";
				}
			}
			break;
		case 2:
			if (customListMap.containsKey(geneID)) {
				CustomDataset item2 = customListMap.get(geneID);
				LOG2FC = String.valueOf(item2.getLabel3());
				if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
						|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
					LOG2FC = "";
				}
			}

			break;
		case 3:
			if (customListMap.containsKey(geneID)) {
				CustomDataset item3 = customListMap.get(geneID);
				LOG2FC = String.valueOf(item3.getLabel4());
				if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
						|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
					LOG2FC = "";
				}
			}

			break;
		case 4:
			if (customListMap.containsKey(geneID)) {
				CustomDataset item5 = customListMap.get(geneID);
				LOG2FC = String.valueOf(item5.getLabel5());
				if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
						|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
					LOG2FC = "";
				}
			}

			break;
		case 5:
			if (customListMap.containsKey(geneID)) {
				CustomDataset item6 = customListMap.get(geneID);
				LOG2FC = String.valueOf(item6.getLabel6());
				if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
						|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
					LOG2FC = "";
				}
			}

			break;

		}

		return LOG2FC;
	}
}
	
		