package annominer.ui.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.ui.web.UserTrack;
/**
* 
*
*
*UploadsCheck is used to check whether the user already uploaded some files (genomic regions / gene list / custom annotation) and uplad the relative datatable in the web user interface
*/
@WebServlet(name="UploadsCheck", urlPatterns={"/UploadsCheck"})
@MultipartConfig
public class UploadsCheck extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(ManageUploads.class.getCanonicalName());
    
	private HttpSession session;
	private String remove;
	
	
	
	/** post call
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	
	
	/** http request
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		////////////////////
		// Manage session //
		////////////////////
		
		session = request.getSession();
//		session =request.getSession(false);
//		session.invalidate();
		remove = request.getParameter("removeTrack");
//		LOGGER.log(Level.INFO, "New session created: " + session.isNew() );
//		LOGGER.log(Level.INFO, "SessionID: " + session.getId() );
		
		@SuppressWarnings("unchecked")
		Map<String, UserTrack> usertracks = (HashMap<String, UserTrack>) session.getAttribute("usertracks");
		
		if( usertracks != null ) { 

			if (usertracks.containsKey(remove)) {

				usertracks.remove(remove);
			}
		}

		if (usertracks != null) {


			response.setContentType("application/json;charset=utf-8");

			final PrintWriter writer = response.getWriter();
			if (usertracks.size() != 0) {
				/* Check if we have already uploaded files or not */
				StringBuffer json = new StringBuffer("[");

				for (String id : usertracks.keySet()) {
					json.append(usertracks.get(id).toJSON()
							+ ","); /*
									 * as value it append:
									 * "{\"id\":\""+trackid+"\",\"name\":\""+trackid+"\",\"type\":\""+tracktype+
									 * "\,\"date\":\""+update+"\"}"
									 */
				}

				String outstr = json.substring(0, json.length() - 1) + "]";

				writer.println("{\"data\":" + outstr + "}");
//					LOGGER.log(Level.INFO, "{\"data\":"+ outstr +"}" );
			} else {
				
				writer.println("{\"data\":null}");
				
			}
			if (writer != null) {
				writer.close();
			}
	        
		}


		return;
		
	}

}


