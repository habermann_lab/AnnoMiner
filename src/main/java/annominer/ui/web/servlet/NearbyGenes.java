package annominer.ui.web.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.Chromosome;
import annominer.CustomDataset;
import annominer.GenomeAnnotation;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.ui.web.UserTrack;


@WebServlet(name="NearbyGenes", urlPatterns={"/NearbyGenes"})
public class NearbyGenes extends HttpServlet {

	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(NearbyGenes.class.getCanonicalName());

	private HttpSession session;

	private String assembly,gtype,trackid,strandGene,side,trackid3;
	private String[] tsel;
	public Chromosome chromosome;
	private boolean strand;
	private List<Integer> targetintervals;
	private ArrayList<CustomDataset> customList;
	private HashMap<String,List<String>> targetgenes;
	private TreeMap<Long,Transcript> collectionUp, collectionDown;
	private HashMap<String, List<String>> geneidsL1,geneidsL2,geneidsL3,geneidsL4,geneidsL5,geneidsR1,geneidsR2,geneidsR3,geneidsR4,geneidsR5,geneidsOverlap;
	private int index=0, indexL=0,indexR=0, downCounter=0, upCounter=0, colSelect;
	private Transcript gene;
	private ConvertIDs testList;
	private File IDFile;
	private boolean customAnno;
	private PrintWriter out;
	private boolean log2fc;
	private int log2fcPOS;
	private boolean disciminateUpDown;
	private HashMap<String, CustomDataset> customListMap;


//	private long startTime;
//	private long endTime;
//	private long totalTime;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	respond(request,response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		respond(request,response);
	}


	@SuppressWarnings("unused")
	private void respond(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		System.out.println("NearbyGenes");
	    ////////////////////////////////
	    // Parse request-parameters : //
		////////////////////////////////
    	
    	try {
			assembly = request.getParameter("assembly");/* assembly value of the reference genome */
			gtype = request.getParameter(
					"gtype");/* gtype is the value of the variable "ganno": annotation resource */
			trackid = request.getParameter(
					"trackid");/*
								 * trackid value of "ianno" : name of the uploaded file
								 */
			trackid3 = request.getParameter("trackid3");
			/* value of the selected columns of the plot: */
			tsel = (request.getParameter("targets[]")).split(",");
			// LOGGER.log(Level.INFO, "tsel: " +

			if (request.getParameter("strand").equals("1")) {
				strand = true;
			} else {
				strand = false;
			}
			// choice for what to highlight in the plot
			if (request.getParameter("highlighter").equals("1")) {
				disciminateUpDown = true;
			} else {
				disciminateUpDown = false;
			}

			out = response.getWriter();
			// check for the genomic region file
			if (trackid == null) {
				out.println("{\"message\":\"Upload and select the input file before to start! \"}");
				return;
			}
			if (trackid3 == null) {
				out.println("{\"message\":\"Upload and select the expression file before to start! \"}");
				return;
			}

    	}catch(Exception e){
		 	out.println( "{\"message\":\"Wrong input values. Check parameters types and sizes. \"}");
		 	return;
    	}
//		LOGGER.log(Level.INFO, "Trackid for NearbyGenes: " + trackid ); /*used the logger to print in console the name of the uploaded file used for the analysis*/
//		LOGGER.log(Level.INFO, "assembly: " + assembly ); 
//		LOGGER.log(Level.INFO, "gtype: " + gtype ); 
//		LOGGER.log(Level.INFO, "tsel: " + request.getParameter("targets[]").length() ); 
//		LOGGER.log(Level.INFO, "strand: " + strand ); 
//		LOGGER.log(Level.INFO, "Value for expression set: " + trackid3 ); 
		/* construct a new source object with this source, host, user*/
		Source source = new Source("sql","genome-mysql.cse.ucsc.edu","genome");

		////////////////////
		// Manage session //
		////////////////////

		session = request.getSession();
		
		customAnno = true;

		@SuppressWarnings("unchecked")
		HashMap<String, ArrayList<CustomDataset>> customDatasets = (HashMap<String, ArrayList<CustomDataset>>) session
				.getAttribute("customDatasets");

		response.setContentType("application/json;charset=utf-8");

		/*
		 * control to check if we have uploaded yet any id lists file and, if we had,
		 * store the contents in an array named "list_inputsid"
		 */
		if (customDatasets.isEmpty() == true) {
			out.println("{\"message\":\"Please upload a custom annotation file first!\"}");
			return;
		} else {
			customList = customDatasets.get(trackid3);
			colSelect = customList.listIterator(0).next().getLabels().size();
		}
		
//		control if the custom annotation file contains the log2FC field
		log2fc = false;
		String[] log2fcTypo = { "log2fc", "log2foldchange", "log2 fc", "log2 foldchange", "log2 fold change" };
		for (int c = 0; c < colSelect; c++) {
			for (int i = 0; i < log2fcTypo.length; i++) {
				if (customList.listIterator(0).next().getLabels().get(c).equalsIgnoreCase(log2fcTypo[i])) {
					log2fcPOS = c;
					log2fc = true;
					break;
				}
			}
		}
		if (!log2fc) {
			out.println("{\"message\":\"Your custom annotation file must contain the log2FC field!\"}");
			return;
		}

		response.setContentType("application/json;charset=utf-8");
		
		@SuppressWarnings("unchecked")
		Map<String, UserTrack> usertracks = (HashMap<String, UserTrack>) session.getAttribute("usertracks");
		UserTrack datatrack;

		 /*control to check if the user has uploaded at least one file*/
		 if( usertracks.isEmpty() == true ) {
		 	out.println( "{\"message\":\"Please upload a bed-file first.\"}");
		 	return;
		 } else {
		 	datatrack = usertracks.get(trackid);/*datatrack contains the file name*/
		 }
		 if( datatrack == null ) {
		 	out.println( "{\"message\":\"An error occurred. Track not found: "+trackid+"\"}");
		 	return;
		 }

     	///////////////////////////////////
		///// Calculate and send results //
		///////////////////////////////////

		// check if contains only one peak
		GenomeRegionTrack peaktrack = new GenomeRegionTrack(datatrack.trackid, session.getId(), source);

//		int size = 0;
//		for (Chromosome regionsPerChrom : peaktrack) {
//			size += regionsPerChrom.getRegionNumber();
//		}
//		if (size > 1) {/**/
//			out.println("{\"message\":\"For the NearbyGenes analysis you must provide one peak per time!\"}");
//			return;
//		}

		GenomeAnnotation genome = new GenomeAnnotation(assembly, source, gtype);
		geneIdConvertion(customAnno);

		customListMap = new HashMap<String, CustomDataset>();
		for (CustomDataset item : customList) {
			String key = testList.dict.get(item.getSymbol());
			customListMap.put(key, item);
		}

		 try {

	     	if ( request.getParameter("targets[]").equals("0,0,0,0,0,0,0,0,0,0,0") ) {
//				startTime = System.nanoTime();
				parseIntersections(peaktrack, genome);
//				endTime = System.nanoTime();
//				totalTime = endTime - startTime;
//				System.out.println("first parse: " + (double) totalTime / 1_000_000_000.0);

	     		// LOGGER.log(Level.INFO, "printNearbyGenesResultsJSON " + printNearbyGenesResultsJSON() ); 
//				startTime = System.nanoTime();

				out.println("{" + printNearbyGenesResultsJSON() + ",\"message\":\"Choose a target-region\","
						+ getLog2FC() + "}");
//				System.out.println("{" + printNearbyGenesResultsJSON() + ",\"message\":\"Choose a target-region\","
//						+ getLog2FC() + "}");

//				endTime = System.nanoTime();
//				totalTime = endTime - startTime;
//				System.out.println("time to printNearbyGenesResultsJSON 1: " + (double) totalTime / 1_000_000_000.0);
	     	 
	     	}else {

//				startTime = System.nanoTime();
//				endTime = System.nanoTime();
//				totalTime = endTime - startTime;
//				System.out.println("time to geneIdConvertion: " + (double) totalTime / 1_000_000_000.0);

//				startTime = System.nanoTime();
	     		parseIntersections(peaktrack, genome);
//				endTime = System.nanoTime();
//				totalTime = endTime - startTime;
//				System.out.println("first parse: " + (double) totalTime / 1_000_000_000.0);

	     		this.targetintervals = combineTargetIntervalSelection();/*array representing the regions selected by the user in the barplot*/

	     		this.targetgenes= getTargetGenes(); //build an HashMap merging all the genes belonging to the regions selected by the user
	     		
//				startTime = System.nanoTime();;
				out.println("{" + printNearbyGenesResultsJSON() + "," + geneResults2JSON() + "," + getLog2FC() + "}");
//				System.out.println(
//						"{" + printNearbyGenesResultsJSON() + "," + geneResults2JSON() + "," + getLog2FC() + "}");
//				endTime = System.nanoTime();
//				totalTime = endTime - startTime;
//				System.out.println("time to printNearbyGenesResultsJSON 2: " + (double) totalTime / 1_000_000_000.0);
	     	}

		 } catch (ChromIDException e) {
		 	out.println("{\"message\":\""+e.getMessage()+"! ... Make sure your organism-selection is correct.\"}");
		 	return;
		 }


    }

	/**
	 * 
	 * @param track
	 * @param genome
	 * @throws ChromIDException
	 */
	 public void parseIntersections(GenomeRegionTrack track, GenomeAnnotation genome) throws ChromIDException {

		 	chromosome = null;
		 	gene=null;
		 	geneidsL1 = new HashMap<String, List<String>>();
		 	geneidsL2 = new HashMap<String, List<String>>();
		 	geneidsL3 = new HashMap<String, List<String>>();
		 	geneidsL4 = new HashMap<String, List<String>>();
		 	geneidsL5 = new HashMap<String, List<String>>();
		 	geneidsR1 = new HashMap<String, List<String>>();
		 	geneidsR2 = new HashMap<String, List<String>>();
		 	geneidsR3 = new HashMap<String, List<String>>();
		 	geneidsR4 = new HashMap<String, List<String>>();
		 	geneidsR5 = new HashMap<String, List<String>>();
		 	geneidsOverlap = new HashMap<String, List<String>>();
		 	ListIterator<Region> iterator=null;
		 	boolean it_next = true;
		 	boolean it_ended = false;

		for (Chromosome chromtrack : track) {
			 		
			chromosome = genome.getChromByID(chromtrack.pid);

	 		if (chromosome.getRegionNumber() == 0) {/**/
//	 			LOGGER.log(Level.INFO, "Genome-annotation has no genes for chromosome: " + chromtrack.pid );
	 			continue;
	 		}
	 		
			for (Region region : chromtrack) {

		 		it_next = true;
		 		it_ended = false;
		 		iterator = chromosome.listIterator();
		 		index=0; indexL=0; indexR=0;  downCounter=0; upCounter=0;
			 	collectionDown = new TreeMap<Long,Transcript>();
			 	collectionUp = new TreeMap<Long,Transcript>();	
	 			
	 			if (it_ended) {	 				
	 				
//					System.out.println("ENDED");

	 			} else {
	 				
//	 				System.out.println("uploaded region");
//	 				System.out.println("region L " + region.getLeft());
//	 				System.out.println("region R " + region.getRight());

					outer: while (true) {
	 					
	 					if (it_next) {

	 						if ( iterator.hasNext() ) {

								gene = (Transcript) iterator.next();
	 							
							} else {
	 							
	 							// System.out.println("Last gene " + gene.symbol);
	 							// System.out.println("index Left " + indexL);
	 							// System.out.println("index Right " + indexR);
		 						
								indexR = index;
	 							buildNearbyGenes();
	 							it_next = false;
	 							it_ended = true;
	 							break;
	 						}
	 					}
 						if(!gene.canonical) {
							while (!gene.canonical) {
								if (iterator.hasNext()) {
									++index;
									gene = (Transcript) iterator.next();
								} else {
									continue outer;
								}
							}
						}
						if (gene.isLeftOf(region)) {
							// System.out.println("gene left");
	 						indexL = index;
							index++;
							
						} else if (gene.isRightOf(region)) {
	 						// System.out.println("gene right");

	 						indexR = index;		
							buildNearbyGenes();
							it_next = false;
							it_ended = true;
	 						break;
	 					
						} else {
//							System.out.println("overlapping gene: " + gene.symbol);
//							System.out.println("overlapping gene: " + gene.pid);
	 						
	 						if(gene.strand) {
	 							strandGene = "+";
	 						}else {
	 							strandGene = "-";
							}
							List<String> list = Arrays.asList(gene.pid, strandGene);
							if (geneidsOverlap.containsKey(gene.symbol)) {
							 }else {
								geneidsOverlap.put(gene.symbol, list);
							 }
							index++;
						}
	 				}
	 				
	 			}
	 			
	 		}
	 		
	 	}
	 	
	 }

	 /**This method return the array representing the target intervals selected by the user (barplot)
	  * 
	  * @return targets a list of integer
	  */
	 private List<Integer> combineTargetIntervalSelection() {

	 	List<Integer> targets = new ArrayList<Integer>();

	 	for( int i=0; i<tsel.length; i++ ) {
	 		if ( tsel[i].equals("1") ) {
				targets.add(i);
//	 			LOGGER.log(Level.INFO, "Targets: " + i);
	 		}
	 	}
	 	return targets;

	 }
	 
	 /**This method populate each HashMap referring to each position and also do a check of the strand in case of consensus option
	  * 
	  */	 
	 public void buildLists () {	 

		if (side.equals("Down")) {
		 		// System.out.println("Downstream");
				 downCounter=0;
				 while(downCounter<5) {
					 gene=(Transcript)chromosome.listIterator(indexR).next();
//					 System.out.println(gene.symbol);
					 	 
				if (!gene.canonical) {
						 while(!gene.canonical) {
						 ++indexR;
						 gene=(Transcript)chromosome.listIterator(indexR).next();
	 
					 }} 
					 
				if (gene.strand == true) {
 
						collectionDown.put(gene.getLeft(),gene);
						++downCounter;
						++indexR;

					}else {

						collectionDown.put(gene.getRight(),gene);
					++indexR;
				}
				 }
				 
			 }else{
				 
//				 System.out.println("Upstream");
				 upCounter=0;
			while (upCounter < 5) {
//					 System.out.println(gene.symbol);					 
					 gene=(Transcript)chromosome.listIterator(indexL).next();
					 
					 if(!gene.canonical) {
					 	while(!gene.canonical){//store only canonical		
						--indexL;
						gene = (Transcript) chromosome.listIterator(indexL).next();
					 	}
					 }
						 
				if (gene.strand == false) {
						collectionUp.put(gene.getRight(),gene);
						++upCounter;
					--indexL;
				} else {
						collectionUp.put(gene.getLeft(),gene);
						--indexL; 
					}
						 
						 
				 }
			 }		
			 
	 }
	 

	 /**This method builds a count for each region (es. R1 for the first gene to the right) and an HashMap for each region containing transcript id and gene symbol
	  * 
	  */
	 public void storeResults() {
		 
		// System.out.println("store results");
		if(side.equals("Down")){

			downCounter = 0;
			// System.out.println("size down "+collectionDown.size());
			while(downCounter<5) {

				gene = collectionDown.get(collectionDown.firstKey()) ;

				if(strand) {
					while(gene.strand==false) {
						collectionDown.remove(collectionDown.firstKey());
						gene = collectionDown.get(collectionDown.firstKey()) ;
					}
				}

				if(gene.strand) {
					strandGene = "+";
				}else {
					strandGene = "-";
				}
				
				List<String> list = Arrays.asList(gene.pid, strandGene);
				 
				 switch(downCounter){
				 	case 0:
				 			
						 downCounter++;
						 
					if (geneidsR1.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsR1.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsR1.get(gene.pid));						 
						 collectionDown.remove(collectionDown.firstKey());
						 break;
						 
				 	case 1:
						 downCounter++;
						 
					if (geneidsR2.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsR2.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsR2.get(gene.pid));
						 collectionDown.remove(collectionDown.firstKey());
						 break;
						 
				 	case 2:
						 downCounter++;
						 
					if (geneidsR3.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsR3.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsR3.get(gene.pid));
						 collectionDown.remove(collectionDown.firstKey());
						 break;
						 
				 	case 3:
						 downCounter++;
						 
					if (geneidsR4.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsR4.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsR4.get(gene.pid));
						 collectionDown.remove(collectionDown.firstKey());
						 break;
						 
				 	case 4:
						 downCounter++;
						 
					if (geneidsR5.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsR5.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsR5.get(gene.pid));
						 collectionDown.remove(collectionDown.firstKey());
						 break;

				 }
				
			}

			
		} else {
			
			upCounter = 0;
			// System.out.println("size up     "+collectionUp.size());
			while(upCounter<5) {
				
				gene = collectionUp.get(collectionUp.lastKey()) ;

				if(strand) {
					while(gene.strand==true) {
						collectionUp.remove(collectionUp.lastKey());
						gene = collectionUp.get(collectionUp.lastKey()) ;
					}
				}
			
				if(gene.strand) {
					strandGene = "+";
				}else {
					strandGene = "-";
				}
				
				List<String> list = Arrays.asList(gene.pid, strandGene);
				 
				 switch(upCounter){
				 	case 0:
				 		
						 upCounter++;
						 
					if (geneidsL1.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsL1.put(gene.symbol, list);
						 }
						 // System.out.println(geneidsL1.get(gene.pid));
						 collectionUp.remove(collectionUp.lastKey());
						 break;
						 
				 	case 1:
						 upCounter++;
						 
					if (geneidsL2.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsL2.put(gene.symbol, list);
						 }
						 // System.out.println(geneidsL2.get(gene.pid));
						 collectionUp.remove(collectionUp.lastKey());
						 break;
						 
				 	case 2:
						 upCounter++;
						 
					if (geneidsL3.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsL3.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsL3.get(gene.pid));
						 collectionUp.remove(collectionUp.lastKey());
						 break;
						 
				 	case 3:
						 upCounter++;
						 
					if (geneidsL4.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsL4.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsL4.get(gene.pid));
						 collectionUp.remove(collectionUp.lastKey());
						 break;
						 
				 	case 4:
						 upCounter++;
						 
					if (geneidsL5.containsKey(gene.symbol)) {
							 
						 }else {
						geneidsL5.put(gene.symbol, list);
						 }
//						 System.out.println(geneidsL5.get(gene.pid));
						 collectionUp.remove(collectionUp.lastKey());
						 break;

				 }
				
			}
			
			
		}
		 
	 }
	 
	 
	 
	 
	 /**This method builds a count for each region (es. R1 for the first gene to the right) and an HashMap for each region containing transcript id and gene symbol
	  * 
	  */
	 public void buildNearbyGenes() {
		 
		 try {
//			System.out.println("build nearby genes down");
			 
			side = "Down";
			buildLists();
			storeResults();				

		 	} catch (RuntimeException exception) {


			}
			
		try{
//			System.out.println("build nearby genes up");
			
			side = "Up";
			buildLists();
			storeResults();	
			
			} catch (RuntimeException exception) {

			 
			}
	 }
		

	 /**This method merge the containt of the different HashMap in according to the user choice
	  * 
	  * @return tgenes an HashMap containing the results
	  */
	 private HashMap<String, List<String>> getTargetGenes() {

		 	HashMap<String, List<String>> tgenes = new HashMap<String, List<String>>();
		 	

			for( int target : this.targetintervals) {
//				LOGGER.log(Level.INFO, "target"+target);
				switch( target) {
				case 0:
					tgenes.putAll(geneidsL5);
					break;
				case 1:
					tgenes.putAll(geneidsL4);
					break;
				case 2:
					tgenes.putAll(geneidsL3);
					break;
				case 3:
					tgenes.putAll(geneidsL2);
					break;
				case 4:
					tgenes.putAll(geneidsL1);
					break;
				case 5:
					tgenes.putAll(geneidsOverlap);
					break;
				case 6:
					tgenes.putAll(geneidsR1);
					break;
				case 7:
					tgenes.putAll(geneidsR2);
					break;
				case 8:
					tgenes.putAll(geneidsR3);
					break;
				case 9:
					tgenes.putAll(geneidsR4);
					break;
				case 10:
					tgenes.putAll(geneidsR5);
					break;
			}}
//			LOGGER.log(Level.INFO, "number of genes"+tgenes.size());
			return tgenes;

		}
	 
	 
	 /**This method will prepare a string starting from the genes counts (L1,L2..ecc) to build the barplot
	  * 
	  * @return string
	  */
	 public String printNearbyGenesResultsJSON() {

			String flankcov = "[";
			
			if ( geneidsL5.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsL5.size()) + "],";
			}
				
			if ( geneidsL4.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsL4.size()) + "],";
			}
			
			if ( geneidsL3.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsL3.size()) + "],";
			}
			
			if ( geneidsL2.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsL2.size()) + "],";
			}
			
			if ( geneidsL1.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsL1.size()) + "],";
			}
			
			if ( geneidsOverlap.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsOverlap.size()) + "],";
			}
			
			if ( geneidsR1.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsR1.size()) + "],";
			}
			
			if ( geneidsR2.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsR2.size()) + "],";
			}
			
			if ( geneidsR3.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsR3.size()) + "],";
			}
			
			if ( geneidsR4.size() == 0 ) {
				flankcov += "[0],";
			} else {
				flankcov += "[" +  ((int)geneidsR4.size()) + "],";
			}
			
			if ( geneidsR5.size() == 0 ) {
				flankcov += "[0]]";
			} else {
				flankcov += "[" +  ((int)geneidsR5.size()) + "]]";
			}

			return "\"flankcov\":"+flankcov;
			
	}

	/**
	 * This method will prepare a string starting from the genes counts (L1,L2..ecc)
	 * to build the barplot
	 * 
	 * @return string
	 */
	public String returnLog(String geneID) {
		String LOG2FC = "0";
		switch (log2fcPOS) {
		case 1:
			if (customListMap.containsKey(geneID)) {
			CustomDataset item = customListMap.get(geneID);
			LOG2FC = String.valueOf(item.getLabel2());
			if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
					|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
				LOG2FC = "";
			}
			}
//			for (CustomDataset item : customList) {
//				String geneName = testList.dict.get(item.getSymbol());
//				if (geneID.equals(geneName)) {
//					LOG2FC = String.valueOf(item.getLabel2());
//					if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
//							|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
//						LOG2FC = "";
//					}
//				}
//
//			}
			
			break;
		case 2:
			if (customListMap.containsKey(geneID)) {
			CustomDataset item2 = customListMap.get(geneID);
			System.out.println(item2);
			LOG2FC = String.valueOf(item2.getLabel3());
			System.out.println(LOG2FC);
			if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
					|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
				LOG2FC = "";
			}
			}
//			for (CustomDataset item : customList) {
//				String geneName = testList.dict.get(item.getSymbol());
//				if (geneID.equals(geneName)) {
//					LOG2FC = String.valueOf(item.getLabel3());
//					if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
//							|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
//						LOG2FC = "";
//					}
//				}
//
//			}
			break;
		case 3:
			if (customListMap.containsKey(geneID)) {
			CustomDataset item3 = customListMap.get(geneID);
			LOG2FC = String.valueOf(item3.getLabel4());
			if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
					|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
				LOG2FC = "";
			}
			}
//			for (CustomDataset item : customList) {
//				String geneName = testList.dict.get(item.getSymbol());
//				if (geneID.equals(geneName)) {
//					LOG2FC = String.valueOf(item.getLabel4());
//					if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
//							|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
//						LOG2FC = "";
//					}
//				}
//
//			}
			break;
		case 4:
			if (customListMap.containsKey(geneID)) {
			CustomDataset item5 = customListMap.get(geneID);
			LOG2FC = String.valueOf(item5.getLabel5());
			if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
					|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
				LOG2FC = "";
			}
			}
//			for (CustomDataset item : customList) {
//				String geneName = testList.dict.get(item.getSymbol());
//				if (geneID.equals(geneName)) {
//					LOG2FC = item.getLabel5();
//					if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
//							|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
//						LOG2FC = "";
//					}
//				}
//
//			}
			break;
		case 5:
			if (customListMap.containsKey(geneID)) {
			CustomDataset item6 = customListMap.get(geneID);
			LOG2FC = String.valueOf(item6.getLabel6());
			if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
					|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
				LOG2FC = "";
			}
			}
//			for (CustomDataset item : customList) {
//				String geneName = testList.dict.get(item.getSymbol());
//				if (geneID.equals(geneName)) {
//					LOG2FC = String.valueOf(item.getLabel6());
//					if ((LOG2FC.equalsIgnoreCase("NA")) || (LOG2FC.equalsIgnoreCase("Inf"))
//							|| (LOG2FC.equalsIgnoreCase("-Inf"))) {
//						LOG2FC = "";
//					}
//				}
//
//			}
			break;

		}

		return LOG2FC;
	}

	/**
	 * This method will prepare a string starting from the genes counts (L1,L2..ecc)
	 * to build the barplot
	 * 
	 * @return string
	 */
	public String getLog2FC() {

		String log2fcValues = "[";

		if (geneidsL5.size() == 0) {
			log2fcValues += "[[0][0]],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsL5.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();

				if (testList.dictRef.containsKey(test)) {
					if (Float.valueOf(returnLog(test)) > 0) {
						countUp++;
					} else if (Float.valueOf(returnLog(test)) < 0) {
						countDown++;
					}
				}
			}
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsL5.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";

			}
		}

		if (geneidsL4.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsL4.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println(countUp);
//			System.out.println(countUp);
//			System.out.println("done returnlog  L4");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsL4.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";

			}
		}

		if (geneidsL3.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsL3.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println(countUp);
//			System.out.println(countUp);
//			System.out.println("done returnlog  L3");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsL3.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";

			}
		}

		if (geneidsL2.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsL2.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  L2");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsL2.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsL1.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsL1.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  L1");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsL1.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsOverlap.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsOverlap.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  ov");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsOverlap.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsR1.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsR1.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  R1");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsR1.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsR2.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsR2.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  R2");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsR2.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsR3.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsR3.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  R3");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsR3.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsR4.size() == 0) {
			log2fcValues += "[0],";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsR4.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  R4");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]],";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]],";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]],";
				} else {
					log2fcValues += "[[0]],";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsR4.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]],";
			}
		}

		if (geneidsR5.size() == 0) {
			log2fcValues += "[0]]";
		} else {
			int countUp = 0;
			int countDown = 0;
			Iterator<String> it = geneidsR5.keySet().iterator();
			while (it.hasNext()) {
				String test = it.next();
				if (testList.dictRef.containsKey(test)) {
				if (Float.valueOf(returnLog(test)) > 0) {
					countUp++;
				} else if (Float.valueOf(returnLog(test)) < 0) {
					countDown++;
				}
				}
			}
//			System.out.println("done returnlog  R5");
			if (disciminateUpDown) {
				if (countUp > countDown) {
					log2fcValues += "[[" + String.valueOf(countUp) + "],[" + String.valueOf(countDown) + "]]]";
				} else if (countUp < countDown) {
					log2fcValues += "[[" + String.valueOf(-countDown) + "],[" + String.valueOf(countUp) + "]]]";
				} else if ((countUp == countDown) && (countUp != 0)) {
					log2fcValues += "[[\"NaN\"],[" + String.valueOf(countUp) + "]]]";
				} else {
					log2fcValues += "[[0]]]";
				}

			} else {
				double ratio = (((double) countUp + (double) countDown) / (double) geneidsR5.size());
				log2fcValues += "[[" + String.valueOf(ratio) + "]]]";
			}
		}
//		System.out.println("log2FC" + log2fcValues);
		return "\"log2FC\":" + log2fcValues;

	}

	/**
	 * This method just build a string starting form the targetgenes (results) to
	 * make it possible to show them to the user (populating a datatable)
	 * 
	 * @return String to populate the DataTable
	 * @throws FileNotFoundException
	 */
	public String geneResults2JSON() throws FileNotFoundException {

		String res = new String();

		res = annotateResults();

		return res;


	}

	public String annotateResults() throws FileNotFoundException {

			
		String resultstring = new String();
		String geneList = new String();
		ArrayList<String> geneListCheck = new ArrayList<String>();
			
			switch(colSelect) {
			
			case 2:{
				


			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

//					System.out.println("strKey " + strKey);

					String geneName = testList.dict.get(item.getSymbol());
//					System.out.println("geneName " + geneName);

					String transcrName = testList.dictRef.get(strKey);
//					System.out.println("transcrName " + transcrName);


					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

					}


				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
						+ transcrName + "\",\"" 
						+ targetgenes.get(strKey).get(1) + "\",\"" + "" + "\"],";

			}
				
			resultstring = resultstring.substring(0, resultstring.length()-1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0)
					+ "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\"],\"data\":[" + resultstring + "]";
		}

			
			
			case 3:{
				
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);


					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

						}

				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\"],";

				}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\"],\"data\":[" + resultstring + "]";
			}
			
			case 4:{
				
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

					}


				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\""
					+ types.get(2) + "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\"],\"data\":[" + resultstring + "]";
		}
			case 5:{
				
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\",\"" + item.getLabel5()
								+ "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

					}

				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\",\"" + "" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\""
					+ types.get(2) + "\",\"" + types.get(3)
					+ "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\"],\"data\":[" + resultstring + "]";
		}
			case 6:{
				
			outer: for (String strKey : targetgenes.keySet()) {

				for (CustomDataset item : customList) {

					String geneName = testList.dict.get(item.getSymbol());
					String transcrName = testList.dictRef.get(strKey);

					if (strKey.equals(geneName)) {

						resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\""
								+ transcrName + "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + item.getLabel2()
								+ "\",\"" + item.getLabel3() + "\",\"" + item.getLabel4() + "\",\"" + item.getLabel5()
								+ "\",\"" + item.getLabel6() + "\"],";
						if (testList.dictRef.get(strKey) != null && !testList.dictRef.get(strKey).trim().isEmpty()
								&& !geneListCheck.contains(testList.dictRef.get(strKey))) {
							geneListCheck.add(testList.dictRef.get(strKey));
							geneList += "\"" + testList.dictRef.get(strKey) + "\",";
						}
						continue outer;

					}


				}

				String transcrName = testList.dictRef.get(strKey);

				resultstring += "[\"" + targetgenes.get(strKey).get(0) + "\",\"" + strKey + "\",\"" + transcrName
						+ "\",\"" + targetgenes.get(strKey).get(1) + "\",\"" + "" + "\",\"" + "" + "\",\"" + ""
						+ "\",\"" + "" + "\",\"" + "" + "\"],";

			}

			resultstring = resultstring.substring(0, resultstring.length() - 1);
			if (geneList != null && !geneList.isEmpty()) {
				geneList = geneList.substring(0, geneList.length() - 1);
			} else {
				geneList = "null";
			}
			ArrayList<String> types = customList.listIterator(0).next().getLabelTypes();

			return "\"labeltypes\":[\"text\",\"text\",\"text\",\"text\",\"" + types.get(0) + "\",\"" + types.get(1)
					+ "\",\""
					+ types.get(2) + "\",\"" + types.get(3) + "\",\"" + types.get(4)
					+ "\"],\"geneList\":[" + geneList
					+ "],\"titles\":[\"Transcript ID\",\"Gene ID\",\"Gene Symbol\",\"Strand\",\""
					+ customList.listIterator(0).next().getLabels().get(1) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(2) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(3) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(4) + "\",\""
					+ customList.listIterator(0).next().getLabels().get(5) + "\"],\"data\":[" + resultstring + "]";
		}
			
		}
			return "number of columns not allowed!";

		}



	/**
	 * This method creates the dictionaries containing the gene ids references
	 * 
	 * @throws FileNotFoundException
	 */	
	public void geneIdConvertion(boolean customAnno) throws FileNotFoundException {
		
		ArrayList<String> test = new ArrayList<String>();
		
		if(customAnno) {
			Iterator<CustomDataset> iterator = customList.iterator();
			while (iterator.hasNext()) {
				test.add(iterator.next().getSymbol());
			}
		}

		// initialize the converter
		testList = new ConvertIDs(assembly, gtype, test);

		// read IDs test table
		String converter = "geneIDconverter/" + assembly + ".txt";

		String urlConverter = getClass().getClassLoader().getResource(converter).toString().split(":")[1];

		// read IDs test table
		IDFile = new File(urlConverter);

		// create the two dictionaries needed for the Ids convertion
		testList.createDict(testList, IDFile);


	}

}