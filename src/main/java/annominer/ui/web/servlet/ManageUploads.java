package annominer.ui.web.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
//import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import annominer.CustomDataset;
import annominer.GenomeRegionTrack;
import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.ui.web.UserTrack;
import tools.datastructures.list.ListOperations;
import tools.datastructures.set.SetOperations;

///////////////////
// ManageUploads //
///////////////////

//This servlet is used to manage the upload of the user provided files as well as the test files

@WebServlet(name="ManageUploads", urlPatterns={"/ManageUploads"})
@MultipartConfig
public class ManageUploads extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
//	private final static Logger LOGGER = Logger.getLogger(ManageUploads.class.getCanonicalName());

	private HttpSession session;
	private String row;
	private boolean example = false;
	private Integer colSelect;
	private String listFromMitox;

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		////////////////////
		// Manage session //
		////////////////////
		
		session = request.getSession();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

//		LOGGER.log(Level.INFO, "New session created: "
//				+ session.isNew());/* print to the console if the session is the new one or not */
//		LOGGER.log(Level.INFO, "SessionID: " + session.getId());/* print to the console the id of the session */
		
		@SuppressWarnings("unchecked")
		Map<String, UserTrack> usertracks = (HashMap<String, UserTrack>) session.getAttribute("usertracks");
		
		/*check if we have uploaded yet any file*/
		if( usertracks == null ) { usertracks = new HashMap<String,UserTrack>(); }
		
		@SuppressWarnings("unchecked")
		Map<String, Set<String>> idlists = (Map<String, Set<String>>) session.getAttribute("idlists");
		
		if (idlists == null) {
			idlists = new HashMap<String, Set<String>>();
		}
		
		
		@SuppressWarnings("unchecked")
		HashMap<String,ArrayList<CustomDataset>> customDatasets = (HashMap<String, ArrayList<CustomDataset>>) session.getAttribute("customDatasets");
		
		if( customDatasets == null ) { customDatasets = new HashMap<String,ArrayList<CustomDataset>>(); }		
				
		String assembly = request.getParameter("assembly");/* assembly is the value referring to the reference genome */
		String trackid = request.getParameter("trackid");/*trackid contains the value of the variable "ianno" that is the name of the uploaded file*/
		String tracktype = request.getParameter("ul_type");/*ul_type contains the choice between the IDlist/Genomic regions/custom upload*/

//		LOGGER.log(Level.INFO, "Assembly for upload: "
//				+ assembly); /*
//								 * print to the console the reference genome selected: at the beginning is empty
//								 */
//		LOGGER.log(Level.INFO, "Track id: " + trackid); /* print to the console the name of the file */
//		LOGGER.log(Level.INFO, "Track type: "
//				+ tracktype); /* print to the console the type of the track (ID list or genomic regions) */
		
		response.setContentType("application/json;charset=utf-8");

		final PrintWriter writer = response.getWriter();
		

		String tempfile = null; 
		String format_error = null;
		example = false;
		// check if it is a test file request
		try {
			if (trackid.equals("IDLIST_EXAMPLE")) {
				String path = "test/ENCFF706DRE_REST_promoter.txt";
				tempfile = getClass().getClassLoader().getResource(path).toString().split(":")[1];
				;
				example = true;

			} else if (trackid.equals("BED_EXAMPLE")) {
				String path = "test/ENCFF706DRE_REST.bed";
				tempfile = uploadToFileTest(
						getClass().getClassLoader().getResource(path).toString().split(":")[1]);
				example = true;

			} else if (trackid.equals("CUSTOM_EXAMPLE")) {
				String path = "test/ENCFF706DRE_REST_promoter_fakeAnnotation.txt";
				tempfile = getClass().getClassLoader().getResource(path).toString().split(":")[1];
				example = true;

			} else if (trackid.startsWith("mitoxToAnno14")) {

				listFromMitox = request.getParameter("geneList");

			} else {
				String size = sizeCheck(request);
				if (size.equals("max")) {

					writer.println("{\"message\": \"The uploaded file is too big! (max size: 10Mb)\"}");
					writer.close();
					return;
				} else {
					tempfile = uploadToFile(request);
				}
			}

//			LOGGER.log(Level.INFO, "tempfile: " + tempfile );
			
			/* genomic regions upload */
			if( tracktype.equals("peaks") ) {
				
				Source source = new Source("file");
				source.fileformat = "bed";
				source.filepath = tempfile;
				new GenomeRegionTrack(trackid, session.getId(), source);
				
				usertracks.put(trackid, new UserTrack(trackid, assembly, tracktype, dateFormat.format(date)));
				
				/* gene set ids upload */
			} else if ( tracktype.equals("idlist") ) {
				
				Set<String> idlist;

				if (trackid.startsWith("mitoxToAnno14")) {
					trackid = trackid.replace("ToAnno14", "");
					idlist = new HashSet<String>(Arrays.asList(listFromMitox.split("\\s+")));
				} else {
					idlist = SetOperations.readSetFromFile(tempfile, 1);
				}

				idlists.put(trackid, idlist);
				usertracks.put(trackid, new UserTrack(trackid, assembly, tracktype, dateFormat.format(date)));
				
				// optional annotation file upload
			}else if (tracktype.equals("custom annotation") ) {
				
				ArrayList<String> labels = new ArrayList<String>();
				ArrayList<String> labeltypes = new ArrayList<String>();
				ArrayList<CustomDataset> customList = new ArrayList<CustomDataset>();
				String formatSelect = new String();
				
				if (example) {

					colSelect = 3;
					
					labels.add("Gene_ID");
					labels.add("Log2FC");
					labels.add("FDR");

					labeltypes.add("num");
					labeltypes.add("num");
					formatSelect = "\t"; //tsv
					

				} else {

					colSelect = Integer.valueOf(request.getParameter("colSelect"));

					labels.add("Gene_ID");
					for (Integer i = 2; i <= colSelect; ++i) {
						labels.add(request.getParameter("Label " + i));
					}

					for (Integer i = 3; i <= (colSelect + 1); ++i) {
						labeltypes.add(request.getParameter("type" + i));
					}

					if (request.getParameter("formatSelect").equals("1")) {
						formatSelect = ","; // csv
					} else {
						formatSelect = "\t"; // tsv
					}

				}
				
				// in case not the example file we will check the columns content (up to 6
				// columns)
				try {
				switch(colSelect) {
				
				case  2:{

						 Iterator<String> iterator1 = ListOperations.readListFromFile( tempfile, 1,formatSelect).iterator();
						 Iterator<String> iterator2 = ListOperations.readListFromFile( tempfile, 2,formatSelect).iterator();		

						 while(iterator1.hasNext()) {

							customList.add(new CustomDataset(iterator1.next(), iterator2.next(), labels, labeltypes));
							 
								}
						 
						 break;}
				
				case  3:{ Iterator<String> iterator1 = ListOperations.readListFromFile( tempfile, 1,formatSelect).iterator();
						 Iterator<String> iterator2 = ListOperations.readListFromFile( tempfile, 2,formatSelect).iterator();
						 Iterator<String> iterator3 = ListOperations.readListFromFile( tempfile, 3,formatSelect).iterator();
						 while(iterator1.hasNext()) {

							customList.add(new CustomDataset(iterator1.next(), iterator2.next(), iterator3.next(),
									labels, labeltypes));

								}						 
						 break;}
				
				case  4: {Iterator<String> iterator1 = ListOperations.readListFromFile( tempfile, 1,formatSelect).iterator();
				 		 Iterator<String> iterator2 = ListOperations.readListFromFile( tempfile, 2,formatSelect).iterator();
				 		 Iterator<String> iterator3 = ListOperations.readListFromFile( tempfile, 3,formatSelect).iterator();
				 		 Iterator<String> iterator4 = ListOperations.readListFromFile( tempfile, 4,formatSelect).iterator();
						 while(iterator1.hasNext()) {

							customList.add(new CustomDataset(iterator1.next(), iterator2.next(), iterator3.next(),
									iterator4.next(), labels, labeltypes));

								}	
				 		 break;}
				case  5:{ Iterator<String> iterator1 = ListOperations.readListFromFile( tempfile, 1,formatSelect).iterator();
						 Iterator<String> iterator2 = ListOperations.readListFromFile( tempfile, 2,formatSelect).iterator();
						 Iterator<String> iterator3 = ListOperations.readListFromFile( tempfile, 3,formatSelect).iterator();
						 Iterator<String> iterator4 = ListOperations.readListFromFile( tempfile, 4,formatSelect).iterator();
						 Iterator<String> iterator5 = ListOperations.readListFromFile( tempfile, 5,formatSelect).iterator();
						 while(iterator1.hasNext()) {

							customList.add(new CustomDataset(iterator1.next(), iterator2.next(), iterator3.next(),
									iterator4.next(), iterator5.next(), labels, labeltypes));

								}	
						 break;}
		
				case  6: {Iterator<String> iterator1 = ListOperations.readListFromFile( tempfile, 1,formatSelect).iterator();
				 		 Iterator<String> iterator2 = ListOperations.readListFromFile( tempfile, 2,formatSelect).iterator();
				 		 Iterator<String> iterator3 = ListOperations.readListFromFile( tempfile, 3,formatSelect).iterator();
				 		 Iterator<String> iterator4 = ListOperations.readListFromFile( tempfile, 4,formatSelect).iterator();
				 		 Iterator<String> iterator5 = ListOperations.readListFromFile( tempfile, 5,formatSelect).iterator();
				 		 Iterator<String> iterator6 = ListOperations.readListFromFile( tempfile, 6,formatSelect).iterator();
						 while(iterator1.hasNext()) {

							customList.add(new CustomDataset(iterator1.next(), iterator2.next(), iterator3.next(),
									iterator4.next(), iterator5.next(), iterator6.next(), labels, labeltypes));

								}	
				 		 break;}
						}
				
				customDatasets.put(trackid, customList);

					usertracks.put(trackid, new UserTrack(trackid, assembly, tracktype, dateFormat.format(date)));

				
				
				}catch (NoSuchElementException | ArrayIndexOutOfBoundsException fcf) {
					
					format_error = "Invalid upload: mismatch between columns declared and columns presents in the uploaded file";
				
				}

			}else {
				
				writer.println( "{\"message\": \"Invalid upload type\"}");
			}
			
			
	    } catch (FileNotFoundException fne) {
	        
//	    	LOGGER.log(Level.INFO, "Error while loading uploaded file to database {0} is being uploaded to {1}", new Object[]{fne.getMessage()} );
	        
	    } catch (FileFormatException ffe) {
	    	
	    	format_error = ffe.getMessage();
//	    	LOGGER.log(Level.WARNING,ffe.getMessage());
	    	
	    } finally {
	    	
			// sort the file to speed up further computations
			if ((tempfile != null) && (!example)) {
				/* it builds two empty files: tempfile and tempfile_sorted */
	        	File file = new File(tempfile);
	    		file.delete();
	    		file = new File(tempfile+"_sorted");
	    		file.delete();
	        }
	        /*Check if we have uploaded files or not*/
	        if(  usertracks.keySet().isEmpty() ) {
	        	
		        	if(format_error==null){
		        		writer.println( "{\"message\":\"The server couldn't upload any files. Please check your format and make sure to select the proper upload type.\"}" );
		        	} else {
		        		writer.println( "{\"message\":\""+format_error+"\"}" );
		        	}
	        	
	        	
	        } else {
	        
		        StringBuffer json = new StringBuffer("[");
				// save the user file name
				for( String id : usertracks.keySet() ) { 
					json.append(usertracks.get(id).toJSON() + ","); 
				}
				
				String outstr =  json.substring(0, json.length()-1) + "]";
				
				// alongside with the file name the date will be returned and showed in the
				// uploaded files datatable
				if(format_error!=null){
					writer.println("{\"message\":\""+format_error+"\",\"data\":"+outstr+"}");
				} else {
					writer.println("{\"data\":"+outstr+"}");
				}
				
	        }
			/*close the writer*/
	        if (writer != null) {
	            writer.close();
	        }
	        
	        
	        
	    }
		/*
		 * set attributes to the session: usertracks, idlists, CustomDataset used later
		 * by the other servlets
		 */
		session.setAttribute("usertracks",usertracks);
		if(idlists != null) { 
			session.setAttribute("idlists",idlists); 
		}
		
		if(customDatasets != null) { 
			session.setAttribute("customDatasets",customDatasets); 
		}
		
		return;
		
	}
	

	/** This method return a string with the name of the file uploaded
	 * @param      part    id to the filename.
	 */
//	private String getFileName(final Part part) {
//	    
////		final String partHeader = part.getHeader("content-disposition");
////	    LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
//	    for (String content : part.getHeader("content-disposition").split(";")) {
//	        if (content.trim().startsWith("filename")) {
//	            return content.substring(
//	                    content.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    
//	    return null;
//	    
//	}
	/**
	 * This method return if the size of the uploaded file is < 10 MB
	 * 
	 * @param request HttpServletRequest.
	 */
	private String sizeCheck(HttpServletRequest request) throws IOException, ServletException {
		final Part filePart = request.getPart("file");/* the id to reach the filename */
		InputStream filecontent = null;
		try {
			filecontent = filePart.getInputStream();
			int max = 10 * 1024 * 1024; // 10MB
			int size = filecontent.available();
			if (size > max) {
				return "max";
			}else {
				return "ok";
			}
	//		filePart.getSize();
		} finally {
		}
	}
	
	/**
	 * this method assign to the tempfile (string variable) the absolute temppath +
	 * tempfilename(id of the session) where the file is uploaded to
	 * 
	 * @param request   HttpServletRequest.
	 * @param tracktype
	 */
	private String uploadToFile(HttpServletRequest request) throws ServletException, IOException {

		final String temppath = System.getProperty("java.io.tmpdir") + File.separator;
		final Part filePart = request.getPart("file");/* the id to reach the filename */
//		final String filename = getFileName(filePart);

		OutputStream out = null;
		InputStream filecontent = null;
		String tempfilename = session.getId();
		
		try {
			out = new FileOutputStream(new File(temppath + tempfilename));
			filecontent = filePart.getInputStream();

	        int read = 0;
	        final byte[] bytes = new byte[1024];
	        
	        while ((read = filecontent.read(bytes)) != -1) {
	        	
				out.write(bytes, 0, read);
	        }
	        
//	        LOGGER.log(Level.INFO, "File {0} is being uploaded to {1}", new Object[]{filename, temppath});/*print to console the absolute path where the file is uploaded*/
	        
	    } catch (FileNotFoundException fne) {
	        
//	        LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", new Object[]{fne.getMessage()});/**/
	        
	    } finally {
	    /*close the file stream*/
	    	if (out != null) {
	            out.close();
	        }
	        if (filecontent != null) {
	            filecontent.close();
	        }
	        
	    }

		return temppath + tempfilename;
		
	}

	/**
	 * this method manage the load of the test files contained internally in the
	 * annominer's folder src/main/resources/test
	 * 
	 * @param path of test file.
	 */
	private String uploadToFileTest(String path) throws IOException {

		final String temppath = System.getProperty("java.io.tmpdir") + File.separator;
//		final String filename = "humanID.txt";/* the file name */

		OutputStream out = null;
		InputStream filecontent = null;
		String tempfilename = session.getId();

		try {
			out = new FileOutputStream(new File(temppath + tempfilename));
			BufferedReader csvReader = new BufferedReader(new FileReader(path));
			while ((row = csvReader.readLine()) != null) {
				String[] data = row.split("\n");
				byte b[] = data[0].getBytes();
				out.write(b);
				out.write('\n');
			}
			csvReader.close();

//			LOGGER.log(Level.INFO, "File {0} is being uploaded to {1}", new Object[] { filename,temppath });/* print to console the absolute path where the file is uploaded */

		} catch (FileNotFoundException fne) {

//			LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", new Object[] { fne.getMessage() });/**/

		} finally {
			/* close the file stream */
			if (out != null) {
				out.close();
			}
			if (filecontent != null) {
				filecontent.close();
			}

		}

		return temppath + tempfilename;
	}
	
	
	
}

