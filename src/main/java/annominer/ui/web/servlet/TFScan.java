package annominer.ui.web.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import annominer.Chromosome;
import annominer.EnrichmentResult;
import annominer.ExperimentAnnotation;
import annominer.GenomeAnnotationSet;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ExperimentDAO;
import annominer.io.database.mongodb3.ExperimentMongoDAO;
import annominer.math.statistics.Statistics;

//////////////
/// TFScan ///
//////////////

//@WebServlet(name="TFScan", urlPatterns={"/TFScan"})
public class TFScan extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	public File IDFile;
	public String organism;
	private ConvertIDs testList;
	private String dynamicRanges;
	private HashMap<String, Integer> cutoff;
	private String row;
	private String promoterUsString;
	private String target;
//	private long startTime;
//	private long endTime;
//	private long totalTime;
	private static String gtype, trackid, enrich;
	private static int ovval, promoterUs, promoterDs;
	private static boolean ovtype;

	static Set<String> list_inputids;
	static Set<String> list_knowninputids;

	static Set<String> symbols_list;
	static Set<String> symbols_listhits;
	static Set<String> symbols_genome;
	static Set<String> symbols_genomehits;

	static Set<String> transids_genome;
	static Set<String> transids_genomehits;
	static Set<String> transids_list;
	static Set<String> transids_listhits;

	static List<EnrichmentResult> enrichmentresults;

	static GenomeAnnotationSet genome;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	respond(request,response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		respond(request,response);
	}


    private void respond(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	    ////////////////////////////////
	    // Parse request-parameters : //
		////////////////////////////////
	
    	try {
			// model organism
			organism = request.getParameter("assembly");
			// chosen resource
			gtype = request.getParameter("gtype");
			// track name
			trackid = request.getParameter("trackid");
			// upstream promoter boundary definition
			promoterUsString = request.getParameter("promoter_us");

			// if the user select dynamic load the dynamic ranges for all the TFs
			if (promoterUsString.equals("dynamic")) {
				loadDynamicRanges();
			} else {
				promoterUs = Integer.valueOf(promoterUsString);
			}
			// downstream promoter boundary definition
//			System.out.println("promoterUs: " + promoterUs);
			promoterDs = Integer.valueOf(request.getParameter("promoter_ds"));
			// TF or HM enrichment
//			System.out.println("promoterDs: " + promoterDs);
			enrich = request.getParameter("enrich");
			// minimum overlap value
//			System.out.println("enrich: " + enrich);
			ovval = Integer.valueOf(request.getParameter("ovval"));
			// overlap in % or bp
//			System.out.println("ovval: " + ovval);
			ovtype = Boolean.valueOf(request.getParameter("ovtype"));

			if (request.getParameter("ovtype").equals("0")) {
				ovtype = true;
			} else {
				ovtype = false;
			}

			if (trackid == null) {

				PrintWriter out = response.getWriter();
				out.println("{\"message\":\"Upload at least an input file before to start!. \"}");
				return;
			}
		} catch (Exception e) {

			PrintWriter out = response.getWriter();
			out.println("{\"message\":\"Wrong input values. Check parameters types and sizes. \"}");
			return;

		}


		////////////////////
		// Manage session //
		////////////////////

		session = request.getSession();

		@SuppressWarnings("unchecked")
		Map<String, Set<String>> idlists = (Map<String, Set<String>>) session.getAttribute("idlists");

		/* gets the collection from ManageUpload */
			
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();/* Prints formatted representations of objects to a text-output stream */

		Set<String> list_inputidsOld;

		if (idlists.isEmpty() == true) {
			out.println("{\"message\":\"Please upload an ID-list file first!\"}");
			return;
		} else {
			list_inputidsOld = idlists.get(trackid); /* array containing all the ids of the selected file */
		}

		if (list_inputidsOld.isEmpty() == true) {
			out.println("{\"message\":\"An error occurred. Is " + trackid + " an empty file?\"}");

			return;
		}

		// Convert the input IDs:

		// cast the input list
		ArrayList<String> used_list = new ArrayList<String>(list_inputidsOld);

		// initialize the converter
		testList = new ConvertIDs(organism, gtype, used_list);

		// read IDs test table
		String converter = "geneIDconverter/" + organism + ".txt";
		String urlConverter = getClass().getClassLoader().getResource(converter).toString().split(":")[1];
		IDFile = new File(urlConverter);

//		startTime = System.nanoTime();

		// create the two dictionaries needed for the Ids convertion
		testList.createDict(testList, IDFile);

//		endTime = System.nanoTime();
//		totalTime = endTime - startTime;
//		System.out.println("time to createDict: " + (double) totalTime / 1_000_000_000.0);

		// convert Ids
//		startTime = System.nanoTime();

		ArrayList<String> newList = testList.convertInputList();
		list_inputids = new HashSet<String>(newList);

//		endTime = System.nanoTime();
//		totalTime = endTime - startTime;
//		System.out.println("time to convertInputList: " + (double) totalTime / 1_000_000_000.0);


		// test
//		System.out.println("Test convertID____________________");
//		Iterator<String> it = list_inputids.iterator();

//		while (it.hasNext()) {

//		System.out.println(it.next());

//		}
//		System.out.println("End Test convertID____________________");


		////////////////////////////////
		// Calculate and send results //
		////////////////////////////////

		Source source = new Source();
//		Source source = new Source("sql", "genome-mysql.cse.ucsc.edu", "genome");
		list_knowninputids = new HashSet<>();
		symbols_genome = new HashSet<>();
		symbols_list = new HashSet<>();
		transids_genome = new HashSet<>();
		transids_list = new HashSet<>();

		List<ExperimentAnnotation> experiments = getExperiments(organism);
		genome = new GenomeAnnotationSet(organism, source, gtype, false);


		String message = getKnownIDs();

		if (list_knowninputids.size() < 10) {
			out.println("{\"message\":\"" + message
					+ "<br />A minimum of 10 known ids is required for this analysis.<br />Please make sure that your selection of organism and genome-annotation (refseq, ensembl ...) matches your id-list and your identifiers are gene ids.\"}");
			return;
		}

		enrichmentresults = new ArrayList<EnrichmentResult>();

//		if (organism.equals("dm3") || organism.equals("dm6") || organism.equals("ce11")) {
//			source = new Source("modencode");
//		}

		try {
//			startTime = System.nanoTime();
			for (ExperimentAnnotation experiment : experiments) {
				
				
				if (promoterUsString.equals("dynamic")) {
					if (experiment.target.startsWith("egfp-")) {
						target = experiment.target.split("egfp-")[1];
					} else {
						target = experiment.target;
					}
					promoterUs = cutoff.get(target);
				}

				parseIntersections(experiment, organism, source);


			}
//			endTime = System.nanoTime();
//			totalTime = endTime - startTime;
//			System.out.println("time to parseIntersection: " + (double) totalTime / 1_000_000_000.0);


		} catch (ChromIDException e) {
			out.println("{\"message\":\"" + e.getMessage() + "! ... Make sure your organism-selection is correct.\"}");
			return;
		}


//		LOGGER.log(Level.INFO, "enrichmentresults size : " + enrichmentresults.size());
//		startTime = System.nanoTime();

		Collections.sort(enrichmentresults);

		// compute FDR
		for (int i = 0; i < enrichmentresults.size(); i++) {
			double fdr = (enrichmentresults.get(i).pval) * (enrichmentresults.size() / (i + 1));
			if (fdr > 1) {
				fdr = 1;
			}
			enrichmentresults.get(i).setFdr(fdr);
		}
		for (int i = 0; i < (enrichmentresults.size() - 1); i++) {
			if (enrichmentresults.get(i).fdr > enrichmentresults.get(i + 1).fdr) {
				double newFdr = enrichmentresults.get(i + 1).fdr;
				enrichmentresults.get(i).setFdr(newFdr);
				i = 0;
			}
		}

		// compute combined score
		for (int i = 0; i < enrichmentresults.size(); i++) {

			double cs = (-Math.log10(enrichmentresults.get(i).pval)) * (enrichmentresults.get(i).score);

			// set max combined score to 1000
			if (cs > 1000) {
				cs = 1000;
			}

			enrichmentresults.get(i).setCS(cs);

		}
		// resort by combined score
		Collections.sort(enrichmentresults, (a, b) -> a.cs > b.cs ? -1 : a.cs == b.cs ? 0 : 1);
//		endTime = System.nanoTime();
//		totalTime = endTime - startTime;
//		System.out.println("time to compute FDR: " + (double) totalTime / 1_000_000_000.0);

//		startTime = System.nanoTime();
		String tempstring = enrichmentResults2JSON();
//		endTime = System.nanoTime();
//		totalTime = endTime - startTime;
//		System.out.println("time to enrichmentResults2JSON: " + (double) totalTime / 1_000_000_000.0);

		if (tempstring != null) {
			out.println(tempstring);
		} else {
			out.println(
					"{\"message\":\" No results due to a lack of TF-ChIP seq data available for these assembly..\"}");
			}

		}

	/**This method checks if the region and the tregion are overlapping and if it is respecting the overlap value choosen by the user.
	 * If yes the gene name is stored in the hits variable.
	 * 
	 * 
	 * @param transcript is the gene (Transcript object)
	 * @param region is the region in the chromosome
	 * @param chromlength length of the chromosome
	 */
	public static void genecheck(Transcript transcript, Region region, long chromlength) {


		Region tregion = transcript.flankingRegion5p(chromlength, (promoterDs + promoterUs), -promoterDs);

		Region coverage = tregion.intersectionWith(region);

		if (coverage!=null) {

			boolean match = false;

			if (ovtype) {
				match = coverage.length() >= ovval;
			} else {
				match = coverage.length() > ovval * 0.01 * tregion.length();
			}


			if (match) {

				if (list_knowninputids.contains(transcript.pid)) {

					transids_listhits.add(transcript.pid);

					if (symbols_listhits.contains(transcript.symbol)) {

					} else {

						symbols_listhits.add(transcript.symbol);
					}

				}

				transids_genomehits.add(transcript.pid);

			}

		}

	}

	/**This method compute the intersection and check for all the genes within the user-selected region in according to the defined criteria
	 * 
	 * @param experiment object in experiments
	 * @param organism assembly/MongoDB db (es.dm3)
	 * @param source MongoDB collection (es. modencode)
	 * @throws ChromIDException
	 */

	public static void parseIntersections(ExperimentAnnotation experiment, String organism, Source source)

			throws ChromIDException {


		Chromosome chromosome = null;
		Transcript gene = null, fwgene = null;
		long chromsize = 0;

		ListIterator<Region> iterator = null, fwiterator = null;
		boolean it_next = true;
		boolean it_ended = false;

		symbols_listhits = new HashSet<String>();
		symbols_genomehits = new HashSet<String>();
		transids_listhits = new HashSet<String>();
		transids_genomehits = new HashSet<String>();

		GenomeRegionTrack track = new GenomeRegionTrack(experiment.pid, organism);

		// DataTrackOfRegionsMongoDAO data = new DataTrackOfRegionsMongoDAO();
		// data.sql2mongo("hg19", experiment.pid, source);
		// data.modmine2mongo("ce11", experiment.pid, source);
		
		
		for ( Chromosome chromtrack: track) {

			try {
				chromosome = genome.getChromByID(chromtrack.pid);
			} catch (ChromIDException e) {
//				System.out.println("not listed chromosome: " + chromtrack.pid);
				continue;
			}
			if( chromosome == null ) {
//				System.out.println("Genome-annotation has no chromosome named: " + chromtrack.pid + " (track: "
//						+ track.trackid + ")");
				continue;
			}
			if (chromosome.getRegionNumber() == 0) {
//				System.out.println("Genome-annotation has no genes for chromosome: " + chromtrack.pid);
				continue;
			}


			chromsize = chromosome.length;
			iterator = chromosome.listIterator();
			
			it_next = true;
			it_ended = false;


			for (Region region : chromtrack) {

				if (it_ended) {

					//pass;

				} else {
					while (true) {
						if (it_next) {

							if ( iterator.hasNext() ) {

								gene = (Transcript)iterator.next();	
	
								it_next = false;

							} else {

								it_ended = true;
								break;

							}

						}
						
						if (gene.isLeftOf(region, promoterUs)) {
							it_next = true;

						} else if (gene.isRightOf(region, promoterUs)) {
							it_next = false;
							break;


						} else {
							
//							System.out.println("symbol: " + gene.symbol);
//							System.out.println("pid: " + gene.pid);
							genecheck(gene, region, chromsize);
							
							fwiterator= chromosome.listIterator(iterator.nextIndex());
							while (true) {

								if( !(fwiterator.hasNext()) )
									break;

								fwgene = (Transcript)fwiterator.next();

								if (fwgene.isRightOf(region, promoterUs)) {
									break;
								} else if (fwgene.isLeftOf(region, promoterUs)) {
									//pass;

								} else {
									genecheck(fwgene, region, chromsize);

								}
							}

							it_next = false;
							break;

						}

					}

				}

			}

		}


		/////////////////////////////////////////
		/* Write list of matching gene-symbols */
		/////////////////////////////////////////

		int genomesize;
		int genomehits;
		int listsize;
		int listhits;

		Set<String> poshits;


		genomesize = transids_genome.size();
		genomehits = transids_genomehits.size();
		listsize = transids_list.size();
		listhits = transids_listhits.size();
		poshits = symbols_listhits;


		double score = ((double)listhits/listsize)/((double)genomehits/genomesize);
		double pval;

		if ( listhits == 0 ) {
			pval = 1.0;
		} else {

			pval = Statistics.getRightHypergeometricPvalue( listhits, listsize, genomehits, genomesize );

		}


		/////////////////////////////
		/* Store enrichment result */
		/////////////////////////////

		EnrichmentResult result = new EnrichmentResult( experiment, score, pval);

		result.listhits = listhits;
		result.listsize = listsize;
		result.genomehits = genomehits;
		result.genomesize = genomesize;
		result.positives = poshits;

		enrichmentresults.add(result);

	}


	/**
	 * This method return a list of ExperimentAnnotation ("datatracks") for an organism.
	 * "datatracks" contains all the elements that are present in the mongoDB (getObjectIterator method from ExperimentMongoDAO)
	 * for a choosen database(organism) and collection(METACOLLECTION=metadata) (see getCollection method of the class MongoDBConnector)
	 * that respects the filtering for the selected field (es. "targettype") and filter values(es. "TF")
	 * @param organism value of the chosen Assembly (es. dm3)
	 * @return datatracks: collection of all the ExperimentAnnotations elements for the selected organism, collection and filter
	 */
	private static List<ExperimentAnnotation> getExperiments( String organism ) {

		List<ExperimentAnnotation> datatracks = new ArrayList<>();/*constructor*/
		
		ExperimentDAO expdao = ExperimentMongoDAO.getInstance();/*return an ExperimentMongoDAO and open a new MongoClient addressed to the localhost*/

		if(enrich.equals("TFs")) {

			Iterator<ExperimentAnnotation> iter = expdao.getObjectIterator( organism, "targettype", "TF" );
			while ( iter.hasNext() ) {
				datatracks.add( iter.next() );
			}
		}else if(enrich.equals("HMs")){
			Iterator<ExperimentAnnotation> iter = expdao.getObjectIterator( organism, "targettype", "HM" );
			while ( iter.hasNext() ) {
				datatracks.add(iter.next());
			}
		}
		return datatracks;

	}

	/**This method check for each transcript symbol present in the region of each chromosome within the genome and return in a string named "message"
	 * the number of uploaded list ids recognized in our database respect the whole uploaded list.
	 * @param ids_are_transcriptids initially set to false
	 * @return a string (message) containing the number of recognized ids in our database respect the whole uploaded list
	 */
	private static String getKnownIDs() {

		for (Chromosome chromosome: genome.genome) {
			
			for (Region region: chromosome) {
				
				Transcript transcript = (Transcript)region;
				
				if (transcript.symbol == null ) { /*if the transcript has no symbol it skips*/
					//pass
				} else {

					transids_genome.add(transcript.pid);

					if (list_inputids.contains(transcript.symbol)) {

						if (transids_list.contains(transcript.pid)) {

						} else {
							transids_list.add(transcript.pid);
						}

						if (symbols_list.contains(transcript.symbol)) {
							// pass
						} else {
							symbols_list.add(transcript.symbol);
						}

					}
				}
			}
		}

		list_knowninputids = transids_list;

		String message = list_inputids.size() - symbols_list.size() + " unknown in " + list_inputids.size()
				+ " input ids recognized";
//		LOGGER.log(Level.INFO, message );

		return message;


	}

	/**
	 * method to retrieve the gene targets for each TF
	 * 
	 * @param targets
	 * @return
	 */
	public String convertGeneListAsString(HashSet<String> targets) {

		int len;

		if (targets == null) {
			return "";
		} else {
			len = targets.size();
			if (len == 0) {
				return "";
			}
		}

		StringBuffer ostring = new StringBuffer();

		TreeSet<String> posranked = new TreeSet<String>();
		posranked.addAll(targets);

		for (String pos : posranked) {
			ostring.append(pos);
			ostring.append(";");
		}

		return ostring.substring(0, ostring.length() - 1).toString();

	}

	/**
	 * build the JSON string used to build results visualization
	*@return*@throws FileNotFoundException*/
	public String enrichmentResults2JSON() throws FileNotFoundException {

		StringBuffer resultstring = new StringBuffer();
		StringBuffer enrichPlotTitles = new StringBuffer();
		StringBuffer enrichPlotValues = new StringBuffer();
		StringBuffer enrichPlotProbe = new StringBuffer();
		String finstring;
		String enrichTitles;
		String enrichValues;
		String enrichProbe;

		if (enrichmentresults.size() == 0) {

			return null;

		} else {

			int i = 0;
		for ( EnrichmentResult result : enrichmentresults ) {
			
			resultstring.append("[\"" + result.experiment.target + "\",\"" + result.experiment.probe + "\",\""
					+ result.experiment.getTreatment() + "\",\"" + result.experiment.getReplicate() + "\",\""
					+ result.listhits + "\",\"" + result.listsize + "\",\"" + result.genomehits + "\",\""
						+ result.genomesize + "\",\"" + result.getGeneListAsString() + "\",\""
						+ convertGeneListAsString(testList.convertGeneNames(result.positives)) + "\",\"" + result.score
						+ "\",\"" + result.pval + "\",\"" + result.fdr + "\",\"" + result.cs + "\"],");
				if (i < 10) {
					enrichPlotTitles.append("\"" + result.experiment.target + "\",");
					enrichPlotProbe.append("\"" + result.experiment.probe + "\",");
					enrichPlotValues.append("[" + (i + 1) + "," + result.cs + "],");
					i++;
				}

		}
			// crop the last comma
			enrichTitles = enrichPlotTitles.substring(0, enrichPlotTitles.length() - 1);
			enrichValues = enrichPlotValues.substring(0, enrichPlotValues.length() - 1);
			enrichProbe = enrichPlotProbe.substring(0, enrichPlotProbe.length() - 1);
		

		if (enrich.equals("TFs")) {

				finstring = "{\"titles\":[\"Transcription factor\",\"Probe\",\"Treatment\",\"Replicate\",\"list hits\",\"list-size\",\"genome hits\",\"genome-size\",\"target IDs\",\"target genes\",\"score\",\"p-value\",\"FDR\",\"Combined score\"],\"data\":["
						+ resultstring.substring(0, resultstring.length() - 1) + "],\"enrichTitles\":[" + enrichTitles
						+ "]" + ",\"enrichValues\":[" + enrichValues + "]" + ",\"enrichProbe\":[" + enrichProbe + "]"
						+ "}";
		} else {

				finstring = "{\"titles\":[\"Histone mark\",\"Probe\",\"Treatment\",\"Replicate\",\"list hits\",\"list-size\",\"genome hits\",\"genome-size\",\"target IDs\",\"target genes\",\"score\",\"p-value\",\"Combined score\"],\"data\":["
						+ resultstring.substring(0, resultstring.length() - 1) + "],\"enrichTitles\":[" + enrichTitles
						+ "]" + ",\"enrichValues\":[" + enrichValues + "]" + ",\"enrichProbe\":[" + enrichProbe + "]"
						+ "}";

		}
		}

		return finstring;

	}

	/**
	 * load dynamic range txt file
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void loadDynamicRanges() throws NumberFormatException, IOException {

		dynamicRanges = "DynamicRanges/" + organism + "_" + gtype + "_TF_averageCutoffs.csv";
		String url = getClass().getClassLoader().getResource(dynamicRanges).toString().split(":")[1];
		cutoff = new HashMap<String, Integer>();
		BufferedReader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(url), "UTF-8"));
		;

		while ((row = csvReader.readLine()) != null) {
			String[] data = row.split(",");
			cutoff.put(data[0], Integer.valueOf(data[1]));
		}
		csvReader.close();

		// System.out.println("Test DynamicsRanges____________________");
		//
		// for (Map.Entry<String, Integer> entry : cutoff.entrySet()) {
		// System.out.println(entry.getKey() + "=" + entry.getValue());
		// }
		// System.out.println("End Test DynamicsRanges____________________");

	}

}
