package annominer;


import annominer.exceptions.FileFormatException;
import annominer.io.database.mongodb3.ChromosomeOfRegionsMongoDAO;
import annominer.io.database.mongodb3.DataTrackOfRegionsMongoDAO;

/**
 * genomic region track
 */
public class GenomeRegionTrack extends DataTrack {
	

	/* Constructors */
	/**
	 * constructor
	 * @param trackid
	 * @param organism
	 * @param source
	 * @throws FileFormatException
	 */
	public GenomeRegionTrack(String trackid, String organism, Source source) throws FileFormatException {

		super(trackid,organism,source);/*call the parent constructor with this three arguments, setting this values to the respective Datatrack attributes */

		this.chromdao = ChromosomeOfRegionsMongoDAO.getInstance(); /*connection with mongodb*/
		
		this.trackdao = DataTrackOfRegionsMongoDAO.getInstance(); /*connection with mongodb*/
		
		this.trackdao.update(organism, trackid, source);
		
	}
	/**
	 * constructor
	 * @param trackid
	 * @param organism
	 * @throws FileFormatException
	 */
	public GenomeRegionTrack(String trackid, String organism) throws FileFormatException {

		super(trackid, organism);

		this.chromdao = ChromosomeOfRegionsMongoDAO.getInstance(); /* connection with mongodb */

		this.trackdao = DataTrackOfRegionsMongoDAO.getInstance(); /* connection with mongodb */

	}
	
	/* Interfaces */
		
	
	
	/* Methods */
	
	
	
}




