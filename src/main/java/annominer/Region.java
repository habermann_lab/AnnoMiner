package annominer;
/**
 * genomic region
 */
public class Region implements Comparable<Region> {
	
	public long left, right;
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 */
	public Region(long pos1, long pos2) {
		
		if (pos2 > pos1){
			left = pos1;
			right = pos2;
		} else {
			left = pos2;
			right = pos1;
		}
		
	}
	/**
	 * constructor
	 * @param left
	 * @param right
	 */
	public Region(String left, String right) {
		this( Integer.parseInt(left), Integer.parseInt(right) );
		
	}
	
	
	
	/** compare regions postition by looking at their start
	 * @param region
	 * @return int
	 */
	/* Methods */
	
	public int compareTo(Region region) {
        
		return (int)(this.left - region.left);
		
    }
	
	
	
	/**  is this region contained by another? 
	 * @param region
	 * @return boolean
	 */
	public boolean isWithin(Region region) {
		if (region.left <= left && right <= region.right) {
			return true;
		} else {
			return false;
		}
	}

	/**this method checks if the gene intersects with the region
	 * 
	 * @param region
	 * @return a new region containing the overlap
	 */
	public Region intersectionWith(Region region) {
		
		long p_left, p_right;
		
		if (right < region.left || left > region.right) {
			return null;
		} else {
			if (left > region.left) {
				p_left = left;
			} else {
				p_left = (region.left);
			}
			
			if (right < region.right) {
				p_right = right;
			} else {
				p_right = (region.right);
			}
			
			return new Region(p_left, p_right);
			
		}
	}

	
	/**  is this region overlapping at left another? 
	 * @param region
	 * @param tss_ds
	 * @return boolean
	 */
	public boolean overlapLeft(Region region, Integer tss_ds) {
		
		if ((right < (region.left+tss_ds) && right < region.right)) {
			return true;
		}else {
			return false;
		}
	}
	
	/** is this region overlapping at right another?  
	 * @param region
	 * @param tss_ds
	 * @return boolean
	 */
	public boolean overlapRight(Region region, Integer tss_ds) {
		
		if (left > (region.right-tss_ds) && left > region.left) {
			return true;
		}else {
			return false;
		}
	}
	
	/** is this region at the left of another?  
	 * @param region
	 * @return boolean
	 */
	public boolean isLeftOf(Region region) {
		if (region.left > right) {
			return true;
		} else {
			return false;
		}
	}
	

	
	/** is this region at the left of another + margin?  
	 * @param region
	 * @param margin
	 * @return boolean
	 */
	public boolean isLeftOf(Region region, int margin) {
		if (region.left > right + margin) {
			return true;
		} else {
			return false;
		}
	}
	
	/** is this region at the left of another + distance + margin?  
	 * @param region
	 * @param margin
	 * @param dis
	 * @return boolean
	 */
	public boolean isLeftOf(Region region, int margin, int dis) {
		if (region.left > right + margin + dis) {
			return true;
		} else {
			return false;
		}
	}
	

	
	/** is this region at the right of another?  
	 * @param region
	 * @return boolean
	 */
	public boolean isRightOf(Region region) {
		if (region.right < left) {
			return true;
		} else {
			return false;
		}
	}

	
	/** is this region at the right of another +  some margin?  
	 * @param region
	 * @param margin
	 * @return boolean
	 */
	public boolean isRightOf(Region region, int margin) {
		if (region.right < left - margin) {
			return true;
		} else {
			return false;
		}
	}
	
	/** is this region at the right of another + distance + margin?  
	 * @param region
	 * @param margin
	 * @param dis
	 * @return boolean
	 */
	public boolean isRightOf(Region region, int margin, int dis) {
		if (region.right < left - margin - dis) {
			return true;
		} else {
			return false;
		}
	}

	
	/** get region length
	 * @return int
	 */
	public int length() {
		return (int)(right-left);
	}


	
	/** get position in TSV
	 * @return String
	 */
	public String toTSV() {
		return String.valueOf(left) + '\t' + String.valueOf(right);
	}


	
	/**  get position in JSON
	 * @return String
	 */
	public String toJSON() {
		return '[' + String.valueOf(left) + ',' + String.valueOf(right) + ']';
	}
	
	
	
	/**  get left position
	 * @return long
	 */
	public long getLeft(){
		return this.left;
	}
	
	
	/** get right position
	 * @return long
	 */
	public long getRight(){
		return this.right;
	}
	
	
	
	
}

