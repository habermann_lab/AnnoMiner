package annominer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * transcript definition
 */
public class Transcript extends NamedOrientedRegion implements Iterable<OrientedRegion> {
	
	public OrientedRegion cds;
	public boolean canonical;
	private List<OrientedRegion> exons;
	
	
	/* Constructors */
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param cds
	 * @param exons
	 */
	public Transcript(long pos1, long pos2, boolean strd, String pid, String symbol, OrientedRegion cds, List<OrientedRegion> exons ) {
		super(pos1, pos2, strd, pid, symbol);
		this.cds = cds;
		this.exons = exons;
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param cds
	 * @param exons
	 */
	public Transcript(long pos1, long pos2, boolean strd, String pid, OrientedRegion cds, List<OrientedRegion> exons) {
		this(pos1, pos2, strd, pid, null, cds, exons);
		
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param cds
	 */
	public Transcript(long pos1, long pos2, boolean strd, String pid, String symbol, OrientedRegion cds ) {
		super(pos1, pos2, strd, pid, symbol);
		this.cds = cds;
		this.exons = new ArrayList<OrientedRegion>(20);
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param cds
	 */
	public Transcript(long pos1, long pos2, boolean strd, String pid, OrientedRegion cds ) {
		this(pos1, pos2, strd, pid, null, cds);
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param cds
	 * @param exons
	 */
	public Transcript(String pos1, String pos2, String strd, String pid, String symbol, OrientedRegion cds, List<OrientedRegion> exons) {
		super(pos1, pos2, strd, pid, symbol);
		this.cds = cds; 
		this.exons = exons;
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param cds
	 * @param exons
	 */
	public Transcript(String pos1, String pos2, String strd, String pid, OrientedRegion cds, List<OrientedRegion> exons) {
		this(pos1, pos2, strd, pid, null, cds, exons);
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param cds
	 */
	public Transcript(String pos1, String pos2, String strd, String pid, String symbol, OrientedRegion cds) {
		super(pos1, pos2, strd, pid, symbol);
		this.cds = cds; 
		this.exons = new ArrayList<OrientedRegion>(20);
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param cds
	 */
	public Transcript(String pos1, String pos2, String strd, String pid, OrientedRegion cds) {
		this(pos1, pos2, strd, pid, null, cds);
	}
	
	
	
	/** get iterator through exons
	 * @return Iterator<OrientedRegion>
	 */
	/* Interfaces */
	
	public Iterator<OrientedRegion> iterator() {        
        Iterator<OrientedRegion> iter = exons.iterator();
        return iter;
    }
	
	
	
	/** add exon
	 * @param exon
	 */
	/* Methods */
	
	
	public void addExon(OrientedRegion exon) {
		exons.add(exon);
	}
	
	
	
	/** get coding sequence
	 * @return OrientedRegion
	 */
	public OrientedRegion getCDS() {
		return this.cds;
	}
	
	
	
	/** get 5' UTR region
	 * @return OrientedRegion
	 */
	public OrientedRegion get5pUTR() {
		
		if (cds.left == cds.right) {
			return null;
		}
		if (strand) {
			if (cds.left == left) {
				return null;
			} else {
				return new OrientedRegion(left,cds.left,strand);
			}
		} else {
			if (cds.right == right) {
				return null;
			} else {
				return new OrientedRegion(cds.right,right,strand);
			}
		}
	}
						
		
	
	/** get 3'utr region
	 * @return OrientedRegion
	 */
	public OrientedRegion get3pUTR() {
		
		if (cds.left == cds.right) {
			return null;
		}
		if (strand) {
			if (cds.right == right) {
				return null;
			} else {
				return new OrientedRegion(cds.right,right,strand);
			}
		} else {
			if (cds.left == left) {
				return null;
			} else {		
				return new OrientedRegion(left,cds.left,strand);
			}
		}
	}
	
	
	
	/** return exon number
	 * @return int
	 */
	public int getExonNumber() {
		return exons.size();
	}
	
	/**Knowing the regions of the exons this method will return a list of introns (defined as the region between the end of an exon and the start of the next one) 
	 * 
	 * @return an array or introns (OrientedRegions)
	 */
	public ArrayList<OrientedRegion> getIntrons() {
		
		ArrayList<OrientedRegion> introns = new ArrayList<OrientedRegion>();
		long lastright = 0;
		for (OrientedRegion exon : exons) {
			if (lastright != 0) {
				introns.add(new OrientedRegion(lastright,exon.left,strand));
			}
			lastright = exon.right;
		}
		return introns;
	}
	
	
	
	/** return results to TSV
	 * @return String
	 */
	@Override
	public String toTSV(){

		if (symbol!=null) {
			return String.valueOf(left) + '\t' + String.valueOf(right) + '\t' + strandToString()+ '\t' + pid+ '\t' + symbol + '\t' + String.valueOf(cds.left) + '\t' + String.valueOf(cds.right);
		} else {
			return String.valueOf(left) + '\t' + String.valueOf(right) + '\t' + strandToString()+ '\t' + pid + '\t' + String.valueOf(cds.left) + '\t' + String.valueOf(cds.right);
		}
	
	}
	
	
	
	/** return results to JSON
	 * @return String
	 */
	@Override
	public String toJSON() {
		StringBuilder strg = new StringBuilder();
		if (!exons.isEmpty()) {
			for (OrientedRegion exon : exons) {
				strg.append('['+String.valueOf(exon.left)+','+String.valueOf(exon.right)+"],");
			}
			strg.setLength(strg.length() - 1);
		}
		if (symbol==null) {
			return '['+String.valueOf(left)+','+String.valueOf(right)+','+strandToJSON()+",\""+pid+"\",["+String.valueOf(cds.left)+','+String.valueOf(cds.right)+"],["+strg+"]]";	
		} else {
			return '['+String.valueOf(left)+','+String.valueOf(right)+','+strandToJSON()+",\""+pid+"\",\""+symbol+"\",["+String.valueOf(cds.left)+','+String.valueOf(cds.right)+"],["+strg+"]]";	
		}
	}
	
	
	/** check if canoncal
	 * @return boolean
	 */
	public boolean isLongestTranscript() {
		return this.canonical;
	}
	
	
	
	/** compare if the name of two transcripts is the same
	 * @param obj
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Transcript) {
			Transcript transcript = (Transcript) obj;
			return this.pid.equals(transcript.pid);
		}
		return false;
	}

	
	/** get hashcode
	 * @return int
	 */
	@Override
	public int hashCode() {
		return pid.hashCode();
	}
	
	
}
