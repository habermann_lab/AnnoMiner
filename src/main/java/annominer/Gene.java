package annominer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * gene definition as strand transcript and gene symbol and start stop positions
 */
public class Gene extends NamedOrientedRegion implements Iterable<Transcript> {

	private List<Transcript> transcripts;
	
	
	/* Constructors */
	/**
	 * gene constructor
	 * @param trans
	 */
	public Gene(Transcript trans) {
		super(trans.left, trans.right, trans.strand, trans.symbol);
		this.transcripts = new ArrayList<Transcript>();
		this.transcripts.add(trans);
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param transcripts
	 */
	public Gene(long pos1, long pos2, boolean strd, String pid, String symbol, List<Transcript> transcripts) {
		super(pos1, pos2, strd, pid, symbol);
		this.transcripts = transcripts;
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param transcripts
	 */
	public Gene(long pos1, long pos2, boolean strd, String pid, List<Transcript> transcripts) {
		this(pos1, pos2, strd, pid, null, transcripts);
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 */
	public Gene(long pos1, long pos2, boolean strd, String pid, String symbol) {
		super(pos1, pos2, strd, pid, symbol);
		this.transcripts = new ArrayList<Transcript>(10);
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 */
	public Gene(long pos1, long pos2, boolean strd, String pid) {
		super(pos1, pos2, strd, pid);
		this.transcripts = new ArrayList<Transcript>(10);
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 * @param transcripts
	 */
	public Gene(String pos1, String pos2, String strd, String pid, String symbol, List<Transcript> transcripts) {
		super(pos1, pos2, strd, pid, symbol);
		this.transcripts = transcripts;
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param transcripts
	 */
	public Gene(String pos1, String pos2, String strd, String pid, List<Transcript> transcripts) {
		this(pos1, pos2, strd, pid, null, transcripts);	
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 */
	public Gene(String pos1, String pos2, String strd, String pid, String symbol) {
		super(pos1, pos2, strd, pid, symbol);
		this.transcripts = new ArrayList<Transcript>(20);
	}
	/**
	 * gene constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 */
	public Gene(String pos1, String pos2, String strd, String pid) {
		super(pos1, pos2, strd, pid);
		this.transcripts = new ArrayList<Transcript>(20);
	}
		
	
	
	
	
	/** iterator through transcripts
	 * @return Iterator<Transcript>
	 */
	/* Interfaces */
	
	public Iterator<Transcript> iterator() {        
        Iterator<Transcript> iter = transcripts.iterator();
        return iter; 
    }
	
	
	
	/** add transcript to gene
	 * @param transcript
	 */
	/* Methods */
	
	public void add(Transcript transcript) {
		transcripts.add(transcript);
		if(transcript.left<this.left){this.left=transcript.left;}
		if(transcript.right>this.right){this.right=transcript.right;}
	}
	
	
	/** get the transcript
	 * @return Transcript
	 */
	public Transcript getTranscript() {
		return transcripts.get(0);
	}
	
	
	/** get number of transcript per gene
	 * @return int
	 */
	public int getTranscriptNumber(){
		return transcripts.size();
	}
	
	
	
	/** return gene info as JSON
	 * @return String
	 */
	@Override
	public String toJSON() {
		StringBuilder strg = new StringBuilder();
		if (!transcripts.isEmpty()) {
			for (Transcript trans : transcripts) {
				strg.append(trans.toJSON() + ',');
			}
			strg.setLength(strg.length() - 1);
		}
		if (symbol==null) {
			return '['+String.valueOf(left)+','+String.valueOf(right)+','+strandToJSON()+",\""+pid+"\",["+strg+"]]";	
		} else {
			return '['+String.valueOf(left)+','+String.valueOf(right)+','+strandToJSON()+",\""+pid+"\",\""+symbol+"\",["+strg+"]]";
		}
	}
	
	
}
