package annominer.math.statistics;

import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.rosuda.JRI.Rengine;
/**
 * Pval computation
 */
public class Statistics {
	
	static Rengine re = null;
	
	private Statistics() {}
	
	
	
	/** get Hypergeometric Pvalue using R
	 * @param sample_pos
	 * @param sample_size
	 * @param population_pos
	 * @param population_size
	 * @return double
	 */
	public static double getHyperPvalFromR ( int sample_pos, int sample_size, int population_pos, int population_size ) {
		
		/* Using the JRI interface to R */
		
		if ( re == null ) {
			// new R-engine
			re=new Rengine (new String [] {"--vanilla"}, false, null);
			if (!re.waitForR()) {
				System.out.println ("Cannot load R");
				return 0;
			}
		}
			
		double pval = re.eval ("phyper("+ sample_pos+","+population_pos+","+(population_size-population_pos)+","+sample_size+",lower.tail=FALSE)").asDouble();
		
		re.end();
		
		return pval;
		
	}
	
	
	
	/** get Hypergeometric Pvalue
	 * @param sample_pos
	 * @param sample_size
	 * @param population_pos
	 * @param population_size
	 * @return double[]
	 */
	public static double[] getHypergeometricPvalues( int sample_pos, int sample_size, int population_pos, int population_size )  {
		
		/* Using the org.apache.commons.math3 library */
		
		HypergeometricDistribution hd = new HypergeometricDistribution(population_size, population_pos, sample_size);
		
		return new double[] {hd.cumulativeProbability(sample_pos+1), hd.upperCumulativeProbability(sample_pos+1)} ;
		
	}
	
	
	
	/** get one tail Hypergeometric Pvalue
	 * @param sample_pos
	 * @param sample_size
	 * @param population_pos
	 * @param population_size
	 * @return double
	 */
	public static double getRightHypergeometricPvalue( int sample_pos, int sample_size, int population_pos, int population_size )  {
		
		/* Using the org.apache.commons.math3 library */
		
		HypergeometricDistribution hd = new HypergeometricDistribution(population_size, population_pos, sample_size);
		
		return hd.upperCumulativeProbability(sample_pos+1);
		
	}
	
	
	

}
