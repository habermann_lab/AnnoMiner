package annominer.math.statistics;

import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.rosuda.JRI.Rengine;
/**
 * generate Pval
 */
public class PValueGenerator {
	
	
	private Rengine re;
	
	public PValueGenerator(){
		re=null;
	}
	
	
	
	/** get Hypergeometric Pvalue from R
	 * @param sample_pos
	 * @param sample_size
	 * @param population_pos
	 * @param population_size
	 * @return double
	 */
	public double getHyperPvalFromR ( int sample_pos, int sample_size, int population_pos, int population_size ) {
		
		/* Using the JRI interface to R */
		
		if ( re == null ) {
			// new R-engine
			re=new Rengine (new String [] {"--vanilla"}, false, null);
			if (!re.waitForR()) {
				System.out.println ("Cannot load R");
				return 0;
			}
		}
			
		double pval = re.eval ("phyper("+sample_pos+","+population_pos+","+(population_size-population_pos)+","+sample_size+",lower.tail=FALSE)").asDouble();
		
		re.end();
		
		return pval;
		
	}
	
	
	
	/** get Hypergeometric Pvalue
	 * @param sample_pos
	 * @param sample_size
	 * @param population_pos
	 * @param population_size
	 * @return double
	 */
	public static double getRightHypergeometricPval ( int sample_pos, int sample_size, int population_pos, int population_size )  {
		
		/* Using the org.apache.commons.math3 library */ 
		
		HypergeometricDistribution hd = new HypergeometricDistribution(population_size, population_pos, sample_size);
		
		return hd.cumulativeProbability(sample_pos);
		
	}
	
	
	
}
