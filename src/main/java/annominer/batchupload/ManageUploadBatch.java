package annominer.batchupload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import javax.servlet.ServletException;

import annominer.GenomeRegionTrack;
import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.ui.web.UserTrack;
import tools.datastructures.set.SetOperations;

public class ManageUploadBatch {

	private String assembly, tracktype, path;
	private HashMap<String, UserTrack> usertracks;
	private HashMap<String, Set<String>> idlists;

	public ManageUploadBatch(String assembly, String tracktype, String path) {
		this.assembly = assembly;
		this.tracktype = tracktype;
		this.path = path;
	}

	public void upload() throws IOException, ServletException {
		
		System.out.println("batchupload"); 
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();


		usertracks = new HashMap<String,UserTrack>();

		/*two empty reference variables of type string*/
		Map<String, String> tempfile = null; 
		String format_error = null;
		
		try {
			
			tempfile = uploadToFile(path); /*after the uploadToFile method it will contains the temppath and the tempfilename*/
			/*gene-centered annotation*/
			if( tracktype.equals("peaks") ) {
				for (Map.Entry<String, String> entry : tempfile.entrySet()) {
					String trackid = entry.getKey();
					Source source = new Source("file");/*constructor assign source=file*/
					source.fileformat = "bed";/*assign to the public attribute of the class fileformat="bed"*/

					source.filepath = entry.getValue();/*assign to the public attribute of the class filepath the absolute path to the uploaded file*/
//				
					new GenomeRegionTrack(trackid, "batch", source);/*constructor of GenomeRegionTrack assign values (organism, file name) to DataTrack and construct a new MongoClient to the localhost(open the connection with mongoDB)*/
//				
					usertracks.put( trackid, new UserTrack(trackid,assembly,tracktype,dateFormat.format(date)) );/*put in usertracks collection the pair trackid(file name): and the relative UserTrack*/
				}
//			/*enrichment analysis*/	
			} else if ( tracktype.equals("idlist") ) {

				idlists = new HashMap<String,Set<String>>();

				for (Map.Entry<String, String> entry : tempfile.entrySet()) {
					Set<String> idlist = SetOperations.readSetFromFile( entry.getValue(), 1 );/*array builded with the content of the file (ids list)*/
					String trackid = entry.getKey();
					System.out.println("Track id: " + trackid);
//					System.out.println("Track genes: " + idlist.toArray()[0]);
					idlists.put(trackid, idlist); /*assign to HashMap idlists the pair trackid(file name): idlist(array with all ids)*/
					usertracks.put( trackid, new UserTrack(trackid,assembly,tracktype,dateFormat.format(date)) );/*put in usertracks collection the pair trackid(file name): and the relative UserTrack*/
				}

			}else {
				
				System.out.println("{\"message\": \"Invalid upload type\"}");
			}
			
			
	    } catch (FileFormatException ffe) {
	    	
	    	format_error = ffe.getMessage();
			System.out.println(ffe.getMessage());
	    	
	    } finally {
	    	
	        
	        /*Check if we have uploaded files or not*/
	        if(  usertracks.keySet().isEmpty() ) {
	        	
		        	if(format_error==null){
					System.out.println(
							"{\"message\":\"The server couldn't upload any files. Please check your format and make sure to select the proper upload type.\"}");
		        	} else {
					System.out.println("{\"message\":\"" + format_error + "\"}");
		        	}
	        	
	        	
	        } else {
	        
		        StringBuffer json = new StringBuffer("[");
				
				for( String id : usertracks.keySet() ) { 
					json.append(usertracks.get(id).toJSON() + ","); 
				}
				
				String outstr =  json.substring(0, json.length()-1) + "]";
				
				if(format_error!=null){
					System.out.println("{\"message\":\"" + format_error + "\",\"data\":" + outstr + "}");
				} else {
					System.out.println("{\"data\":" + outstr + "}");
				}
				
	        }
	        
	        
	        
	    }
		
		return;
		
	}
	
	public HashMap<String, Set<String>> getIdlists() {

		return idlists;

	}

	/** this method assign to the tempfile (string variable) the absolute temppath + tempfilename(id of the session) where the file is uploaded to
	* @param      request    HttpServletRequest. 
	 * @throws IOException 
	 * @throws ServletException 
	*/
	@SuppressWarnings("null")
	private Map<String,String> uploadToFile(String path) throws IOException, ServletException{

		final String temppath = System.getProperty("java.io.tmpdir") + File.separator; /*File.separator is just the "/" character*/


		File folder = new File(path);
//		System.out.println(Level.INFO, "File "+path);
		File[] listOfFiles = folder.listFiles();
//		System.out.println(Level.INFO, "File 1 "+listOfFiles[1]);

		
		Map<String,String> tempfiles = new HashMap<String,String>();
		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile()) {
			File file = listOfFiles[i];
			String fname = file.getName().substring( file.getName().lastIndexOf("\\") + 1, file.getName().lastIndexOf("."));
		  

			String filecontent = null;
			String tempfilename = "batch"; /*id of the session*/
		
			try {
		        filecontent = readLineByLineJava8(file.getPath());/*it will store the content of the file*/  
		        
		        BufferedWriter writer = new BufferedWriter(new FileWriter(temppath + tempfilename + i ));
		        writer.write(filecontent); 
		        writer.close();

					System.out.println("File {0} is being uploaded to {1}" + new Object[] { fname,
							temppath });/* print to console the absolute path where the file is uploaded */

		        tempfiles.put(fname,temppath + tempfilename + i) ;

		    } catch (FileNotFoundException fne) {
		        
					System.out
							.println("Problems during file upload. Error: {0}" +
							new Object[] { fne.getMessage() });/**/
		        
		    } finally {
        
		    }
		  }
		}	

		return tempfiles;
		
	}
	
    //Read file content into string with - Files.lines(Path path, Charset cs)
	 
    private static String readLineByLineJava8(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
 
        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
 
        return contentBuilder.toString();
    }	
	
	
}



