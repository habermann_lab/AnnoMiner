package annominer.batchupload;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;

public class BatchUploadAndEnrich {
		
	public static HashMap<String, Set<String>> idlists;


	public static void main(String[] args) throws IOException, ServletException { // Program entry point
		
		/////// define parameters///////

		// for batch upload
		String assembly = "hg38";
		String tracktype = "idlist";
		Set<Integer> ovvalues = new HashSet<>(Arrays.asList(20));
		Set<Integer> promoterDss = new HashSet<>(Arrays.asList(50));
		Set<String> promoterUsStrings = new HashSet<>(Arrays.asList("dynamic"));
		Set<String> gtypes = new HashSet<>(Arrays.asList("refseq"));
		String inPathString = "/home/fabio/Desktop/BenchMark_AnnoMiner/Benchmarking_ChEA3/RNA-seq";
		String outPathString = "/home/fabio/Desktop/NewBenchmark/";
		String enrichment = "TFs";
		String dynamicRanges = "";

		/// batchupload

		ManageUploadBatch batch = new ManageUploadBatch(assembly, tracktype, inPathString);
		
		batch.upload();

		idlists = batch.getIdlists();


			for (String gtype : gtypes) {

				if (gtype.equals("genecode")) {
				dynamicRanges = "/home/fabio/Desktop/Cutoffs_RM_median_3std_2020/hg38-Cutoffs_RM_median_3std_2020-averaged/hg38_genecodeRM_res3_averageCutoffs.csv";
				} else if (gtype.equals("refseq")) {
				dynamicRanges = "/home/fabio/Desktop/Cutoffs_RM_median_3std_2020/hg38-Cutoffs_RM_median_3std_2020-averaged/hg38_refseqRM_res3_averageCutoffs.csv";
				} else if (gtype.equals("ucsc")) {
				dynamicRanges = "/home/fabio/Desktop/Cutoffs_RM_median_3std_2020/hg38-Cutoffs_RM_median_3std_2020-averaged/hg38_ucscRM_res3_averageCutoffs.csv";
				}

				for (Integer ovvalue : ovvalues) {
					for (Integer promoterDs : promoterDss) {
						for (String promoterUsString : promoterUsStrings) {
							BatchEnrich batchenrich = new BatchEnrich(assembly, gtype, ovvalue, promoterDs,
								promoterUsString, inPathString, outPathString, enrichment, idlists,
									dynamicRanges);
							batchenrich.batchEnrichment();
						}
					}
				}

			}
		}
		


}
