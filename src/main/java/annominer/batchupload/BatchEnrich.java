package annominer.batchupload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import annominer.Chromosome;
import annominer.EnrichmentResult;
import annominer.ExperimentAnnotation;
import annominer.GenomeAnnotationSet;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.convertGeneIDs.ConvertIDs;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ExperimentDAO;
import annominer.io.database.mongodb3.ExperimentMongoDAO;
import annominer.math.statistics.Statistics;


public class BatchEnrich {


	private String row;
	private String target;
	private File f;
	private String organism;
	private String gtype;
	private Integer ovvalue;
	private String promoterUsString;
	private String inPathString;
	private String outPathString;
	private HashMap<String, Set<String>> idlists;
	private HashMap<String, Integer> cutoff;
	private ConvertIDs testList;
	private ArrayList<String> used_list;
	private File IDFile;
	private String dynamicRanges;

	private static String enrich, inPath, outPath;
	private static int ovval, promoterUs, promoterDs;
	private static boolean ovtype;

	static Set<String> list_inputids;
	static Set<String> list_knowninputids;

	static Set<String> symbols_list;
	static Set<String> symbols_listhits;
	static Set<String> symbols_genome;
	static Set<String> symbols_genomehits;

	static Set<String> transids_genome;
	static Set<String> transids_genomehits;
	static Set<String> transids_list;
	static Set<String> transids_listhits;

	static List<EnrichmentResult> enrichmentresults;

	static GenomeAnnotationSet genome;
	
	public BatchEnrich(String organism, String gtype, Integer ovvalue, Integer promoterDs, String promoterUsString,
			String inPathString, String outPathString, String enrichment,
			HashMap<String, Set<String>> idlists, String dynamicRanges) {

		this.organism = organism;
		this.gtype = gtype;
		this.ovvalue = ovvalue;
		BatchEnrich.promoterDs = promoterDs;
		this.promoterUsString = promoterUsString;
		this.inPathString = inPathString;
		this.outPathString = outPathString;
		BatchEnrich.enrich = enrichment;
		this.idlists = idlists;
		this.dynamicRanges = dynamicRanges;
		BatchEnrich.ovtype = true; // bp overlap

	}


	public void batchEnrichment() throws NumberFormatException, IOException {


		if (promoterUsString.equals("dynamic")) {

			// read the cutoff file
			cutoff = new HashMap<String, Integer>();

			BufferedReader csvReader = new BufferedReader(new FileReader(
					dynamicRanges));
			while ((row = csvReader.readLine()) != null) {
				String[] data = row.split(",");
				cutoff.put(data[0], Integer.valueOf(data[1]));
			}
			csvReader.close();
				
		} else {
				
			promoterUs = Integer.valueOf(promoterUsString);
				
		}

		inPath = inPathString;

		outPath = outPathString + gtype +"/"+ "ovval" + String.valueOf(ovvalue) + "_us" + promoterUsString + "ds"
				+ String.valueOf(promoterDs);


		// build directories if they doesn't exist
	    File directory = new File(outPathString);
	    if (! directory.exists()){
	        directory.mkdir();
		}

		File directory2 = new File(outPathString + gtype + "/");
		if (!directory2.exists()) {
			directory2.mkdir();
		}

		// check if the analysis has been already done
		File directory3 = new File(outPath);
		if (directory3.exists()) {
			return;
		} else {
		// build the output directory
		f = new File(outPath);
		f.mkdir();
			        	
		}

		////////////////////////////////
		// Calculate and send results //
		////////////////////////////////

		Source source = new Source("sql", "genome-mysql.cse.ucsc.edu", "genome");

		list_knowninputids = new HashSet<>();
		symbols_genome = new HashSet<>();
		symbols_list = new HashSet<>();
		transids_genome = new HashSet<>();
		transids_list = new HashSet<>();

		List<ExperimentAnnotation> experiments = getExperiments(organism);

		genome = new GenomeAnnotationSet(organism, source, gtype, false);

		File folder = new File(inPath);
		File[] listOfFiles = folder.listFiles();
						
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				File file = listOfFiles[i];
				String fname = file.getName().substring(file.getName().lastIndexOf("\\") + 1,
						file.getName().lastIndexOf("."));

				// Convert the input IDs:

				Set<String> list_inputid = idlists.get(fname); /* array containing all the ids of the selected file */

				// cast the input list
				used_list = new ArrayList<String>(list_inputid);

				// initialize the converter
				testList = new ConvertIDs(organism, gtype, used_list);

				// read IDs test table
				IDFile = new File("/home/fabio/Annominer/src/main/resources/geneIDconverter/" + organism + ".txt");

				// create the two dictionaries needed for the Ids convertion
				testList.createDict(testList, IDFile);

				// convert Ids
				ArrayList<String> newList = testList.convertInputList();

				// cast the new input list
				list_inputids = new HashSet<String>(newList);

				list_knowninputids = new HashSet<>();
				symbols_genome = new HashSet<>();
				symbols_list = new HashSet<>();
				transids_genome = new HashSet<>();
				transids_list = new HashSet<>();
			
				String message = getKnownIDs();

				if (list_knowninputids.size() < 10) {
					System.out.println(message
							+ "A minimum of 10 known ids is required for this analysis.<br />Please make sure that your selection of organism and genome-annotation (refseq, ensembl ...) matches your id-list and your identifiers are gene ids.");
					return;
				}
			
				enrichmentresults = new ArrayList<EnrichmentResult>();/* contructor */
			
				/*
				 * in case that our selected organism in c.elegans or drosophila we switch our
				 * source to modENCODE
				 */
				if (organism.equals("dm3") || organism.equals("dm6") || organism.equals("ce10")
						|| organism.equals("ce11")) {
					source = new Source("modencode");
				}
			
				try {

					for (ExperimentAnnotation experiment : experiments) {

						if (promoterUsString.equals("dynamic")) {

							if (experiment.target.startsWith("egfp-")) {
								target = experiment.target.split("egfp-")[1];
							} else {
								target = experiment.target;
							}
							promoterUs = cutoff.get(target);
						}

						parseIntersections(experiment, organism, source);

					}

				} catch (ChromIDException e) {
					System.out.println(e.getMessage() + "! ... Make sure your organism-selection is correct.");
					return;
				}
				// sort the list for ascending p-value
				Collections.sort(enrichmentresults);

				// compute the FDR (Benjamini-Hochberg correction)
				// first step after the ranking process compute the adjusted p-val (q val)

				for (int i1 = 0; i1 < enrichmentresults.size(); i1++) {

					double fdr = (enrichmentresults.get(i1).pval) * (enrichmentresults.size() / (i1 + 1));

					if (fdr > 1) {
						fdr = 1;
					}

					enrichmentresults.get(i1).setFdr(fdr);

				}
				// second step check if is at one point descending substitute the precedent
				// higher value with the smaller following one
				for (int i1 = 0; i1 < (enrichmentresults.size() - 1); i1++) {

					if (enrichmentresults.get(i1).fdr > enrichmentresults.get(i1 + 1).fdr) {
						double newFdr = enrichmentresults.get(i1 + 1).fdr;
						enrichmentresults.get(i1).setFdr(newFdr);
						i1 = 0;
					}

				}
					
				String tempstring = enrichmentResults2JSON();

				final String temppath = outPath + File.separator;
				BufferedWriter writer = new BufferedWriter(new FileWriter(temppath + fname));
				writer.write(tempstring);
				writer.close();
	
	
			}
		}
	}


	/**This method checks if the region and the tregion are overlapping and if it is respecting the overlap value choosen by the user.
	 * If yes the gene name is stored in the hits variable.
	 * 
	 * 
	 * @param transcript is the gene (Transcript object)
	 * @param region is the region in the chromosome
	 * @param chromlength length of the chromosome
	 */
	public static void genecheck(Transcript transcript, Region region, long chromlength) {

		Region tregion = transcript.flankingRegion5p(chromlength, (promoterDs + promoterUs), -promoterDs);

		Region coverage = tregion.intersectionWith(region);

		if (coverage!=null) {

			boolean match = false;

			if (ovtype) {
				match = coverage.length() >= ovval;
			} else {
				match = coverage.length() > ovval * 0.01 * tregion.length();
			}


			if (match) {


					if (list_knowninputids.contains(transcript.pid)) {

						transids_listhits.add(transcript.pid);

						if (symbols_listhits.contains(transcript.symbol)) {

						} else {

							symbols_listhits.add(transcript.symbol);
						}

					}

					transids_genomehits.add(transcript.pid);

				}

			}

	}

	/**This method compute the intersection and check for all the genes within the user-selected region in according to the defined criteria
	 * 
	 * @param experiment object in experiments
	 * @param organism assembly/MongoDB db (es.dm3)
	 * @param source MongoDB collection (es. modencode)
	 * @param score_on_transcriptcounts set to true
	 * @throws ChromIDException
	 */

	public static void parseIntersections(ExperimentAnnotation experiment, String organism, Source source)
			throws ChromIDException {


		Chromosome chromosome = null;
		Transcript gene=null, fwgene=null;
		long chromsize=0;

		ListIterator<Region> iterator=null,fwiterator=null;
		boolean it_next = true;
		boolean it_ended = false;

		symbols_listhits = new HashSet<String>();
		symbols_genomehits = new HashSet<String>();
		transids_listhits = new HashSet<String>();
		transids_genomehits = new HashSet<String>();

		GenomeRegionTrack track = new GenomeRegionTrack(experiment.pid, organism, source);/*open connection with mongoDB and set DataTrack to these parameters*/
			
		// create the metadata collection
		
		//populate the database with the regions collections
//		DataTrackOfRegionsMongoDAO data = new DataTrackOfRegionsMongoDAO();
//		data.sql2mongo("hg19", experiment.pid,  source);
		
//		data.modmine2mongo("ce11", experiment.pid,  source);
		
		/*es . for dm3 and modencode source track is a collection of mongoDB collections in the database dm3 for modencode: modencode_3806, modencode_3231 ecc.*/
		for ( Chromosome chromtrack: track) {
			//for the moment in the genome assembly are not stored the mitochondrial genomes
			try {
				chromosome = genome.getChromByID(chromtrack.pid);
			} catch (ChromIDException e) {
				System.out.println("not listed chromosome: " + chromtrack.pid);
				continue;
			}
			
			/*check to see if the collection has the defined chromosome*/
			if( chromosome == null ) {
//				LOGGER.log(Level.INFO, "Genome-annotation has no chromosome named: " + chromtrack.pid + " (track: "+track.trackid+")" );
				continue;
			}
			/*check to see if within the chromosome we have gene regions*/
			if (chromosome.getRegionNumber() == 0) {
//				LOGGER.log(Level.INFO, "Genome-annotation has no genes for chromosome: " + chromtrack.pid );
				continue;
			}


			chromsize = chromosome.length;/*length of the chromosome in the collection es. modencode_3806*/
			
			iterator = chromosome.listIterator();/*iterator through the regions of the chromosome*/
			
			it_next = true;
			it_ended = false;


			for ( Region region: chromtrack) {/*for each region in the chromosome*/

				if (it_ended) {

					//pass;

				} else {
					while (true) {
						if (it_next) {

							if ( iterator.hasNext() ) {

								gene = (Transcript) iterator.next();

								it_next = false;

							} else {

								it_ended = true;
								break;

							}

						}
						
						/*select for the coverage analysis only the genes that have the TF within the defined promoterUs limit*/
						if (gene.isLeftOf(region, promoterUs)) {

							it_next = true;

						} else if (gene.isRightOf(region, promoterUs)) {

							it_next = false;
							break;


						} else {
							
//							System.out.println("symbol: " + gene.symbol);
//							System.out.println("pid: " + gene.pid);

							genecheck(gene, region, chromsize);/*check if there is a match between the gene/region overlap in according to the overlap choosen by the user*/
							
							fwiterator= chromosome.listIterator(iterator.nextIndex());
							while (true) {

								if( !(fwiterator.hasNext()) )
									break;

								fwgene = (Transcript)fwiterator.next();


								if (fwgene.isRightOf(region, promoterUs)) {

									break;

								} else if (fwgene.isLeftOf(region, promoterUs)) {

									//pass;

								} else {


									genecheck(fwgene, region, chromsize);/*check if there is a match between the fwgene/region overlap in according to the overlap choosen by the user*/

								}

							}


							it_next = false;
							break;

						}

					}

				}

			}

		}


		/////////////////////////////////////////
		/* Write list of matching gene-symbols */
		/////////////////////////////////////////

		int genomesize;
		int genomehits;
		int listsize;
		int listhits;

		Set<String> poshits;


		genomesize = transids_genome.size();
		genomehits = transids_genomehits.size();
		listsize = transids_list.size();
		listhits = transids_listhits.size();
		poshits = symbols_listhits;


		double score = ((double)listhits/listsize)/((double)genomehits/genomesize);
		double pval;

		if ( listhits == 0 ) {
			pval = 1.0;
		} else {

			pval = Statistics.getRightHypergeometricPvalue( listhits, listsize, genomehits, genomesize );

		}


		/////////////////////////////
		/* Store enrichment result */
		/////////////////////////////

		EnrichmentResult result = new EnrichmentResult( experiment, score, pval);

		result.listhits = listhits;
		result.listsize = listsize;
		result.genomehits = genomehits;
		result.genomesize = genomesize;
		result.positives = poshits;

		enrichmentresults.add(result);

	}

	/**
	 * This method return a list of ExperimentAnnotation ("datatracks") for an organism.
	 * "datatracks" contains all the elements that are present in the mongoDB (getObjectIterator method from ExperimentMongoDAO)
	 * for a choosen database(organism) and collection(METACOLLECTION=metadata) (see getCollection method of the class MongoDBConnector)
	 * that respects the filtering for the selected field (es. "targettype") and filter values(es. "TF")
	 * @param organism value of the chosen Assembly (es. dm3)
	 * @return datatracks: collection of all the ExperimentAnnotations elements for the selected organism, collection and filter
	 */
	private static List<ExperimentAnnotation> getExperiments( String organism ) {

		List<ExperimentAnnotation> datatracks = new ArrayList<>();/*constructor*/
		
		ExperimentDAO expdao = ExperimentMongoDAO.getInstance();/*return an ExperimentMongoDAO and open a new MongoClient addressed to the localhost*/

		if(enrich.equals("TFs")) {

			Iterator<ExperimentAnnotation> iter = expdao.getObjectIterator( organism, "targettype", "TF" );
			while ( iter.hasNext() ) {
				datatracks.add( iter.next() );
			}
		}else if(enrich.equals("HMs")){
			Iterator<ExperimentAnnotation> iter = expdao.getObjectIterator( organism, "targettype", "HM" );
			while ( iter.hasNext() ) {
				datatracks.add( iter.next() );
			}
		}

		/*return the collection of all the ExperimentAnnotations elements*/
		return datatracks;

	}

	/**
	 * This method check for each transcript symbol present in the region of each
	 * chromosome within the genome and return in a string named "message" the
	 * number of uploaded list ids recognized in our database respect the whole
	 * uploaded list.
	 * 
	 * @param ids_are_transcriptids initially set to false
	 * @return a string (message) containing the number of recognized ids in our
	 *         database respect the whole uploaded list
	 */
	private static String getKnownIDs() {

		for (Chromosome chromosome : genome.genome) {

			for (Region region : chromosome) {

				Transcript transcript = (Transcript) region;

				if (transcript.symbol == null) { /* if the transcript has no symbol it skips */
					// pass
				} else {


						transids_genome.add(transcript.pid);

						if (list_inputids.contains(transcript.symbol)) {

							if (transids_list.contains(transcript.pid)) {

							} else {
								transids_list.add(transcript.pid);
							}

							if (symbols_list.contains(transcript.symbol)) {
								// pass
							} else {
								symbols_list.add(transcript.symbol);
							}

						}

					}
				}
		}

		list_knowninputids = transids_list;/*
											 * in symbols_list we have all the IDs shared by the uploaded list and the
											 * database
											 */


		String message = list_inputids.size() - symbols_list.size() + " unknown in " + list_inputids.size()
				+ " input ids recognized";
		/*
		 * message says we have (difference between the uploaded list size and the
		 * shared ids list size) unknown in (the whole uploaded list) input ids
		 * recognized
		 */
		System.out.println(message);

		return message;

	}

	public String convertGeneListAsString(HashSet<String> targets) {

		int len;

		if (targets == null) {
			return "";
		} else {
			len = targets.size();
			if (len == 0) {
				return "";
			}
		}

		StringBuffer ostring = new StringBuffer();

		TreeSet<String> posranked = new TreeSet<String>();
		posranked.addAll(targets);

		for (String pos : posranked) {
			ostring.append(pos);
			ostring.append(";");
		}

		return ostring.substring(0, ostring.length() - 1).toString();

	}

	public String enrichmentResults2JSON() throws FileNotFoundException {

		StringBuffer resultstring = new StringBuffer();

		String finstring;

		if (enrichmentresults.size() == 0) {

			return null;

		} else {

		for ( EnrichmentResult result : enrichmentresults ) {

				resultstring.append(result.experiment.target + "\t" + result.experiment.probe + "\t"
						+ result.experiment.getTreatment() + "\t" + result.experiment.getReplicate() + "\t"
						+ result.listhits + "\t" + result.listsize + "\t" + result.genomehits + "\t" + result.genomesize
						+ "\t" + result.getGeneListAsString() + "\t"
						+ convertGeneListAsString(testList.convertGeneNames(result.positives)) + "\t" + result.score
						+ "\t" + result.pval + "\t" + result.fdr + "\n");

			}

			System.out.println("resultstring: " + resultstring.length());

			if (enrich.equals("TFs")) {

				finstring = "Transcription factor\tProbe\tTreatment\tReplicate\tlist hits\tlist-size\tgenome hits\tgenome-size\ttarget IDs\ttarget genes\tscore\tp-value\tFDR\n"
						+ resultstring;
			} else {

				finstring = "Histone mark\tProbe\tTreatment\tReplicate\tlist hits\tlist-size\tgenome hits\tgenome-size\ttarget IDs\ttarget genes\tscore\tp-value\tFDR\n"
						+ resultstring;

		}
		}

		return finstring;

	}

}
