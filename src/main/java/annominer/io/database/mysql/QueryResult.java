package annominer.io.database.mysql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Iterator;

public class QueryResult implements Iterator<Object[]>, Iterable<Object[]> {

	private ResultSet resultset;
	private int cols;
	
	QueryResult(ResultSet resultset) {
		this.resultset = resultset;
		
		ResultSetMetaData meta;
		try {
			meta = resultset.getMetaData();
			this.cols = meta.getColumnCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/* Interfaces */
	
	@Override
	public Iterator<Object[]> iterator() {
		return this;
	}
	
	@Override
	public boolean hasNext() {
		try {
			return resultset.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Object[] next() {
		
		Object[] line = new Object[this.cols];
		
		try {
			
			for (int i=0; i<this.cols; i++) {
				line[i] = resultset.getObject(i+1);
			};
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return line;
	}

	@Override
	public void remove() {}
	
	
	/* Methods */
	
	public int getColumnNumber() {
		return cols;
	}
	
	
	
	
}
