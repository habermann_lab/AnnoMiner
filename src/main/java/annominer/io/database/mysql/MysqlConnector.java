package annominer.io.database.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TimeZone;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import annominer.Source;


public class MysqlConnector {
	
	private static final String STD_HOST = "localhost";
	//private static final int STD_PORT = 3306;
	private static final String STD_USER = "root";
	private static final String STD_PASSWORD = "mei";
	
	
	private static MysqlConnector connector = new MysqlConnector();
	
	private static MysqlDataSource localmysql;
	private static Connection connection;
	
	
	private MysqlConnector() {
		connector = new MysqlConnector();
	}
	
	
	public static MysqlConnector getInstance() {
		return connector;
	}
	
	
	public void connect( String database) {
		connect( STD_HOST, 0, STD_USER, STD_PASSWORD, database);
	}
	
	public void connect( String host, String user, String password, String database ) {
		connect(host, 0, user, password, database);
	}
	
	public void connect(Source source, String database) {
		connect(source.host, source.port, source.user, source.password, database);
	}
	
	public void connect(String host, int port, String user, String password, String database) {
		
		localmysql = new MysqlDataSource();
		localmysql.setUser(user);
		localmysql.setServerName(host);
		if (port != 0) { localmysql.setPort(port); }
		if (password != null) {	localmysql.setPassword(password); }
		localmysql.setDatabaseName(database);
		
		
		try {
			connection = localmysql.getConnection();
		
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
		} 
		
	}
	
	
	public QueryResult query( String query ) {
		
		try {
			
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(query);
			
			return new QueryResult(resultset);
			
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
			return null;
		}
		
	}
	
	
	
	public QueryResult pivotQuery( String query, String[] aggregatecolumns ) {
		
		try {
			
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(query);
			
			return new QueryResult(resultset);
			
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
			return null;
		}
		
	}
	
	
	
	
	public ResultSet queryToResultSet( String query ) {
		
		try {
			
			Statement statement = connection.createStatement();
			ResultSet resultset = statement.executeQuery(query);			
			
			return resultset;
			
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
			return null;
		}
		
		
		
	}

	
	public int update( String query ) {
		
		try {
		
		Statement statement = connection.createStatement();
		int response =  statement.executeUpdate(query);
		return response;
		
		
		} catch ( SQLException sqlException) {
			System.out.println(query);
			sqlException.printStackTrace();
			return 0;
		}
		
		
		
	}
	
	
	public void close() {
		
		try {
			connection.close();
			
		} catch ( Exception exception ) {
			exception.printStackTrace();
		}
		
	}
	
	
	private void setTimeZone(){
		
		TimeZone timeZone = TimeZone.getDefault();
        System.out.println("Default time zone is:");
        System.out.println(timeZone);
        
        System.out.println("Setting UTC as default time zone");
        TimeZone newTimeZone = TimeZone.getTimeZone("UTC");
        TimeZone.setDefault(newTimeZone);
        
	}
	
	
	
}


