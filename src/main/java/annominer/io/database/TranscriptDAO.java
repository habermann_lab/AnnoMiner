package annominer.io.database;

import java.util.Iterator;

import annominer.Transcript;
import annominer.exceptions.ChromIDException;

public interface TranscriptDAO {
	
	public Transcript getObject( String organism, String trackid, String pid ) throws ChromIDException;
	public Iterator<Transcript> getObjectIterator(String organism, String trackid);
	
}



