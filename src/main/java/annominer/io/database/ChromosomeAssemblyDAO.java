package annominer.io.database;

import java.util.Iterator;

import annominer.ChromosomeAssembly;

public interface ChromosomeAssemblyDAO {
	
	public ChromosomeAssembly getObject( String organism, String pid );
	public Iterator<ChromosomeAssembly> getObjectIterator(String organism);
	

}
