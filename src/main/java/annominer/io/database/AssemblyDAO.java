package annominer.io.database;

import java.util.Iterator;

import annominer.ChromosomeAssembly;

public interface AssemblyDAO {
	
	public Iterator<ChromosomeAssembly> getObjectIterator(String organism);
	public ChromosomeAssembly getObjectByID(String id, String organism);
	
}
