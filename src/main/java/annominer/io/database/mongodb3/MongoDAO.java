package annominer.io.database.mongodb3;

import org.bson.Document;


public interface MongoDAO<T> {
	
	public final static String METACOLLECTION = "metadata";
	public final static String REMOTEDATABASECOLLECTION = "remotedb_metadata";
	public final static String INDEXCOLLECTION = "indexdb";
	
	public T getObject( Document entry );
	
	
}
