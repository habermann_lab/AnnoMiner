package annominer.io.database.mongodb3;


import java.util.List;

import org.bson.Document;
import annominer.Chromosome;
import annominer.OrientedRegion;



public class ChromosomeOfOrientedRegionsMongoDAO extends ChromosomeMongoDAO {
	
	//Singleton member:
	private static ChromosomeOfOrientedRegionsMongoDAO chromdao = new ChromosomeOfOrientedRegionsMongoDAO();	
	
	
	/* Constructors */
	
	private ChromosomeOfOrientedRegionsMongoDAO() {
		super();
	}
	
	
	
	/* Interfaces */
	
	
	/* Methods */
	
	
	public static ChromosomeOfOrientedRegionsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	@Override
	public Chromosome getObject( Document chrom ) {
		
		Chromosome chromosome = new Chromosome(chrom.getString("pid"), chrom.getLong("length") );
		
		@SuppressWarnings("unchecked")
		List<List<Object>> regionlist = (List<List<Object>>)chrom.get("regions");
		
		for(List<Object> regionx : regionlist) {
			
			long tleft = (long)regionx.get(0);
			long tright = (long)regionx.get(1);
			boolean strand = (boolean)regionx.get(2);
			
			OrientedRegion region = new OrientedRegion(tleft,tright,strand);
			chromosome.add(region);
			
		}
		
		return chromosome;
		
	}
	
	
	
	
}


