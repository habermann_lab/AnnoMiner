package annominer.io.database.mongodb3;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.io.file.TSVReader;

import com.mongodb.client.MongoCollection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import org.bson.Document;
import org.intermine.metadata.Model;
import org.intermine.pathquery.Constraints;
import org.intermine.pathquery.OrderDirection;
import org.intermine.pathquery.PathQuery;
import org.intermine.webservice.client.core.ServiceFactory;
import org.intermine.webservice.client.results.JsonRow;
import org.intermine.webservice.client.services.QueryService;


public class DataTrackOfRegionsMongoDAO extends DataTrackMongoDAO {
	
	
	private static DataTrackOfRegionsMongoDAO trackdao = new DataTrackOfRegionsMongoDAO();
	
	/* Constructors */
	
	public DataTrackOfRegionsMongoDAO() {
		super();
		chromdao = ChromosomeOfRegionsMongoDAO.getInstance();
	}
	
	
	public static DataTrackOfRegionsMongoDAO getInstance() {
		return trackdao;
	}
	
	
	
	public void sql2mongo(String organism, String trackid, Source source) {
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		MysqlDataSource localmysql = null;
		
		
		localmysql = new MysqlDataSource();
		localmysql.setUser(source.user);
		localmysql.setServerName(source.host);
		localmysql.setDatabaseName(organism);
		if (source.password != null) {
			localmysql.setPassword(source.password);
		}
		
		
		MysqlDataSource datasource = localmysql;
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		
		String lastchrom = null;
		List<List<Object>> regionlist = new ArrayList<List<Object>>();
		
		Long chromlength = 10000L;
		
		try {
			
			connection = datasource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT chrom, chromStart, chromEnd FROM "+ trackid +" WHERE SUBSTRING(chrom,-6)!=\'random\' ORDER BY chrom, chromStart");
			
			while ( resultSet.next() ) {
						
				if ( resultSet.getString(1).equals(lastchrom) ) {
					
					regionlist.add( Arrays.asList((Object)resultSet.getLong(2),(Object)resultSet.getLong(3)) );
					
				} else {
					
					if ( lastchrom != null ) {
						
						Document chrom = new Document("pid",lastchrom);
						chrom.append("length", chromlength);
						chrom.append("regions", regionlist);
						
						coll.insertOne(chrom);
						
					}
					
					lastchrom = resultSet.getString(1);
					
					regionlist = new ArrayList<List<Object>>();
					
					regionlist.add( Arrays.asList((Object)resultSet.getLong(2),(Object)resultSet.getLong(3)) );
					
				}	
				
			}
				
			if ( lastchrom != null ) {
				
				Document chrom = new Document("pid",lastchrom);
				chrom.append("length", chromlength);
				chrom.append("regions", regionlist);
				
				coll.insertOne(chrom);
				
			}	
				
				
	
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			}
			catch ( Exception exception ) {
				exception.printStackTrace();
			}
		}
		
				
	}
	
	
	public void modmine2mongo(String organism, String trackid, Source source) {
    	
		// MongoDB //
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		List<List<Object>> regionlist = new ArrayList<List<Object>>();
		
		// Intermine //
		ServiceFactory factory = new ServiceFactory(ROOT);
		QueryService service = factory.getQueryService();
        Model model = factory.getModel();
        
        PathQuery query = new PathQuery(model);
        
        System.out.println("trackid: " + trackid);
        
        // Select the output columns:
        query.addViews("Submission.features.chromosome.primaryIdentifier",
                "Submission.features.chromosomeLocation.start",
                "Submission.features.chromosomeLocation.end");
        
        // Add order by
        query.addOrderBy("Submission.features.chromosome.primaryIdentifier", OrderDirection.ASC);
        query.addOrderBy("Submission.features.chromosomeLocation.start", OrderDirection.ASC);
        
        // Filter the results with the following constraints:
        query.addConstraint(Constraints.eq("Submission.DCCid", trackid));
        
        Iterator<List<Object>> rows = service.getRowListIterator(query);
        
		Long chromlength = 10000L;
		String lastchrom = null;
		String lastchrom_ucsc = null;
		
		
		while (rows.hasNext()) {
			
			JsonRow row = (JsonRow) rows.next();
			
			if ( ((String)row.get(0)).equals(lastchrom) ) {
				
				regionlist.add( Arrays.asList( (Object)Long.valueOf((int)row.get(1)), (Object)Long.valueOf((int)row.get(2))) );
				
			} else {
				
				if (lastchrom != null) {
					
					Document chrom = new Document("pid",lastchrom_ucsc);
					chrom.append("length", chromlength);
					chrom.append("regions", regionlist);
					coll.insertOne(chrom);
					
				}
				
				lastchrom = (String)row.get(0);
				
				if ( lastchrom.startsWith("chr") ) {
					lastchrom_ucsc = lastchrom;
				} else {
					
					if( lastchrom.equals("MtDNA") ) {
						lastchrom_ucsc = "chrM";
					} else {
						lastchrom_ucsc = "chr" + lastchrom;
					}
				}

				regionlist = new ArrayList<List<Object>>();
				
				regionlist.add( Arrays.asList( (Object)Long.valueOf((int)row.get(1)), (Object)Long.valueOf((int)row.get(2))) );
				
				
				
				
				
			}
			
		}
		
		
		
		if ( lastchrom != null ) {
			
			Document chrom = new Document("pid",lastchrom_ucsc);
			chrom.append("length", chromlength);
			chrom.append("regions", regionlist);
			coll.insertOne(chrom);
			
		}
		
		
		
	}
	
	
	protected void file2mongo ( String organism, String trackid, Source source) throws FileFormatException {
		
		final Runtime runtime = Runtime.getRuntime();
		
		int chromidcol, startcol, endcol;
		
		if ( source.fileformat.equals("bed")) {
			chromidcol = 0; startcol=1; endcol=2;
		} else if ( source.fileformat.equals("gff3")) {
			chromidcol = 0; startcol=3; endcol=4;
		} else {
			chromidcol = 0; startcol=1; endcol=2;
		}
		
		
		try {
			Process process = runtime.exec("sort -k "+(chromidcol+1)+","+(chromidcol+1)+" -k "+(startcol+1)+","+(startcol+1)+"n -o "+source.filepath+"_sorted "+source.filepath);
			process.waitFor();
		} catch (IOException e ) {
			// Silence error
		} catch (InterruptedException g ) {
			
		}
		
		
		TSVReader ifh = new TSVReader( source.filepath+"_sorted" );
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		List<List<Object>> regionlist = new ArrayList<List<Object>>();
		
		String lastchrom = null;
		String lastchrom_ucsc = null;
		Long chromlength = 10000L;
		
		int linecounter = 0;
		
		String[] temp = {"",""};
		
		try {
			
			
			for (String[] line : ifh) {
				
				linecounter++;
				temp = line;
				
				if( line[0].startsWith("#") ) {continue;}
				
				if ( line[chromidcol].equals(lastchrom) ) {
					
					regionlist.add( Arrays.asList( (Object)Long.parseLong(line[startcol]), (Object)Long.parseLong(line[endcol]) ) );
					
				} else {
					
					if ( lastchrom != null ) {
						
						Document chrom = new Document("pid",lastchrom_ucsc);
						chrom.append("length", chromlength);
						chrom.append("regions", regionlist);
						
						coll.insertOne(chrom);
						
					}
					
					lastchrom = line[chromidcol];
					
					if ( lastchrom.startsWith("chr") ) {
						lastchrom_ucsc = lastchrom;
					} else {
						
						if( lastchrom.equals("MtDNA") ) {
							lastchrom_ucsc = "chrM";
						} else {
							lastchrom_ucsc = "chr" + lastchrom;
						}
					}
					
					regionlist = new ArrayList<List<Object>>();
					
					regionlist.add( Arrays.asList( (Object)Long.parseLong(line[startcol]), (Object)Long.parseLong(line[endcol]) ) );		
				}
				
			
			}
			
			if ( lastchrom != null ) {
				
				Document chrom = new Document("pid",lastchrom_ucsc);
				chrom.append("length", chromlength);
				chrom.append("regions", regionlist);
				coll.insertOne(chrom);
				
			}
		
		} catch ( ArrayIndexOutOfBoundsException e ) {
			
			this.mongoconnector.deleteCollection(organism, trackid);
			throw new FileFormatException( source.fileformat, linecounter );
			
		} catch ( NumberFormatException e ) {
			
			this.mongoconnector.deleteCollection(organism, trackid);
			throw new FileFormatException( source.fileformat, linecounter );
			
		} finally {
			
			ifh.close();
			
			File file = new File(source.host+"_sorted");
			file.delete();
			
		}
		
		
	}
	
	
	
	
	
}
