package annominer.io.database.mongodb3;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import annominer.Source;
import annominer.io.file.TSVReader;


public class GenomeMongoDAO extends DataTrackMongoDAO {
	
	private static GenomeMongoDAO trackdao = new GenomeMongoDAO();
	
	/* Constructors */
	
	public GenomeMongoDAO() {
		super(); /*new MongoClient addressed to the local host*/
		chromdao = ChromosomeOfTranscriptsMongoDAO.getInstance(); /*chromdao ChromosomeMongoDAO*/
	}
	
	
	public static GenomeMongoDAO getInstance() {
		return trackdao;
	}
	
	
	protected void sql2mongo(String organism, String trackid, Source source) {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		MysqlDataSource localmysql = null;
		
		localmysql = new MysqlDataSource();
		localmysql.setUser(source.user);
		localmysql.setServerName(source.host);
		localmysql.setDatabaseName(organism);
		if (source.password != null) {
			localmysql.setPassword(source.password);
		}
		
		MysqlDataSource datasource = localmysql;
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		String lastgene = null;
		String lastchrom = null;
		long lastchromlength = 0;
		
		List<Object> longest = null;
		long longest_len = 0;
		
		Map<String,Integer> gene_idx = new HashMap<String,Integer>();
		List<List<Object>> genelist = new ArrayList<List<Object>>();
		
		
		try {
			
			connection = datasource.getConnection();
			statement = connection.createStatement();
			
			if (trackid.equals("refseq")) {
				resultSet = statement.executeQuery(
						"SELECT ncbiRefSeq.chrom, chromInfo.size, ncbiRefSeq.name, ncbiRefSeq.name2, ncbiRefSeq.txStart, ncbiRefSeq.txEnd, ncbiRefSeq.strand, ncbiRefSeq.cdsStart, ncbiRefSeq.cdsEnd, ncbiRefSeq.exonStarts, ncbiRefSeq.exonEnds FROM ncbiRefSeq LEFT JOIN chromInfo ON chromInfo.chrom=ncbiRefSeq.chrom WHERE !(INSTR(ncbiRefSeq.chrom,'_')) ORDER BY ncbiRefSeq.chrom, ncbiRefSeq.txStart");
			} else if (trackid.equals("refseq2")) {
				resultSet = statement.executeQuery(
						"SELECT refGene.chrom, chromInfo.size, refGene.name, refGene.name2, refGene.txStart, refGene.txEnd, refGene.strand, refGene.cdsStart, refGene.cdsEnd, refGene.exonStarts, refGene.exonEnds FROM refGene LEFT JOIN chromInfo ON chromInfo.chrom=refGene.chrom WHERE !(INSTR(refGene.chrom,'_')) ORDER BY refGene.chrom, refGene.txStart");
			} else if (trackid.equals("ensembl")) {
				resultSet = statement.executeQuery("SELECT ensGene.chrom, chromInfo.size, ensGene.name, ensGene.name2, ensGene.txStart, ensGene.txEnd, ensGene.strand, ensGene.cdsStart, ensGene.cdsEnd, ensGene.exonStarts, ensGene.exonEnds FROM ensGene LEFT JOIN chromInfo ON chromInfo.chrom=ensGene.chrom WHERE !(INSTR(ensGene.chrom,'_')) ORDER BY ensGene.chrom, ensGene.txStart");
			} else if ( trackid.equals("ucsc") ) {
				resultSet = statement.executeQuery("SELECT knownGene.chrom, chromInfo.size, knownGene.name, kgXref.geneSymbol, knownGene.txStart, knownGene.txEnd, knownGene.strand, knownGene.cdsStart, knownGene.cdsEnd, knownGene.exonStarts, knownGene.exonEnds FROM knownGene LEFT JOIN kgXref ON kgXref.kgID=knownGene.name LEFT JOIN chromInfo ON chromInfo.chrom=knownGene.chrom WHERE SUBSTRING(knownGene.chrom,-6)!=\'random\' ORDER BY knownGene.chrom, knownGene.txStart");
			} else if ( trackid.equals("genecode") ) {
				String version = "34";

				if (organism.equals("mm10")) {
					version = "M25";
				} else if (organism.equals("hg19")) {
					version = "34lift37";
				}

				resultSet = statement.executeQuery("SELECT wgEncodeGencodeBasicV"+version+".chrom, chromInfo.size, wgEncodeGencodeBasicV"+version+".name, wgEncodeGencodeAttrsV"+version+".geneId, wgEncodeGencodeBasicV"+version+".txStart, wgEncodeGencodeBasicV"+version+".txEnd, wgEncodeGencodeBasicV"+version+".strand, wgEncodeGencodeBasicV"+version+".cdsStart, wgEncodeGencodeBasicV"+version+".cdsEnd, wgEncodeGencodeBasicV"+version+".exonStarts, wgEncodeGencodeBasicV"+version+".exonEnds FROM wgEncodeGencodeBasicV"+version+" LEFT JOIN wgEncodeGencodeAttrsV"+version+" ON wgEncodeGencodeBasicV"+version+".name=wgEncodeGencodeAttrsV"+version+".transcriptId LEFT JOIN chromInfo ON chromInfo.chrom=wgEncodeGencodeBasicV"+version+".chrom WHERE SUBSTRING(wgEncodeGencodeBasicV"+version+".chrom,-6)!=\'random\' AND SUBSTRING(wgEncodeGencodeBasicV"+version+".chrom,-3)!=\'alt\' AND SUBSTRING(wgEncodeGencodeBasicV"+version+".chrom,1,5)!=\'chrUn\' ORDER BY wgEncodeGencodeBasicV"+version+".chrom, wgEncodeGencodeBasicV"+version+".txStart");   
			} else if ( trackid.equals("flybase") ) {
				resultSet = statement.executeQuery(
						"SELECT flyBaseGene.chrom, chromInfo.size, flyBaseGene.name, flyBaseGene.name, flyBaseGene.txStart, flyBaseGene.txEnd, flyBaseGene.strand, flyBaseGene.cdsStart, flyBaseGene.cdsEnd, flyBaseGene.exonStarts, flyBaseGene.exonEnds FROM flyBaseGene LEFT JOIN chromInfo ON chromInfo.chrom=flyBaseGene.chrom WHERE !(INSTR(flyBaseGene.chrom,'_')) ORDER BY flyBaseGene.chrom, flyBaseGene.txStart");
			} else if (trackid.equals("wormbase")) {
				resultSet = statement.executeQuery(
						"SELECT ws245Genes.chrom, chromInfo.size, ws245Genes.name, ws245Genes.name, ws245Genes.txStart, ws245Genes.txEnd, ws245Genes.strand, ws245Genes.cdsStart, ws245Genes.cdsEnd, ws245Genes.exonStarts, ws245Genes.exonEnds FROM ws245Genes LEFT JOIN chromInfo ON chromInfo.chrom=ws245Genes.chrom WHERE !(INSTR(ws245Genes.chrom,'_')) ORDER BY ws245Genes.chrom, ws245Genes.txStart");
			} else {
				System.out.println("Invalid genome annotation: "+trackid);
			}
			
			while ( resultSet.next() ) {
				
				if ( resultSet.getString(1).equals(lastchrom) ) {
					
					if ( resultSet.getString(4).equals(lastgene) ) {
						
						if ( resultSet.getLong(6)-resultSet.getLong(5) > longest_len ) {
							
							longest.set(8, false);
							
							longest = sql2Transcript(resultSet,true);
							genelist.add(longest);
							
							longest_len = resultSet.getLong(6)-resultSet.getLong(5);
							
							gene_idx.put(resultSet.getString(4), genelist.size() -1);
							
						} else {
							genelist.add(sql2Transcript(resultSet,false));
						}
							
					} else {
						
						if ( gene_idx.containsKey(resultSet.getString(4)) ) {
							
							List<Object> clongest_len = genelist.get( gene_idx.get(resultSet.getString(4)) );
							
							if ( (Long)clongest_len.get(1) + 5000 < resultSet.getLong(5) ) {
								
								genelist.add(sql2Transcript(resultSet,true));
								gene_idx.put(resultSet.getString(4), genelist.size() -1);
								
								
							} else {
								if ( resultSet.getLong(6)-resultSet.getLong(5) > (long)clongest_len.get(1)-(long)clongest_len.get(0) ) {
									
									clongest_len.set(8,false);
									genelist.add(sql2Transcript(resultSet,true));
									gene_idx.put(resultSet.getString(4), genelist.size() -1);
									
								} else {
									genelist.add(sql2Transcript(resultSet,false));
								}
							}
							
						} else {
							
							longest = sql2Transcript(resultSet,true);
							longest_len = resultSet.getLong(6)-resultSet.getLong(5);
							genelist.add(longest);
							gene_idx.put(resultSet.getString(4), genelist.size() -1);
							
						}
						
						lastgene = resultSet.getString(4);
						
					}	
					
				} else {
					
					if ( lastchrom != null ) {
						
						Document chrom = new Document("pid",lastchrom);
						chrom.append("length", lastchromlength);
						chrom.append("transcripts", genelist);							  
						
						coll.insertOne(chrom);
						
					}
					
					genelist = new ArrayList<List<Object>>();
					gene_idx = new HashMap<String,Integer>();
					
					lastchrom = resultSet.getString(1);
					lastchromlength = resultSet.getLong(2);
					lastgene = resultSet.getString(4);
					
					longest = sql2Transcript(resultSet,true);										
					longest_len = resultSet.getLong(6)-resultSet.getLong(5);
					genelist.add(longest);
					
					gene_idx.put(resultSet.getString(4), genelist.size() -1);
					
				}	
				
			}
				
			if ( lastchrom != null ) {
				
				Document chrom = new Document("pid",lastchrom);
				chrom.append("length", lastchromlength);
				chrom.append("transcripts", genelist);
				
				coll.insertOne(chrom);
				
			}	
				
				
	
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			}
			catch ( Exception exception ) {
				exception.printStackTrace();
			}
		}
		
				
	}
	
	
	private static List<Object> sql2Transcript(ResultSet resultset, boolean longest) throws SQLException {
		
		boolean strand = resultset.getString(7).equals("+");
		
		List<Object> trans = Arrays.asList( resultset.getLong(5),
											resultset.getLong(6),
											strand,
											resultset.getString(3),
											resultset.getString(4),
											resultset.getLong(8),
											resultset.getLong(9),
											sql2Exons(resultset, strand),
											longest
											);
		
		return trans;
		
	}
	
	
	private static List<List<Object>> sql2Exons(ResultSet resultset, boolean strand) throws SQLException {
		
		Blob startblob = resultset.getBlob(10);
		Blob endblob = resultset.getBlob(11);
		
		String startstr = new String(startblob.getBytes(1l, (int) startblob.length()));
		String endstr = new String(endblob.getBytes(1l, (int) endblob.length()));
		
		String[] starts = startstr.split(",");
		String[] ends = endstr.split(",");
		
		List<List<Object>> exlist = new ArrayList<List<Object>>();
		
		for (int i=0; i<starts.length; i++) {
			
			exlist.add( Arrays.asList( (Object)Long.valueOf(starts[i]), (Object)Long.valueOf(ends[i]), (Object)strand ) );
			
		}
		
		return exlist;
		
	}
	
	
	protected void modmine2mongo(String organism, String trackid, Source source) {
		
		//DEPRECATED: obtaining gene-models from modmine is notsupported any more
		
		String fname = "filepath";
		TSVReader ifh = new TSVReader( fname );
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		String lastchrom = null;
		long lastchromlength = 0;
		
		String lastgene = null;
		String lasttrans = null;
		boolean strand = true;
		
		List<List<Object>> genelist = new ArrayList<List<Object>>();
		List<List<Object>> translist = new ArrayList<List<Object>>();
		List<List<Object>> exlist = null;
		
		long longest = 0;
		int longest_idx = 0;
		
		
		for ( String[] row: ifh) {
			
			
			if ( row[0].equals(lastchrom) ) {
				
				if ( row[2].equals(lastgene) ) {
					
					if (row[7].equals(lasttrans) ) {
					
						exlist.add( intermine2Exon(row, strand) );
						
					} else {
						
						exlist = new ArrayList<List<Object>>();
						
						if ( ( Long.valueOf(row[9]) - Long.valueOf(row[8]) ) > longest ) {
							
							((List<Object>)translist.get(longest_idx)).set(8, false);
							
							translist.add( intermine2Transcript(row, strand, exlist, true) );
							
							longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
							longest_idx = translist.size() -1;
							
						} else {
							
							translist.add( intermine2Transcript(row, strand, exlist, false) );
							
						}
							
						exlist.add( intermine2Exon(row, strand) );
						
						lasttrans = row[7];
					
					}
					
				} else {
					
					translist = new ArrayList<List<Object>>();
					exlist = new ArrayList<List<Object>>();
					
					strand = row[6].equals("1");
					
					genelist.add( intermine2Gene(row, strand, translist) );
					
					translist.add( intermine2Transcript(row, strand, exlist, true) );
					
					exlist.add( intermine2Exon(row, strand) );
					
					longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
					longest_idx = 0;
					
					lastgene = row[2];
					lasttrans = row[7];
				
				}	
					
			} else {
				
				if (lastchrom != null) {
				
					Document chrom = new Document("pid",lastchrom);
					chrom.append("length", lastchromlength);
					chrom.append("genes", genelist);
					coll.insertOne(chrom);
					
				}	
				
				strand = row[6].equals("1");
				
				genelist = new ArrayList<List<Object>>();
				translist = new ArrayList<List<Object>>();
				exlist = new ArrayList<List<Object>>();
				
				genelist.add( intermine2Gene(row, strand, translist) );
				translist.add( intermine2Transcript(row, strand, exlist, true) );
				exlist.add( intermine2Exon(row, strand) );
				
				longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
				longest_idx = 0;
				
				lastchrom = row[0];
				lastchromlength = Long.valueOf(row[1]);
				lastgene = row[2];
				lasttrans = row[7];
			
			}
		
		}
		
			
		if ( lastchrom != null ) {
			
			Document chrom = new Document("pid",lastchrom);
			chrom.append("length", lastchromlength);
			chrom.append("genes", genelist);
			coll.insertOne(chrom);	
			
		}
		
		
		ifh.close();
		
		
		
	}
		
	
	private static List<Object> intermine2Exon(String[] row, boolean strand) {
		
		return Arrays.asList( (Object)Long.valueOf(row[12]), (Object)Long.valueOf(row[13]), (Object)strand );
		
	}
	
	
	private static List<Object> intermine2Transcript(String[] row, boolean strand, List<List<Object>> exlist, boolean longest) {
		
		return Arrays.asList( 	Long.valueOf(row[8]),
								Long.valueOf(row[9]),
								strand,
								row[7],
								row[2],
								Long.valueOf(row[10]),
								Long.valueOf(row[11]),
								exlist,
								longest
							);
		
	}
	
	
	private static List<Object> intermine2Gene(String[] row, boolean strand, List<List<Object>> translist) {
		
		return Arrays.asList(	Long.valueOf(row[4]),
								Long.valueOf(row[5]),
								strand,
								row[2],
								row[3],
								translist
							);
		
	}
	
	
	protected void file2mongo( String organism, String trackid, Source source) {
		//TODO
	}
	
	
	
	
	
}
