package annominer.io.database.mongodb3;

import static com.mongodb.client.model.Filters.eq;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;

import annominer.Chromosome;
import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.io.database.DataTrackDAO;


public abstract class DataTrackMongoDAO implements DataTrackDAO {
	
	
	protected static final String ROOT = "http://intermine.modencode.org/release-33/service"; /*set from release-32 to release-33*/
	
	protected ChromosomeMongoDAO chromdao;
	
	protected MongoDBConnector mongoconnector;
	
	/* Constructors */
	
	protected DataTrackMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	/* Methods */
	
	@Override
	public MongoDBIterator<Chromosome> getIterator( String organism, String trackid ) {
		return new MongoDBIterator<Chromosome>( chromdao, mongoconnector.getCursor( organism, trackid ) );
	}
	
	
	@SuppressWarnings("unused")
	@Override
	public boolean update(String organism, String trackid, Source source) {

		MongoCollection<Document> indexcoll = this.mongoconnector.getCollection(organism, "dbindex");
		Document entry = indexcoll.find( eq("1", trackid) ).first();
		
		if (entry == null) {
			
			insert(organism, trackid, source);
			indexcoll.insertOne(new Document( "1", trackid ));
			
			return false;
			
		} else {

			long time = System.currentTimeMillis();

			long inserttime = ((ObjectId) entry.get("_id")).getTime();
			
			if (time - inserttime < EXPIRATION_TIME || EXPIRATION_TIME < 0) {

				return true;

			} else {

//				indexcoll.deleteOne(eq("1", trackid));
				insert(organism, trackid, source);

				indexcoll.insertOne(new Document("1", trackid));

				return true;

			}
		}
	}
	
	
	private void insert(String organism, String trackid, Source source) throws FileFormatException {

		this.mongoconnector.getDatabase(organism).getCollection(trackid).drop();
		
		if ( source.source.equals("sql") ) {
			sql2mongo(organism, trackid, source);
		} else if ( source.source.equals("modencode") ) {
			modmine2mongo(organism, trackid, source);
		} else if ( source.source.equals("file") ) {/*it is call in the case of file upload (method defined in DataTrakOfRegionsMongoDAO)*/
			file2mongo(organism, trackid, source);
		} else {
			System.out.println("Error: Unknown data source: "+source.source+" !!!");
			
		}

	}
	
	
	protected abstract void sql2mongo( String organism, String trackid, Source source);
	protected abstract void modmine2mongo( String organism, String trackid, Source source);
	protected abstract void file2mongo( String organism, String trackid, Source source);
	
	
}



