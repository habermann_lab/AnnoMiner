package annominer.io.database.mongodb3;


import java.util.List;

import org.bson.Document;
import annominer.Chromosome;
import annominer.Region;


public class ChromosomeOfRegionsMongoDAO extends ChromosomeMongoDAO {
	
	
	//Singleton member:
	private static ChromosomeOfRegionsMongoDAO chromdao = new ChromosomeOfRegionsMongoDAO();
	
	
	
	/* Constructors */
	
	protected ChromosomeOfRegionsMongoDAO() {
		
		super();
	}
	
	
	/* Interfaces */
	
	
	
	/* Methods */
	
	
	public static ChromosomeOfRegionsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	@Override
	public Chromosome getObject( Document chrom ) {
		
		Chromosome chromosome = new Chromosome(chrom.getString("pid"), chrom.getLong("length"));
		
		@SuppressWarnings("unchecked")
		List<List<Object>> regionlist = (List<List<Object>>)chrom.get("regions");
		
		for(List<Object> regionx : regionlist) {
			
			long tleft = (long)regionx.get(0);
			long tright = (long)regionx.get(1);
			
			Region region = new Region(tleft,tright);
			chromosome.add(region);
			
		}
		
		return chromosome;
		
	}
	
	
}
