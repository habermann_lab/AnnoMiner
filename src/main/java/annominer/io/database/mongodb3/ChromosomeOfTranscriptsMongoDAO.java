package annominer.io.database.mongodb3;


import java.util.List;

import org.bson.Document;

import annominer.Chromosome;
import annominer.OrientedRegion;
import annominer.Transcript;


public class ChromosomeOfTranscriptsMongoDAO extends ChromosomeMongoDAO {
	
	//Singleton member:
	private static ChromosomeOfTranscriptsMongoDAO chromdao = new ChromosomeOfTranscriptsMongoDAO();	
	
	
	/* Constructors */
	
	private ChromosomeOfTranscriptsMongoDAO() {
		super();
	}
	
	
	
	/* Interfaces */
	
	
	/* Methods */
	
	
	public static ChromosomeOfTranscriptsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	
	public Chromosome getObject( Document chrom ) {
		
		Chromosome chromosome = new Chromosome(chrom.getString("pid"), chrom.getLong("length") );
		
		@SuppressWarnings("unchecked")
		List<List<Object>> regionlist = (List<List<Object>>)chrom.get("transcripts");
		
		for(List<Object> transcript : regionlist) {
			
			long tleft = (long)transcript.get(0);
			long tright = (long)transcript.get(1);
			boolean strand = (boolean)transcript.get(2);
			String tpid = (String)transcript.get(3);
			String tsymbol = (String)transcript.get(4);
			long cleft = (long)transcript.get(5);
			long cright = (long)transcript.get(6);
			
			Transcript trans = new Transcript(tleft,tright,strand,tpid,tsymbol,new OrientedRegion(cleft,cright,strand));
			trans.canonical = (boolean)transcript.get(8);
			
			@SuppressWarnings("unchecked")
			List<List<Object>> exons = (List<List<Object>>)transcript.get(7);
			
			for (List<Object> exon : exons) {
				
				long eleft = (long)exon.get(0);
				long eright = (long)exon.get(1);
				
				trans.addExon(new OrientedRegion(eleft, eright, strand));
			}
			
			chromosome.add(trans);
			
		}
		
		return chromosome;
		
	}
	
	
	
}


