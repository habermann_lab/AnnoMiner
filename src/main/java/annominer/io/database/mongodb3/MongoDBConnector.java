package annominer.io.database.mongodb3;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import org.apache.log4j.LogManager;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;


public class MongoDBConnector {
	
	private static String HOST = "127.0.0.1";
	//private static String HOST = "10.133.0.53"; //vm2-annominer
	private static int PORT = 27017;
	
	public static MongoClient mongoclient;
	
	private static MongoDBConnector connector = new MongoDBConnector();
	
	private MongoDBConnector() {
		connect();
	}
	
	
	public static MongoDBConnector getInstance() {
		
		return connector;
	}
	
	
	public void connect() {
		
		connect(HOST, PORT);
	}

	
	
	public void connect(String host, int port) {
		
		//LogManager.getLogger("org.mongodb.driver.connection").setLevel(org.apache.log4j.Level.OFF);
        //LogManager.getLogger("org.mongodb.driver.management").setLevel(org.apache.log4j.Level.OFF);
        //LogManager.getLogger("org.mongodb.driver.cluster").setLevel(org.apache.log4j.Level.OFF);
        //LogManager.getLogger("org.mongodb.driver.protocol.insert").setLevel(org.apache.log4j.Level.OFF);
        //LogManager.getLogger("org.mongodb.driver.protocol.query").setLevel(org.apache.log4j.Level.OFF);
        //LogManager.getLogger("org.mongodb.driver.protocol.update").setLevel(org.apache.log4j.Level.OFF);
		//props.setProperty("log4j.logger.org.mongodb.driver", "WARN");
		//Logger mongoLogger = Logger.getLogger( "com.mongodb" ); mongoLogger.setLevel(Level.SEVERE);
		mongoclient = new MongoClient( host , port );
		//mongoclient = MongoClients.create(new ConnectionString("mongodb://"+host+":"+port));
		
	}
	
	public void close() {
		mongoclient.close();
	}
	
	public MongoDatabase getDatabase(String db) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid);
	}
	
	public MongoCollection<Document> getCollection(String db, String coll) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).getCollection(coll);
	}
	
	public MongoCursor<Document> getCursor(String db, String coll) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).getCollection(coll).find().iterator();
	}
	
	public Document getDocument(String db, String coll, Bson filter) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).getCollection(coll).find(filter).first();
	}
	
	public Document getDocument(String db, String coll, BasicDBObject queryobj) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).getCollection(coll).find(queryobj).first();
	}
	
	public FindIterable<Document> findDocuments(String db, String coll, BasicDBObject queryobj) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).getCollection(coll).find(queryobj);
	}
	
	public static MongoClient getMongoClient() {
		//Expose MongoClient
		return mongoclient;
	}
	
	public MongoIterable<String> getCollectionList(String db) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDatabase(dbid).listCollectionNames();
	}
	
	public Set<String> getCollectionList(DB db) {
		return db.getCollectionNames();
	}
	
	public long getInsertionTime( DBObject entry) {
		return ((ObjectId)entry.get("_id")).getTime();
	}
	
	public void deleteDatabase( String database ) {
		mongoclient.dropDatabase( database );
	}
	
	public void deleteCollection( String database, String collection ) {
		mongoclient.getDatabase(database).getCollection(collection).drop();
	}
	
	
}
