package annominer.io.database.mongodb3;

import java.util.Iterator;

import org.bson.Document;

import com.mongodb.client.MongoCursor;


public class MongoDBIterator<T> implements Iterator<T> {
	
	private MongoCursor<Document> cursor;
	private MongoDAO<T> dao;
	
	
	/* Constructors */
	
	MongoDBIterator(MongoDAO<T> dao, MongoCursor<Document> cursor) {
		this.dao = dao;
		this.cursor = cursor;
		
	}
	
	/* Interfaces */
	
	@Override
	public boolean hasNext() {
		return cursor.hasNext();
	}
	
	@Override
	public T next() {
		return dao.getObject( this.cursor.next() );
	}
	
	
	@Override
	public void remove() {}
	
	
	
}
