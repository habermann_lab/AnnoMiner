package annominer.io.database.mongodb3;

import java.util.Iterator;

import annominer.ChromosomeAssembly;
import annominer.io.database.AssemblyDAO;

public class AssemblyMongoDAO implements AssemblyDAO {
	
	private static AssemblyMongoDAO assemblydao = new AssemblyMongoDAO();
	
	/* Constructors */
	
	protected AssemblyMongoDAO() {
		super();
		assemblydao = AssemblyMongoDAO.getInstance();
	}
	
	
	public static AssemblyMongoDAO getInstance() {
		return assemblydao;
	}


	@Override
	public Iterator<ChromosomeAssembly> getObjectIterator(String organism) {
		return null;
	}


	@Override
	public ChromosomeAssembly getObjectByID(String id, String organism) {
		return null;
	}
	
	
	
	
	
}
