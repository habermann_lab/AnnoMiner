package annominer.io.database.mongodb3;


import static com.mongodb.client.model.Filters.eq;

import java.util.List;

import org.bson.Document;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.*;

import annominer.ExperimentAnnotation;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ExperimentDAO;
import annominer.io.database.mongodb3.ExperimentMongoDAO;


public class ExperimentMongoDAO implements ExperimentDAO, MongoDAO<ExperimentAnnotation> {
	
	private static ExperimentMongoDAO experimentdao = new ExperimentMongoDAO();
	private static MongoDBConnector mongoconnector;
	
	/* Constructors */
	
	public ExperimentMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	/* Interfaces */
	
	
	public ExperimentAnnotation getByID( String pid, String organism ) throws ChromIDException {
		
		Document entry = ExperimentMongoDAO.mongoconnector.getDocument(organism, METACOLLECTION, eq("pid", pid) );
		
		if (entry == null) {
			throw new ChromIDException(pid,organism);
		} else {
			return getObject(entry);
		}
		
	}
	
	
	
	public void store(ExperimentAnnotation experiment, String db) {
		
		Document query = new Document();
		
		query.put("pid", experiment.pid);
		query.put("probe", experiment.probe);
		query.put("method", experiment.method);
		
		addMongoField(query,"target", experiment.target);
		addMongoField(query,"targettype", experiment.targettype);
		addMongoField(query,"treatment", experiment.treatment);
		addMongoField(query,"rpl", experiment.replicate);
		addMongoField(query,"geo", experiment.geoacc);
		addMongoField(query,"project", experiment.project);
		addMongoField(query,"stage", experiment.stage);
		
		
		MongoCollection<Document> coll = mongoconnector.getCollection( db, METACOLLECTION );		
		coll.insertOne(query);
		
	}
	
	
	
	/* Methods */
	
	
	public static ExperimentMongoDAO getInstance() {
		
		return experimentdao;
		
	}
	
	
	public ExperimentAnnotation getObject( Document entry ) {
		
		ExperimentAnnotation experiment = new ExperimentAnnotation( entry.getString("pid"), entry.getString("probe"), entry.getString("method") );
		
		experiment.target = entry.getString("target");
		experiment.targettype = entry.getString("targettype");
		experiment.geoacc = entry.getString("geo");
		
		if( entry.get("treatment") != null ) {
			experiment.treatment = entry.getString("treatment");
		}
		
		if( entry.get("rpl") != null ) {
			experiment.replicate = entry.getString("rpl");
		}
				
		return experiment;
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database) {
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find().iterator() );
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database, String field, String filter) {
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find( eq(field, filter) ).iterator() );
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database, String[][] field_filter_pairs) {
		
		Document query = new Document();
		
		for(int i=0; i<field_filter_pairs.length; i++) {
			query.put(field_filter_pairs[i][0], field_filter_pairs[i][1]);
		}
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find(query).iterator() );
		
	}
	
	
	private static void addMongoField( Document obj, String var, String val ) {
		
		if (!(  val == null || val.equals("None") ) ) {
			obj.put(var, val);
		}
		
	}
	
	

}


