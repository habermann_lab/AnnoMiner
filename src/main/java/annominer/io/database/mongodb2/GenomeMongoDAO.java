package annominer.io.database.mongodb2;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import annominer.Source;
import annominer.io.file.TSVReader;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


public class GenomeMongoDAO extends DataTrackMongoDAO {
	
	private static GenomeMongoDAO trackdao = new GenomeMongoDAO();
	
	/* Constructors */
	
	protected GenomeMongoDAO() {
		super();
		chromdao = ChromosomeOfTranscriptsMongoDAO.getInstance();
	}
	
	
	public static GenomeMongoDAO getInstance() {
		return trackdao;
	}
	
	
	protected void sql2mongo(String organism, String trackid, Source source) {
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		MysqlDataSource localmysql = null;
		
		
		localmysql = new MysqlDataSource();
		localmysql.setUser(source.user);
		localmysql.setServerName(source.host);
		localmysql.setDatabaseName(organism);
		if (source.password != null) {
			localmysql.setPassword(source.password);
		}
		
		MysqlDataSource datasource = localmysql;
		
		/* MongoDB */
		DBCollection coll = this.mongoconnector.getCollection(organism, trackid);
		
		String lastgene = null;
		String lastchrom = null;
		long lastchromlength = 0;
			
		BasicDBList longest = null;
		long longest_len = 0;
				
		Map<String,Integer> gene_idx = new HashMap<String,Integer>();
		BasicDBList genelist = new BasicDBList();
		
		
		try {
			
			connection = datasource.getConnection();
			statement = connection.createStatement();
			
			if (trackid.equals("refseq") ) {
				resultSet = statement.executeQuery("SELECT refGene.chrom, chromInfo.size, refGene.name, refGene.name2, refGene.txStart, refGene.txEnd, refGene.strand, refGene.cdsStart, refGene.cdsEnd, refGene.exonStarts, refGene.exonEnds FROM refGene LEFT JOIN chromInfo ON chromInfo.chrom=refGene.chrom WHERE !(INSTR(refGene.chrom,'_')) ORDER BY refGene.chrom, refGene.txStart");
			} else if ( trackid.equals("ensembl") ) {
				resultSet = statement.executeQuery("SELECT ensGene.chrom, chromInfo.size, ensGene.name, ensGene.name2, ensGene.txStart, ensGene.txEnd, ensGene.strand, ensGene.cdsStart, ensGene.cdsEnd, ensGene.exonStarts, ensGene.exonEnds FROM ensGene LEFT JOIN chromInfo ON chromInfo.chrom=ensGene.chrom WHERE !(INSTR(ensGene.chrom,'_')) ORDER BY ensGene.chrom, ensGene.txStart");
			} else if ( trackid.equals("flybase") ) {
				resultSet = statement.executeQuery("SELECT flyBaseGene.chrom, chromInfo.size, flyBaseGene.name, flyBaseGene.name, flyBaseGene.txStart, flyBaseGene.txEnd, flyBaseGene.strand, flyBaseGene.cdsStart, flyBaseGene.cdsEnd, flyBaseGene.exonStarts, flyBaseGene.exonEnds FROM refGene LEFT JOIN chromInfo ON chromInfo.chrom=flyBaseGene.chrom WHERE !(INSTR(flyBaseGene.chrom,'_')) ORDER BY flyBaseGene.chrom, flyBaseGene.txStart");	
			} else {
				System.out.println("Invalid genome annotation: "+trackid);
			}
			
			while ( resultSet.next() ) {
						
				if ( resultSet.getString(1).equals(lastchrom) ) {
					
					if ( resultSet.getString(4).equals(lastgene) ) {
						
						if ( resultSet.getLong(6)-resultSet.getLong(5) > longest_len ) {
							
							longest.set(8,false);
							
							longest = sql2Transcript(resultSet,true);
							genelist.add(longest);
							
							longest_len = resultSet.getLong(6)-resultSet.getLong(5);
							
							gene_idx.put(resultSet.getString(4), genelist.size() -1);
							
						} else {
							genelist.add(sql2Transcript(resultSet,false));
						}
							
					} else {
						
						if ( gene_idx.containsKey(resultSet.getString(4)) ) {
							
							BasicDBList clongest_len = (BasicDBList)genelist.get( gene_idx.get(resultSet.getString(4)) );
							
							gene_idx.get(resultSet.getString(4));
							
							if ( (Long)clongest_len.get(1) + 5000 < resultSet.getLong(5) ) {
								
								genelist.add(sql2Transcript(resultSet,true));
								gene_idx.put(resultSet.getString(4), genelist.size() -1);
								
								
							} else {
								if ( resultSet.getLong(6)-resultSet.getLong(5) > (long)clongest_len.get(1)-(long)clongest_len.get(0) ) {
									
									clongest_len.set(8,false);
									genelist.add(sql2Transcript(resultSet,true));
									gene_idx.put(resultSet.getString(4), genelist.size() -1);
									
								} else {
									genelist.add(sql2Transcript(resultSet,false));
								}
							}
							
						} else {
							
							longest = sql2Transcript(resultSet,true);
							longest_len = resultSet.getLong(6)-resultSet.getLong(5);
							genelist.add(longest);
							gene_idx.put(resultSet.getString(4), genelist.size() -1);
							
						}
						
						lastgene = resultSet.getString(4);
						
					}	
					
				} else {
					
					if ( lastchrom != null ) {
						
						BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
						chrom.append("length", lastchromlength);
						chrom.append("transcripts", genelist);
						
						coll.insert(chrom);
						
					}
					
					genelist = new BasicDBList();
					gene_idx = new HashMap<String,Integer>();
					
					lastchrom = resultSet.getString(1);
					lastchromlength = resultSet.getLong(2);
					lastgene = resultSet.getString(4);
					
					longest = sql2Transcript(resultSet,true);										
					longest_len = resultSet.getLong(6)-resultSet.getLong(5);
					genelist.add(longest);
					
					gene_idx.put(resultSet.getString(4), genelist.size() -1);
					
				}	
				
			}
				
			if ( lastchrom != null ) {
				
				BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
				chrom.append("length", lastchromlength);
				chrom.append("transcripts", genelist);
				
				coll.insert(chrom);
				
			}	
				
				
	
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			}
			catch ( Exception exception ) {
				exception.printStackTrace();
			}
		}
		
				
	}
	
	
	private static BasicDBList sql2Transcript(ResultSet resultset, boolean longest) throws SQLException {
		
		boolean strand = resultset.getString(7).equals("+");
		
		BasicDBList trans = new BasicDBList();
		trans.add(resultset.getLong(5));
		trans.add(resultset.getLong(6));
		trans.add(strand);
		trans.add(resultset.getString(3));
		trans.add(resultset.getString(4));
		trans.add(resultset.getLong(8));
		trans.add(resultset.getLong(9));
		trans.add(sql2Exons(resultset, strand));
		trans.add(longest);
		
		return trans;
	}
	
	
	private static BasicDBList sql2Exons(ResultSet resultset, boolean strand) throws SQLException {
		
		Blob startblob = resultset.getBlob(10);
		Blob endblob = resultset.getBlob(11);
		
		String startstr = new String(startblob.getBytes(1l, (int) startblob.length()));
		String endstr = new String(endblob.getBytes(1l, (int) endblob.length()));
		
		String[] starts = startstr.split(",");
		String[] ends = endstr.split(",");
		
		BasicDBList exlist = new BasicDBList();
		
		for (int i=0; i<starts.length; i++) {
			
			BasicDBList exon = new BasicDBList();
			exon.add( Long.valueOf(starts[i]) );
			exon.add( Long.valueOf(ends[i]) );
			exon.add(strand);
			exlist.add(exon);
		}
		
		return exlist;
		
	}
	
	
	protected void modmine2mongo(String organism, String trackid, Source source) {
		
		//DEPRECATED: obtaining gene-models from modmine is notsupported any more
		
		String fname = "filepath";
		TSVReader ifh = new TSVReader( fname );
		
		/* MongoDB */
		DBCollection coll = this.mongoconnector.getCollection(organism, trackid);
		
		String lastchrom = null;
		long lastchromlength = 0;
		
		String lastgene = null;
		String lasttrans = null;
		boolean strand = true;
		
		BasicDBList genelist = new BasicDBList();
		BasicDBList translist = new BasicDBList();
		BasicDBList exlist = null;
		
		long longest = 0;
		int longest_idx = 0;
		
		
		for ( String[] row: ifh) {
			
			
			if ( row[0].equals(lastchrom) ) {
				
				if ( row[2].equals(lastgene) ) {
					
					if (row[7].equals(lasttrans) ) {
					
						exlist.add( intermine2Exon(row, strand) );
						
					} else {
						
						exlist = new BasicDBList();
						
						if ( ( Long.valueOf(row[9]) - Long.valueOf(row[8]) ) > longest ) {
							
							((BasicDBList)translist.get(longest_idx)).set(8, false);
							
							translist.add( intermine2Transcript(row, strand, exlist, true) );
							
							longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
							longest_idx = translist.size() -1;
							
						} else {
							
							translist.add( intermine2Transcript(row, strand, exlist, false) );
							
						}
						
						exlist.add( intermine2Exon(row, strand) );
						
						lasttrans = row[7];
					
					}
					
				} else {
					
					translist = new BasicDBList();
					exlist = new BasicDBList();
					
					strand = row[6].equals("1");
					
					genelist.add( intermine2Gene(row, strand, translist) );
					
					translist.add( intermine2Transcript(row, strand, exlist, true) );
					
					exlist.add( intermine2Exon(row, strand) );
					
					longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
					longest_idx = 0;
					
					lastgene = row[2];
					lasttrans = row[7];
				
				}	
					
			} else {
				
				if (lastchrom != null) {
					
					BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
					chrom.append("length", lastchromlength);
					chrom.append("genes", genelist);
					coll.insert(chrom);
					
				}	
				
				strand = row[6].equals("1");
				
				genelist = new BasicDBList();
				translist = new BasicDBList();
				exlist = new BasicDBList();
				
				genelist.add( intermine2Gene(row, strand, translist) );
				translist.add( intermine2Transcript(row, strand, exlist, true) );
				exlist.add( intermine2Exon(row, strand) );
				
				longest = Long.valueOf(row[9]) - Long.valueOf(row[8]);
				longest_idx = 0;
				
				lastchrom = row[0];
				lastchromlength = Long.valueOf(row[1]);
				lastgene = row[2];
				lasttrans = row[7];
			
			}
		
		}
		
			
		if ( lastchrom != null ) {
			
			BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
			chrom.append("length", lastchromlength);
			chrom.append("genes", genelist);
			coll.insert(chrom);	
			
		}
		
		
		ifh.close();
		
		
		
	}
		
	
	private static BasicDBList intermine2Exon(String[] row, boolean strand) {
		
		BasicDBList exon = new BasicDBList();
		exon.add( Long.valueOf(row[12]) );
		exon.add( Long.valueOf(row[13]) );
		exon.add(strand);
		
		return exon;
		
	}
	
	
	private static BasicDBList intermine2Transcript(String[] row, boolean strand, BasicDBList exlist, boolean longest) {
		
		BasicDBList transcript = new BasicDBList();
		
		transcript.add( Long.valueOf(row[8]) );
		transcript.add( Long.valueOf(row[9]) );
		transcript.add( strand );
		transcript.add( row[7] );
		transcript.add( row[2] );
		transcript.add( Long.valueOf(row[10]) );
		transcript.add( Long.valueOf(row[11]) );
		transcript.add( exlist );
		transcript.add( longest );
		
		return transcript;
		
	}
	
	
	private static BasicDBList intermine2Gene(String[] row, boolean strand, BasicDBList translist) {
		
		BasicDBList gene = new BasicDBList();
		
		gene.add( Long.valueOf(row[4]) );
		gene.add( Long.valueOf(row[5]) );
		gene.add( strand );
		gene.add( row[2] );
		gene.add( row[3] );
		gene.add( translist );
		
		return gene;
		
	}
	
	
	protected void file2mongo( String organism, String trackid, Source source) {
		//TODO !!!
	}
	
	
	
	
	
}
