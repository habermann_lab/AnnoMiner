package annominer.io.database.mongodb2;


import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import org.bson.types.ObjectId;

import annominer.Chromosome;
import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.io.database.DataTrackDAO;


public abstract class DataTrackMongoDAO implements DataTrackDAO {
	
	protected static final String ROOT = "http://intermine.modencode.org/release-33/service";
	
	protected ChromosomeMongoDAO chromdao;
	
	protected MongoDBConnector mongoconnector;
	
	/* Constructors */
	
	protected DataTrackMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	/* Methods */
	
	@Override
	public MongoDBIterator<Chromosome> getIterator( String organism, String trackid ) {
		return new MongoDBIterator<Chromosome>( chromdao, mongoconnector.getCursor( organism, trackid ) );
	}
	
	
	@SuppressWarnings("unused")
	@Override
	public boolean update(String organism, String trackid, Source source) throws FileFormatException {
		
		
		BasicDBObject query = new BasicDBObject();
		query.put("1", trackid);
		
		DBCollection indexcoll = this.mongoconnector.getCollection(organism, "dbindex");
		DBObject entry = indexcoll.findOne(query);
		
		
		if (entry == null) {
			
			insert(organism, trackid, source);
			indexcoll.insert(query);
			
			return false;
			
		} else {
			
			long time = System.currentTimeMillis();
			long inserttime = ((ObjectId)entry.get("_id")).getTime();
			
			if ( time-inserttime < EXPIRATION_TIME || EXPIRATION_TIME < 0) {
				
				return true;
				
			} else {
				
				indexcoll.remove(query);
				
				insert(organism, trackid, source);
				
				indexcoll.insert(query);
				
				return true;
				
			}
		}
		
		
	}
	
	
	private void insert (String organism, String trackid, Source source) throws FileFormatException {
		
		this.mongoconnector.getCollection(organism, trackid).remove( new BasicDBObject() );
		
		if ( source.source.equals("sql") ) {
			sql2mongo(organism, trackid, source);
		} else if ( source.source.equals("modencode") ) {
			modmine2mongo(organism, trackid, source);
		} else if ( source.source.equals("file") ) {
			file2mongo(organism, trackid, source);
		} else {
			System.out.println("Error: Unknown data source: "+source.source+" !!!");
			
		}
		
		
	}
	
	
	protected abstract void sql2mongo( String organism, String trackid, Source source);
	protected abstract void modmine2mongo( String organism, String trackid, Source source);
	protected abstract void file2mongo( String organism, String trackid, Source source);
	
	
}




