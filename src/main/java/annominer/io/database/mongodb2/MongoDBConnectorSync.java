package annominer.io.database.mongodb2;

import java.net.UnknownHostException;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


public class MongoDBConnectorSync {
	
	private static String HOST = "127.0.0.1";
	private static int PORT = 27017;
	
	private static MongoClient mongoclient;
	private volatile static MongoDBConnectorSync connector = new MongoDBConnectorSync();
	
	private MongoDBConnectorSync() {
		connect();
	}
	
	public static MongoDBConnectorSync getInstance() {
		
		if (connector==null) {
			
			synchronized (MongoDBConnectorSync.class) {
				if (connector==null) {
					connector = new MongoDBConnectorSync();
				}
			}
		}
		
		return connector;
	}
	
	
	public void connect() {
		connect(HOST, PORT);
	}
	
	
	public void connect(String host, int port) {
		
		mongoclient = new MongoClient(host, port);
		
	}
	
	
	public DB getDatabase(String db) {
		return mongoclient.getDB(db);	
	}
	
	public DBCollection getCollection(String db, String coll) {
		return mongoclient.getDB(db).getCollection(coll);	
	}
	
	public DBCursor getCursor(String db, String coll) {
		return mongoclient.getDB(db).getCollection(coll).find();
	}
	
	public DBObject getDocument(String db, String coll, BasicDBObject queryobj) {
		return mongoclient.getDB(db).getCollection(coll).findOne(queryobj);
	}
	
	
	
}
