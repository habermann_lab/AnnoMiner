package annominer.io.database.mongodb2;

import java.net.UnknownHostException;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import org.bson.types.ObjectId;


public class MongoDBConnector {
	
	private static String HOST = "127.0.0.1";
	private static int PORT = 27017;
	
	private static MongoClient mongoclient;
	
	private static MongoDBConnector connector = new MongoDBConnector();
	
	private MongoDBConnector() {
		connect();
	}
	
	
	public static MongoDBConnector getInstance() {
		return connector;
	}
	
	
	public void connect() {
		connect(HOST, PORT);
	}
	
	
	public void connect(String host, int port) {
		
		mongoclient = new MongoClient(host, port);
		
	}
	
	public void close() {
		mongoclient.close();
	}
	
	
	public DB getDatabase(String db) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid);	
	}
	
	public DBCollection getCollection(String db, String coll) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid).getCollection(coll);
	}
	
	public DBCursor getCursor(String db, String coll) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid).getCollection(coll).find();
	}
	
	public DBObject getDocument(String db, String coll, BasicDBObject queryobj) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid).getCollection(coll).findOne(queryobj);
	}
	
	public DBCursor findDocuments(String db, String coll, BasicDBObject queryobj) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid).getCollection(coll).find(queryobj);
	}
	
	public static MongoClient getMongoClient() {
		//Expose MongoClient
		return mongoclient;
	}
	
	public Set<String> getCollectionList(String db) {
		String dbid = db.replaceAll(" ", "_");
		return mongoclient.getDB(dbid).getCollectionNames();
	}
	
	public Set<String> getCollectionList(DB db) {
		return db.getCollectionNames();
	}
	
	public long getInsertionTime( DBObject entry) {
		return ((ObjectId)entry.get("_id")).getTime();
	}
	
	public void deleteDatabase( String database ) {
		mongoclient.dropDatabase( database );
	}
	
	public void deleteCollection( String database, String collection ) {
		mongoclient.getDB(database).getCollection(collection).drop();
	}
	
	
}
