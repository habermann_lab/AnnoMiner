package annominer.io.database.mongodb2;


import com.mongodb.BasicDBList;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import annominer.Chromosome;
import annominer.OrientedRegion;
import annominer.Transcript;


public class ChromosomeOfTranscriptsMongoDAO extends ChromosomeMongoDAO {
	
	//Singleton member:
	private static ChromosomeOfTranscriptsMongoDAO chromdao = new ChromosomeOfTranscriptsMongoDAO();	
	
	
	/* Constructors */
	
	private ChromosomeOfTranscriptsMongoDAO() {
		super();
	}
	
	
	
	/* Interfaces */
	
	
	/* Methods */
	
	
	public static ChromosomeOfTranscriptsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	
	public Chromosome getObject( DBObject chrom ) {
		
		Chromosome chromosome = new Chromosome((String)chrom.get("pid"), (long)chrom.get("length") );
		
		BasicDBList dblist = (BasicDBList)chrom.get("transcripts");
		
		for (int i=0;i<dblist.size();i++) {
			DBObject gene = (DBObject) dblist.get(i);
			
			
			/* Only add longest transcripts */
			//if ( (boolean)gene.get("8") == false ) { continue; }
			//if (canonical && !((boolean)gene.get("8"))) { continue; }
			
			
			long tleft = (long)gene.get("0");
			long tright = (long)gene.get("1");
			boolean strand = (boolean)gene.get("2");
			String tpid = (String)gene.get("3");
			String tsymbol = (String)gene.get("4");
			long cleft = (long)gene.get("5");
			long cright = (long)gene.get("6");
			
			
			
			Transcript trans = new Transcript(tleft,tright,strand,tpid,tsymbol,new OrientedRegion(cleft,cright,strand));
			trans.canonical = (boolean)gene.get("8");
			
			BasicDBList exons = (BasicDBList)gene.get("7");
			
			for (int j=0;j<exons.size();j++) {
				DBObject exon = (DBObject) exons.get(j);
				long eleft = (long)exon.get("0");
				long eright = (long)exon.get("1");
				
				trans.addExon(new OrientedRegion(eleft, eright, strand));
			}
			
			chromosome.add(trans);
			
		}
		
		return chromosome;
		
	}
	
	
	public Chromosome getObject( DBCursor mongocursor ) {
		return getObject( mongocursor.next() );
	}
	
	
	
	
}


