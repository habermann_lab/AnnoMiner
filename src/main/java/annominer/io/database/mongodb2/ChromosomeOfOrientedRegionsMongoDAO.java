package annominer.io.database.mongodb2;


import com.mongodb.BasicDBList;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import annominer.Chromosome;
import annominer.OrientedRegion;



public class ChromosomeOfOrientedRegionsMongoDAO extends ChromosomeMongoDAO {
	
	//Singleton member:
	private static ChromosomeOfOrientedRegionsMongoDAO chromdao = new ChromosomeOfOrientedRegionsMongoDAO();	
	
	
	/* Constructors */
	
	private ChromosomeOfOrientedRegionsMongoDAO() {
		super();
	}
	
	
	
	/* Interfaces */
	
	
	/* Methods */
	
	
	public static ChromosomeOfOrientedRegionsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	@Override
	public Chromosome getObject( DBObject chrom ) {
		
		Chromosome chromosome = new Chromosome((String)chrom.get("pid"), (long)chrom.get("length") );
		
		BasicDBList regionlist = (BasicDBList)chrom.get("regions");
		
		for (int i=0;i<regionlist.size();i++) {
			DBObject regionx = (DBObject) regionlist.get(i);
			
			long tleft = (long)regionx.get("0");
			long tright = (long)regionx.get("1");
			boolean strand = (boolean)regionx.get("2");
			
			OrientedRegion region = new OrientedRegion(tleft,tright,strand);
			chromosome.add(region);
			
		}
		
		return chromosome;
		
	}
	
	
	public Chromosome getObject( DBCursor mongocursor ) {
		return getObject(mongocursor.next() );
	}
	
	
	
	
}


