package annominer.io.database.mongodb2;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import annominer.Source;
import annominer.exceptions.FileFormatException;
import annominer.io.file.TSVReader;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import org.intermine.metadata.Model;
import org.intermine.pathquery.Constraints;
import org.intermine.pathquery.OrderDirection;
import org.intermine.pathquery.PathQuery;
import org.intermine.webservice.client.core.ServiceFactory;
import org.intermine.webservice.client.results.JsonRow;
import org.intermine.webservice.client.services.QueryService;


public class DataTrackOfRegionsMongoDAO extends DataTrackMongoDAO {
	
	
	private static DataTrackOfRegionsMongoDAO trackdao = new DataTrackOfRegionsMongoDAO();
	
	/* Constructors */
	
	protected DataTrackOfRegionsMongoDAO() {
		super();
		chromdao = ChromosomeOfRegionsMongoDAO.getInstance();
	}
	
	
	public static DataTrackOfRegionsMongoDAO getInstance() {
		return trackdao;
	}
	
	
	protected void sql2mongo(String organism, String trackid, Source source) {
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		MysqlDataSource localmysql = null;
		
		
		localmysql = new MysqlDataSource();
		localmysql.setUser(source.user);
		localmysql.setServerName(source.host);
		localmysql.setDatabaseName(organism);
		if (source.password != null) {
			localmysql.setPassword(source.password);
		}
		
		
		MysqlDataSource datasource = localmysql;
		
		/* MongoDB */
		DBCollection coll = this.mongoconnector.getCollection(organism, trackid);
		
		
		String lastchrom = null;
		BasicDBList regionlist = new BasicDBList();
		BasicDBList region = new BasicDBList();
		
		Long chromlength = 10000L;
		
		try {
			
			connection = datasource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery("SELECT chrom, chromStart, chromEnd FROM "+ trackid +" WHERE SUBSTRING(chrom,-6)!=\'random\' ORDER BY chrom, chromStart");
			
			while ( resultSet.next() ) {
						
				if ( resultSet.getString(1).equals(lastchrom) ) {
					
					region = new BasicDBList();
					region.add(resultSet.getLong(2));
					region.add(resultSet.getLong(3));
					regionlist.add(region);
					
				} else {
					
					if ( lastchrom != null ) {
						
						/* MongoDB Inserts */
						BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
						chrom.append("length", chromlength);
						chrom.append("regions", regionlist);
						
						coll.insert(chrom);
						
					}
					
					lastchrom = resultSet.getString(1);
					
					regionlist = new BasicDBList();
					region = new BasicDBList();
					
					region.add(resultSet.getLong(2));
					region.add(resultSet.getLong(3));
					regionlist.add(region);
					
				}	
				
			} //END while
				
			if ( lastchrom != null ) {
				
				/* MongoDB Inserts */
				BasicDBObject chrom = new BasicDBObject("pid",lastchrom);
				chrom.append("length", chromlength);
				chrom.append("regions", regionlist);
				
				coll.insert(chrom);
				
			}	
				
				
	
		} catch ( SQLException sqlException) {
			sqlException.printStackTrace();
		} finally {
			try {
				resultSet.close();
				statement.close();
				connection.close();
			}
			catch ( Exception exception ) {
				exception.printStackTrace();
			}
		}
		
				
	}
	
	/*
	protected void modmine2mongo(String organism, String trackid, Source source) {
		//dummy because real function isn't working in webserver
	}
	*/
	
	
	protected void modmine2mongo(String organism, String trackid, Source source) {
		
		
		// MongoDB //
		DBCollection coll = this.mongoconnector.getCollection(organism, trackid);
		BasicDBList regionlist = new BasicDBList();
		
		// Intermine //
		ServiceFactory factory = new ServiceFactory(ROOT);
		QueryService service = factory.getQueryService();
		
        Model model = factory.getModel();
        PathQuery query = new PathQuery(model);
        
        query.addViews("Submission.features.chromosome.primaryIdentifier",
                "Submission.features.chromosomeLocation.start",
                "Submission.features.chromosomeLocation.end");
        
        query.addOrderBy("Submission.features.chromosome.primaryIdentifier", OrderDirection.ASC);
        query.addOrderBy("Submission.features.chromosomeLocation.start", OrderDirection.ASC);
        
        query.addConstraint(Constraints.eq("Submission.DCCid", trackid));
        
        
        Iterator<List<Object>> rows = service.getRowListIterator(query);
		
		Long chromlength = 10000L;
		String lastchrom = null;
		String lastchrom_ucsc = null;
		
		
		while (rows.hasNext()) {
			
			JsonRow row = (JsonRow) rows.next();
			
			if ( ((String)row.get(0)).equals(lastchrom) ) {
				
				BasicDBObject region = new BasicDBObject();
				region.append("0",Long.valueOf((int)row.get(1)));
				region.append("1",Long.valueOf((int)row.get(2)));
				regionlist.add(region);
					
			} else {
				
				if (lastchrom != null) {
					
					BasicDBObject chrom = new BasicDBObject("pid",lastchrom_ucsc);
					chrom.append("length", chromlength);
					chrom.append("regions", regionlist);
					coll.insert(chrom);
					
				}
				
				lastchrom = (String)row.get(0);
				
				if ( lastchrom.startsWith("chr") ) {
					lastchrom_ucsc = lastchrom;
				} else {
					
					if( lastchrom.equals("MtDNA") ) {
						lastchrom_ucsc = "chrM";
					} else {
						lastchrom_ucsc = "chr" + lastchrom;
					}
				}

				
				regionlist = new BasicDBList();
				
				BasicDBObject region = new BasicDBObject();
				region.append("0",Long.valueOf((int)row.get(1)));
				region.append("1",Long.valueOf((int)row.get(2)));
				regionlist.add(region);
				
				
				
			
			}
		
		} //End for
		
		
		
		if ( lastchrom != null ) {
			
			BasicDBObject chrom = new BasicDBObject("pid",lastchrom_ucsc);
			chrom.append("length", chromlength);
			chrom.append("regions", regionlist);
			coll.insert(chrom);
			
		}
		
		
	}
	
	
	protected void file2mongo ( String organism, String trackid, Source source) throws FileFormatException {
		
		final Runtime runtime = Runtime.getRuntime();
		
		int chromidcol, startcol, endcol;
		
		if ( source.fileformat.equals("bed")) {
			chromidcol = 0; startcol=1; endcol=2;
		} else if ( source.fileformat.equals("gff3")) {
			chromidcol = 0; startcol=3; endcol=4;
		} else {
			chromidcol = 0; startcol=1; endcol=2;
		}
		
		
		try {
			Process process = runtime.exec("sort -k "+(chromidcol+1)+","+(chromidcol+1)+" -k "+(startcol+1)+","+(startcol+1)+"n -o "+source.filepath+"_sorted "+source.filepath);
			process.waitFor();
		} catch (IOException e ) {
			// Silence exception
		} catch (InterruptedException g ) {
			
		}
		
		
		TSVReader ifh = new TSVReader( source.filepath+"_sorted" );
		
		/* MongoDB */
		DBCollection coll = this.mongoconnector.getCollection(organism, trackid);
		
		BasicDBList regionlist = new BasicDBList();
		BasicDBList region = new BasicDBList();
		
		
		String lastchrom = null;
		String lastchrom_ucsc = null;
		
		Long chromlength = 10000L;
		
		int linecounter = 0;
		
		String[] temp = {"",""};
		
		try {
			
			
			for (String[] line : ifh) {
				
				linecounter++;
				temp = line;
				
				if( line[0].startsWith("##") ) {continue;}
				
				if ( line[chromidcol].equals(lastchrom) ) {
					
					region = new BasicDBList();
					region.add( Long.parseLong(line[startcol]) );
					region.add( Long.parseLong(line[endcol]) );
					regionlist.add(region);
					
				} else {
					
					if ( lastchrom != null ) {
						
						BasicDBObject chrom = new BasicDBObject("pid",lastchrom_ucsc);
						chrom.append("length", chromlength);
						chrom.append("regions", regionlist);
						
						coll.insert(chrom);
						
					}
					
					lastchrom = line[chromidcol];
					
					if ( lastchrom.startsWith("chr") ) {
						lastchrom_ucsc = lastchrom;
					} else {
						
						if( lastchrom.equals("MtDNA") ) {
							lastchrom_ucsc = "chrM";
						} else {
							lastchrom_ucsc = "chr" + lastchrom;
						}
					}
					
					
					regionlist = new BasicDBList();
					region = new BasicDBList();
					
					region.add( Long.parseLong(line[startcol]) );
					region.add( Long.parseLong(line[endcol]) );
					regionlist.add(region);
					
				}
				
			
			}
			
			if ( lastchrom != null ) {
				
				BasicDBObject chrom = new BasicDBObject("pid",lastchrom_ucsc);
				chrom.append("length", chromlength);
				chrom.append("regions", regionlist);
				
				coll.insert(chrom);
				
			}
		
		} catch ( ArrayIndexOutOfBoundsException e ) {
			
			this.mongoconnector.deleteCollection(organism, trackid);
			throw new FileFormatException( source.fileformat, linecounter );
			
		} catch ( NumberFormatException e ) {
			
			this.mongoconnector.deleteCollection(organism, trackid);
			throw new FileFormatException( source.fileformat, linecounter );
			
		} finally {
			
			ifh.close();
			
			File file = new File(source.host+"_sorted");
			file.delete();
			
			
		}
		
		
	}
	
	
	
	
	
}
