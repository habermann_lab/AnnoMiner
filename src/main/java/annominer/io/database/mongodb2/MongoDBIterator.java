package annominer.io.database.mongodb2;

import java.util.Iterator;

import com.mongodb.DBCursor;


public class MongoDBIterator<T> implements Iterator<T> {
	
	private DBCursor cursor;
	private MongoDAO<T> dao;
	
	
	/* Constructors */
	
	MongoDBIterator(MongoDAO<T> dao, DBCursor cursor) {
		this.dao = dao;
		this.cursor = cursor;
		
	}
	
	/* Interfaces */
	
	@Override
	public boolean hasNext() {
		return cursor.hasNext();
	}
	
	@Override
	public T next() {
		return dao.getObject( this.cursor.next() );
	}
	
	
	@Override
	public void remove() {}
	
	
	
}
