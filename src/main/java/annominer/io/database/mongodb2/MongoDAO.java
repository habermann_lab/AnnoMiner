package annominer.io.database.mongodb2;

import com.mongodb.DBObject;


public interface MongoDAO<T> {
	
	public final static String METACOLLECTION = "metadata";
	public final static String REMOTEDATABASECOLLECTION = "remotedb_metadata";
	public final static String INDEXCOLLECTION = "indexdb";
	
	public T getObject( DBObject entry );
	
	
}
