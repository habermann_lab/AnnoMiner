package annominer.io.database.mongodb2;


import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import annominer.ExperimentAnnotation;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ExperimentDAO;
import annominer.io.database.mongodb2.ExperimentMongoDAO;


public class ExperimentMongoDAO implements ExperimentDAO, MongoDAO<ExperimentAnnotation> {
	
	private static ExperimentMongoDAO experimentdao = new ExperimentMongoDAO();
	private static MongoDBConnector mongoconnector;
	
	/* Constructors */
	
	private ExperimentMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	/* Interfaces */
	
	public ExperimentAnnotation getByID( String pid, String organism ) throws ChromIDException {
		
		BasicDBObject query = new BasicDBObject();
		query.put("pid", pid);
		
		DBObject entry = ExperimentMongoDAO.mongoconnector.getDocument(organism, METACOLLECTION, query);
		
		if (entry == null) {
			throw new ChromIDException(pid,organism);
		} else {
			return getObject(entry);
		}
	}
	
	
	
	public void store(ExperimentAnnotation experiment, String db) {
		
		BasicDBObject query = new BasicDBObject();
		
		query.put("pid", experiment.pid);
		query.put("probe", experiment.probe);
		query.put("method", experiment.method);
		
		addMongoField(query,"target", experiment.target);
		addMongoField(query,"targettype", experiment.targettype);
		addMongoField(query,"treatment", experiment.treatment);
		addMongoField(query,"rpl", experiment.replicate);
		addMongoField(query,"geo", experiment.geoacc);
		
		DBCollection coll = mongoconnector.getCollection( db, METACOLLECTION );		
		coll.insert(query);
		
	}
	
	
	
	/* Methods */
	
	
	public static ExperimentMongoDAO getInstance() {
		
		return experimentdao;
		
	}
	
	
	public ExperimentAnnotation getObject( DBObject entry ) {
		
		ExperimentAnnotation experiment = new ExperimentAnnotation( (String)entry.get("pid"), (String)entry.get("probe"), (String)entry.get("method") );
		
		experiment.target = (String)entry.get("target");
		experiment.targettype = (String)entry.get("targettype");
		experiment.geoacc = (String)entry.get("geo");
		
		if( entry.get("treatment") != null ) {
			experiment.treatment = (String)entry.get("treatment");
		}
		
		if( entry.get("rpl") != null ) {
			experiment.replicate = (String)entry.get("rpl");
		}
				
		return experiment;
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database) {
		
		BasicDBObject query = new BasicDBObject();
		query.put("targettype" , "TF");
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find());
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database, String field, String filter) {
		
		BasicDBObject query = new BasicDBObject();
		query.put(field , filter);
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find(query));
		
	}
	
	
	public MongoDBIterator<ExperimentAnnotation> getObjectIterator(String database, String[][] field_filter_pairs) {
		
		BasicDBObject query = new BasicDBObject();
		
		for(int i=0; i<field_filter_pairs.length; i++) {
			query.put(field_filter_pairs[i][0] , field_filter_pairs[i][1]);
		}
		
		return new MongoDBIterator<ExperimentAnnotation>(this, mongoconnector.getCollection(database, METACOLLECTION).find(query));
		
	}
	
	
	private static void addMongoField( BasicDBObject obj, String var, String val ) {
		
		if (!(  val == null || val.equals("None") ) ) {
			obj.put(var, val);
		}
		
	}

}


