package annominer.io.database.mongodb2;


import java.util.Iterator;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import annominer.Chromosome;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ChromosomeDAO;


public abstract class ChromosomeMongoDAO implements ChromosomeDAO, MongoDAO<Chromosome> {
	
	//Singleton member:
	private static ChromosomeMongoDAO chromdao;
	
	protected MongoDBConnector mongoconnector;
	
	
	
	
	/* Constructors */
	
	protected ChromosomeMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	
	/* Interfaces */
	
	@Override
	public Chromosome getObject(String organism, String trackid, String pid) throws ChromIDException{
		
		BasicDBObject query = new BasicDBObject();
		query.put("pid", pid);
		
		DBObject entry = this.mongoconnector.getDocument(organism, trackid, query);
		
		if (entry == null) {
			throw new ChromIDException(organism,pid);
		} else {
			return getObject(entry);
		}
		
	}
	
	
	@Override
	public Iterator<Chromosome> getObjectIterator(String organism, String trackid) {
		
		return new MongoDBIterator<Chromosome> ( this, mongoconnector.getCursor(organism, trackid) );
		
	}
	
	
	/* Methods */
	
	
	public static ChromosomeMongoDAO getInstance() {
		return chromdao;
	}
	
	
	public Chromosome getObject( DBCursor mongocursor ) {
		return getObject(mongocursor.next());
	}
	
	
	public abstract Chromosome getObject( DBObject chrom );
	
	
	

}
