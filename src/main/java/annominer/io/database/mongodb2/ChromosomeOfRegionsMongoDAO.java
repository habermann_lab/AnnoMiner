package annominer.io.database.mongodb2;


import com.mongodb.BasicDBList;
import com.mongodb.DBObject;

import annominer.Chromosome;
import annominer.Region;


public class ChromosomeOfRegionsMongoDAO extends ChromosomeMongoDAO {
	
	
	//Singleton member:
	private static ChromosomeOfRegionsMongoDAO chromdao = new ChromosomeOfRegionsMongoDAO();
	
	
	
	/* Constructors */
	
	protected ChromosomeOfRegionsMongoDAO() {
		super();
	}
	
	
	/* Interfaces */
	
	
	
	/* Methods */
	
	
	public static ChromosomeOfRegionsMongoDAO getInstance() {
		return chromdao;
	}
	
	
	@Override
	public Chromosome getObject( DBObject chrom ) {
		
		Chromosome chromosome = new Chromosome((String)chrom.get("pid"), (long)chrom.get("length"));
		
		BasicDBList regionlist = (BasicDBList)chrom.get("regions");
		
		for (int i=0;i<regionlist.size();i++) {
			
			DBObject regionx = (DBObject) regionlist.get(i);
			
			long tleft = (long)regionx.get("0");
			long tright = (long)regionx.get("1");
			
			Region region = new Region(tleft,tright);
			chromosome.add(region);
			
		}
		
		return chromosome;
		
	}
	
	
	
	
}
