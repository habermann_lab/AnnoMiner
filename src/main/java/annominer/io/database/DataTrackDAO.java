package annominer.io.database;

import java.util.Iterator;

import annominer.Chromosome;
import annominer.Source;

public interface DataTrackDAO {
	
	public static final long MINUTE = 60000;
	public static final long HOUR = 3600000;
	public static final long DAY =  86400000;
	public static final long WEEK = 604800000;
	
	public static long EXPIRATION_TIME = -1;
	
	public Iterator<Chromosome> getIterator(String organism, String trackid);
	public boolean update(String organism, String trackid, Source source);
	
	
}
