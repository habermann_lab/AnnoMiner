package annominer.io.database.mongodb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import static com.mongodb.client.model.Filters.*;

import annominer.OrientedRegion;
import annominer.Transcript;
import annominer.exceptions.ChromIDException;
import annominer.io.database.TranscriptDAO;
import annominer.io.database.mongodb.MongoDAO;
import annominer.io.database.mongodb.MongoDBConnector;
import annominer.io.database.mongodb.MongoDBIterator;


public class TranscriptMongoDAO  implements TranscriptDAO, MongoDAO<Transcript>{
	
	//Singleton member:
	
	//private static TranscriptMongoDAO transdao;
	
	protected MongoDBConnector mongoconnector;
		
		
	/* Constructors */
		
	public TranscriptMongoDAO() {
		mongoconnector = MongoDBConnector.getInstance();
	}
	
	
	/* Interfaces */
	
	@Override
	public Transcript getObject(Document doc) {
		
		List<Object> entry = (List<Object>)doc.get("1");
		
		long tleft = (long)entry.get(0);
		long tright = (long)entry.get(1);
		boolean strand = (boolean)entry.get(2);
		String tpid = (String)entry.get(3);
		String tsymbol = (String)entry.get(4);
		long cleft = (long)entry.get(5);
		long cright = (long)entry.get(6);
		
		Transcript trans = new Transcript(tleft,tright,strand,tpid,tsymbol,new OrientedRegion(cleft,cright,strand));
		trans.canonical = (boolean)entry.get(8);
		
		@SuppressWarnings("unchecked")
		List<List<Object>> exons = (List<List<Object>>)entry.get(7);
		
		for (List<Object> exon : exons) {
			
			long eleft = (long)exon.get(0);
			long eright = (long)exon.get(1);
			
			trans.addExon(new OrientedRegion(eleft, eright, strand));
			
		}
		
		return trans;
		
	}
	
	/*
	
	@Override
	public Transcript getObject(Document entry) {
		
		long tleft = (long)entry.get("left");
		long tright = (long)entry.get("right");
		boolean strand = (boolean)entry.get("strand");
		String tpid = (String)entry.get("pid");
		String tsymbol = (String)entry.get("gid");
		long cleft = (long)entry.get("cds_left");
		long cright = (long)entry.get("cds_right");
		
		Transcript trans = new Transcript(tleft,tright,strand,tpid,tsymbol,new OrientedRegion(cleft,cright,strand));
		trans.canonical = (boolean)entry.get("canonical");
		
		@SuppressWarnings("unchecked")
		List<List<Object>> exons = (List<List<Object>>)entry.get("exons");
		
		for (List<Object> exon : exons) {
			
			long eleft = (long)exon.get(0);
			long eright = (long)exon.get(1);
			
			trans.addExon(new OrientedRegion(eleft, eright, strand));
		}
		
		return trans;
		
	}
	
	*/
	
	@Override
	public Transcript getObject(String organism, String trackid, String pid) throws ChromIDException {
		
		Document entry = this.mongoconnector.getDocument(organism, trackid, eq("pid", pid));
		
		if (entry == null) {
			throw new ChromIDException(organism,pid);
		} else {
			return getObject(entry);
		}
		
	}
	
	
	@Override
	public Iterator<Transcript> getObjectIterator(String organism, String trackid) {
		return new MongoDBIterator<Transcript> ( this, mongoconnector.getCursor(organism, trackid) );
	}
	
	
	
	/* Methods */
	
//	public static TranscriptMongoDAO getInstance() {
//		return transdao;
//	}
	
	/*
	public void saveObject(Transcript trans, String organism, String trackid, String chrom) {
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		List<List<Object>> exlist = new ArrayList<List<Object>>();
		
		for(OrientedRegion exon : trans){
			exlist.add( Arrays.asList( exon.left, exon.right, exon.strand ) );
		}
		
		Document doc = new Document();
		doc.append("0",chrom);
		List<Object> translist = Arrays.asList( trans.left, trans.right, trans.strand, trans.pid, trans.symbol, trans.cds.left, trans.cds.right, exlist, trans.canonical );
		doc.append("1",translist);
		coll.insertOne(doc);
		
	}
	*/
	
	
	public void saveObject(Transcript trans, String organism, String trackid, String chrom) {
		
		MongoCollection<Document> coll = this.mongoconnector.getCollection(organism, trackid);
		
		List<List<Object>> exlist = new ArrayList<List<Object>>();
		
		for(OrientedRegion exon : trans){
			exlist.add( Arrays.asList( (Object)exon.left, (Object)exon.right, (Object)exon.strand ) );
		}
		
		Document doc = new Document();
		
		doc.append("chrom", chrom);
		doc.append("left", trans.left);
		doc.append("right", trans.right);
		doc.append("strand", trans.strand);
		doc.append("pid", trans.pid);
		doc.append("gid", trans.symbol);
		doc.append("cds_left", trans.cds.left);
		doc.append("cds_right", trans.cds.right);
		doc.append("exons", exlist);
		doc.append("canonical", trans.canonical);
		
		//List<Object> translist = Arrays.asList( chrom, trans.left, trans.right, trans.strand, trans.pid, trans.symbol, trans.cds.left, trans.cds.right, exlist, trans.canonical );
		coll.insertOne(doc);
		
	}
	
	
	
}
