package annominer.io.database;

import java.util.Iterator;

import annominer.ExperimentAnnotation;
import annominer.exceptions.ChromIDException;


public interface ExperimentDAO {
	
	public ExperimentAnnotation getByID( String pid, String organism ) throws ChromIDException;
	public Iterator<ExperimentAnnotation> getObjectIterator(String organism);
	public Iterator<ExperimentAnnotation> getObjectIterator(String organism, String field, String value);
	public Iterator<ExperimentAnnotation> getObjectIterator(String organism, String[][] field_filter_pairs);
	public void store(ExperimentAnnotation experiment, String db);
	
}

