package annominer.io.database;

import java.util.Iterator;

import annominer.Chromosome;
import annominer.exceptions.ChromIDException;

public interface ChromosomeDAO {
	
	public Chromosome getObject( String organism, String trackid, String pid ) throws ChromIDException;
	public Iterator<Chromosome> getObjectIterator(String organism, String trackid);
	
	
}
