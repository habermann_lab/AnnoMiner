package annominer.io.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;


public class TSVReader implements AutoCloseable, Iterable<String[]>, Iterator<String[]> {
	
	private BufferedReader reader;
	private String separator = "\t";
	private String line;
	
	
public TSVReader(String fname) {
		
		try {
			reader = new BufferedReader( new FileReader( fname ));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		try {
			line = reader.readLine();
		} catch ( IOException e ) {
			System.err.println( "Read error in file");
			try { reader.close(); } catch ( Exception ec ) { ec.printStackTrace(); }
		}
		
	}
	
	
	public TSVReader(String fname, String separator) {
		this(fname);
		this.separator = separator;
	}
	
	
	@Override
	public boolean hasNext() {
		return (line!=null);
	}
	
	@Override
	public String[] next() {
		
		String[] cols = line.split(separator,-1);
		
		if (line != null) {
			try {
				line = reader.readLine();
			} catch ( IOException e ) {
				System.err.println( "Read error in file");
				try { reader.close(); } catch ( Exception ec ) { ec.printStackTrace(); }
			}
		}
		
		return cols;
		
		
	}
	
	
	@Override
	public Iterator<String[]> iterator() {
		return this;
	}
	
	
	//@Override
	//public void remove() {}
	
	
	@Override
	public void close() {
		try { reader.close(); } catch ( Exception ec ) { ec.printStackTrace(); }
	}
	
	public void skipHeaderLine() {
		skipHeaderLines(1);
	}
	
	public void skipHeaderLines( int linenumber ) {
		
		try {
			
			if (linenumber == 1) {
				line = reader.readLine();
			} else {
				for( int i=0; i<linenumber; i++) {
					line = reader.readLine();
				}
			}
		
		} catch ( IOException e ) {
			System.err.println( "Read error in file");
			try { reader.close(); } catch ( Exception ec ) { ec.printStackTrace(); }
		}
		
	}
	
	
}
