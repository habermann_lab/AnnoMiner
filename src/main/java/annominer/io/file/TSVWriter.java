package annominer.io.file;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class TSVWriter implements AutoCloseable {
	
	
	private BufferedWriter out;
	private String separator = "\t";
	private boolean newline = true;
	
	
	public TSVWriter(String fname) {
		
		FileWriter fstream = null;
		
		try {
			fstream = new FileWriter(fname);
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
		out = new BufferedWriter(fstream);
			
	}
	
	
	public TSVWriter(String fname, String separator) {
		this(fname);
		this.separator = separator;
	}
	
	
	public void writeln(Object... args) {
		
		int max = args.length - 1;
		
		try {
			
			if ( newline == false ) out.write(separator);
			
			for (int i=0; i<max; i++) {
				
				if ( args[i] == null ) out.write(separator);
				else out.write(args[i]+separator);
				
			}
			
			if ( args[max] == null ) out.write("\n");
			else out.write(args[max]+"\n");
			this.newline = true;
			
		
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
	}
	
	
	public void write(Object... args) {
		
		int max = args.length - 1;
		
		try {
			
			if ( newline == false ) out.write('\t');
			
			for (int i=0; i<max; i++) {
				if ( args[i] == null ) out.write(separator);
				else out.write(args[i]+separator);
			}
			
			if ( args[max] != null ) out.write( "" + args[max] );
			this.newline = false;
			
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
	}
	
	
	public void endl() {
		try {
			out.write('\n');
			this.newline = true;
		} catch (IOException e) {
			e.printStackTrace();
			
		}
	}
	
	
	@Override
	public void close() {
		
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
		
		
	
	
	

}
