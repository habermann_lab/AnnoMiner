package annominer;

import java.util.HashSet;
import java.util.Set;
//import java.util.logging.Logger;

//import annominer.ui.web.servlet.PeakToGene;
/**
 * coverage of each genomic feature 
 */
public class GeneCoverage {
	
	private Set<String> geneids;
	private long chromlength;
	private int flrange;
	private int tss_upstream, tss_downstream;
	private double f1,f2;
	private long cds_cov = 0, cds_len = 0,
	tss_cov = 0, tss_len = 0,
	utr_5p_cov = 0, utr_5p_len = 0,
	utr_3p_cov = 0, utr_3p_len = 0,
	flanking_5p1k_cov = 0, flanking_5p1k_len = 0,
	flanking_5p2k_cov = 0, flanking_5p2k_len = 0,
	flanking_5p3k_cov = 0, flanking_5p3k_len = 0,
	flanking_3p1k_cov = 0, flanking_3p1k_len = 0,
	flanking_3p2k_cov = 0,flanking_3p2k_len = 0,
	flanking_3p3k_cov = 0, flanking_3p3k_len = 0;
	
//	private final static Logger LOGGER = Logger.getLogger(PeakToGene.class.getCanonicalName());
	/**
	 * gene coverage constructor
	 * @param flrange
	 * @param tss_upstream
	 * @param tss_downstream
	 */
	public GeneCoverage( int flrange, int tss_upstream, int tss_downstream ) {
		
		this.flrange = flrange;
		this.tss_upstream = tss_upstream;
		this.tss_downstream = tss_downstream;
	
	}
	/**
	 * gene coverage constructor
	 */
	public GeneCoverage() {
		this(1000,0,500);
	}
				
	/**This method given the length of the chromosome assign it to the variable chromlength and initialize an hashset named geneids
	 * 
	 * @param length chromsize
	 */
	public void nextChrom( long length ) {
		geneids = new HashSet<String>();
		chromlength = length;
	}

	/**This method calculates the coverage between the region and all the features in the nearby regions
	 * 
	 * @param gene
	 * @param region
	 */
	public void addGene( Transcript gene, Region region ) {
		
		boolean newgene = true;
		
		Region coverage, tregion;
		
		Transcript transcript = gene;
		/*check to see if the gene pid is already in the list, if it is not is added to geneids*/
		if (geneids.contains(gene.pid) ) {
			newgene = false;
		} else {
			newgene = true;
			geneids.add(gene.pid);
		}
		
		tregion = gene.flankingRegion5p(chromlength, flrange, tss_upstream);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "up flanking 1: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_5p1k_cov += coverage.length();
			if (newgene)
				flanking_5p1k_len += flrange;
		}
		
		tregion = gene.flankingRegion5p(chromlength, flrange ,flrange+tss_upstream);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "up flanking 2: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_5p2k_cov += coverage.length();
			if (newgene)
				flanking_5p2k_len += flrange;
		}
		
		tregion = gene.flankingRegion5p(chromlength, flrange,2*flrange+tss_upstream);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "up flanking 3: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_5p3k_cov += coverage.length();
			if (newgene)
				flanking_5p3k_len += flrange;
		}
		
		tregion = gene.flankingRegion3p(chromlength, flrange);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "down flanking 1: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_3p1k_cov += coverage.length();
			if (newgene)
				flanking_3p1k_len += flrange;
		}
		
		tregion = gene.flankingRegion3p(chromlength, flrange, flrange);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "down flanking 2: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_3p2k_cov += coverage.length();
			if (newgene)
				flanking_3p2k_len += flrange;
		}
		
		tregion = gene.flankingRegion3p(chromlength, flrange, 2*flrange);
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "down flanking 3: " + tregion.left + "--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				flanking_3p3k_cov += coverage.length();
			if (newgene)
				flanking_3p3k_len += flrange;
		}
		

		coverage = transcript.cds.intersectionWith(region);
//		LOGGER.log(Level.INFO, "cds: " + transcript.cds.left +"--"+transcript.cds.right);
		if (coverage!= null)
			cds_cov += coverage.length();
		if (newgene)
			cds_len += transcript.cds.length();

		
		//TSS definition	

		tregion = transcript.flankingRegion5p(chromlength, tss_downstream + tss_upstream, -tss_downstream);
//		LOGGER.log(Level.INFO, "tss: " + tregion.left +"--"+tregion.right);
		
		coverage = tregion.intersectionWith(region);
		if (coverage!= null)
			tss_cov += coverage.length();	
		if (newgene)
			tss_len += tregion.length();
		
		
		tregion = transcript.get5pUTR();
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "5utr: " + tregion.left +"--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				utr_5p_cov += coverage.length();
			if (newgene)
				utr_5p_len += tregion.length();
			
		}
		
		tregion = transcript.get3pUTR();		
		
		if (tregion != null) {
//			LOGGER.log(Level.INFO, "3utr: " + tregion.left +"--"+tregion.right);
			coverage = tregion.intersectionWith(region);
			if (coverage!= null)
				utr_3p_cov += coverage.length();
			if (newgene)
				utr_3p_len += tregion.length();
		}	

		
	}		

	/**Store in a string all the gene coverage (genecov) results in JSON format
	 * Each one used to plot the respective barplot 
	 * @return a String (JSON format)
	 */
	public String printCoverageResultsJSON() {

		
		String flankcov = "[";
		
		if ( flanking_5p2k_len == 0 ) {
			flankcov += "[\"- " + 3 * flrange + "bp\",0],";
		} else {
			flankcov += "[\"- " + 3 * flrange + "bp\"," + (((double) flanking_5p3k_cov) / flanking_5p3k_len) + "],";
		}
			
		if ( flanking_5p2k_len == 0 ) {
			flankcov += "[\"- " + 2 * flrange + "bp\",0],";
		} else {
			flankcov += "[\"- " + 2 * flrange + "bp\"," + (((double) flanking_5p2k_cov) / flanking_5p2k_len) + "],";
		}
		
		if ( flanking_5p1k_len == 0 ) {
			flankcov += "[\"- " + flrange + "bp\",0],";
		} else {
			flankcov += "[\"- " + flrange + "bp\"," + (((double) flanking_5p1k_cov) / flanking_5p1k_len) + "],";
		}
		
		if ( tss_len == 0 ) {
			flankcov += "[\"TSS ("+tss_upstream+" - "+tss_downstream+"bp)\",0],";
		} else {
			flankcov += "[\"TSS ("+tss_upstream+" - "+tss_downstream+"bp)\"," +  (((double)tss_cov)/tss_len ) + "],";
		}
		
		if ( utr_5p_len == 0 ) {
			flankcov += "[\"5\'UTR\",0],";
		} else {
			flankcov += "[\"5\'UTR\"," +  (((double)utr_5p_cov)/utr_5p_len ) + "],";
		}

		if ( cds_cov == 0 ) {
			flankcov += "[\"CDS\",0],";
		} else {
			flankcov += "[\"CDS\"," +  (((double)cds_cov)/cds_len ) + "],";
		}
		
		if ( utr_3p_len == 0 ) {
			flankcov += "[\"3\'UTR\",0],";
		} else {
			flankcov += "[\"3\'UTR\"," +  (((double)utr_3p_cov)/utr_3p_len ) + "],";
		}
		
		if ( flanking_3p1k_len == 0 ) {
			flankcov += "[\"+ " + flrange + "bp\",0],";
		} else {
			flankcov += "[\"+ " + flrange + "bp\"," + (((double) flanking_3p1k_cov) / flanking_3p1k_len) + "],";
		}
		
		if ( flanking_3p2k_len == 0 ) {
			flankcov += "[\"+ " + 2 * flrange + "bp\",0],";
		} else {
			flankcov += "[\"+ " + 2 * flrange + "bp\"," + (((double) flanking_3p2k_cov) / flanking_3p2k_len) + "],";
		}
		
		if ( flanking_3p3k_len == 0 ) {
			flankcov += "[\"+ " + 3 * flrange + "bp\",0]]";
		} else {
			flankcov += "[\"+ " + 3 * flrange + "bp\"," + (((double) flanking_3p3k_cov) / flanking_3p3k_len) + "]]";
		}
		

		return flankcov;
		
	}
	
	
	/** get first flanking region coverage
	 * @return double
	 */
	public double getF1(){
		if ( flanking_5p1k_len == 0 ) {
			f1= 0 ;
		} else {
			f1= (((double)flanking_5p1k_cov)/flanking_5p1k_len );
		}
	    return f1;}
	
	
	/** get second flanking region coverage
	 * @return double
	 */
	public double getF2(){
		if ( flanking_3p1k_len == 0 ) {
			f2=0;
		} else {
			f2=(((double)flanking_3p1k_cov)/flanking_3p1k_len );
		}
		return f2;}
	
	
	
}
