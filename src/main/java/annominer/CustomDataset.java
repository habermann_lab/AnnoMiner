package annominer;

import java.io.Serializable;
import java.util.ArrayList;

/** class defining the custom annotation dataset 
 */
public class CustomDataset implements Serializable{
	

private static final long serialVersionUID = 1L;
	private String symbol, label2 = "", label3 = "", label4 = "", label5 = "", label6 = "";
private ArrayList<String> labels = new ArrayList<String>();
	private ArrayList<String> labeltypes = new ArrayList<String>();

	/** custom dataset constructor
	 */
	public CustomDataset(){
		
	}
	/** custom dataset constructor
	 */
	public CustomDataset(String symbol, String label2, ArrayList<String> labels, ArrayList<String> labeltypes) {
		this.labels=labels;
		this.labeltypes = labeltypes;
		this.symbol = symbol;
		this.label2=label2;
		
	}
	/** custom dataset constructor
	 */
	public CustomDataset(String symbol, String label2, String label3, ArrayList<String> labels,
			ArrayList<String> labeltypes) {
		this.labels=labels;
		this.labeltypes = labeltypes;
		this.symbol = symbol;
		this.label2=label2;
		this.label3=label3;
		
	}
	/** custom dataset constructor
	 */
	public CustomDataset(String symbol, String label2, String label3, String label4, ArrayList<String> labels,
			ArrayList<String> labeltypes) {
		this.labels=labels;
		this.labeltypes = labeltypes;
		this.symbol = symbol;
		this.label2=label2;
		this.label3=label3;
		this.label4=label4;
		
	}
	/** custom dataset constructor
	 */
	public CustomDataset(String symbol, String label2, String label3, String label4, String label5,
			ArrayList<String> labels, ArrayList<String> labeltypes) {
		this.labels=labels;
		this.labeltypes = labeltypes;
		this.symbol = symbol;
		this.label2=label2;
		this.label3=label3;
		this.label4=label4;
		this.label5=label5;
		
	}
	/** custom dataset constructor
	 */
	public CustomDataset(String symbol, String label2, String label3, String label4, String label5, String label6,
			ArrayList<String> labels, ArrayList<String> labeltypes) {
		this.labels=labels;
		this.labeltypes = labeltypes;
		this.symbol = symbol;
		this.label2=label2;
		this.label3=label3;
		this.label4=label4;
		this.label5=label5;
		this.label6=label6;
		
	}
	
	/** get gene symbol
	 * @return symbol String
	 */
	public String getSymbol() {// is the first label
		return this.symbol;
	   }	
	
	/** get labels of custom dataset
	 * @return labels ArrayList<String>
	 */
	public ArrayList<String> getLabels(){
		return this.labels;
	}	

	
	/** get label types: if text or numeric
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getLabelTypes() {
		return this.labeltypes;
	}
	
	/** get label
	 * @return String
	 */
	public String getLabel2() {
		return this.label2;
	}
	
	/**  get label
	 * @return String
	 */
	public String getLabel3() {
		return this.label3;
	}
	
	/**  get label
	 * @return String
	 */
	public String getLabel4() {
		return this.label4;
	}
	
	/**  get label
	 * @return String
	 */
	public String getLabel5() {
		return this.label5;
	}
	
	/**  get label
	 * @return String
	 */
	public String getLabel6() {
		return this.label6;
	}
}
