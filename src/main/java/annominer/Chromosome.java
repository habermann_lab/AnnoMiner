package annominer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;


/**
*Chromosome define the chromosome genomic regions
*@param pid is the chomosome name
*@param length in basepairs
*@param regions annotated on the chromosome
*/
public class Chromosome implements Iterable<Region> {
	
	public final String pid;
	public long length;
	protected List<Region> regions;
	
	
	
	/**
	*Chromosome constructor
	*@param pid is the chomosome name
	*@param length is basepairs
	*/
	public Chromosome(String pid, long length){
		this.pid = pid;
		this.length = length;
		this.regions = new ArrayList<Region>(10000);
	}
	/**
	*Chromosome constructor
	*@param pid is the chomosome name
	*@param length is basepairs
	*@param regions annotated on the chromosome
	*/
	public Chromosome(String pid, long length, List<Region> regions){
		this.pid = pid;
		this.length = length;
		this.regions = regions;
	}
	
	/* Interfaces */
	/**
	*iterator through chromosome regions
	*/
	public Iterator<Region> iterator() {
        Iterator<Region> iter = regions.iterator();
        return iter;
    }
	
	/**
	*list iterator through chromosome regions
	*/
	public ListIterator<Region> listIterator() {
        ListIterator<Region> iter = regions.listIterator();
        return iter;
    }
	/**
	*list iterator through chromosome regions
	*@param i number of regions
	*/
	public ListIterator<Region> listIterator(int i) {
        ListIterator<Region> iter = regions.listIterator(i);
        return iter;
    }
	
	
	/* Methods */
	
	/**
	*add a region
	*@param region
	*/
	public void add(Region region) {
		this.regions.add(region);
	}
	
	/**
	*get number of regions in chromosome
	*/
	public int getRegionNumber() {
		return regions.size();
	}
	
	/**
	*return chromosome and its lenght in tsv 
	*/
	public String toTSV(){
		return pid + '\t' + String.valueOf(length);
	}
	
	/**
	*return chromosome name, regions and its lenght in JSON format
	*/
	public String toJSON() {
		StringBuilder strg = new StringBuilder();
		if (!regions.isEmpty()) {
			for (Region region : regions) {
				strg.append(region.toJSON() + ",\n");
			}
			strg.setLength(strg.length() - 2);
		}
		return "{\"pid\":\"" + pid +"\",\"length\":"+String.valueOf(length)+",\"regions\":["+strg+"]}";
		
	}
	
	/**
	*get genes lying on a chromosome
	*@return gene id, gene length and list of genes lying on it
	*/
	public Chromosome getChromosomeOfGenes(){
		
		Map<String,Gene> dict = new HashMap<String,Gene>();
		
		for(Region region : this.regions){
			
			Transcript trans=(Transcript) region;
			if( dict.containsKey(trans.symbol) ){
				dict.get(trans.symbol).add(trans);
			} else {
				dict.put(trans.symbol,new Gene(trans));
			}
		}
		
		List<Region> genes = dict.values().stream().collect( Collectors.toList() );
		Collections.sort(genes);
		
		return new Chromosome(this.pid,this.length,genes);
		
	}
	
	
}




