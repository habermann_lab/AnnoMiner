package annominer;
	
/** annotation linked to each experiment
 */	
public class ExperimentAnnotation {
	
	public String pid;
	public String probe;
	public String method;
	
	public String target;
	public String targettype;
	public String treatment;
	public String replicate;
	public String geoacc;
	public String stage;
	public String project;
	
	public int list_hits;
	public int genome_hits;
	
	/**constructor of experiment annotation
	 */	
	public ExperimentAnnotation() {};
	/**constructor of experiment annotation
	 * 
	 * @param pid
	 * @param probe
	 * @param method
	 */
	public ExperimentAnnotation(String pid, String probe, String method) {
		
		this.pid = pid;
		this.probe = probe;
		this.method = method;
		
	}
	
	
	/** get treatment
	 * @return String
	 */
	public String getTreatment() {
		if( treatment != null ) {
			return this.treatment;
		} else {
			return "";
		}
	}
	
	
	/** get replicate
	 * @return String
	 */
	public String getReplicate() {
		if( replicate != null ) {
			return this.replicate;
		} else {
			return "";
		}
	}
	
}
