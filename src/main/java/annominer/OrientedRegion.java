package annominer;
/**
 * oriented region
 */
public class OrientedRegion extends Region {
	
	public boolean strand; 
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 */
	public OrientedRegion(long pos1, long pos2, boolean strd) {
		super(pos1, pos2);
		strand = strd;
	}
	/**
	 * constructor
	 * @param left
	 * @param right
	 * @param strd
	 */
	public OrientedRegion(String left, String right, String strd) {
		super(left, right);
		
		if ( strd.equals("+") || strd.equals("1") ) {
			strand = true;
		} else if ( strd.equals("-") || strd.equals("-1") ) {
			strand = false;
		} else {
			// exception
			throw new IllegalArgumentException("Invalid strand identifyer: " + strd );
		}
	}



	
	/** return borders to 5'
	 * @param region
	 * @param rangev
	 * @param offset
	 * @return boolean
	 */
	public boolean borders5PrimeTo(Region region, int rangev, int offset){
		
		if (strand) {
			if (region.right + offset < left) {
				if (left - offset - region.right < rangev) {
					
					return true;
					
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			if (right + offset < region.left) {
				if (region.left - right - offset < rangev) {
					
					return true;
					
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
						
	
	
	/** return borders to 3'
	 * @param region
	 * @param rangev
	 * @param offset
	 * @return boolean
	 */
	public boolean borders3PrimeTo(Region region, int rangev, int offset) {
		
		if (strand) {
			if (right + offset < region.left) {
				if (region.left - right - offset < rangev) {
					
					return true;
					
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			if (region.right + offset < left) {
				if (left - offset - region.right < rangev) {
					
					return true;
					
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	
	
	/** return flanking region at 5' + offset + range	 
	 * @param maxv
	 * @param rangev
	 * @param offset
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion5p(long maxv, int rangev, int offset) {
		
		if (strand) {
			if ( (left - rangev - offset) < 0 ) {
				if ( (left - offset) < 0 ) {
					return null;
				} else {
					return new OrientedRegion(0, left - offset, strand);
				}
			} else {
				return new OrientedRegion(left - rangev - offset, left - offset, strand);
			}
		} else {
			if ( (right + rangev + offset) > maxv ) {
				if ( (right + offset) > maxv ) {
					return null;
				} else {
					return new OrientedRegion(right + offset, maxv, strand);
				}
			} else {
				return new OrientedRegion(right + offset, right + rangev + offset, strand);
			}
		}
	}
	
	
	
	/** return flanking region at 5' + range	 
	 * @param maxv
	 * @param rangev
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion5p(long maxv, int rangev) {
		return this.flankingRegion5p(maxv, rangev, 0);
	}
	
	
	/** return flanking region at 5' + distance + range for long range interactions	 
	 * @param maxv
	 * @param rangev
	 * @param dis
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion5pLRI(long maxv, int rangev, int dis) {
		
		if (strand) {
			if ( (left - rangev - dis) < 0 ) {
				if ( (left - dis) < 0 ) {
					return null;
				} else {
					return new OrientedRegion(0, left - dis, strand);
				}
			} else {
				return new OrientedRegion(left - rangev - dis, left - dis, strand);
			}
		} else {
			if ( (right + rangev + dis) > maxv ) {
				if ( (right + dis) > maxv ) {
					return null;
				} else {
					return new OrientedRegion(right + dis, maxv, strand);
				}
			} else {
				return new OrientedRegion(right + dis, right + rangev + dis, strand);
			}
		}
	}
	
	/** return flanking region at 3' + distance + range for long range interactions	 
 	 * @param maxv
	 * @param rangev
	 * @param dis
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion3pLRI(long maxv, int rangev, int dis) {
		
		if (strand) {
			if ( (right + rangev + dis) > maxv ) {
				if ( (right + dis) > maxv ) {
					return null;
				} else {
					return new OrientedRegion(right + dis, maxv, strand);
				}
			} else {
				return new OrientedRegion(right + dis, right + rangev + dis, strand);
			}
		} else {
			if ( (left - rangev - dis) < 0 ) {
				if ( (left -dis) < 0 ) {
					return null;
				} else {
					return new OrientedRegion(0, left - dis, strand);
				}
			} else {
				return new OrientedRegion(left - rangev - dis, left - dis, strand);
			}
		}
	}
	
	
	/** return flanking region at 3' + offset + range
	 * @param maxv
	 * @param rangev
	 * @param offset
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion3p(long maxv, int rangev, int offset) {
		
		if (strand) {
			if ( (right + rangev + offset) > maxv ) {
				if ( (right + offset) > maxv ) {
					return null;
				} else {
					return new OrientedRegion(right + offset, maxv, strand);
				}
			} else {
				return new OrientedRegion(right + offset, right + rangev + offset, strand);
			}
		} else {
			if ( (left - rangev - offset) < 0 ) {
				if ( (left - offset) < 0 ) {
					return null;
				} else {
					return new OrientedRegion(0, left - offset, strand);
				}
			} else {
				return new OrientedRegion(left - rangev - offset, left - offset, strand);
			}
		}
	}
	
	
	/**  return flanking region at 3 ' 
	 * @param maxv
	 * @param rangev
	 * @return OrientedRegion
	 */
	public OrientedRegion flankingRegion3p(long maxv, int rangev) {
		return this.flankingRegion3p(maxv, rangev, 0);
	}
	
	
	
	
	/** return 5' - distance
	 * @param shift
	 * @param maxv
	 * @return long
	 */
	public long pos5pBy(int shift, long maxv) {
		if (strand) {
			if (left - shift < 0) {
				return 0;
			} else {
				return left - shift;
			}
		} else {
			if (maxv != 0 && right + shift > maxv) {
				return maxv;
			} else {
				return right + shift;
			}
		}
	
	}
	
	
	/** return 5'
	 * @param shift
	 * @return long
	 */
	public long pos5pBy(int shift) {
		return this.pos5pBy(shift, 0);
	}
	
	
	
	/** return 3' + distance
	 * @param shift
	 * @param maxv
	 * @return long
	 */
	public long pos3pBy(int shift, long maxv) {
		if (strand) {
			if (maxv !=0 && right + shift > maxv) {
				return maxv;
			} else {
				return right + shift;
			}
		} else {
			if (left - shift < 0) {
				return 0;
			} else {
				return left - shift;
			}
		}
	}
	
	
	/** return 3'
	 * @param shift
	 * @return long
	 */
	public long pos3pBy(int shift) {
		return this.pos3pBy(shift, 0);
	}
	
	
	
	/** get start
	 * @return long
	 */
	public long start() {
		if (strand) {
			return left;
		} else {
			return right;
		}
	}
	
	
	
	/** get end
	 * @return long
	 */
	public long end() {
		if  (strand) {
			return right;
		} else {
			return left;
		}
	}
	
	
	
	/** return strand in string
	 * @return String
	 */
	public String strandToString() {
		if (strand) {
			return "+";
		} else {
			return "-";
		}
	}
	

	
	/** return strand in JSON
	 * @return String
	 */
	public String strandToJSON() {
		if (strand) {
			return "true";
		} else {
			return "false";
		}
	}
	
	
	
	/** return results in TSV
	 * @return String
	 */
	@Override
	public String toTSV() {
		return String.valueOf(left) + '\t' + String.valueOf(right) + '\t' + strandToString();
	}
	
	
	
	/** return results in JSON
	 * @return String
	 */
	@Override
	public String toJSON() {
		return '[' + String.valueOf(left) + ',' + String.valueOf(right) + ',' + strandToJSON() + ']';
	}

}


