package annominer;

import java.util.Iterator;

import annominer.exceptions.ChromIDException;
import annominer.io.database.AssemblyDAO;
import annominer.io.database.ChromosomeAssemblyDAO;

/**
*Assembly defines the genome assembly for a model organism 
*/
public class Assembly implements Iterable<ChromosomeAssembly> {
	
	public String organism;
	
	protected AssemblyDAO assemblydao;
	protected ChromosomeAssemblyDAO chromosomeassemblydao;
	/**
	*@return iterator
	*/
	public Iterator<ChromosomeAssembly> iterator() {
		return assemblydao.getObjectIterator(this.organism);
    }
	/**
	*@param id of the chromosome
	*@return chromosome assembly
	*/
	public ChromosomeAssembly getChromAssemblyByID(String pid) throws ChromIDException {
		return assemblydao.getObjectByID(this.organism, pid);
	}
	
	
	
	
}
