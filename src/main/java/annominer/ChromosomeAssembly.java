package annominer;
/**Define chromoosme by its name and sequence
 */
public class ChromosomeAssembly {
	
	public String chromid;
	public String sequence;
	/**
	*constructor of the class ChromosomeAssembly 
	*@param chromid chromosome name
	*@param sequence is the chromosome sequence as string 
	*/
	public ChromosomeAssembly(String chromid, String sequence){
		
		this.chromid = chromid;
		this.sequence = sequence;
		
	}
	
	
}
