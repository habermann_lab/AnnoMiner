package annominer;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import annominer.exceptions.ChromIDException;
//import jdk.jfr.internal.Logger;
/**
 * genome annotation set
 */
public class GenomeAnnotationSet {
	
	private Map<String,Chromosome> chroms;
	public GenomeAnnotation genome;
	
	/**
	 * contructor
	 * @param organism
	 * @param source
	 * @param gtype
	 * @param canonical
	 */
	public GenomeAnnotationSet(String organism, Source source, String gtype, boolean canonical) {
		
		this.chroms = new HashMap<String,Chromosome>();/*constructor*/
		this.genome = new GenomeAnnotation(organism, source, gtype, canonical);
		/* constructor define in DataTrack organism, source, gtype, canonical and also*/
		/*new MongoClient addressed to the local host;*/
		/*chromdao ChromosomeMongoDAO*/
		
		for (Chromosome chrom : this.genome) {
			/*chroms collection populated by chrom.pid: chrom (that contains also all the regions in the chromosome)*/
			this.chroms.put(chrom.pid, chrom);
			
		}
		
	}
	/**This method, given the id of the chromtrack in the collection experiment (the name of the chromosome), return the pid of the chromosome in the collection chroms
	 * @param pid chromtrack.pid
	 * @return pid of the Chromosome in chroms (chroms.get(pid))
	 * @throws ChromIDException
	 */
	public Chromosome getChromByID( String pid ) throws ChromIDException {
		
		return chroms.get(pid);
		
	}
	
	
	
}
