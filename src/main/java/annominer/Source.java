package annominer;
/**
 * genomic resource
 */
public class Source {
	
	public String source;
	public String host;
	public int port;
	public String user;
	public String password;
	public String filepath;
	public String fileformat;
	
	/**
	 * constructor
	 * @param source
	 * @param host
	 * @param port
	 * @param user
	 * @param password
	 */
	public Source(String source,String host, int port, String user,String password) {
		
		this.source=source;
		this.host=host;
		this.port=port;
		this.user=user;
		this.password=password;
		
	}
	/**
	 * constructor
	 * @param source
	 * @param host
	 * @param user
	 * @param password
	 */
	public Source(String source,String host,String user,String password) {
		
		this.source=source;
		this.host=host;
		this.user=user;
		this.password=password;
		
	}
	/**
	 * constructor
	 * @param sourcetype
	 * @param host
	 * @param user
	 */
	public Source(String sourcetype,String host,String user) {
		
		this.source=sourcetype;
		this.host=host;
		this.user=user;
		
	}
	/**
	 * constructor
	 * @param sourcetype
	 * @param filepath
	 */
	public Source(String sourcetype, String filepath) {
		this.source = sourcetype;
		this.host = filepath;
	}
	/**
	 * constructor
	 * @param source
	 */
	public Source(String source) {
		this.source = source;
	}
	/**
	 * constructor
	 */
	public Source() {}
	
	
}
