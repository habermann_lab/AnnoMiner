package annominer;
/**
 * named oriented region object
 */
public class NamedOrientedRegion extends OrientedRegion {
	
	public String pid;
	public String symbol;
	/**
	 * constructor
	 * 
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 */
	public NamedOrientedRegion(long pos1, long pos2, boolean strd, String pid, String symbol) {
		super(pos1, pos2, strd);
		this.pid = pid;
		this.symbol = symbol;
	}
	/**
	 * constructor
	 * 
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 */
	public NamedOrientedRegion(long pos1, long pos2, boolean strd, String pid) {
		this(pos1,pos2,strd,pid,null);
	}
	/**
	 * constructor
	 * 
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 * @param symbol
	 */
	public NamedOrientedRegion(String pos1, String pos2, String strd, String pid, String symbol) {
		super(pos1, pos2, strd);
		this.pid = pid;
		this.symbol = symbol;
	}
	/**
	 * constructor
	 * @param pos1
	 * @param pos2
	 * @param strd
	 * @param pid
	 */
	public NamedOrientedRegion(String pos1, String pos2, String strd, String pid) {
		this(pos1,pos2,strd,pid,null);
	}
		
	
	
	/**  return results in TSV format
	 * @return String
	 */
	@Override
	public String toTSV() {
		if (symbol!=null) {
			return String.valueOf(left) + '\t' + String.valueOf(right) + '\t' + strandToString()+ '\t' + pid+ '\t' + symbol;
		} else {
			return String.valueOf(left) + '\t' + String.valueOf(right) + '\t' + strandToString()+ '\t' + pid;
		}
	}

	
	
	/**  return results in JSON format
	 * @return String
	 */
	@Override
	public String toJSON() {
		if (symbol!=null) {
			return '[' + String.valueOf(left) + ',' + String.valueOf(right) + ',' + strandToJSON() + ",\"" + pid + "\",\"" + symbol + "\"]";
		} else {
			return '[' + String.valueOf(left) + ',' + String.valueOf(right) + ',' + strandToJSON() + ",\"" + pid + "\"]";
		}
	}
	
	
	
	
}
