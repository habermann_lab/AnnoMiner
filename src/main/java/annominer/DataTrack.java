package annominer;

import java.util.Iterator;

import annominer.exceptions.ChromIDException;
import annominer.io.database.ChromosomeDAO;
import annominer.io.database.DataTrackDAO;

/** class defining the genomic region file 
*/
public abstract class DataTrack implements Iterable<Chromosome> {
	
	public String trackid;
	public String organism;
	
	protected DataTrackDAO trackdao;
	protected ChromosomeDAO chromdao;
	
	
	/* Constructors */
	/** datatrack contructor
	*/
	protected DataTrack(){};
	
	/** datatrack contructor
	*/
	public DataTrack(String trackid, String organism, Source source) {
		
		this.trackid = trackid;
		this.organism = organism;
		
	}
	/** datatrack contructor
	*/
	public DataTrack(String trackid, String organism) {

		this.trackid = trackid;
		this.organism = organism;

	}
	
	
	/** ierator through chromosome
	 * @return Iterator<Chromosome>
	 */
	
	
	public Iterator<Chromosome> iterator() {
		return chromdao.getObjectIterator(this.organism, this.trackid);
    }
	
	
	
	/** set organism
	 * @param organism
	 */
	
	
	public void setOrganism(String organism) {
		this.organism = organism;
	}
	
	
	/** set track id
	 * @param trackid
	 */
	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}
	
	
	/** get chromosome
	 * @param pid of the chromosome
	 * @return Chromosome
	 * @throws ChromIDException
	 */
	public Chromosome getChromByID(String pid) throws ChromIDException {
		return chromdao.getObject(this.organism, this.trackid, pid);
	}
	
	
	
}




