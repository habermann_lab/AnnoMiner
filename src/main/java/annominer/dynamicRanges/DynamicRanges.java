package annominer.dynamicRanges;

import java.io.IOException;

import annominer.exceptions.ChromIDException;

public class DynamicRanges {
	

	public static void main(String[] args) throws ChromIDException, IOException {
		
		Integer margins[] = new Integer[] { 20000 };
		String references[] = new String[] { "refseq", "genecode", "ucsc", "ensembl" };
		String genomes[] = new String[] { "hg19" };
		String features[] = new String[] { "TF" };
		
		for(String assembly: genomes) {

			for(String ref: references) {

				for(Integer margin: margins) {
					
					for(String feature: features) {

						CreateTFDistRanges distOverlap = new CreateTFDistRanges(assembly, ref, margin);

						distOverlap.printAttributes();

						distOverlap.computeDistances(feature);

					}

				}
			}
		}
	   }
	

}
