package annominer.dynamicRanges;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import annominer.Chromosome;
import annominer.ExperimentAnnotation;
import annominer.GenomeAnnotation;
import annominer.GenomeRegionTrack;
import annominer.Region;
import annominer.Source;
import annominer.Transcript;
import annominer.exceptions.ChromIDException;
import annominer.io.database.ExperimentDAO;
import annominer.io.database.mongodb3.ExperimentMongoDAO;


	public class CreateTFDistRanges {

	
		private String assembly,gtype;
		private int tss_ds, margin;
		private Source source;
		private List<Integer> distancesResults;
	private String restring;

		//constructor with default values
	public CreateTFDistRanges(String assembly, String gtype) {
			
		this.assembly = assembly;
		this.gtype = gtype;
		// assigning default values:
		this.tss_ds = 200;/* downstream limit of the promoter */
		this.margin = 20000;/* flanking region */

		}
		//constructor without defaults
	public CreateTFDistRanges(String assembly, String gtype, Integer margin) {

		this.assembly = assembly;
		this.gtype = gtype;
		this.tss_ds = 200;
		this.margin = margin;

		}		
		//constructor without defaults
	public CreateTFDistRanges(String assembly, String gtype, Integer tss_ds, Integer margin) {

		this.assembly = assembly;
		this.gtype = gtype;
		this.tss_ds = tss_ds;
		this.margin = margin;

		}
		
		//method used just for testing
		public void printAttributes() {

			System.out.println("Value for assembly: " + assembly ); 
			System.out.println("Value for gtype: " + gtype );  
			System.out.println("Value for margin: " + margin );  
			System.out.println("Value for tss_ds: " + tss_ds );

		}
		//method used just for testing
		public String printDistances(Map<String, List<Integer>> distResults) {
			String resultstring = "";
		// using iterators
		Iterator<Entry<String, List<Integer>>> itr = distResults.entrySet().iterator();

		while (itr.hasNext()) {
			Entry<String, List<Integer>> entry = itr.next();
//			System.out.println(entry.getKey() + "," + entry.getValue());
			resultstring += entry.getKey() + "\t" + entry.getValue() + "\n";
		}

			return resultstring;
		}

	// method used just for testing
	public String printDistances(String name, List<Integer> distances) {

		StringBuilder resultstring = new StringBuilder("");
		resultstring.append(name + "\t" + "[");

		String s = distances.stream().map(Object::toString).collect(Collectors.joining(","));

		resultstring.append(s);

		resultstring.append("]");

		return resultstring.toString();
	}

	public void computeDistances(String feature) throws ChromIDException, IOException {

		/* construct a new source object with this source, host, user USELESS NOW */
		source = new Source("sql", "genome-mysql.cse.ucsc.edu", "genome");

		// get all experiments (TFs for the assembly)
		List<ExperimentAnnotation> experiments = getExperiments(assembly, feature);

		System.out.println("number of" + feature + "that will be analysed: " + experiments.size());

		GenomeAnnotation genome = new GenomeAnnotation(assembly, source, gtype);

		System.out.println("Genome annotation: " + genome.organism);

		BufferedWriter writer = new BufferedWriter(
				new FileWriter(assembly + "_" + gtype + "_" + feature + "_" + margin + ".txt", true),
				8192 * 4);
		writer.write("PID\ttarget\tdistances\n");

		for (ExperimentAnnotation experiment : experiments) {

			System.out.println("");
			System.out.println(feature + " experiment: " + experiment.pid + " / " + experiment.target);

			GenomeRegionTrack track = new GenomeRegionTrack(experiment.pid, assembly, source);
			distancesResults = parseIntersections(track, genome);

			restring = printDistances(experiment.pid + "\t" + experiment.target, distancesResults) + "\n";

			writer.write(restring);

		}

		writer.close();

	}



	private static List<ExperimentAnnotation> getExperiments(String organism, String feature) {

			List<ExperimentAnnotation> datatracks = new ArrayList<>();/*constructor*/

			ExperimentDAO expdao = ExperimentMongoDAO.getInstance();/*return an ExperimentMongoDAO and open a new MongoClient addressed to the localhost*/

		Iterator<ExperimentAnnotation> iter = expdao.getObjectIterator(organism, "targettype", feature);
			while ( iter.hasNext() ) {
				datatracks.add( iter.next() );
			}
			/*return the collection of all the ExperimentAnnotations elements*/
			return datatracks;

		}
		

		/**This method parse the intersections and compute the coverages between the track and the regions in the genome
		 * 
		 * @param track
		 * @param genome
		 * @param checkgene 
		 * @param calculatecoverage 
		 * @throws ChromIDException
		 */
		public List<Integer> parseIntersections(GenomeRegionTrack track, GenomeAnnotation genome){

			List<Integer> distances = new ArrayList<Integer>();
			Chromosome chromosome = null;
			Transcript gene=null;
			ListIterator<Region> iterator=null;
			boolean it_next = true;
			boolean it_ended = false;
			int dis;
			for ( Chromosome chromtrack: track) {
				try {
					chromosome = genome.getChromByID(chromtrack.pid);
				} catch (ChromIDException e) {
					//					System.out.println("not listed chromosome: " + chromtrack.pid);
					continue;
				}


				/*check to see if within the chromosome we have gene regions or not*/
				if (chromosome.getRegionNumber() == 0) {/**/
					//					System.out.println("Genome-annotation has no genes for chromosome: " + chromtrack.pid );
					continue;
				}

				iterator = chromosome.listIterator();/*iterator through the regions of the chromosome*/
				it_next = true;
				it_ended = false;


				for ( Region region: chromtrack) {

					//print out the region analysed
					//					System.out.println("region: " + chromtrack.pid+":"+region.left +":"+ region.right);

					//restart the iterator
					iterator = chromosome.listIterator();
					it_next = true;
					it_ended = false;

					if (it_ended) {

						//pass;

					} else {
						while (true) {
							if (it_next) {

								if ( iterator.hasNext() ) {
									//									System.out.println("has next ");
									gene = (Transcript)iterator.next();/*next gene in the chromosome (cast into transcript)*/
									it_next = false;

								} else {
									//test
									it_next = false;														
									it_ended = true;
									break;

								}

							}
							if (gene.isLeftOf(region, margin)) {
								/*control if the left coordinate of the region (uploaded) is greater than the gene's region right coordinate plus the margin*/
								//								System.out.println("gene far left " + gene.symbol);
								it_next = true;

							} else if (gene.isRightOf(region, margin)) {	
								/*control if the right coordinate of the region (uploaded) is less than the gene's region left coordinate minus the margin*/
								//								System.out.println("gene far right " + gene.symbol);
								it_next = false;
								break;

							} else if (gene.isLeftOf(region)){
								if(!gene.strand) {
									dis = (int)(region.left - gene.right) ;
									distances.add(dis);
									//										System.out.println("gene left: " + gene.symbol + " - dist: " + dis);
								}
								it_next = true;
							}else if (gene.isRightOf(region)){
								if(gene.strand) {
									dis = (int)(gene.left - region.right) ;									
									distances.add(dis);
									//										System.out.println("gene right: " + gene.symbol+ " - dist: " + dis);
								}
								it_next = true;
							}else {
								if(gene.overlapLeft(region, tss_ds)) {
									if(!gene.strand) {
										distances.add(0);
										//								        System.out.println("gene overlap left " + gene.symbol);
									}
								}
								if(gene.overlapRight(region, tss_ds)) {
									if(gene.strand) {
										distances.add(0);
										//								        System.out.println("gene overlap right " + gene.symbol);
									}
								}
								it_next = true;
								//pass;
							}

						}//close while

					}//close else

				}//close for
			}//ended for loop
			return distances;
		}//ended parseintersection

	}//close the class
