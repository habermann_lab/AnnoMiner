package annominer.convertGeneIDs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class MainConvert {

	private static String assembly;
	private static String gtype;
	public static ArrayList<String> newList;
	private static HashSet<String> list_inputids;

	public static void main(String[] args) throws FileNotFoundException {

		// parameters

		assembly = "hg38";

		gtype = "ucsc";

		// read input test list

		File inputFile = new File("/home/fabio/Downloads/Test_files/Human.txt");

		Scanner sc = new Scanner(inputFile);

		HashSet<String> input = new HashSet<String>();

		while (sc.hasNextLine()) {
			String str = sc.nextLine();
//			System.out.println(str);
			input.add(str);
		}
		sc.close();
		
		Iterator<String> it = input.iterator();
		System.out.println(it.next());

		//real input of annominer
		ArrayList<String> used_list = new ArrayList<String>(input);

		// start the converter

		ConvertIDs testList = new ConvertIDs(assembly, gtype, used_list);


		// read IDs test table

		File IDFile = new File("/home/fabio/Annominer/src/main/resources/geneIDconverter/" + assembly + ".txt");

		testList.createDict(testList, IDFile);

		ArrayList<String> newListToUse = testList.convertInputList();
		list_inputids = new HashSet<String>(newListToUse);
		System.out.println("different ids");


		Iterator<String> it2 = list_inputids.iterator();
		System.out.println(it2.next());

//		while (it.hasNext()) {
//
//			System.out.println(it.next());
//
//		}

	}

}
