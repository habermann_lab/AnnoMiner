package annominer.convertGeneIDs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ConvertIDs {

	private String assembly;
	private String gtype;
	private ArrayList<String> list;
	public HashMap<String, String> dict;
	public HashMap<String, String> dictRef;
	public HashMap<String, String> dictCustom;

	// constructor with default values
	public ConvertIDs(String assembly, String gtype, ArrayList<String> list) {

		this.assembly = assembly;
		this.gtype = gtype;
		this.list = list;
	}

	// method used just for testing
	public void printAttributes() {

		System.out.println("Value for assembly: " + assembly);
		System.out.println("Value for gtype: " + gtype);
		System.out.println("List size: " + list.size() + " first element: " + list.iterator().next());
		;

	}

	public HashMap<String, String> getConvertedDict() {
		HashMap<String, String> convdict = new HashMap<String, String>();
		for (String oldID : dict.keySet()) {
			if (list.contains(oldID)) {
				convdict.put(oldID, dict.get(oldID));
			}
		}
		return convdict;
	}

	public ArrayList<String> convertInputList() {

		ArrayList<String> newList = new ArrayList<String>();
		for (String oldID : dict.keySet()) {
			if (list.contains(oldID)) {
				newList.add(dict.get(oldID));
			}
		}
		return newList;

	}

	public HashSet<String> convertGeneNames(Set<String> ids) {

		HashSet<String> newList = new HashSet<String>();
		for (String oldID : dictRef.keySet()) {
			if (ids.contains(oldID)) {
				newList.add(dictRef.get(oldID));
			}
		}
		return newList;
	}

	public void createDict(ConvertIDs testList, File IDFile) throws FileNotFoundException {



		Scanner sc = new Scanner(IDFile);

		// check which is the reference choosen by the user
		int indexRef = 0;
		int indexInput = 0;

		if (sc.hasNextLine()) {

			String[] strArray = sc.nextLine().split("\\t");
			String A = strArray[0];
			String B = strArray[1];
			String C = strArray[2];
			String D = strArray[3];


			if (testList.gtype.equalsIgnoreCase(A)) {
				indexRef = 0;
			} else if (testList.gtype.equalsIgnoreCase(B)) {
				indexRef = 1;
			} else if (testList.gtype.equalsIgnoreCase(C)) {
				indexRef = 2;
			} else if (testList.gtype.equalsIgnoreCase(D)) {
				indexRef = 3;
			}

		}

		// load all the ids
		ArrayList<String> AList = new ArrayList<String>();
		ArrayList<String> BList = new ArrayList<String>();
		ArrayList<String> CList = new ArrayList<String>();
		ArrayList<String> DList = new ArrayList<String>();

		while (sc.hasNextLine()) {
			String[] strArray = sc.nextLine().split("\\t");
			AList.add(strArray[0]);
			BList.add(strArray[1]);
			CList.add(strArray[2]);
			DList.add(strArray[3]);
		}
		sc.close();


		// and check against which the user upload match most
		for (String id : testList.list) {
			if (AList.contains(id)) {
				indexInput = 0;
				break;
			} else if (BList.contains(id)) {
				indexInput = 1;
				break;
			} else if (CList.contains(id)) {
				indexInput = 2;
				break;
			} else if (DList.contains(id)) {
				indexInput = 3;
				break;
			}
		}
		// create dictionary containing the matched id and the reference

		dict = new HashMap<String, String>();

		dictRef = new HashMap<String, String>();

		Scanner sc2 = new Scanner(IDFile);

		// skip the header
		sc2.nextLine();

		while (sc2.hasNextLine()) {
			String[] strArray = sc2.nextLine().split("\\t");

			dict.put(strArray[indexInput], strArray[indexRef]);

			dictRef.put(strArray[indexRef], strArray[0]);
		}
		sc2.close();
	}


}