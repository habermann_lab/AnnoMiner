# AnnoMiner web server
 
AnnoMiner web tool allows to integrate in a flexible way epigenetic state, transcription factor occupancy and transcriptomics data to predict transcriptional regulators. AnnoMiner is available at https://kailash.ibdm.univ-amu.fr/AnnoMiner.
 
 
# AnnoMiner for tool developers / local use
 
In case you are interested in AnnoMiner's local installation and use, for testing or developmental purposes, please read the following:
 
## 1. IDE and WAR file generation
 
For an optimal development of the tool we suggest to use Eclipse IDE.
Maven is used as project manager, use it to generate the WAR file (Web Application Resource or Web application ARchive) needed for the tomcat's deployment; and to run the tool from your favourite web browser locally.
 
## 2. organisation of the code
 
Find here the project organisation. The code itself has been extensively commented.
 
**lib:**
 
'lib' stores different libraries in JAR format used by AnnoMiner such as stax, intermine and httpclient.
 
**scripts:**
 
'scripts' stores bash/python scripts which can be used to populate the mongoDB with other genomics regions files.
 
**setup:**
 
'setup' stores the maven intermine dependencies.
 
**src/main:**
 
- java: Find here all the Java classes building the tool. Under "tools" are present third-party datastructures classes used by AnnoMiner. AnnoMiner's classes are present in the "annominer" folder. Here we find classes defining the basic objects such as "Region", "Assembly", "Chromosome" and various folders for specific purposes: "io" (input/output), "ui" (user interface), "math/statistics", "exceptions", "dynamicRanges" (for dynamics ranges computation, here the user can generate customized files with tailored criteria), "convertGeneIDs" and "batchupload" (used to upload in batch hundreds of gene lists, used for benchmark purpose). **Importantly** under ui/web/servlet we find the code behind the main functions of the tool: TFscan (TF/HM enrichment), PeakToGene (Peak annotation), PeakCorrelation (Peak intergation), NearbyGenes (Nearby genes annotation), LongRangeInteractions, ManageUpload and UploadsCheck (respectively for data upload and data checking). These are the files the user / developer is interested in if wants to modify the behaviours of the tool.
- resources: Find here "test" files which the user can automatically use or download from the web interface, "geneIDconverter" containing the gene IDs retrieved from BiomaRt and the computed "DynamicRanges" for each TF and all model organisms.
- webapp: Find here all the CSS, HTML and JS file which are composing the web interface. Note that all the actions are managed by the annominer.JS file.
 
**pom.mxl:**
 
Maven's Project Object Model (POM) containing information about the project, dependencies and configuration details needed and used by Maven to build the project.
 
**bin:**
 
'bin' stores the binary files used internally by AnnoMiner. We find all the already explored folders, so: "lib", "setup", "src/main", "target" and the "pom.xml" file. 
 
## 3. mongoDB/tomcat installation
 
For these purposes please follow the guidelines present in AnnoMiner_local_use-installation made available under the AnnoMiner paper's folder and clone the subfolder mongoDB_dump.

