
$(document).ready(function(){

	
	//include script and the navbar:
	
	$("#includenavbar").load("navbar.html");
	$("#includefooter").load("footer.html");

	$(".annobuttons").hide();
	$("#progress").hide();
	$("#UpTab").hide();
	$(".step2").hide();
	$("#btn_dwl_rslt").hide();
	$("#new").hide();
	$(".dis").css('display','none');
	$(".promoter").css('display','none');
	$(".orientation").css('display','none');
	//Function that calls ManageUpload
	UploadsCheck();
	
	//solution to refresh the forms once that the user refresh the page from browser button
	$(document).ready(function(e) {
	    var $input = $('#refresh');

	    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
	});
	
	//////////////////////
	// Page Navigations //
	//////////////////////

		//to have the collapse feature of the three containers
		  $("#Up, #Up2, #Up3, #Up4").collapse({"toggle": true, 'parent': '#navaccordion'});
			
		//to solve resize problems with plot
			var windowsize = $(window).width();

			$(window).resize(function() {
			  var windowsize = $(window).width();
				if (windowsize > 1199) {
						$('body').addClass("sol");
				}
				if (windowsize < 1199) {
						$('body').removeClass("sol");
				}
			});
				
	//////////////////////
	// Global variables //
	//////////////////////

			var last_datatable=null;
			var last_datatable_header=null;

	//////////////////////////////////
	// Set target-region overlap type
	//////////////////////////////////

	$('#ovl_s').change( function() {

		ovtype = $("#ovl_s").val();
		if (ovtype == 0) {
			ovval_rel = $("#ovl_t").val();
			$("#ovl_t").val(ovval_abs);
		}else{
			ovval_abs = $("#ovl_t").val();
			$("#ovl_t").val(ovval_rel);
		}


	});
	
	//defoult : 

	var assembly = "hg19";
	var species = "hg19";
	var ovtype = '0';
	var ovval_abs = '120';
	var ovval_rel = '20';


	///////////////////////////
	// User track management //
	///////////////////////////

		var utracks = new Array();
	  
	    // definition of file upload form
		var ul_trackid_p1 = '<hr class="my-4"><div class="ultrack"><div id="ul_trackid" ><input type="text" name="trackid" class="form-control" value="';
		var ul_trackid_p2 = '"/></div></div>';
		var ul_datatype = '<div class="ultrack2"><select id="ul_type" name="ul_type" class="form-control" >\
								<option value="peaks">Genomic regions</option>\
								<option value="idlist">ID list</option>\
							</select></div>';
		var ul_button = '<input type="submit" id="ul_button" class="fileUpload btn btn-primary btn-lg" value="Upload"/>';

		//after the click of upload button the div with your upload is shown
		$('body').on('click', '#ul_button', function() {
			$("#UpTab").show();
		});


		// Get the modal (alert with informations about input formats)
		var modal = document.getElementById('myModal');
		// Get the button that opens the modal
		var btn = document.getElementById("idlist");
		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];
		// When the user clicks the button, open the modal
		btn.onclick = function() {
		    modal.style.display = "block";
		}
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.display = "none";
		}

		// Get the modal
		var modal2 = document.getElementById('myModal2');
		// Get the button that opens the modal
		var btn2 = document.getElementById("bedfile");
		// Get the <span> element that closes the modal
		var span2 = document.getElementsByClassName("close2")[0];
		// When the user clicks the button, open the modal
		btn2.onclick = function() {
		    modal2.style.display = "block";
		}
		// When the user clicks on <span> (x), close the modal
		span2.onclick = function() {
		    modal2.style.display = "none";
		}

	 //Choose the file to upload
		$('#filechoice').change(function(event){

			$in=$(this);
			var path = $in.val();
			var fname = path.substring( path.lastIndexOf("\\") + 1, path.lastIndexOf("."));

			$("#ul_fname").empty();
			$("#ul_fname").append(ul_trackid_p1+fname+ul_trackid_p2+ul_datatype+ul_button);

			});
		
	//Alert in case of wrong format otherwise step2
		function UploadsCheck() {
			var cginame = "/AnnoMiner/UploadsCheck";
			$.post(
				cginame,
				function(data){
					if (data.data != null){
						$("#UpTab").show();						
						$("#Up2").show();
						utracks = data.data;
						updateAnalysisSelections();
						updateUTrackView(utracks);
						$(".step1").hide();
						$(".step2").show();
		}});}


		$('#ul_target').load(function() {
		    	$("#ul_alerts").empty();
//		    	$("#UpTab").show();
		    
				var ul_json = $.parseJSON($("#ul_target").contents().text());
	
				if(ul_json.message!=null){
					$("#ul_alerts").append('<div class="alert alert-danger alert-dismissible" role="alert"><a href="#ul_alerts" class="close" data-dismiss="alert" aria-label="Close">&times;</a>'+ul_json.message+'</div>');
		
				}
	
				if(ul_json.data!=null){
					utracks = ul_json.data;
					updateAnalysisSelections();
					updateUTrackView(utracks);
					$("html, body").delay(30).animate({scrollTop: $('#AO').offset().top - 90}, 2000);
					$(".step1").hide();
					$(".step2").show();
				}
		});


	// updateUTrackView //

	function updateUTrackView(data) {

		var titles = [{"sTitle":"Track id","sWidth":"100%"},{"sTitle":"Data-type","sWidth":"100%"}];
		var values = new Array();

		for( var i=0; i<utracks.length; ++i ) {

			values.push([utracks[i].id, utracks[i].type]);

		}

		if(utracks.length<11){
			$("#utrack_view").DataTable({
				"aaData": values,
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers",
				"sDom" : '<"col-sm-6"f>'

			});
		} else {
			$("#utrack_view").DataTable({
				"aaData": values,
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers"
			});
		}
		$('div.dataTables_filter').appendTo('#filt');
		$('div.dataTables_length').appendTo('#rank');

		$("#paginate").append($(".dataTables_paginate"));
		$("#info").append($(".dataTables_info"));
		$(".usertrack").show();

		if($('#utrack_manager').is(':hidden')) {
            $('#utrack_manager').slideDown();
        }


	}



	//////////////////////////////////////////////
	// Selection of analysis and its parameters //
	//////////////////////////////////////////////

		var genomes = {
				"hg19":["refseq","ensembl"],
				"hg38":["refseq","ucsc","genecode"],
				"mm9":["refseq","ensembl"],
				"mm10":["refseq","ucsc"],
				"dm3":["refseq","ensembl"],
				"dm6":["refseq","ensembl"],
				"ce10":["refseq","ensembl"],
				"sacCer3":["ensembl"]
		};

		addGenomeSel();
		addTrackSel();

		$('#assembly').change(function(event){

			species = $(this).val();
			var analysis = $("#analyis").val();
			
			$("#trackoptions").empty();
			$("#trackoptions2").empty();
			
			
			if( analysis == "genes" ) {
				addGenomeSel();
				addTrackSel();
			} else if ( analysis == "corr" ) {
				addGenomeSel();
				addTrackSel();
				addTrackSel2();
			} else if ( analysis == "TFs" ) {
				addGenomeSel();
				addListSel();
			} else if ( analysis == "NG" ) {
				addGenomeSel();
				addTrackSel();
			}else if ( analysis == "LRI" ) {
				addGenomeSel();
				addTrackSel();
			}
			$("#ul_form_assembly").val(species);

		});
		//which options show to the user in according to the feature
		$('#analyis').change(function(event){
			var analysis = $("#analyis").val();
			
			
			if ( analysis == "genes" || analysis == "corr"){
				$(".orientation").hide();
				$(".ovl").show();
				$(".tss").show();
				$(".dis").hide();
				$(".flanking").show();
				$(".canonical").show();
				$(".promoter").hide();

			}else if ( analysis == "TFs" ){
				$(".orientation").hide();
				$(".ovl").show();
				$(".tss").hide();
				$(".dis").hide();
				$(".flanking").hide();
				$(".canonical").hide();
				$(".promoter").show();

			}else if ( analysis == "NG" ){
				$(".orientation").show();
				$(".ovl").hide();
				$(".tss").hide();
				$(".dis").hide();
				$(".flanking").hide();
				$(".canonical").hide();
				$(".promoter").hide();

			}else if ( analysis == "LRI" ){
				$(".orientation").show();
				$(".dis").show();
				$(".ovl").hide();
				$(".tss").hide();
				$(".flanking").show();
				$(".canonical").show();
				$(".promoter").hide();
			};
		});

		$('#analyis').change(function(event){
				updateAnalysisSelections();
		});

	//Action in case of button click

		$("#btn_rslt , #textout2").click(function(){

			$(".step2").remove();
			$(".step1").remove();
			$("#progress").show();
			$("#ana_alerts").empty();
			$("#textout").empty();
			$("#textout2").empty();
			$("#textout3").empty();
	    	$("#R").removeClass("d-none d-xl-block");
			$("#FU, #AO").addClass("d-none d-xl-block");
			// TRY TO FIX SECOND PLOT ISSUE
			$("#tab1_secondplot").remove();		
			$("#plots").append('<div class="row" id="tab1_secondplot"></div>');
			$("#tab1_thirdplot").remove();		
			$("#plots").append('<div class="row" id="tab1_thirdplot"></div>');
		

		//function to evoke in base to the type of analysis
			if( $('#analyis').val()== "genes" ) {
				peak2gene();
			}else if ( $('#analyis').val()== "corr" ){
				peakCorrelation();
			}else if ( $('#analyis').val()== "TFs" ){
				tfscan();
			}else if ( $('#analyis').val()== "LRI" ){
				LongRangeInteractions();
			}else if ( $('#analyis').val()== "NG" ){
				NearbyGenes();
			}

			});
		
		//Action in case of download button click
		$("#btn_dwl_rslt, #btn_dwl_rslt2").click(function(){

			var tsv = JSON2TSV( last_datatable.data() );

			window.open("data:text/tab-separated-values;charset=utf-8," + escape(tsv));

		});

	// updateAnalysisSelections //

	function updateAnalysisSelections() {

		var analysis = $('#analyis').val();
		$("#trackoptions").empty();
		$("#trackoptions2").empty();
		$("#corr").empty();


		if( analysis == "genes" ) {
			addGenomeSel();
			addTrackSel();
		} else if ( analysis == "corr" ) {
			addGenomeSel();
			addTrackSel();
			addTrackSel2();
			$("#corr").append("<hr class=\"my-4\" >");
		} else if ( analysis == "TFs" ) {
			addGenomeSel();
			addListSel();
		}else if ( analysis == "LRI" ) {
			addGenomeSel();
			addTrackSel();
		}else if ( analysis == "NG" ) {
			addGenomeSel();
			addTrackSel();
		}

	}

	// addGenomeSel //

	function addGenomeSel() {

		var ostring = '<div class="form-group"><label>Select a genome-annotation resource:</label>';
		ostring += buildSelectField( {name:"ganno",values:genomes[species]} );
		ostring += '</div>';

		$("#trackoptions").append( '<div>'+ostring+'</div>' );

	}

	// addTrackSel //

	function addTrackSel() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno",values:ulists} );
		}

		ostring += '</div>';

		$("#trackoptions").append( '<div>'+ostring+'</div>' );

	}
	
	// addTrackSel2 //

	function addTrackSel2() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}

		var ostring = '<div class="form-group"><label>Select a second track with your genomic regions:</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno2",values:ulists} );
		}

		ostring += '</div>';

		$("#trackoptions2").append( '<div>'+ostring+'</div>' );

	}

	// addListSel //

	function addListSel() {

		var track;
		var ulists = new Array();

		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "idlist" ) { ulists.push( track.id ); }
		}


		var ostring = '<div class="form-group"><label>Select ID-list</label>';

		if ( ulists.length == 0 ) {
			ostring += "<div class='alert-danger'>Please upload an ID-list first!</div>";
		} else {
			ostring += buildSelectField( {name:"ianno",values:ulists} );
		}

		ostring += '</div>';

		$("#trackoptions").append( '<div>'+ostring+'</div>' );

	}

	// buildSelectField //

	function buildSelectField(data) {
		var ostring = '<select id="'+data.name+'" name="'+data.name+'" class="form-control" >';

		for( var i=0; i<data.values.length; ++i ) {
			ostring += '<option value="'+data.values[i]+'">'+data.values[i]+'</option>';
		}
		ostring += '</select>';
		return ostring;
	}

	//////////////////////////
	// peak2gene annotation //
	//////////////////////////

	var dblue = "#45A29E";
	var lblue = "#66FCF1";
	var red = "#F13C20";
	var peak = '#1F2833';

	var gplotdata = [];

	var cr_gn1 = false;

	var gselect = [0,0,0,0,0,0,0,0,0,0];

	var gcols_df = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];

	var gcols = [];
	
	//////////////////////////
	// peakCorrelation //
	//////////////////////////

	var dblue = "#45A29E";
	var lblue = "#66FCF1";
	var red = "#F13C20";
	var peak = '#1F2833';

	var gplotdata1 = [];
	var gplotdata2 = [];

	var cr_gn1 = false;
	var cr_gn2 = false;

	var gselect1 = [0,0,0,0,0,0,0,0,0,0];
	var gselect2 = [0,0,0,0,0,0,0,0,0,0];

	var gcols_df1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var gcols_df2 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];

	var gcols1 = [];
	var gcols2 = [];


	//////////////////////////////////////
	// LongRangeInteractions annotation //
	//////////////////////////////////////


	var LRI_plotdata = [];

	var cr_LRI = false;

	var LRI_select = [0,0]; 

	var LRI_cols_df = [lblue,dblue];

	var LRI_cols = [];

	////////////////////////////
	// Nearby Genes annotation /
	////////////////////////////


	var NG_plotdata = [];

	var cr_NG = false;

	var NG_select = [0,0,0,0,0,0,0,0,0,0];

	var NG_cols_df = [lblue,lblue,lblue,lblue,lblue,peak,dblue,dblue,dblue,dblue,dblue];

	var NG_cols = [];


	

	function JSON2TSV(objArray) {

		var sep = "\t"

		//var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
		var array = objArray;

	    var str = '';
	    var line = '';


	    for (var i in last_datatable_header) {
            line += last_datatable_header[i] + sep;
        }

	    line = line.slice(0, -1);
	    str += line + '\r\n';


	    for (var i = 0; i < array.length; i++) {
	        var line = '';


            for (var index in array[i]) {
                line += array[i][index] + sep;
            }

	        line = line.slice(0, -1);
	        str += line + '\r\n';
	    }

	    return str;

	}



	function tfscan() {
		$(".annobuttons").hide();
		$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);

		var cginame = "/AnnoMiner/TFScan";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#ianno").val();
		var ovtype = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var prom_us = $("#promoter_us").val();
		var prom_ds = $("#promoter_ds").val();


		if(last_datatable!=null) {
			last_datatable.destroy();
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');


		}
		$("#gtable").empty();
		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,ovval:ovl,ovtype:ovtype, promoter_us:prom_us, promoter_ds:prom_ds},
			function(data){

				if (data.message != null){
					$("#ana_alerts").append('<div class="alert alert-danger" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}


				if (data.data != null){

									$("#gtable").addClass("resize");
									var tfscan_titles = [{"sTitle":"Target factor"},{"sTitle":"Probe"},{"sTitle":"Treatment"},{"sTitle":"Replicate"},{"sTitle":"list"},{"sTitle":"list size"},{"sTitle":"genome"},{"sTitle":"genome size"},{"sTitle":"target genes"},{"sTitle":"score"},{"sTitle":"p-value"}];

									last_datatable_header = data.titles;

									last_datatable = $("#gtable").DataTable({
										"aaData": data.data,
										"aoColumns": tfscan_titles,
										"bProcessing": true,
										"bDestroy": true,
										"bAutoWidth": false,
										"pagingType": "simple",
										"order": [[9,'asc']],
										"aoColumnDefs": [
										{ "bVisible": true, "aTargets": [ 0,1,2,3,4,6,8,9,10 ] },
										{ "bVisible": false, "aTargets": [ '_all' ] }
										]
									});
					$("#textout").append( '<hr class="my-4"><div style="font-size:16px; text-align:center;">'+ "For the reference genome: "+ "<strong>"+ $("#assembly").val() +  "</strong> the genome size is: <strong>"+ data.data[1][7] +  "</strong><br>and<br>the " + "<strong>"+ $("#ganno").val() + "</strong>"+  " resource list size is: <strong>"+ data.data[1][5]+ "</strong>"+ '</div><hr class="my-4">' );
					//to solve page resize issues
					$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
					$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
					$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
					$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

					
					$('#gtable').on( 'click', 'td', function () {
								if(last_datatable.cell( this ).data()){
									var cell = last_datatable.cell( this ).data();
									alert( cell.replace(/;/g, "\n") );
					}});

					$('#gtable').on( 'mouseover mouseout', 'td', function () {
							$(this).toggleClass("cellev");
					});
					$('#gtable').on( 'mouseover mouseout', 'tr', function () {
							$(this).toggleClass("rowev");
					});

					$("#btn_dwl_rslt").show();
					$("#new").show();

				} else {

					$("#btn_dwl_rslt").hide();
					$("#new").hide();

				}


				$("#progress").hide();


			},

			"json"

		);

	}



function peak2gene() {
		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/PeakToGene";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		var ovl_s = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();

		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");

		if(last_datatable!=null) {
			last_datatable.destroy();
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		}
		$("#gtable").empty();

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[gselect],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can},
			function(data){
				
				
				gplotdata = [];

				if (data.flankcov){

					gplotdata = data.flankcov;
					gcols = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					gplot(gplotdata, tid);
					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

						var p2g_titles = [{"sTitle":"Primary ID","sWidth":"50%"},{"sTitle":"Symbol","sWidth":"50%"}];
						$(".annobuttons").show();
						last_datatable_header = data.titles;

						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": p2g_titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						$('#gtable').on( 'click', 'td', function () {
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});

				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

			},

			"json"


		);

		gselect = [0,0,0,0,0,0,0,0,0,0];
		cr_gn = false


	}


		function gplot(data, tid) {

			$("#tab1_secondplot").empty();
			$("#tab1_secondplot").width("600px");

			var plot1 = $.jqplot('tab1_secondplot', [data],{
				title: 'Basepair-coverage of gene-intervals in: ' + tid,

				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
				},

				seriesColors: gcols,

				axesDefaults: {
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						angle: -30,
						fontSize: '10pt'
					}
				},

				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer
					},
					yaxis: {
						label: "covered / total length",
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer
					}
				},

				grid: {
					background: '#ffffff',
					shadow: false
				}


			});

			if (!(cr_gn)){
				$('#tab1_secondplot').bind('jqplotDataClick',
					function (ev, seriesIndex, pointIndex, data) {

						var i = data[0] - 1;

						if (gselect[i] == 1) {
							gselect[i] = 0;
							gcols[i] = gcols_df[i];
						} else {
							gselect[i] = 1;
							gcols[i] = red;
						}

						gplot(gplotdata, tid);

					

					}
				);

				cr_gn = true
			}

		}//function gplot()




	function peakCorrelation() {
		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/PeakCorrelation";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid1 = $("#tanno").val();
		var tid2 = $("#tanno2").val();
		var ovl_s = $("#ovl_s").val();
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();


		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");
		$("#tab1_thirdplot").empty();
		$("#tab1_thirdplot").height("0px");

		if(last_datatable!=null) {
			last_datatable.destroy();
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');

		}
		$("#gtable").empty();

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid1:tid1,trackid2:tid2,'targets1[]':[gselect1],'targets2[]':[gselect2],ovval:ovl,ovtype:ovl_s,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can},
			function(data){
				
				gplotdata1 = [];
				gplotdata2 = [];

				if ((data.flankcov1)&&(data.flankcov2)){
					
					gplotdata1 = data.flankcov1;
					gcols1 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					corrplot(gplotdata1,"tab1_secondplot", gcols1, gselect1, cr_gn1,tid1,gcols_df1);
					
					gplotdata2 = data.flankcov2;
					gcols2 = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					corrplot(gplotdata2,"tab1_thirdplot", gcols2, gselect2,cr_gn2,tid2,gcols_df2);

					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
					$("#tab1_thirdplot").empty();
					$("#tab1_thirdplot").height("0px");
				}

				if (data.message1 != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message1+'</div>');
				}
				if (data.message2 != null){
					$("#textout3").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message2+'</div>');
				}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

						var p2g_titles = [{"sTitle":"Primary ID","sWidth":"50%"},{"sTitle":"Symbol","sWidth":"50%"}];
						$(".annobuttons").show();
						last_datatable_header = data.titles;

						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": p2g_titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						$('#gtable').on( 'click', 'td', function () {
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});

				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

			},

			"json"


		);

		gselect1 = [0,0,0,0,0,0,0,0,0,0];
		gselect2 = [0,0,0,0,0,0,0,0,0,0];
		cr_gn1 = false;
		cr_gn2 = false;


	}


		function corrplot(data , tag, gcols, gselect, cr_gn, tid, gcols_df) {


			var plt_data=data;
			var ash ="#"+tag;
			
			$(ash).empty();
			$(ash).width("600px");

			var plot1 = $.jqplot(tag, [data],{
				title: 'Basepair-coverage of gene-intervals in : ' + tid,

				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
				},

				seriesColors: gcols,

				axesDefaults: {
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						angle: -30,
						fontSize: '10pt'
					}
				},

				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer
					},
					yaxis: {
						label: "covered / total length",
						labelRenderer: $.jqplot.CanvasAxisLabelRenderer
					}
				},

				grid: {
					background: '#ffffff',
					shadow: false
				}


			});

			if (!(cr_gn)){
				$(ash).bind('jqplotDataClick',
					function (ev, seriesIndex, pointIndex, data) {

						var i = data[0] - 1;
						
						if (gselect[i] == 1) {
							gselect[i] = 0;
							gcols[i] = gcols_df[i];
						} else {
							gselect[i] = 1;
							gcols[i] = red;
						}

						corrplot(plt_data , tag, gcols, gselect, cr_gn,tid,gcols_df);
		

					}
				);

				cr_gn = true
			}

		}//function gplot()


	function LongRangeInteractions() {
		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/LongRangeInteractions";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		var dis = $("#dis").val();
		var flrg = $("#flrange").val();
		var orient = $("input[name='orientation']:checked").val();
		var can = $("input[name='canonical']:checked").val();
		
		$("#tab1_secondplot").empty();


		if(last_datatable!=null) {
			last_datatable.destroy();
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');
		}
		$("#gtable").empty();

		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[LRI_select],dis:dis,flrange:flrg,orientation: orient,canonical:can},
			function(data){
				
				LRI_plotdata = [];
				
				if (data.flankcov){
					
					LRI_plotdata = data.flankcov;
					
					LRIplot(LRI_plotdata);
					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
					}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

						var p2g_titles = [{"sTitle":"Primary ID","sWidth":"33%"},{"sTitle":"Symbol","sWidth":"33%"},{"sTitle":"Orientation","sWidth":"33%"}];
						$(".annobuttons").show();
						last_datatable_header = data.titles;

						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": p2g_titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						$('#gtable').on( 'click', 'td', function () {
							if(this.cellIndex==0||this.cellIndex==1){
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);}
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});

				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

	 		},

			"json"


	 	);

		LRI_select = [0,0];
		cr_LRI = false;

	}


			function LRIplot(data) {

			$("#tab1_secondplot").empty();
			$("#tab1_secondplot").addClass("genePlot");
			$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 35px;color:#45A29E">Long Range Interactions</p></div><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-4"></div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-2 box" id="box1"></div><div class="col-3 boxd" id="dbox3"></div><div class="col-1 boxm"></div><div class="col-3 boxd" id="dbox3"></div><div class="col-2 box" id="box2"></div></div><div class="row am-row2" style="margin-top: -45px;"><div class="col-2">flanking region</div><div class="col-3">distance</div><div class="col-1" style="margin-left: -0.75rem">peak</div><div class="col-3" style="margin-left: +3rem">distance</div><div class="col-2" style="margin-left: -2.25rem"> flanking region</div></div></div>');

	        $('[id^="box"]').click(function(){
		        	id=this.id;
		        	val=id.split("box")[1]-1;
		        	if(data[val]!=0){
		    		var color = LRI_select[val] ? '#45A29E':'red';
	   			$(this).css('background-color', color);
	   			LRI_select[val] = LRI_select[val] ? 0:1;}
			});
			
			$('.box').mouseleave(function(){
		        	id=this.id;
		        	val=id.split("box")[1]-1;
		        	if(!LRI_select[val]){
		        		$(this).css('background-color', '#66FCF1');}
			});
			$('.box').mouseenter(function(){

				id=this.id;
		        	val=id.split("box")[1]-1;
		        	if(!LRI_select[val]){
		        		$(this).css('background-color', '#45A29E');}
		        		$(this).prop('title','Number of genes: '+data[val]+', in the flanking region of: '+$("#flrange").val()+' bp');
			});

			$('.boxd').mouseenter(function(){
				id=this.id;
	        		val=id.split("box")[1]-1;
	   			$(this).prop('title','The selected distance value is: '+$("#dis").val()+' bp');
			});
			



		}//function LRIplot()


function NearbyGenes() {
		$(".annobuttons").hide();
		var cginame = "/AnnoMiner/NearbyGenes";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		var orient = $("input[name='orientation']:checked").val();

		$("#tab1_secondplot").empty();
		
		if(last_datatable!=null) {
			last_datatable.destroy();
			$("#gtable").remove();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" cellspacing="0"></table>');
		}
		$("#gtable").empty();
		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[NG_select], orientation: orient},
			function(data){
				
				NG_plotdata = [];
				

				if (data.flankcov){

					NG_plotdata = data.flankcov;
					NGplot(NG_plotdata);
					$("#textout2").append('<div align="center" class="drawer_body"><button type="button" class="fileUpload btn btn-primary btn-lg"><span class="glyphicon glyphicon-cog"></span>&nbspShow Genes!</button></div>');


				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}

				if (data.message != null){
					$("#textout").append('<div class="alert alert-warning" style="text-align: center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
					}


				if (data.data != null){
					$("html, body").delay(30).animate({scrollTop: $('#gtable_container').offset().top - 90}, 2000);

						var p2g_titles = [{"sTitle":"Primary ID","sWidth":"33%"},{"sTitle":"Symbol","sWidth":"33%"},{"sTitle":"Orientation","sWidth":"33%"}];
						$(".annobuttons").show();
						last_datatable_header = data.titles;

						last_datatable = $("#gtable").DataTable({
							"aaData": data.data,
							"aoColumns": p2g_titles,
							"bProcessing": true,
							"bDestroy": true,
							"bAutoWidth": false,
							"pagingType": "simple"

						});
						//to solve page resize issues
						$("#gtable_length").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_filter").parent().removeClass("col-sm-6").addClass("col-6");
						$("#gtable_info").parent().removeClass("col-sm-5").addClass("col-5");
						$("#gtable_paginate").parent().removeClass("col-sm-7").addClass("col-7");

						$('#gtable').on( 'click', 'td', function () {
							if(this.cellIndex==0||this.cellIndex==1){
									var link = buildLink(ass,gtp,this.cellIndex, last_datatable.cell( this ).data());
									window.open(link);}
									//alert( this.cellIndex);
						});

						$('#gtable').on( 'mouseover mouseout', 'td', function () {
								$(this).toggleClass("cellev");
						});
						$('#gtable').on( 'mouseover mouseout', 'tr', function () {
								$(this).toggleClass("rowev");
						});

				} else {
					$("html, body").delay(30).animate({scrollTop: $('#R').offset().top - 90}, 2000);
					$("#btn_dwl_rslt").hide();
					$("#new").hide();
				}

				$("#progress").hide();

	 		},

			"json"


	 	);

		NG_select = [0,0,0,0,0,0,0,0,0,0];

		cr_NG = false;

		}

				function NGplot(data) {

			//$("#tab1_secondplot").empty();
			$("#tab1_secondplot").addClass("genePlot");
			$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 35px;color:#45A29E">Nearby Genes</p></div><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-4"></div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-1 box" id="box1"></div><div class="col-1 box" id="box2"></div><div class="col-1 box" id="box3"></div><div class="col-1 box" id="box4"></div><div class="col-1 box" id="box5"></div><div class="col-1 boxm"></div><div class="col-1 box" id="box6"></div><div class="col-1 box" id="box7"></div><div class="col-1 box" id="box8"></div><div class="col-1 box" id="box9"></div><div class="col-1 box" id="box10"></div></div><div class="row am-row2" style="margin-top: -45px;"><div class="col-1">5<sup>TH</sup></div><div class="col-1">4<sup>TH</sup></div><div class="col-1">3<sup>RD</sup></div><div class="col-1">2<sup>ND</sup></div><div class="col-1">1<sup>ST</sup></div><div class="col-1" style="margin-left: -0.75rem">peak</div><div class="col-1" style="margin-left: +0.75rem">1<sup>ST</sup></div><div class="col-1">2<sup>ND</sup></div><div class="col-1">3<sup>RD</sup></div><div class="col-1">4<sup>TH</sup></div><div class="col-1">5<sup>TH</sup></div></div></div>')

	        $('[id^="box"]').click(function(){
	        		id=this.id;
	        		val=id.split("box")[1]-1;
	        		if(data[val]!=0){
	        		var color = NG_select[val] ? '#45A29E':'red';
	   			$(this).css('background-color', color);
	   			NG_select[val] = NG_select[val] ? 0:1;
	   		}

			});

				$('.box').mouseleave(function(){
		        	id=this.id;
		        	val=id.split("box")[1]-1;
		        	if(!NG_select[val]){
	   			$(this).css('background-color', '#66FCF1');}
			});
			$('.box').mouseenter(function(){

				id=this.id;
		        	val=id.split("box")[1]-1;
	        	if(!NG_select[val]){
	   			$(this).css('background-color', '#45A29E');}
	   			$(this).prop('title','Number of genes: '+data[val]);
			});


		}



		function buildLink(ass,gtp,index,id){


			var link='';

			//assembly drosophila
			if(ass=='dm3'|ass=='dm6'){
				if(gtp=='refseq'){
					if(index==0){
						link = 'https://www.ncbi.nlm.nih.gov/gene?term='+id+'[All]%20AND%20%22Drosophila%20melanogaster%22[porgn]&cmd=DetailsSearch';
					}else if (index==1){
						link = 'https://www.ncbi.nlm.nih.gov/gene?term='+id+'[Gene]%20AND%20%22Drosophila%20melanogaster%22[porgn]&cmd=DetailsSearch';
					}
				}else if(gtp=='ensembl'){
					if(index==0){
						link = 'http://flybase.org/reports/'+id+'.html';
					}else if (index==1){
						link = 'http://flybase.org/reports/'+id+'.html';
					}
				}
			}
			//Caenorhabditis elegans (c.elegans10)
			if(ass=='ce10'){
				if(gtp=='refseq'){
					if(index==0){
						link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Caenorhabditis+elegans%22%5Bporgn%5D';
					}else if (index==1){
						link = 'https://www.wormbase.org/species/c_elegans/gene/'+id;
					}
				}else if(gtp=='ensembl'){
					if(index==0){
						link = 'http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;t='+id;
					}else if (index==1){
						link = 'http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;g='+id;
					}
				}
			}

			//Mus musculus
			if(ass=='mm9'|ass=='mm10'){
				if(gtp=='refseq'){
					if(index==0){
						link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Mus+musculus%22%5Bporgn%5D';
					}else if (index==1){
						link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BGene%5D+AND+%22Mus+musculus%22%5Bporgn%5D';
					}
				}else if(gtp=='ensembl'|gtp=='ucsc'){
					if(index==0){
						link = 'https://www.ensembl.org/Mus_musculus/Gene/Summary?t='+id;
					}else if (index==1){
						link = 'https://www.ensembl.org/Mus_musculus/Gene/Summary?g='+id;
					}
				}
			}

			//Saccaromyces cervisae (sacCer3)
			if(ass=='sacCer3'){
				if(index==0|index==1){
				link='https://www.yeastgenome.org/locus/'+id;}
			}

			//homo sapiens
			if(ass=='hg19'|ass=='hg38'){
				if(gtp=='refseq'){
					if(index==0){
						link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BAll%5D+AND+%22Homo+sapiens%22%5Bporgn%5D';
					}else if (index==1){
						link = 'https://www.ncbi.nlm.nih.gov/gene/?term='+id+'%5BGene%5D+AND+%22Homo+sapiens%22%5Bporgn%5D';
					}
				}else if(gtp=='ensembl'|gtp=='ucsc'|gtp=='gencode'){
					if(index==0){
						link = 'https://www.ensembl.org/Homo_sapiens/Gene/Summary?t='+id;
					}else if (index==1){
						link = 'https://www.ensembl.org/Homo_sapiens/Gene/Summary?g='+id;
					}
				}
			}


			return link;
		
		}

		$("html, body").animate({scrollTop: $('#FU').offset().top - 90}, 1000);

});
