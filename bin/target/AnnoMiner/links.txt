Drosophila(dm3/dm6)

	refseq (same search for genes and transcripts)

		https://www.ncbi.nlm.nih.gov/gene?term=Gpb5[Gene]%20AND%20%22Drosophila%20melanogaster%22[porgn]&cmd=DetailsSearch

		https://www.ncbi.nlm.nih.gov/gene/?term=NM_001110865%5BAll%5D+AND+%22Drosophila+melanogaster%22%5Bporgn%5D

	ensembl (same search for genes and transcripts)

		http://flybase.org/reports/FBtr0304148.html

		http://flybase.org/reports/FBgn0085664.html

Caenorhabditis elegans (c.elegans10)

	refseq (same search for genes and transcripts)

		https://www.wormbase.org/species/c_elegans/gene/rrn-1.1

		https://www.ncbi.nlm.nih.gov/gene/?term=NR_000053%5BAll%5D+AND+%22Caenorhabditis+elegans%22%5Bporgn%5D


	ensembl (same search for genes and transcripts)

		http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;g=cTel29B.1

		http://metazoa.ensembl.org/Caenorhabditis_elegans/Gene/Summary?db=core;t=cTel29B.1

Mus musculus (mm9)

	refseq (same search for genes and transcripts)

		https://www.ncbi.nlm.nih.gov/gene/?term=NM_007758%5BAll%5D+AND+%22Mus+musculus%22%5Bporgn%5D

		https://www.ncbi.nlm.nih.gov/gene/?term=Cr2%5BGene%5D+AND+%22Mus+musculus%22%5Bporgn%5D


	ensembl (same search for genes and transcripts)

		https://www.ensembl.org/Mus_musculus/Gene/Summary?g=ENSMUSG00000026616

		https://www.ensembl.org/Mus_musculus/Gene/Summary?g=ENSMUST00000043104

Mus musculus (mm10)

	refseq (same search for genes and transcripts)

		https://www.ncbi.nlm.nih.gov/gene/?term=NM_007758%5BAll%5D+AND+%22Mus+musculus%22%5Bporgn%5D

		https://www.ncbi.nlm.nih.gov/gene/?term=Cr2%5BGene%5D+AND+%22Mus+musculus%22%5Bporgn%5D


	ucsc (same search for genes and transcripts)

		https://www.ensembl.org/Mus_musculus/Gene/Summary?g=ENSMUSG00000026616

		https://www.ensembl.org/Mus_musculus/Gene/Summary?g=ENSMUST00000043104
Saccaromyces cervisae (sacCer3)


	ensembl (same search for genes and transcripts)

		https://www.yeastgenome.org/locus/YAR075W

Homo sapiens (hg19)

	refseq (same search for genes and transcripts)

		https://www.ncbi.nlm.nih.gov/gene/?term=NM_170725%5BAll%5D+AND+%22Homo+sapiens%22%5Bporgn%5D

		https://www.ncbi.nlm.nih.gov/gene/?term=PGBD2%5BGene%5D+AND+%22Homo+sapiens%22%5Bporgn%5D


	ensembl (same search for genes and transcripts)

		https://www.ensembl.org/Homo_sapiens/Gene/Summary?g=ENST00000430973

		https://www.ensembl.org/Homo_sapiens/Gene/Summary?g=ENSG00000233084

Homo sapiens (hg38)

	refseq (same search for genes and transcripts)

		https://www.ncbi.nlm.nih.gov/gene/?term=NM_170725%5BAll%5D+AND+%22Homo+sapiens%22%5Bporgn%5D

		https://www.ncbi.nlm.nih.gov/gene/?term=PGBD2%5BGene%5D+AND+%22Homo+sapiens%22%5Bporgn%5D

	gencode


		https://www.ensembl.org/Homo_sapiens/Gene/Summary?g=ENST00000363625.1

		https://www.ensembl.org/Homo_sapiens/Gene/Summary?g=ENSG00000200495.1

	ucsc

		http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=uc057rcx.1

		http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=RNU6-1205P