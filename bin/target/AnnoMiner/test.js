$(document).ready(function(){

		var dblue = "#45A29E";
		var lblue = "#66FCF1";
		var red = "#F13C20";
		var peak = '#1F2833';
		var NG_plotdata = [];

		var cr_NG = false;

		var NG_select = [0,0,0,0,0,0,0,0,0,0];
		var NG_cols = [lblue,lblue,lblue,lblue,lblue,dblue,dblue,dblue,dblue,dblue];
		var data = [['souki smells'], [14], ['im a pro and i love souki'], [1], [1], [1], [1], [1], [1], [1]];

		function NGplot(data) {

			//$("#tab1_secondplot").empty();
			$("#tab1_secondplot").addClass("genePlot");
			$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 35px;color:#45A29E">Nearby Genes</p></div><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-4"></div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-1 box" id="box1"></div><div class="col-1 box" id="box2"></div><div class="col-1 box" id="box3"></div><div class="col-1 box" id="box4"></div><div class="col-1 box" id="box5"></div><div class="col-1 boxm"></div><div class="col-1 box" id="box6"></div><div class="col-1 box" id="box7"></div><div class="col-1 box" id="box8"></div><div class="col-1 box" id="box9"></div><div class="col-1 box" id="box10"></div></div><div class="row am-row2" style="margin-top: -45px;"><div class="col-1">5°</div><div class="col-1">4°</div><div class="col-1">3°</div><div class="col-1">2°</div><div class="col-1">1°</div><div class="col-1" style="margin-left: -0.75rem">peak</div><div class="col-1" style="margin-left: +0.75rem">1°</div><div class="col-1">2°</div><div class="col-1">3°</div><div class="col-1">4°</div><div class="col-1">5°</div></div></div>')

	        $('[id^="box"]').click(function(){
	        	id=this.id;
	        	val=id.split("box")[1]-1;
	    		var color = NG_select[val] ? '#45A29E':'red';
	   			$(this).css('background-color', color);
	    		NG_select[val] = !NG_select[val];
			});

			$('.box').mouseleave(function(){
	        	id=this.id;
	        	val=id.split("box")[1]-1;
	        	if(!NG_select[val]){
	   			$(this).css('background-color', '#66FCF1');}
			});
			$('.box').mouseenter(function(){

				id=this.id;
	        	val=id.split("box")[1]-1;
	        	if(!NG_select[val]){
	   			$(this).css('background-color', '#45A29E');}
	   			$(this).prop('title','Number of genes: '+data[val]);
			});


		}


			function LRIplot(data) {

			//$("#tab1_secondplot").empty();
			$("#tab1_secondplot").addClass("genePlot");
			$("#tab1_secondplot").append('<div class="container-fluid" style="background-color: white"><div class="row am-row"><p class="col-lg-12" style="text-align: center;font-size: 35px;color:#45A29E">Long Range Interactions</p></div><div class="row am-row2" style="margin-bottom: -80px;"><div class="col-4" style="font-style: italic;">Upstream</div><div class="col-4"></div><div class="col-4" style="font-style: italic;">Downstream</div></div><div class="row am-row2" style="margin-top: 80px;"><div class="col-2 box" id="box1"></div><div class="col-3 boxd" id="dbox3"></div><div class="col-1 boxm"></div><div class="col-3 boxd" id="dbox3"></div><div class="col-2 box" id="box2"></div></div><div class="row am-row2" style="margin-top: -45px;"><div class="col-2">flanking region</div><div class="col-3">distance</div><div class="col-1" style="margin-left: -0.75rem">peak</div><div class="col-3" style="margin-left: +3rem">distance</div><div class="col-2" style="margin-left: -2.25rem"> flanking region</div></div></div>');

	        $('[id^="box"]').click(function(){
	        	id=this.id;
	        	val=id.split("box")[1]-1;
	    		var color = NG_select[val] ? '#45A29E':'red';
	   			$(this).css('background-color', color);
	    		NG_select[val] = !NG_select[val];
			});
			
			$('.box').mouseleave(function(){
	        	id=this.id;
	        	val=id.split("box")[1]-1;
	        	if(!NG_select[val]){
	   			$(this).css('background-color', '#66FCF1');}
			});
			$('.box').mouseenter(function(){

				id=this.id;
	        	val=id.split("box")[1]-1;
	        	if(!NG_select[val]){
	   			$(this).css('background-color', '#45A29E');}
	   			$(this).prop('title','Number of genes: '+data[val]+', in the flanking region of: '+data[3]+' bp');
			});

			$('.boxd').mouseenter(function(){
				id=this.id;
	        	val=id.split("box")[1]-1;
	   			$(this).prop('title','The selected distance value is: '+data[2]+' bp');
			});
			


			}


	LRIplot(data);
});