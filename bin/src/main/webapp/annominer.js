 
$(document).ready(function(){
		
	$("#progress").hide();
	
	
	//////////////////////
	// Page Navigations //
	//////////////////////
	
	$('.drawer_header').click(function() {
		
		$(this).next('div').slideUp();
        
        if($(this).next().is(':hidden')) {
            $(this).next().slideDown();
        }
        
        return false;
        
	});
	
	
	
	$('#analysis_options').hide();
	$('#utrack_manager').hide();
	$('#analysis_results').hide();
	
	//format buttons:
	//$('#btn_rslt').button();
	//$('#btn_download').button();
	
	
	
	//////////////////////
	// Global variables //
	//////////////////////
	
	var last_datatable=null;
	var last_datatable_header=null;
	
	
	///////////////////////////
	// User track management //
	///////////////////////////
	
	var utracks = new Array();
	
	var ul_trackid_p1 = '<div class="col-lg-2"><div id="ul_trackid" ><input type="text" name="trackid" class="form-control" value="';
	var ul_trackid_p2 = '"/></div></div>';
	var ul_datatype = '<div class="col-lg-2"><select id="ul_type" name="ul_type" class="form-control" >\
							<option value="peaks">Genomic regions</option>\
							<option value="idlist">ID list</option>\
						</select></div>';
	var ul_button = '<input type="submit" id="ul_button" class="btn btn-primary" value="Upload" />';
	//var ul_button = '<div class="linemember"><input type="button" id="ul_button" value="Upload"  /></div>';
	
	//var rx_fname = "/.*\\(+*)(\..*){0:1}$/";
	
	
	$('#filechoice').change(function(event){
		
		//event.preventDefault();
		$in=$(this);
		var path = $in.val();
		var fname = path.substring( path.lastIndexOf("\\") + 1, path.lastIndexOf("."));
		
		//$("#ul_fname").html($in.val());
		$("#ul_fname").empty();
		$("#ul_fname").append(ul_trackid_p1+fname+ul_trackid_p2+ul_datatype+ul_button);
		
		/*
		$('#ul_form').bind('submit',function(){
			$('#ul_target').empty();
	    });
		*/
		
		//$("#ul_datatype").buttonset();
		//$("#ul_datatype").addClass("ui-button ui-widget ui-state-default ui-corner-all");
		//$("#ul_button").button();
		//$('#ul_trackid').addClass("ui-button ui-widget ui-state-default ui-corner-all");
		
		
	});
	
	
	$('#ul_target').load(function() {
		
		var ul_json = $.parseJSON($("#ul_target").contents().text());
		//alert( utracks[utracks.length-1].id );
		
		if(ul_json.message!=null){
			$("#ul_alerts").append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+ul_json.message+'</div>');
			//alert(ul_json.message);
		}
		
		if(ul_json.data!=null){
			utracks = ul_json.data;
			updateAnalysisSelections();
			updateUTrackView(utracks);
		}
		
		
		//$("#ul_target").empty();
		
	});
	
		
	
	function updateUTrackView(data) {
		
		//$('#utrack_view').empty();
		
		//var jstring = "{\"aoColumns\":[{\"sTitle\":\"Track id\",\"sWidth\":\"15%\"},{\"sTitle\":\"Data-type\"}],\"aaData\":[";
		
		var titles = [{"sTitle":"Track id","sWidth":"15%"},{"sTitle":"Data-type"}];
		var values = new Array();
		
		for( var i=0; i<utracks.length; ++i ) {
			
			values.push([utracks[i].id, utracks[i].type]);
			
		}
		
		//alert(jstring);
		
		
		//$("#utrack_view thead tr").empty();
		//$("#utrack_view tfoot tr").empty();
		$("#utrack_view").empty();
		
		//console.log(utracks.length());
		
		if(utracks.length<11){
			$("#utrack_view").DataTable({
				"aaData": values,
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers",
				"sDom" : 'f'
				//"pageLength": 25,
				//"sDom": 'rt' //without footer and pagination
				//"sDom": 'lfrtip' //full table annotations
				//"aaSorting": [[0,'asc']]
				//"bJQueryUI": true
			});
		} else {
			$("#utrack_view").DataTable({
				"aaData": values,
				"aoColumns": titles,
				"bProcessing": true,
				"bDestroy": true,
				"bAutoWidth": false,
				"sPaginationType": "full_numbers"
			});
		}
		
		
		if($('#utrack_manager').is(':hidden')) {
            $('#utrack_manager').slideDown();
        }
		
	
	}
	
	
	
	//////////////////////////////////////////////
	// Selection of analysis and its parameters //
	//////////////////////////////////////////////
	
	var genomes = {
			"hg19":["refseq","ensembl","ucsc"],
			"hg38":["refseq","genecode"],
			"mm9":["refseq","ensembl","ucsc"],
			"mm10":["refseq","ensembl","ucsc"],
			//"dm3":["flybase","refseq","ensembl"],
			"dm3":["refseq","ensembl"],
			"ce10":["refseq","ensembl"],
			"sacCer3":["refseq","ensembl"]
	};
	
	
	var species = "hg19";
	addGenomeSel();
	addTrackSel();
	
	$('#assembly').change(function(event){
		
		species = $(this).val();
		var analysis = $("#analyis").val();
		$("#trackoptions").empty();
		if( analysis == "genes" ) {
			addGenomeSel();
			addTrackSel();
		} else if ( analysis == "TFs" ) {
			addGenomeSel();
			addListSel();
		}
		
		$("#ul_form_assembly").val(species);
		
	});
	
	
	$('#analyis').change(function(event){
		
		updateAnalysisSelections();
		
	});
	
	
	
	function updateAnalysisSelections() {
		
		var analysis = $('#analyis').val();
		$("#trackoptions").empty();
		
		if( analysis == "genes" ) {
			addGenomeSel();
			addTrackSel();
		} else if ( analysis == "TFs" ) {
			addGenomeSel();
			addListSel();
		}
		
	}
	
	
	
	function addGenomeSel() {
		
		//var analysis = $("#analyis").val();			
		
		var ostring = '<div class="form-group col-lg-2"><label>Select genome-annotation</label>';
		ostring += buildSelectField( {name:"ganno",values:genomes[species]} );
		ostring += '</label>';
		
		$("#trackoptions").append( '<div class="cell">'+ostring+'<div>' );
		
	}
	
	
	function addTrackSel() {
					
		var track;
		var ulists = new Array();
		
		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "peaks" ) { ulists.push( track.id ); }
		}
		
		var ostring = '<div class="form-group col-lg-2"><label>Select experimental track</label>';
		
		if ( ulists.length == 0 ) {
			ostring += "<div>Please upload a peak track first!</div>";
		} else {
			ostring += buildSelectField( {name:"tanno",values:ulists} );
		}
		
		ostring += '</div>';
		
		$("#trackoptions").append( '<div class="cell">'+ostring+'<div>' );
		
	}
	
	
	function addListSel() {		
		
		var track;
		var ulists = new Array();
		
		for( var i=0; i<utracks.length; ++i ) {
			track = utracks[i];
			if( track.type == "idlist" ) { ulists.push( track.id ); }
		}
		
		
		var ostring = '<div class="form-group col-lg-2"><label>Select ID-list</label>';
		
		if ( ulists.length == 0 ) {
			ostring += "<div>Please upload an ID-list first!</div>";
		} else {
			ostring += buildSelectField( {name:"ianno",values:ulists} );
		}
		
		ostring += '</div>';
		
		$("#trackoptions").append( '<div class="cell">'+ostring+'<div>' );
		
	}
	
	
	function buildSelectField(data) {
		var ostring = '<select id="'+data.name+'" name="'+data.name+'" class="form-control" >';
		
		for( var i=0; i<data.values.length; ++i ) {
			ostring += '<option value="'+data.values[i]+'">'+data.values[i]+'</option>';
		}
		ostring += '</select>';
		return ostring;
	}
	
	
	//////////////////////////
	// peak2gene annotation //
	//////////////////////////	
	
	var dblue = "#1D4B9A";
	var lblue = "#68A2D8";
	var red = "#ff0000";
	
	var gplotdata = [];
	var eplotdata = [];
	//var rdata;
	
	var cr_gn = false;
	var cr_ex = false;
	
	var gselect = [0,0,0,0,0,0,0,0,0,0];
	var eselect = [0,0,0,0,0,0,0,0,0,0];
	var gcols_df = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
	var ecols_df = [dblue,lblue,dblue,lblue,dblue,lblue,dblue,lblue,lblue,dblue];
	var gcols = [];
	var ecols = [];
	
	var ovtype_std = '0';
	var ovtype = '0';
	
	var ovval_abs = '120';
	var ovval_rel = '20';
	
	//Default-values:
	var ovval_absst = '120';
	var ovval_relst = '20';
	var tssus_st = '0';
	var tssds_st = '1000';
	
	
	$("#btn_rslt").click(function(){
		
		$("#progress").show();
		
		if( $('#analyis').val()== "genes" ) {
			peak2gene();
		} else {
			tfscan();
		}
		
	});
	
	
	$("#btn_dwl_rslt").click(function(){
		
		var tsv = JSON2TSV( last_datatable.data() );
		
		window.open("data:text/tab-separated-values;charset=utf-8," + escape(tsv));
		
	});
	
	
	
	function JSON2TSV(objArray) {
	    
		var sep = "\t"
		
		//var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
		var array = objArray;
	    
	    var str = '';
	    var line = '';
	    
	   
	    for (var i in last_datatable_header) {
            line += last_datatable_header[i] + sep;
        }
	    
	    line = line.slice(0, -1);
	    str += line + '\r\n';
	    

	    for (var i = 0; i < array.length; i++) {
	        var line = '';

	        
            for (var index in array[i]) {
                line += array[i][index] + sep;
            }
	        
	        line = line.slice(0, -1);
	        str += line + '\r\n';
	    }
	    
	    return str;
	    
	}
	
	
	
	function tfscan() {
		
		var cginame = "/AnnoMiner/TFScan";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#ianno").val();
		
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();
		
		
		$("#tab1_firstplot").empty();
		$("#tab1_firstplot").height("0px");
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").height("0px");
		$("#tab1_thirdplot").empty();
		$("#tab1_thirdplot").height("0px");
		
		
		if(last_datatable!=null) { 
			last_datatable.destroy();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" width="100%" cellspacing="0"></table>');
		}
		$("#gtable").empty();
		
		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,ovval:ovl,ovtype:ovtype,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can},
			function(data){
				
				if (data.message != null){
					$("#ana_alerts").append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}
				
				if (data.data != null){
					
					var tfscan_titles = [{"sTitle":"Target factor","sWidth":"100px"},{"sTitle":"Probe"},{"sTitle":"Treatment"},{"sTitle":"Replicate","sWidth":"70px"},{"sTitle":"list","sWidth":"70px"},{"sTitle":"list size","sWidth":"70px"},{"sTitle":"genome","sWidth":"70px"},{"sTitle":"genome size","sWidth":"70px"},{"sTitle":"target genes"},{"sTitle":"score"},{"sTitle":"p-value"}];  
					if($('#analysis_results').is(':hidden')) {
			            $('#analysis_results').slideDown();
			        }
					
					last_datatable_header = data.titles;
					
					last_datatable = $("#gtable").DataTable({
						"aaData": data.data,
						"aoColumns": tfscan_titles,
						"bProcessing": true,
						"bDestroy": true,
						"bAutoWidth": false,
						"sPaginationType": "full_numbers",
						//"pageLength": 25,
						//"bJQueryUI": true,
						"order": [[9,'asc']]
					});
					
					
					$("#btn_dwl_rslt").show();
					
				} else {
					
					$("#btn_dwl_rslt").hide();
					
				}
				
				
				$("#progress").hide();
				
				
			},
				
			"json"
			//"html"
			
		);//post
		
	}
	
	
	
	function peak2gene() {
		
		var cginame = "/AnnoMiner/PeakToGene";
		var ass = $("#assembly").val();
		var gtp = $("#ganno").val();
		var tid = $("#tanno").val();
		
		var ovl = $("#ovl_t").val();
		var tssus = $("#tss_us").val();
		var tssds = $("#tss_ds").val();
		var flrg = $("#flrange").val();
		var can = $("input[name='canonical']:checked").val();
		
		//alert(targets.join(','))
		
		if(last_datatable!=null) { 
			last_datatable.destroy();
			$("#gtable_container").append('<table id="gtable" class="table table-striped table-bordered" width="100%" cellspacing="0"></table>');
		}
		$("#gtable").empty();
		
		$.post(
			cginame,
			{assembly:ass,gtype:gtp,trackid:tid,'targets[]':[gselect],ovval:ovl,ovtype:ovtype,tss_us:tssus,tss_ds:tssds,flrange:flrg,canonical:can},
			function(data){
				//$("#tab1 #textout").html(data);
				
				//$("#tab1 #covplot").empty();
				//$("#gtable thead tr").empty();
				//$("#gtable tfoot tr").empty();
				
				gplotdata = [];
				eplotdata = [];
				
				if($('#analysis_results').is(':hidden')) {
		            $('#analysis_results').slideDown();
		        }
				
				//$('#accordion').accordion('destroy');
				
				if (data.genecov){
					
					$("#tab1_firstplot").empty();
					$("#tab1_firstplot").width("400px");
					
					var plot1 = $.jqplot('tab1_firstplot', [data.genecov],{
						title: 'Basepair-coverage of genes',
						
						seriesDefaults:{
							renderer:$.jqplot.BarRenderer,
							rendererOptions: {fillToZero: true, barWidth : 30}
						},
						
						seriesColors: ["#1D4B9A"],
						
						axesDefaults: {
							tickRenderer: $.jqplot.CanvasAxisTickRenderer,
							tickOptions: {
								angle: -30,
								fontSize: '10pt'
							}
						},
						
						axes: {
							xaxis: {
								renderer: $.jqplot.CategoryAxisRenderer
							},
							//yaxis: {
							//	pad: 2.0
							//}
						},
						
						grid: {
							background: '#ffffff',
    						shadow: false
						}
						
						
					});
					
					
				} else {
					$("#tab1_firstplot").empty();
					$("#tab1_firstplot").height("0px");
				}
				
				if (data.flankcov){
					
					//gplot(data.flankcov);
					gplotdata = data.flankcov;
					gcols = [lblue,lblue,lblue,dblue,dblue,dblue,dblue,lblue,lblue,lblue];
					gplot(gplotdata);
					
				} else {
					$("#tab1_secondplot").empty();
					$("#tab1_secondplot").height("0px");
				}
				
				
				if (data.exoncov){
					
					eplotdata = data.exoncov;
					ecols = [dblue,lblue,dblue,lblue,dblue,lblue,dblue,lblue,lblue,dblue];
					eplot(eplotdata);
					
				} else {
					$("#tab1_thirdplot").empty();
					$("#tab1_thirdplot").height("0px");
				}
				
				
				if (data.message != null){
					$("#ana_alerts").append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.message+'</div>');
				}
				
				if (data.data != null){
					
					//$("#gtable thead tr").empty();
					//$("#gtable tfoot tr").empty();
					
					var p2g_titles = [{"sTitle":"Primary ID","sWidth":"15%"},{"sTitle":"Symbol"}];
					
					last_datatable_header = data.titles;
					
					last_datatable = $("#gtable").DataTable({
						"aaData": data.data,
						"aoColumns": p2g_titles,
						"bProcessing": true,
						"bDestroy": true,
						"bAutoWidth": false,
						"sPaginationType": "full_numbers"
						//"pageLength": 25
						//"bJQueryUI": true
						//"aaSorting": [[0,'asc']]
					});
					
					$("#btn_dwl_rslt").show();
					
				} else {
					
					$("#btn_dwl_rslt").hide();
					
				}
				
				$("#progress").hide();
				
			},
			
			"json"
			//"html"
		
		);//post
		
		gselect = [0,0,0,0,0,0,0,0,0,0];
		eselect = [0,0,0,0,0,0,0,0,0,0];
		
		
	}
	
	
	
	function gplot(data) {
		
		$("#tab1_secondplot").empty();
		$("#tab1_secondplot").width("600px");
		
		var plot1 = $.jqplot('tab1_secondplot', [data],{
			title: 'Basepair-coverage of gene-intervals',
			
			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
			},
			
			seriesColors: gcols,
			
			axesDefaults: {
				tickRenderer: $.jqplot.CanvasAxisTickRenderer,
				tickOptions: {
					angle: -30,
					fontSize: '10pt'
				}
			},
			
			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer
				},
				yaxis: {
					label: "covered / total length",
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
					//pad: 2.0
				}
			},
			
			grid: {
				background: '#ffffff',
				shadow: false
			}
			
			
		});
		
		if (!(cr_gn)){
			$('#tab1_secondplot').bind('jqplotDataClick',
				function (ev, seriesIndex, pointIndex, data) {
					//alert('series: '+seriesIndex+', point: '+pointIndex+' col: '+data[0]+', val: '+data[1]);
					var i = data[0] - 1;
					//alert(gselect[i][0]);
					
					if (gselect[i] == 1) {
						gselect[i] = 0;
						gcols[i] = gcols_df[i];
					} else {
						gselect[i] = 1;
						gcols[i] = red;
					}
					
					gplot(gplotdata);
					
					//if (gselect[i][1]){alert('true')} else {alert('false')};
				}
			);
			
			cr_gn = true
		}
			
	}//function gplot()
	
	
	function eplot(data) {
		$("#tab1_thirdplot").empty();
		$("#tab1_thirdplot").width("600px");
		
		
		var plot1 = $.jqplot('tab1_thirdplot', [data],{
			title: 'Basepair-coverage of exons & introns',
			
			seriesDefaults:{
				renderer:$.jqplot.BarRenderer,
				rendererOptions: {fillToZero: true, barWidth:30, barMargin:30, varyBarColor:true}
			},
			
			seriesColors: ecols,
			
			axesDefaults: {
				tickRenderer: $.jqplot.CanvasAxisTickRenderer,
				tickOptions: {
					angle: -30,
					fontSize: '10pt'
				}
			},
						
			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer
				},
				yaxis: {
					label: "covered / total length",
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
					//pad: 2.0
				}
			},
			
			grid: {
				background: '#ffffff',
				shadow: false
			}
			
			
		});
	
	}//function eplot(data)
	
	//////////////////////////////////
	// Set target-region overlap type 
	//////////////////////////////////
	
	$('#ovl_s').change( function() {
		
		ovtype = $("#ovl_s").val();
		if (ovtype == 0) {	
			ovval_rel = $("#ovl_t").val();
			$("#ovl_t").val(ovval_abs);
		}else{
			ovval_abs = $("#ovl_t").val();
			$("#ovl_t").val(ovval_rel);
		}
		//alert(ovtype);
	
	});
	
	

});// $(document).ready(function(){








/*
$(window).load( function() {
	$.getScript('./js/jdata.js');
});
*/



//css 
/*
table property to wrap long cell content in tables:  
word-wrap:break-word;


*/







