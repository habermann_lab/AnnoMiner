### Script to create the colletions needed to populate the AnnoMiner's mongoDB using TF and HM Chip-seq data from: modERN, ENCODE, modENCODE
#first download the metadata from: https://www.encodeproject.org/summary/?type=Experiment&status=released with at leas the 6 fields: 
#accession, target of assay, biosample summary, description, biological replicate, biosample treatment
#
#Then this script generate metadata and starting from the its accession ID generates a json collection file for each peak file fetching encode site by: 

# Matching in the Json file, with the accesion ID, the id of the bed narrow peak file found by the query es:
# assembly = es. "dm6" tooken from the file name
# file_type = "bed narrowPeak"
# output_type = "optimal idr thresholded peaks"

#import the needed libraries
import io
import os
import requests
import json
import gzip
import pandas as pd
import sys

#define the homedirectory where the metadata tables are stored: oss. each file should start with assembly name followerd by a "_" char
metadata_dir = "/home/fabio/Documents/AnnoMiner_Enrich_metadata"
os.chdir(metadata_dir)

#output to console the argument given in input
print('Generating the collection given the following arguments {}'.format(sys.argv))

#select one of the 2 way to run the script
try:
    batch = False
    assembly = sys.argv[1]
    print('First argument (assembly name) {}'.format(sys.argv[1])) # first argument by default is always the name of the file itself
except:
    batch = True
    print('First argument (default) : batch')

#format the TF data
for file in os.listdir(metadata_dir):

    if ((file.endswith(".tsv"))&(~file.startswith("HM"))):
    
        if not (batch):
            
            if not (file.startswith(assembly)):
                continue
           
        EncodeFile1 = pd.read_csv(file, sep = '\t', header = 1, )

        EncodeFile1['targettype'] = 'TF'

        EncodeFile1 = EncodeFile1.rename(columns={
            'Accession':'pid',
            'Target of assay':'target',
            'Biosample summary':'probe',
            'Description':'method',
            'Biological replicate':'rpl',
            'Biosample treatment':'treatment',
        })
        
        assembly = file.split("_")[0]
        print('processing the assembly {}'.format(assembly))
        
        #store Ids in list
        IDs = EncodeFile1['pid']

        #format the HM data
        if((assembly=="GRCh38")|(assembly=="hg19")|(assembly=="mm9")|(assembly=="mm10")):
            
            for file in os.listdir(metadata_dir):
                
                if ((file.endswith("fillNA.tsv"))&(file.startswith("HM_{}".format(assembly)))):
                    
                    EncodeFile2 = pd.read_csv(file, sep = '\t', header = 0, )
        
                    #added targetype field used by Annominer to retrieve the collections for the enrichment of HM Chipseq exp
                    EncodeFile2['targettype'] = 'HM'

                    EncodeFile2 = EncodeFile2.rename(columns={
                        'Accession':'pid',
                        'Target of assay':'target',
                        'Biosample summary':'probe',
                        'Description':'method',
                        'Biological replicate':'rpl',
                        'Biosample treatment':'treatment',
                    })
                    
                    #store Ids in list
                    IDsHM = EncodeFile2['pid']

                    print('and relative HM datasets in {}'.format(file))

        # create collections directory

        os.mkdir(assembly)
        
        os.mkdir(os.path.join(assembly, 'collections'))
        
        os.chdir(assembly)
        
        #for each id open the Json file from ENCODE and retrieve the accession id of the file we are itnerested in 
        dictAccession ={}
        removeIDs = list()

        #TF IDs
        for ID in IDs:

            # Force return from the server in JSON format
            headers = {'accept': 'application/json'}

            # This URL locates the ENCODE biosample with accession number
            url = 'https://www.encodeproject.org/biosample/'+ID+'/?format=json'

            # GET the object
            response = requests.get(url, headers=headers)

            # Extract the JSON response as a Python dictionary
            biosample = response.json()

            #Check in the file list the one in which we are interested in
            control = 0

            for i in range(len(biosample.get('files'))):


                #define the parameters to choose our peaks files (added upper to avoid the different typo mismatch present in the db)       
                ot = biosample.get('files')[i].get('output_type')
                ft = biosample.get('files')[i].get('file_type')
                ass = biosample.get('files')[i].get('assembly')
                st = biosample.get('files')[i].get('status')
                
                if all(v is not None for v in [ot, ft, ass, st]):
                    #https://www.encodeproject.org/chip-seq/transcription_factor/
                    cond1 = (ot.upper() == 'optimal idr thresholded peaks'.upper())|(ot.upper() == 'conservative IDR peaks'.upper())|(ot.upper() == 'conservative IDR thresholded peaks'.upper())
                    cond2 = ft.upper() == 'bed narrowPeak'.upper()
                    cond3 = ass.upper() == assembly.upper()
                    cond4 = st.upper() == 'released'.upper()
                    cond = cond1&cond2&cond3&cond4
                else:
                    cond=False

                if(cond):
                    accessionTFBS = biosample.get('files')[i].get('accession')
                    #store a dictionary with the pair of accession IDs
                    dictAccession[accessionTFBS] = ID

                    #download the peak file from encode
                    downURL = 'https://www.encodeproject.org' + biosample.get('files')[i].get('href')
                    filename = downURL.split("/")[-1]        
                    with open(filename, "wb") as f:
                        r = requests.get(downURL)
                        f.write(r.content)
                    control = 1
            #if there is not the dataset of interest ID is removed
            if(control == 0):

                removeIDs.append(ID)
                print('TF Accession ID: {} does not have a narrow peak file with idr threshold. Will be deleted from the metadata file.'.format(ID))
        
        #generate collection for HM
        if((assembly=="GRCh38")|(assembly=="hg19")|(assembly=="mm9")|(assembly=="mm10")):

            for ID in IDsHM:

                # Force return from the server in JSON format
                headers = {'accept': 'application/json'}

                # This URL locates the ENCODE biosample with accession number
                url = 'https://www.encodeproject.org/biosample/'+ID+'/?format=json'

                # GET the object
                response = requests.get(url, headers=headers)

                # Extract the JSON response as a Python dictionary
                biosample = response.json()
                
                #Check in the file list the one in which we are interested in
                control = 0   
                
                #check if among all the available files there are the file in which we are interested in
                for i in range(len(biosample.get('files'))):

                    #define the parameters to choose our peaks files (added upper to avoid the different typo mismatch present in the db)        
                    ot = biosample.get('files')[i].get('output_type')
                    ft = biosample.get('files')[i].get('file_type')
                    ass = biosample.get('files')[i].get('assembly')
                    st = biosample.get('files')[i].get('status')
                    cond = False
                    
                    if all(v is not None for v in [ot, ft, ass, st]):
                        
                        #define the parameters to choose our peaks files: https://www.encodeproject.org/chip-seq/histone
                        cond1 = (ot.upper() == 'replicated peaks'.upper())|(ot.upper() == 'idr peaks'.upper())|(ot.upper() == 'idr thresholded peaks'.upper())|(ot.upper() == 'optimal idr thresholded peaks'.upper())
                        cond2 = (ft.upper() == 'bed narrowPeak'.upper())|(ft.upper() =='bed broadPeak'.upper())
                        cond3 = ass.upper() == assembly.upper()
                        cond4 = st.upper() == 'released'.upper()
                        cond = cond1&cond2&cond3&cond4

                    
                        if(cond):

                            accessionTFBS = biosample.get('files')[i].get('accession')
                            #store a dictionary with the pair of accession IDs
                            dictAccession[accessionTFBS] = ID

                            #download the peak file from encode
                            downURL = 'https://www.encodeproject.org' + biosample.get('files')[i].get('href')
                            filename = downURL.split("/")[-1]        
                            with open(filename, "wb") as f:
                                r = requests.get(downURL)
                                f.write(r.content)
                            control = 1
                            
                if(control == 0):
                    removeIDs.append(ID)
                    print('HM Accession ID: {} does not have a narrow peak file with replicate peaks nor a broad peak file. Will be deleted from the metadata file.'.format(ID)) 
        
        if((assembly=="GRCh38")|(assembly=="hg19")|(assembly=="mm9")|(assembly=="mm10")):
            EncodeFile = pd.concat([EncodeFile1,EncodeFile2])
        else:
            EncodeFile = EncodeFile1

        #print the accession IDs dictionary
        df = pd.DataFrame.from_dict(dictAccession, orient="index")   
        df.to_csv(assembly + '_AccessionKeys.tsv',sep="\t",header=False, index=True)
        print('generate the accession key map file')
        
        
        #build metadata collection with entries that ony have the type of file selected

        EncodeFile[['pid','target','probe','method','rpl','treatment','targettype']][~EncodeFile['pid'].isin(removeIDs)].to_json(orient="records", path_or_buf="collections/metadata.json")
        print('generated the metadata collection')


        for file in os.listdir("."):
    
            if file.endswith(".gz"):

                df = pd.read_csv(file, compression='gzip', header=None, sep='\t')

                chroms = df[0].unique()

                jsonFile = pd.DataFrame()

                i=0

                for chrom in chroms:    

                    jsonFile.loc[i, 'pid'] = chrom

                    jsonFile.loc[i,'length'] = "NumberLong(10000)"

                    arr = []

                    for start, stop in list(zip(df[df[0]==chrom].iloc[:,1], df[df[0]==chrom].iloc[:,2])):
                        arr.append("[NumberLong({}) , NumberLong({})]".format(start,stop))

                    jsonFile.loc[i,'regions'] = str(arr)

                    i+=1

                key = file.split(".")[0]

                jsonFile.applymap(lambda x: x.replace('\'', '')).to_json(orient="records", path_or_buf='collections/'+dictAccession[key]+'.json')
                print('generate the collection: {}'.format(dictAccession[key]))

        os.chdir(metadata_dir)