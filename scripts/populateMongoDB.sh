#!/bin/bash

#mandatory 2 arguments: name of the assembly (folder containing the collections) and the name of the MongoDB

assembly=$1
database=$2

echo 'selected Assembly is: '$assembly
echo 'selected database is: '$database

path="/home/fabio/Documents/AnnoMiner_Enrich_metadata/"

cd $path$assembly'/collections'

for file in `ls`;
do

collection=`echo $file | cut -f1 -d'.'`

`sed -i 's/\"\[\[/\[\[/g' $file`
`sed -i 's/\"NumberLong(10000)\"/NumberLong(10000)/g' $file`
`sed -i 's/\]\]\"/\]\]/g' $file`

mongoimport --db $database --collection $collection --file $file --jsonArray

echo 'uploaded collection: '$collection

done
